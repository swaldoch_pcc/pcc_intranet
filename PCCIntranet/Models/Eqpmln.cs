﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Eqpmln
    {
        public Guid Idnum { get; set; }
        public Guid Idref { get; set; }
        public long Recnum { get; set; }
        public int Linnum { get; set; }
        public string Dscrpt { get; set; }
        public decimal? Cstcde { get; set; }
        public DateTime? Cmpday { get; set; }
        public short Cycday { get; set; }
        public DateTime? Schdte { get; set; }
        public long Cmphrs { get; set; }
        public int Cychrs { get; set; }
        public long Schhrs { get; set; }
        public long? Empnum { get; set; }
        public string Usrdf1 { get; set; }
        public string Ntetxt { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }

        public Cstcde CstcdeNavigation { get; set; }
        public Employ EmpnumNavigation { get; set; }
        public Eqpmnt IdrefNavigation { get; set; }
        public Eqpmnt RecnumNavigation { get; set; }
    }
}
