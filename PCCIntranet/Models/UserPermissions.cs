﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class UserPermissions
    {
        public int Id { get; set; }
        public bool? Admin { get; set; }
        public bool? Pm { get; set; }
        public bool? Apm { get; set; }
        public bool? Laboredit { get; set; }
        public bool? Bidedit { get; set; }
        public bool? Equipmentmanager { get; set; }
        public bool? Equipmentuser { get; set; }
        public bool? Fielduser { get; set; }
        public bool? Fieldoperationsmanager { get; set; }
        public bool? Executive { get; set; }
        public bool? Financemanager { get; set; }
        public bool? Financeuser { get; set; }
        public bool? Safetymanager { get; set; }
        public bool? Auditmanager { get; set; }
        public bool? Audituser { get; set; }
    }
}
