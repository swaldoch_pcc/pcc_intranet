﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Crcard
    {
        public Guid Idnum { get; set; }
        public Guid Idref { get; set; }
        public long Recnum { get; set; }
        public string Payee1 { get; set; }
        public string Addrs1 { get; set; }
        public string Addrs2 { get; set; }
        public string Ctynme { get; set; }
        public string State { get; set; }
        public string Zipcde { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }

        public Lgract IdrefNavigation { get; set; }
        public Lgract RecnumNavigation { get; set; }
    }
}
