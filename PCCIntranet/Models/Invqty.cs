﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Invqty
    {
        public Guid Idnum { get; set; }
        public Guid Idref { get; set; }
        public long Prtnum { get; set; }
        public int Locnum { get; set; }
        public decimal Qtyohn { get; set; }
        public decimal Qtyavl { get; set; }
        public DateTime? Lststk { get; set; }
        public DateTime? Lstpck { get; set; }
        public DateTime? Lstinv { get; set; }
        public string Mstcnt { get; set; }
        public decimal Begqty { get; set; }
        public int Minstk { get; set; }
        public int Maxstk { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }

        public Tkfprt IdrefNavigation { get; set; }
        public Invloc LocnumNavigation { get; set; }
        public Tkfprt PrtnumNavigation { get; set; }
    }
}
