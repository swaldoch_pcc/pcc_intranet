﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Vndprc
    {
        public Guid Idnum { get; set; }
        public Guid Idref { get; set; }
        public long Recnum { get; set; }
        public byte Linnum { get; set; }
        public byte Vndprf { get; set; }
        public long? Vndnum { get; set; }
        public string Vndord { get; set; }
        public decimal Vnddsc { get; set; }
        public decimal Vndprc1 { get; set; }
        public DateTime? Lstupd { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }

        public Tkfprt IdrefNavigation { get; set; }
        public Tkfprt RecnumNavigation { get; set; }
        public Actpay VndnumNavigation { get; set; }
    }
}
