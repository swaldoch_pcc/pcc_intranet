﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Submtl
    {
        public Submtl()
        {
            SbmtlnIdrefNavigation = new HashSet<Sbmtln>();
            SbmtlnRecnumNavigation = new HashSet<Sbmtln>();
        }

        public Guid Idnum { get; set; }
        public long Recnum { get; set; }
        public string Subnum { get; set; }
        public long Jobnum { get; set; }
        public long Phsnum { get; set; }
        public long? Divnum { get; set; }
        public string Dscrpt { get; set; }
        public string Usrdf1 { get; set; }
        public string Usrdf2 { get; set; }
        public long? Vndnum { get; set; }
        public long? Clnnum { get; set; }
        public string Attion { get; set; }
        public string Addrs1 { get; set; }
        public string Addrs2 { get; set; }
        public string Ctynme { get; set; }
        public string State { get; set; }
        public string Zipcde { get; set; }
        public long? Empnum { get; set; }
        public byte? Subtyp { get; set; }
        public DateTime? Subdte { get; set; }
        public DateTime? Reqdte { get; set; }
        public DateTime? Recdte { get; set; }
        public string Sntvia { get; set; }
        public DateTime? Entdte { get; set; }
        public string Usrnme { get; set; }
        public byte Hotlst { get; set; }
        public byte Lckedt { get; set; }
        public string Ntetxt { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }

        public Reccln ClnnumNavigation { get; set; }
        public Cstdiv DivnumNavigation { get; set; }
        public Employ EmpnumNavigation { get; set; }
        public Actrec JobnumNavigation { get; set; }
        public Jobphs Jobphs { get; set; }
        public Smttyp SubtypNavigation { get; set; }
        public Actpay VndnumNavigation { get; set; }
        public SubmU SubmU { get; set; }
        public ICollection<Sbmtln> SbmtlnIdrefNavigation { get; set; }
        public ICollection<Sbmtln> SbmtlnRecnumNavigation { get; set; }
    }
}
