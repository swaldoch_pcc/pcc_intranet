﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Clncnt
    {
        public Guid Idnum { get; set; }
        public Guid Idref { get; set; }
        public long Recnum { get; set; }
        public int Linnum { get; set; }
        public string Cntnme { get; set; }
        public string Jobttl { get; set; }
        public string Phnnum { get; set; }
        public string Phnext { get; set; }
        public string EMail { get; set; }
        public string Cllphn { get; set; }
        public string Faxnum { get; set; }
        public string Othphn { get; set; }
        public string Othdsc { get; set; }
        public string Ntetxt { get; set; }
        public string Linref { get; set; }
        public string Msolid { get; set; }
        public string Unqcnt { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }

        public Reccln IdrefNavigation { get; set; }
        public Reccln RecnumNavigation { get; set; }
    }
}
