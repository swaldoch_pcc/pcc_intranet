﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Bnkrec
    {
        public Bnkrec()
        {
            BnklinIdrefNavigation = new HashSet<Bnklin>();
            BnklinRecnumNavigation = new HashSet<Bnklin>();
        }

        public Guid Idnum { get; set; }
        public long Recnum { get; set; }
        public long Lgract { get; set; }
        public string Dscrpt { get; set; }
        public DateTime? Svedte { get; set; }
        public DateTime? Stmdte { get; set; }
        public DateTime? Cutdte { get; set; }
        public decimal Curbal { get; set; }
        public decimal Stmbeg { get; set; }
        public decimal Stmend { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }

        public Lgract LgractNavigation { get; set; }
        public ICollection<Bnklin> BnklinIdrefNavigation { get; set; }
        public ICollection<Bnklin> BnklinRecnumNavigation { get; set; }
    }
}
