﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Chgtyp
    {
        public Chgtyp()
        {
            Prmchg = new HashSet<Prmchg>();
        }

        public Guid Idnum { get; set; }
        public byte Recnum { get; set; }
        public string Typnme { get; set; }
        public string Ntetxt { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }

        public ICollection<Prmchg> Prmchg { get; set; }
    }
}
