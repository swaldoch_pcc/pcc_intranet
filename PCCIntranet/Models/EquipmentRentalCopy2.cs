﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class EquipmentRentalCopy2
    {
        public Guid RentalId { get; set; }
        public DateTime? DateOut { get; set; }
        public DateTime? DateIn { get; set; }
        public int? Jobnum { get; set; }
        public string Comments { get; set; }
        public int? Oldrentalid { get; set; }
        public string Job { get; set; }
        public string EquipmentId { get; set; }
        public string User { get; set; }
    }
}
