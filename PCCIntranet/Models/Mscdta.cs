﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Mscdta
    {
        public Guid Idnum { get; set; }
        public string Dtanme { get; set; }
        public string Dtalin { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }
    }
}
