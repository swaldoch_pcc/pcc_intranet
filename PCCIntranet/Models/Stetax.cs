﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Stetax
    {
        public Guid Idnum { get; set; }
        public string State { get; set; }
        public string Stetax1 { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }
    }
}
