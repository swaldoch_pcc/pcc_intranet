﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Emppay
    {
        public Guid Idnum { get; set; }
        public Guid Idref { get; set; }
        public long Empnum { get; set; }
        public DateTime? Chgdte { get; set; }
        public byte Rtetyp { get; set; }
        public decimal Orgamt { get; set; }
        public decimal Chgamt { get; set; }
        public decimal Pctchg { get; set; }
        public decimal Newamt { get; set; }
        public string Ntetxt { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }

        public Employ EmpnumNavigation { get; set; }
        public Employ IdrefNavigation { get; set; }
    }
}
