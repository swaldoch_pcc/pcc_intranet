﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Invout
    {
        public Guid Idnum { get; set; }
        public Guid Idref { get; set; }
        public long Recnum { get; set; }
        public long Prtnum { get; set; }
        public decimal Trnqty { get; set; }
        public DateTime? Trndte { get; set; }
        public byte Trntyp { get; set; }
        public byte Srcnum { get; set; }
        public long Lgrref { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }

        public Invbal IdrefNavigation { get; set; }
        public Tkfprt PrtnumNavigation { get; set; }
        public Invbal RecnumNavigation { get; set; }
        public Source SrcnumNavigation { get; set; }
    }
}
