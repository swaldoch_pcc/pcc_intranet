﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Tkfprt
    {
        public Tkfprt()
        {
            Apivln = new HashSet<Apivln>();
            Arivln = new HashSet<Arivln>();
            Asmprt = new HashSet<Asmprt>();
            HiscstIdrefNavigation = new HashSet<Hiscst>();
            HiscstRecnumNavigation = new HashSet<Hiscst>();
            Invbal = new HashSet<Invbal>();
            InverseLabnumNavigation = new HashSet<Tkfprt>();
            Invhst = new HashSet<Invhst>();
            Invout = new HashSet<Invout>();
            InvqtyIdrefNavigation = new HashSet<Invqty>();
            InvqtyPrtnumNavigation = new HashSet<Invqty>();
            InvserIdrefNavigation = new HashSet<Invser>();
            InvserRecnumNavigation = new HashSet<Invser>();
            Invtln = new HashSet<Invtln>();
            Pcorln = new HashSet<Pcorln>();
            Rcapln = new HashSet<Rcapln>();
            Rcarln = new HashSet<Rcarln>();
            Rqprln = new HashSet<Rqprln>();
            Srvlin = new HashSet<Srvlin>();
            Tkflin = new HashSet<Tkflin>();
            VndprcIdrefNavigation = new HashSet<Vndprc>();
            VndprcRecnumNavigation = new HashSet<Vndprc>();
        }

        public Guid Idnum { get; set; }
        public long Recnum { get; set; }
        public string Prtnme { get; set; }
        public string Prtunt { get; set; }
        public string Binnum { get; set; }
        public string Alpnum { get; set; }
        public string Msdsnm { get; set; }
        public string Mannme { get; set; }
        public string Mannum { get; set; }
        public string Usrdf1 { get; set; }
        public string Usrdf2 { get; set; }
        public decimal? Cstcde { get; set; }
        public byte? Csttyp { get; set; }
        public decimal? Prttsk { get; set; }
        public long? Prtcls { get; set; }
        public int? Dftloc { get; set; }
        public DateTime? Lstupd { get; set; }
        public decimal Reordr { get; set; }
        public decimal Minord { get; set; }
        public decimal Pkgqty { get; set; }
        public decimal Prtwgt { get; set; }
        public decimal Avgcst { get; set; }
        public decimal Prtcst { get; set; }
        public decimal Labunt { get; set; }
        public decimal Prtbil { get; set; }
        public decimal Qtyohn { get; set; }
        public byte Stkitm { get; set; }
        public byte Serinv { get; set; }
        public decimal Mrkupr { get; set; }
        public long? Labnum { get; set; }
        public string Ntetxt { get; set; }
        public string Prtspc { get; set; }
        public string Imgfle { get; set; }
        public byte Srvprt { get; set; }
        public short Oemdur { get; set; }
        public byte Reqivt { get; set; }
        public byte Sbjpst { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }
        public byte Inactv { get; set; }

        public Cstcde CstcdeNavigation { get; set; }
        public Csttyp CsttypNavigation { get; set; }
        public Invloc DftlocNavigation { get; set; }
        public Tkfprt LabnumNavigation { get; set; }
        public Prtcls PrtclsNavigation { get; set; }
        public Schtsk PrttskNavigation { get; set; }
        public TkfpU TkfpU { get; set; }
        public ICollection<Apivln> Apivln { get; set; }
        public ICollection<Arivln> Arivln { get; set; }
        public ICollection<Asmprt> Asmprt { get; set; }
        public ICollection<Hiscst> HiscstIdrefNavigation { get; set; }
        public ICollection<Hiscst> HiscstRecnumNavigation { get; set; }
        public ICollection<Invbal> Invbal { get; set; }
        public ICollection<Tkfprt> InverseLabnumNavigation { get; set; }
        public ICollection<Invhst> Invhst { get; set; }
        public ICollection<Invout> Invout { get; set; }
        public ICollection<Invqty> InvqtyIdrefNavigation { get; set; }
        public ICollection<Invqty> InvqtyPrtnumNavigation { get; set; }
        public ICollection<Invser> InvserIdrefNavigation { get; set; }
        public ICollection<Invser> InvserRecnumNavigation { get; set; }
        public ICollection<Invtln> Invtln { get; set; }
        public ICollection<Pcorln> Pcorln { get; set; }
        public ICollection<Rcapln> Rcapln { get; set; }
        public ICollection<Rcarln> Rcarln { get; set; }
        public ICollection<Rqprln> Rqprln { get; set; }
        public ICollection<Srvlin> Srvlin { get; set; }
        public ICollection<Tkflin> Tkflin { get; set; }
        public ICollection<Vndprc> VndprcIdrefNavigation { get; set; }
        public ICollection<Vndprc> VndprcRecnumNavigation { get; set; }
    }
}
