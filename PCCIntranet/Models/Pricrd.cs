﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Pricrd
    {
        public Guid Idnum { get; set; }
        public int Recnum { get; set; }
        public string Trnnum { get; set; }
        public DateTime? Trndte { get; set; }
        public string Paydsc { get; set; }
        public byte Status { get; set; }
        public long? Issuer { get; set; }
        public long? Subact { get; set; }
        public decimal Dbtamt { get; set; }
        public decimal Crdamt { get; set; }
        public byte Active { get; set; }
        public byte Ccrclr { get; set; }
        public byte Srcnum { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }

        public Lgract IssuerNavigation { get; set; }
        public Lgrsub Lgrsub { get; set; }
        public Source SrcnumNavigation { get; set; }
    }
}
