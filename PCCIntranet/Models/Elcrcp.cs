﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Elcrcp
    {
        public Guid Idnum { get; set; }
        public long Lgrrec { get; set; }
        public string Trngid { get; set; }
        public string Vanref { get; set; }
        public string Payor { get; set; }
        public string Paytyp { get; set; }
        public string Last4 { get; set; }
        public decimal Paymnt { get; set; }
        public string Dtetim { get; set; }
        public byte Trntyp { get; set; }
        public Guid Idref { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }

        public Lgrtrn IdrefNavigation { get; set; }
    }
}
