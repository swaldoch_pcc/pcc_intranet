﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Brdftg
    {
        public Guid Idnum { get; set; }
        public long Prtcls { get; set; }
        public decimal Cstbdf { get; set; }
        public decimal Bilbdf { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }

        public Prtcls PrtclsNavigation { get; set; }
    }
}
