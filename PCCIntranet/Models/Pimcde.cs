﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Pimcde
    {
        public Guid Idnum { get; set; }
        public short Cdenum { get; set; }
        public string Contry { get; set; }
        public string Taxste { get; set; }
        public byte Taxtyp { get; set; }
        public string Sdipyr { get; set; }
        public byte W2Box { get; set; }
        public string W2code { get; set; }
        public byte T4Box { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }
    }
}
