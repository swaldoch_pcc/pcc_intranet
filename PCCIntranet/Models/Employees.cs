using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Employees
    {
        public int Id { get; set; }
        public string Fname { get; set; }
        public string Lname { get; set; }
        public DateTime? HireDate { get; set; }
        public string Title { get; set; }
        public int? Manager { get; set; }
        public string Location { get; set; }
        public string PrefName { get; set; }
    }
}
