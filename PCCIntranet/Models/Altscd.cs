﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Altscd
    {
        public Guid Idnum { get; set; }
        public string Usrnme { get; set; }
        public string Runtim { get; set; }
        public string Mchnam { get; set; }
        public string Tskgid { get; set; }
        public DateTime? Lstrun { get; set; }
        public byte Wrndys { get; set; }
        public DateTime? Wrnshw { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }
        public byte V19flg { get; set; }
    }
}
