﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Calnte
    {
        public Guid Idnum { get; set; }
        public long Recnum { get; set; }
        public DateTime? Caldte { get; set; }
        public TimeSpan? Caltim { get; set; }
        public TimeSpan? Endtim { get; set; }
        public byte? Apttyp { get; set; }
        public long? Empnum { get; set; }
        public long? Clnnum { get; set; }
        public string Caltxt { get; set; }
        public string Rectyp { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }

        public Apttyp ApttypNavigation { get; set; }
        public Reccln ClnnumNavigation { get; set; }
    }
}
