﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Qrylst
    {
        public Guid Idnum { get; set; }
        public string Tblnme { get; set; }
        public string Qrynme { get; set; }
        public string Qrytbl { get; set; }
        public string Qryfld { get; set; }
        public string Qrysrt { get; set; }
        public string Qrygrp { get; set; }
        public string Qrysel { get; set; }
        public string Qrycrt { get; set; }
        public byte Omwlck { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }
    }
}
