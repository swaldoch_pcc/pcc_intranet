﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Bnktrn
    {
        public Guid Idnum { get; set; }
        public string Trnhsh { get; set; }
        public long TrnId { get; set; }
        public Guid BnkId { get; set; }
        public string Trntyp { get; set; }
        public decimal Amount { get; set; }
        public string Chknum { get; set; }
        public string Refnum { get; set; }
        public string Trnnme { get; set; }
        public string PayId { get; set; }
        public string Addrs1 { get; set; }
        public string Addrs2 { get; set; }
        public string Ctynme { get; set; }
        public string State { get; set; }
        public string Zipcde { get; set; }
        public string Extnme { get; set; }
        public DateTime? Pstdte { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }
    }
}
