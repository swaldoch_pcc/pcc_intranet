﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Tsktyp
    {
        public Tsktyp()
        {
            Schlin = new HashSet<Schlin>();
            Schtsk = new HashSet<Schtsk>();
        }

        public Guid Idnum { get; set; }
        public byte Recnum { get; set; }
        public string Tsknme { get; set; }
        public string Tskclr { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }

        public ICollection<Schlin> Schlin { get; set; }
        public ICollection<Schtsk> Schtsk { get; set; }
    }
}
