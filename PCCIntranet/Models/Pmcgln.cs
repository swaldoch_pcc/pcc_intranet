﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Pmcgln
    {
        public Guid Idnum { get; set; }
        public Guid Idref { get; set; }
        public long Recnum { get; set; }
        public int Linnum { get; set; }
        public string Dscrpt { get; set; }
        public decimal Estamt { get; set; }
        public decimal Ovhmrk { get; set; }
        public decimal Pftmrk { get; set; }
        public decimal Reqprc { get; set; }
        public decimal Aprprc { get; set; }
        public decimal? Cstcde { get; set; }
        public byte? Csttyp { get; set; }
        public string Usrdf1 { get; set; }
        public string Ntetxt { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }

        public Cstcde CstcdeNavigation { get; set; }
        public Csttyp CsttypNavigation { get; set; }
        public Prmchg IdrefNavigation { get; set; }
        public Prmchg RecnumNavigation { get; set; }
    }
}
