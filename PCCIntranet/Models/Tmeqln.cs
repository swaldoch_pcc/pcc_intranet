﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Tmeqln
    {
        public Guid Idnum { get; set; }
        public Guid Idref { get; set; }
        public int Recnum { get; set; }
        public int Linnum { get; set; }
        public short? Eqptyp { get; set; }
        public long? Eqpnum { get; set; }
        public decimal Oprrte { get; set; }
        public decimal Stdrte { get; set; }
        public decimal Idlrte { get; set; }
        public decimal Minhrs { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }

        public Eqpmnt EqpnumNavigation { get; set; }
        public Eqptyp EqptypNavigation { get; set; }
        public Timeqp IdrefNavigation { get; set; }
        public Timeqp RecnumNavigation { get; set; }
    }
}
