﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Lgrset
    {
        public Guid Idnum { get; set; }
        public long Lowlgr { get; set; }
        public long Hghlgr { get; set; }
        public long Lowcsh { get; set; }
        public long Hghcsh { get; set; }
        public long Lowast { get; set; }
        public long Hghast { get; set; }
        public long Lowwip { get; set; }
        public long Hghwip { get; set; }
        public long Lowoth { get; set; }
        public long Hghoth { get; set; }
        public long Lowfix { get; set; }
        public long Hghfix { get; set; }
        public long Lowdpr { get; set; }
        public long Hghdpr { get; set; }
        public long Lowcur { get; set; }
        public long Hghcur { get; set; }
        public long Lowlng { get; set; }
        public long Hghlng { get; set; }
        public long Loweqt { get; set; }
        public long Hgheqt { get; set; }
        public long Lowown { get; set; }
        public long Hghown { get; set; }
        public long Lowopr { get; set; }
        public long Hghopr { get; set; }
        public long Lowinc { get; set; }
        public long Hghinc { get; set; }
        public long Lowdir { get; set; }
        public long Hghdir { get; set; }
        public long Loweqp { get; set; }
        public long Hgheqp { get; set; }
        public long Lowovh { get; set; }
        public long Hghovh { get; set; }
        public long Lowadm { get; set; }
        public long Hghadm { get; set; }
        public long Lowtax { get; set; }
        public long Hghtax { get; set; }
        public long DedAr { get; set; }
        public long? DedSr { get; set; }
        public long DedAp { get; set; }
        public long? Dedinv { get; set; }
        public long? Dedeqp { get; set; }
        public long? Deddpr { get; set; }
        public long? Dedlns { get; set; }
        public long? ArTax { get; set; }
        public long ArFin { get; set; }
        public long ArDsc { get; set; }
        public long ArWip { get; set; }
        public long ArErn { get; set; }
        public long? Eqpjcs { get; set; }
        public long? Eqprpr { get; set; }
        public long? Eqprvn { get; set; }
        public long ApCmp { get; set; }
        public long ApDsc { get; set; }
        public long? ApTax { get; set; }
        public long? ApSub { get; set; }
        public long FvWip { get; set; }
        public long FvDcs { get; set; }
        public long? FvDsb { get; set; }
        public long FvOhc { get; set; }
        public long? FvOhs { get; set; }
        public byte Invmth { get; set; }
        public long? Invofs { get; set; }
        public long Dftact { get; set; }
        public long? ArHld { get; set; }
        public long? ApHld { get; set; }
        public DateTime? Fscyrd { get; set; }
        public byte Curprd { get; set; }
        public byte Vfydte { get; set; }
        public long Lstlgr { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }
        public byte Blkexp { get; set; }
        public long? UseAp { get; set; }
        public long? Usewip { get; set; }
        public long? Usedcs { get; set; }
        public long? Useohc { get; set; }
        public long? Useeqc { get; set; }
        public byte Requse { get; set; }
    }
}
