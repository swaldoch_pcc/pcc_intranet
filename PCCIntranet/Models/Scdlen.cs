﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Scdlen
    {
        public Guid Idnum { get; set; }
        public Guid Idref { get; set; }
        public long Recnum { get; set; }
        public int Linnum { get; set; }
        public string Lennum { get; set; }
        public long? Vndnum { get; set; }
        public long? Scdvnd { get; set; }
        public string Lenrcd { get; set; }
        public DateTime? Dtercd { get; set; }
        public DateTime? Fstdte { get; set; }
        public DateTime? Lstdte { get; set; }
        public string Lenrgt { get; set; }
        public string Jntchk { get; set; }
        public string Finwvr { get; set; }
        public DateTime? Findte { get; set; }
        public string Usrdf1 { get; set; }
        public string Ntetxt { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }

        public Actrec IdrefNavigation { get; set; }
        public Actrec RecnumNavigation { get; set; }
        public Actpay ScdvndNavigation { get; set; }
        public Actpay VndnumNavigation { get; set; }
    }
}
