﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Actrec
    {
        public Actrec()
        {
            Acpinv = new HashSet<Acpinv>();
            Acrinv = new HashSet<Acrinv>();
            Aiafrm = new HashSet<Aiafrm>();
            BiditmIdrefNavigation = new HashSet<Biditm>();
            BiditmRecnumNavigation = new HashSet<Biditm>();
            Coresp = new HashSet<Coresp>();
            Dlypyr = new HashSet<Dlypyr>();
            Eqprvw = new HashSet<Eqprvw>();
            ExportIdrefNavigation = new HashSet<Export>();
            ExportJobnumNavigation = new HashSet<Export>();
            Fldrpt = new HashSet<Fldrpt>();
            Invalc = new HashSet<Invalc>();
            JobcntIdrefNavigation = new HashSet<Jobcnt>();
            JobcntRecnumNavigation = new HashSet<Jobcnt>();
            Jobcst = new HashSet<Jobcst>();
            JobpgpIdrefNavigation = new HashSet<Jobpgp>();
            JobpgpRecnumNavigation = new HashSet<Jobpgp>();
            JobphsIdrefNavigation = new HashSet<Jobphs>();
            JobphsRecnumNavigation = new HashSet<Jobphs>();
            Lonfrm = new HashSet<Lonfrm>();
            Pchlst = new HashSet<Pchlst>();
            PchordNavigation = new HashSet<Pchord>();
            Plnrcd = new HashSet<Plnrcd>();
            PrelenIdrefNavigation = new HashSet<Prelen>();
            Prmchg = new HashSet<Prmchg>();
            Rcapjb = new HashSet<Rcapjb>();
            Rcarjb = new HashSet<Rcarjb>();
            Rccjob = new HashSet<Rccjob>();
            Rccpay = new HashSet<Rccpay>();
            Rccrec = new HashSet<Rccrec>();
            Reqinf = new HashSet<Reqinf>();
            Reqprp = new HashSet<Reqprp>();
            ScdlenIdrefNavigation = new HashSet<Scdlen>();
            ScdlenRecnumNavigation = new HashSet<Scdlen>();
            Schedl = new HashSet<Schedl>();
            Schprd = new HashSet<Schprd>();
            Srvinv = new HashSet<Srvinv>();
            Subcon = new HashSet<Subcon>();
            Submtl = new HashSet<Submtl>();
            Tmcdln = new HashSet<Tmcdln>();
            Trnmtl = new HashSet<Trnmtl>();
            Untbll = new HashSet<Untbll>();
            Untlin = new HashSet<Untlin>();
            Vndcrt = new HashSet<Vndcrt>();
        }

        public Guid Idnum { get; set; }
        public long Recnum { get; set; }
        public string Jobnme { get; set; }
        public string Shtnme { get; set; }
        public long? Clnnum { get; set; }
        public string Contct { get; set; }
        public string Addrs1 { get; set; }
        public string Addrs2 { get; set; }
        public string Ctynme { get; set; }
        public string State { get; set; }
        public string Zipcde { get; set; }
        public string Phnnum { get; set; }
        public string Faxnum { get; set; }
        public long? Achtct { get; set; }
        public long? Lender { get; set; }
        public long? Sprvsr { get; set; }
        public long? Slsemp { get; set; }
        public long? Estemp { get; set; }
        public string Usrdf1 { get; set; }
        public string Usrdf2 { get; set; }
        public int? Lcltax { get; set; }
        public int? Slstax { get; set; }
        public long? Lgract { get; set; }
        public long? Dptmnt { get; set; }
        public string Dscdte { get; set; }
        public string Duedte { get; set; }
        public decimal Dsccnt { get; set; }
        public decimal Finchg { get; set; }
        public decimal Retain { get; set; }
        public decimal Cntrct { get; set; }
        public decimal Begbal { get; set; }
        public decimal Endbal { get; set; }
        public DateTime? Biddte { get; set; }
        public DateTime? Plnrcv { get; set; }
        public DateTime? Actbid { get; set; }
        public DateTime? Ctcdte { get; set; }
        public DateTime? Prelen { get; set; }
        public DateTime? Sttdte { get; set; }
        public DateTime? Cmpdte { get; set; }
        public DateTime? Lenfld { get; set; }
        public DateTime? Lenrls { get; set; }
        public short? Jobtyp { get; set; }
        public byte Status { get; set; }
        public byte Pstwip { get; set; }
        public byte Crtfid { get; set; }
        public string Connum { get; set; }
        public string Pchord { get; set; }
        public string Imgfle { get; set; }
        public string Csttym { get; set; }
        public string Csttyl { get; set; }
        public string Csttye { get; set; }
        public string Csttys { get; set; }
        public string Csttyo { get; set; }
        public string Usrcs6 { get; set; }
        public string Usrcs7 { get; set; }
        public string Usrcs8 { get; set; }
        public string Usrcs9 { get; set; }
        public string Ntetxt { get; set; }
        public string Export { get; set; }
        public string Lotclr { get; set; }
        public decimal Lotprm { get; set; }
        public decimal Plnprc { get; set; }
        public decimal Actprc { get; set; }
        public DateTime? Estdte { get; set; }
        public DateTime? Actdte { get; set; }
        public string Lotnum { get; set; }
        public string Modnme { get; set; }
        public long Sqarft { get; set; }
        public string Usrlst { get; set; }
        public byte Catxex { get; set; }
        public byte Pstexm { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }
        public byte Inactv { get; set; }
        public byte Fedsck { get; set; }
        public string Stmeml { get; set; }
        public byte Ncrtck { get; set; }

        public Actpay AchtctNavigation { get; set; }
        public Reccln ClnnumNavigation { get; set; }
        public Dptmnt DptmntNavigation { get; set; }
        public Employ EstempNavigation { get; set; }
        public Jobtyp JobtypNavigation { get; set; }
        public Loctax LcltaxNavigation { get; set; }
        public Actpay LenderNavigation { get; set; }
        public Lgract LgractNavigation { get; set; }
        public Employ SlsempNavigation { get; set; }
        public Taxdst SlstaxNavigation { get; set; }
        public Employ SprvsrNavigation { get; set; }
        public ActrU ActrU { get; set; }
        public Budget Budget { get; set; }
        public Cstcmp Cstcmp { get; set; }
        public Hrscmp Hrscmp { get; set; }
        public Prelen PrelenRecnumNavigation { get; set; }
        public Propsl Propsl { get; set; }
        public Ptotkf Ptotkf { get; set; }
        public Timmat Timmat { get; set; }
        public Untcmp Untcmp { get; set; }
        public Untprp Untprp { get; set; }
        public ICollection<Acpinv> Acpinv { get; set; }
        public ICollection<Acrinv> Acrinv { get; set; }
        public ICollection<Aiafrm> Aiafrm { get; set; }
        public ICollection<Biditm> BiditmIdrefNavigation { get; set; }
        public ICollection<Biditm> BiditmRecnumNavigation { get; set; }
        public ICollection<Coresp> Coresp { get; set; }
        public ICollection<Dlypyr> Dlypyr { get; set; }
        public ICollection<Eqprvw> Eqprvw { get; set; }
        public ICollection<Export> ExportIdrefNavigation { get; set; }
        public ICollection<Export> ExportJobnumNavigation { get; set; }
        public ICollection<Fldrpt> Fldrpt { get; set; }
        public ICollection<Invalc> Invalc { get; set; }
        public ICollection<Jobcnt> JobcntIdrefNavigation { get; set; }
        public ICollection<Jobcnt> JobcntRecnumNavigation { get; set; }
        public ICollection<Jobcst> Jobcst { get; set; }
        public ICollection<Jobpgp> JobpgpIdrefNavigation { get; set; }
        public ICollection<Jobpgp> JobpgpRecnumNavigation { get; set; }
        public ICollection<Jobphs> JobphsIdrefNavigation { get; set; }
        public ICollection<Jobphs> JobphsRecnumNavigation { get; set; }
        public ICollection<Lonfrm> Lonfrm { get; set; }
        public ICollection<Pchlst> Pchlst { get; set; }
        public ICollection<Pchord> PchordNavigation { get; set; }
        public ICollection<Plnrcd> Plnrcd { get; set; }
        public ICollection<Prelen> PrelenIdrefNavigation { get; set; }
        public ICollection<Prmchg> Prmchg { get; set; }
        public ICollection<Rcapjb> Rcapjb { get; set; }
        public ICollection<Rcarjb> Rcarjb { get; set; }
        public ICollection<Rccjob> Rccjob { get; set; }
        public ICollection<Rccpay> Rccpay { get; set; }
        public ICollection<Rccrec> Rccrec { get; set; }
        public ICollection<Reqinf> Reqinf { get; set; }
        public ICollection<Reqprp> Reqprp { get; set; }
        public ICollection<Scdlen> ScdlenIdrefNavigation { get; set; }
        public ICollection<Scdlen> ScdlenRecnumNavigation { get; set; }
        public ICollection<Schedl> Schedl { get; set; }
        public ICollection<Schprd> Schprd { get; set; }
        public ICollection<Srvinv> Srvinv { get; set; }
        public ICollection<Subcon> Subcon { get; set; }
        public ICollection<Submtl> Submtl { get; set; }
        public ICollection<Tmcdln> Tmcdln { get; set; }
        public ICollection<Trnmtl> Trnmtl { get; set; }
        public ICollection<Untbll> Untbll { get; set; }
        public ICollection<Untlin> Untlin { get; set; }
        public ICollection<Vndcrt> Vndcrt { get; set; }
    }
}
