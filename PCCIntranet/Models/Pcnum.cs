﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Pcnum
    {
        public string Pcid { get; set; }
        public string PcNum1 { get; set; }
        public string PurchaseDate { get; set; }
        public string TagNum { get; set; }
        public string Title { get; set; }
        public string Mileage { get; set; }
        public string ModelId { get; set; }
        public string SerialId { get; set; }
        public string ConditionId { get; set; }
        public string Color { get; set; }
        public string YearModel { get; set; }
        public string Location { get; set; }
    }
}
