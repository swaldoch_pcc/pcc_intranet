﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class ReqpU
    {
        public Guid Idnum { get; set; }

        public Reqprp IdnumNavigation { get; set; }
    }
}
