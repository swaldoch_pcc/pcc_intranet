﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Lgrtrn
    {
        public Lgrtrn()
        {
            Elcrcp = new HashSet<Elcrcp>();
            LgtnlnIdrefNavigation = new HashSet<Lgtnln>();
            LgtnlnRecnumNavigation = new HashSet<Lgtnln>();
        }

        public Guid Idnum { get; set; }
        public long Recnum { get; set; }
        public string Trnnum { get; set; }
        public DateTime? Trndte { get; set; }
        public string Pchord { get; set; }
        public string Dscrpt { get; set; }
        public byte Status { get; set; }
        public byte Srcnum { get; set; }
        public byte Actprd { get; set; }
        public DateTime? Entdte { get; set; }
        public long? Vndnum { get; set; }
        public string Payee1 { get; set; }
        public string Payee2 { get; set; }
        public string Addrs1 { get; set; }
        public string Addrs2 { get; set; }
        public string Ctynme { get; set; }
        public string State { get; set; }
        public string Usrdf1 { get; set; }
        public string Usrdf2 { get; set; }
        public long Arcrec { get; set; }
        public string Usrnme { get; set; }
        public long Lgrrec { get; set; }
        public string Ntetxt { get; set; }
        public string Zipcde { get; set; }
        public decimal Chkamt { get; set; }
        public byte Active { get; set; }
        public decimal Jobvar { get; set; }
        public decimal Eqpvar { get; set; }
        public decimal Wipvar { get; set; }
        public byte Ccrclr { get; set; }
        public byte Ccract { get; set; }
        public long Vodrec { get; set; }
        public short Postyr { get; set; }
        public decimal Amt199 { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }
        public byte Bnkcat { get; set; }
        public int Paybch { get; set; }
        public string Trnhsh { get; set; }
        public long? Trnlnk { get; set; }

        public Source SrcnumNavigation { get; set; }
        public Actpay VndnumNavigation { get; set; }
        public ICollection<Elcrcp> Elcrcp { get; set; }
        public ICollection<Lgtnln> LgtnlnIdrefNavigation { get; set; }
        public ICollection<Lgtnln> LgtnlnRecnumNavigation { get; set; }
    }
}
