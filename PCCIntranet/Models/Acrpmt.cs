﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Acrpmt
    {
        public Guid Idnum { get; set; }
        public Guid Idref { get; set; }
        public long Recnum { get; set; }
        public string Dscrpt { get; set; }
        public string Chknum { get; set; }
        public DateTime? Chkdte { get; set; }
        public byte Actper { get; set; }
        public decimal Amount { get; set; }
        public decimal Dsctkn { get; set; }
        public decimal Aplcrd { get; set; }
        public long Lgrrec { get; set; }
        public short Postyr { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }

        public Acrinv IdrefNavigation { get; set; }
        public Acrinv RecnumNavigation { get; set; }
    }
}
