﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Fdrpin
    {
        public Guid Idnum { get; set; }
        public Guid Idref { get; set; }
        public long Recnum { get; set; }
        public int Linnum { get; set; }
        public short? Inctyp { get; set; }
        public string Dscrpt { get; set; }
        public TimeSpan? Inctim { get; set; }
        public string Ntetxt { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }

        public Fldrpt IdrefNavigation { get; set; }
        public Inctyp InctypNavigation { get; set; }
        public Fldrpt RecnumNavigation { get; set; }
    }
}
