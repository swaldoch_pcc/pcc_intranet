﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Taxdst
    {
        public Taxdst()
        {
            Acpinv = new HashSet<Acpinv>();
            Acrinv = new HashSet<Acrinv>();
            Actpay = new HashSet<Actpay>();
            Actrec = new HashSet<Actrec>();
            Aiafrm = new HashSet<Aiafrm>();
            Pchord = new HashSet<Pchord>();
            Rccrec = new HashSet<Rccrec>();
            Reccln = new HashSet<Reccln>();
            Reqprp = new HashSet<Reqprp>();
            Srvinv = new HashSet<Srvinv>();
            Srvloc = new HashSet<Srvloc>();
            Subcon = new HashSet<Subcon>();
            Tkflin = new HashSet<Tkflin>();
            Untbll = new HashSet<Untbll>();
        }

        public Guid Idnum { get; set; }
        public int Recnum { get; set; }
        public string Dstnme { get; set; }
        public int? Entty1 { get; set; }
        public int? Entty2 { get; set; }
        public int? Entty3 { get; set; }
        public int? Entty4 { get; set; }
        public int? Entty5 { get; set; }
        public string Mattax { get; set; }
        public string Labtax { get; set; }
        public string Eqptax { get; set; }
        public string Subtax { get; set; }
        public string Othtax { get; set; }
        public string Usrcs6 { get; set; }
        public string Usrcs7 { get; set; }
        public string Usrcs8 { get; set; }
        public string Usrcs9 { get; set; }
        public string Pfttax { get; set; }
        public string Ntetxt { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }
        public decimal Sumrte { get; set; }

        public Taxent Entty1Navigation { get; set; }
        public Taxent Entty2Navigation { get; set; }
        public Taxent Entty3Navigation { get; set; }
        public Taxent Entty4Navigation { get; set; }
        public Taxent Entty5Navigation { get; set; }
        public ICollection<Acpinv> Acpinv { get; set; }
        public ICollection<Acrinv> Acrinv { get; set; }
        public ICollection<Actpay> Actpay { get; set; }
        public ICollection<Actrec> Actrec { get; set; }
        public ICollection<Aiafrm> Aiafrm { get; set; }
        public ICollection<Pchord> Pchord { get; set; }
        public ICollection<Rccrec> Rccrec { get; set; }
        public ICollection<Reccln> Reccln { get; set; }
        public ICollection<Reqprp> Reqprp { get; set; }
        public ICollection<Srvinv> Srvinv { get; set; }
        public ICollection<Srvloc> Srvloc { get; set; }
        public ICollection<Subcon> Subcon { get; set; }
        public ICollection<Tkflin> Tkflin { get; set; }
        public ICollection<Untbll> Untbll { get; set; }
    }
}
