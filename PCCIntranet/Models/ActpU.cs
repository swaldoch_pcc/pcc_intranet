﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class ActpU
    {
        public Guid Idnum { get; set; }

        public Actpay IdnumNavigation { get; set; }
    }
}
