﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Tmcdln
    {
        public Guid Idnum { get; set; }
        public Guid Idref { get; set; }
        public long Recnum { get; set; }
        public short Linnum { get; set; }
        public DateTime? Dtewrk { get; set; }
        public string Daywrk { get; set; }
        public string Dscrpt { get; set; }
        public string Wrkord { get; set; }
        public long? Jobnum { get; set; }
        public long? Eqpnum { get; set; }
        public int? Loctax { get; set; }
        public string Crtfid { get; set; }
        public long? Phsnum { get; set; }
        public decimal? Cstcde { get; set; }
        public byte Paytyp { get; set; }
        public int? Paygrp { get; set; }
        public decimal Payrte { get; set; }
        public decimal Hrswrk { get; set; }
        public decimal Pcerte { get; set; }
        public decimal Pieces { get; set; }
        public int? Cmpcde { get; set; }
        public long? Dptmnt { get; set; }
        public decimal Jobcst { get; set; }
        public decimal Cmpsub { get; set; }
        public decimal Bensub { get; set; }
        public decimal Cmpwge { get; set; }
        public decimal Cmpgrs { get; set; }
        public short? Absnce { get; set; }
        public string Usrdf1 { get; set; }
        public string Ntetxt { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }
        public decimal Ovtdif { get; set; }

        public Empabs AbsnceNavigation { get; set; }
        public Wkrcmp CmpcdeNavigation { get; set; }
        public Cstcde CstcdeNavigation { get; set; }
        public Dptmnt DptmntNavigation { get; set; }
        public Eqpmnt EqpnumNavigation { get; set; }
        public Payrec IdrefNavigation { get; set; }
        public Actrec JobnumNavigation { get; set; }
        public Jobphs Jobphs { get; set; }
        public Loctax LoctaxNavigation { get; set; }
        public Paygrp PaygrpNavigation { get; set; }
        public Payrec RecnumNavigation { get; set; }
    }
}
