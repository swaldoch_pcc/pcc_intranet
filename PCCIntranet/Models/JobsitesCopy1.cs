﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class JobsitesCopy1
    {
        public int JobsiteId { get; set; }
        public string Name { get; set; }
        public string ProjectManager { get; set; }
        public string Street { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string Owner { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Cell { get; set; }
        public string CalendarStatus { get; set; }
        public string CcLimit { get; set; }
        public string AchLimit { get; set; }
        public string Created { get; set; }
        public string ProjectedStart { get; set; }
        public string JobType { get; set; }
        public string ProjectedCompletion { get; set; }
        public string ActualStart { get; set; }
        public string ActualCompletion { get; set; }
        public string BaselineDurationWorkDays { get; set; }
        public string ActualDurationWorkDays { get; set; }
        public string Status { get; set; }
        public string JobPrefix { get; set; }
        public string JobGroups { get; set; }
        public string Lot { get; set; }
        public string Permit { get; set; }
        public string IndividualPoLimit { get; set; }
        public string OverallPoLimit { get; set; }
        public string Warranty { get; set; }
        public string WorkingTemplate { get; set; }
        public decimal? ContractPrice { get; set; }
        public string JobRunningTotal { get; set; }
        public string PaymentsReceived { get; set; }
        public string OwnerBalance { get; set; }
        public string TotalCostBillPoCost { get; set; }
        public string TotalPaidBillPoPaid { get; set; }
        public string TotalRemainingBillPoRemaining { get; set; }
        public string OwnerActivationStatus { get; set; }
        public string OwnerLastViewed { get; set; }
        public string EstimateStatus { get; set; }
        public string SuperintendentFullName { get; set; }
        public string PCPersonnel { get; set; }
        public string GeneralNotes { get; set; }
        public string ChangeOrderNotes { get; set; }
        public string PermittingPhone { get; set; }
        public string PermittingContact { get; set; }
        public string PermittingRequirements { get; set; }
        public string PermittingNotes { get; set; }
        public string LicensingPhone { get; set; }
        public string LicensingContact { get; set; }
        public string LicensingNeed { get; set; }
        public string LicensingRequirements { get; set; }
        public string LicensingNotes { get; set; }
        public string PermitInfoTapFee { get; set; }
        public string PermitInfoWater { get; set; }
        public string PermitIntoSewer { get; set; }
        public string PermitInfoContact { get; set; }
        public string PermitInfoPhone { get; set; }
        public string PermitInfoNotes { get; set; }
        public string LicenseStateExpires { get; set; }
        public string LicenseCityCountyExp { get; set; }
        public string License { get; set; }
        public string LicenseSubs { get; set; }
        public string LicensesFiledTo { get; set; }
        public string LicensesContact { get; set; }
        public string LicensePhone { get; set; }
        public string LicenseRequirements { get; set; }
        public string LicensesNotes { get; set; }
        public bool? Checkbox { get; set; }
    }
}
