﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Safetyinspectionlog
    {
        public Guid Id { get; set; }
        public string Jobsite { get; set; }
        public string Auditor { get; set; }
        public string Superintendent { get; set; }
        public DateTime? Auditdate { get; set; }
    }
}
