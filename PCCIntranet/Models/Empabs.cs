﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Empabs
    {
        public Empabs()
        {
            Dlypyr = new HashSet<Dlypyr>();
            Fdrpln = new HashSet<Fdrpln>();
            Tmcdln = new HashSet<Tmcdln>();
        }

        public Guid Idnum { get; set; }
        public short Recnum { get; set; }
        public string Resabs { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }

        public ICollection<Dlypyr> Dlypyr { get; set; }
        public ICollection<Fdrpln> Fdrpln { get; set; }
        public ICollection<Tmcdln> Tmcdln { get; set; }
    }
}
