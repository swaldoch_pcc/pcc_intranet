﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Hiscst
    {
        public Guid Idnum { get; set; }
        public Guid Idref { get; set; }
        public long Recnum { get; set; }
        public short Fsclyr { get; set; }
        public decimal Avgcst { get; set; }
        public DateTime? Valdte { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }

        public Tkfprt IdrefNavigation { get; set; }
        public Tkfprt RecnumNavigation { get; set; }
    }
}
