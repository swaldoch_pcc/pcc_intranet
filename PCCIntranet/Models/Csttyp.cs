﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Csttyp
    {
        public Csttyp()
        {
            Actpay = new HashSet<Actpay>();
            Asmprt = new HashSet<Asmprt>();
            Eqpcst = new HashSet<Eqpcst>();
            Jobcst = new HashSet<Jobcst>();
            Lgract = new HashSet<Lgract>();
            Pcorln = new HashSet<Pcorln>();
            Pmcgln = new HashSet<Pmcgln>();
            PtotkfBndtypNavigation = new HashSet<Ptotkf>();
            PtotkfInstypNavigation = new HashSet<Ptotkf>();
            PtotkfTaxtypNavigation = new HashSet<Ptotkf>();
            Rcapeq = new HashSet<Rcapeq>();
            Rcapjb = new HashSet<Rcapjb>();
            Rcareq = new HashSet<Rcareq>();
            Rcarjb = new HashSet<Rcarjb>();
            Rcceqp = new HashSet<Rcceqp>();
            Rccjob = new HashSet<Rccjob>();
            Rqprln = new HashSet<Rqprln>();
            Sbcgln = new HashSet<Sbcgln>();
            Sbcnln = new HashSet<Sbcnln>();
            Srvlin = new HashSet<Srvlin>();
            Srvtyp = new HashSet<Srvtyp>();
            Tkflin = new HashSet<Tkflin>();
            Tkfprt = new HashSet<Tkfprt>();
        }

        public Guid Idnum { get; set; }
        public byte Recnum { get; set; }
        public string Typnme { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }

        public ICollection<Actpay> Actpay { get; set; }
        public ICollection<Asmprt> Asmprt { get; set; }
        public ICollection<Eqpcst> Eqpcst { get; set; }
        public ICollection<Jobcst> Jobcst { get; set; }
        public ICollection<Lgract> Lgract { get; set; }
        public ICollection<Pcorln> Pcorln { get; set; }
        public ICollection<Pmcgln> Pmcgln { get; set; }
        public ICollection<Ptotkf> PtotkfBndtypNavigation { get; set; }
        public ICollection<Ptotkf> PtotkfInstypNavigation { get; set; }
        public ICollection<Ptotkf> PtotkfTaxtypNavigation { get; set; }
        public ICollection<Rcapeq> Rcapeq { get; set; }
        public ICollection<Rcapjb> Rcapjb { get; set; }
        public ICollection<Rcareq> Rcareq { get; set; }
        public ICollection<Rcarjb> Rcarjb { get; set; }
        public ICollection<Rcceqp> Rcceqp { get; set; }
        public ICollection<Rccjob> Rccjob { get; set; }
        public ICollection<Rqprln> Rqprln { get; set; }
        public ICollection<Sbcgln> Sbcgln { get; set; }
        public ICollection<Sbcnln> Sbcnln { get; set; }
        public ICollection<Srvlin> Srvlin { get; set; }
        public ICollection<Srvtyp> Srvtyp { get; set; }
        public ICollection<Tkflin> Tkflin { get; set; }
        public ICollection<Tkfprt> Tkfprt { get; set; }
    }
}
