﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Wkrcmp
    {
        public Wkrcmp()
        {
            CstcdeCmpcd2Navigation = new HashSet<Cstcde>();
            CstcdeCmpcdeNavigation = new HashSet<Cstcde>();
            Dlypyr = new HashSet<Dlypyr>();
            Employ = new HashSet<Employ>();
            Tmcdln = new HashSet<Tmcdln>();
        }

        public Guid Idnum { get; set; }
        public int Recnum { get; set; }
        public string Cdenme { get; set; }
        public string Taxste { get; set; }
        public decimal Pctrte { get; set; }
        public decimal Emehrs { get; set; }
        public decimal Emrhrs { get; set; }
        public decimal Libins { get; set; }
        public decimal Expmod { get; set; }
        public decimal Addmod { get; set; }
        public decimal Maxwge { get; set; }
        public string Ntetxt { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }
        public string Inactv { get; set; }

        public ICollection<Cstcde> CstcdeCmpcd2Navigation { get; set; }
        public ICollection<Cstcde> CstcdeCmpcdeNavigation { get; set; }
        public ICollection<Dlypyr> Dlypyr { get; set; }
        public ICollection<Employ> Employ { get; set; }
        public ICollection<Tmcdln> Tmcdln { get; set; }
    }
}
