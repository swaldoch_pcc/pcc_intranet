﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Dlypyr
    {
        public Guid Idnum { get; set; }
        public DateTime Paydte { get; set; }
        public int Linnum { get; set; }
        public long? Empnum { get; set; }
        public string Dscrpt { get; set; }
        public string Wrkord { get; set; }
        public long? Jobnum { get; set; }
        public long? Eqpnum { get; set; }
        public int? Loctax { get; set; }
        public string Crtfid { get; set; }
        public long? Phsnum { get; set; }
        public decimal? Cstcde { get; set; }
        public byte Paytyp { get; set; }
        public int? Paygrp { get; set; }
        public decimal Payrte { get; set; }
        public decimal Payhrs { get; set; }
        public decimal Pcerte { get; set; }
        public decimal Pieces { get; set; }
        public int? Cmpcde { get; set; }
        public long? Dptmnt { get; set; }
        public long? Opreqp { get; set; }
        public byte Eqpunt { get; set; }
        public decimal Oprhrs { get; set; }
        public decimal Stdhrs { get; set; }
        public decimal Idlhrs { get; set; }
        public byte Bllunt { get; set; }
        public decimal Oprbll { get; set; }
        public decimal Stdbll { get; set; }
        public decimal Idlbll { get; set; }
        public string Usrdf1 { get; set; }
        public string Ntetxt { get; set; }
        public short? Absnce { get; set; }
        public string Lineid { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }

        public Empabs AbsnceNavigation { get; set; }
        public Wkrcmp CmpcdeNavigation { get; set; }
        public Cstcde CstcdeNavigation { get; set; }
        public Dptmnt DptmntNavigation { get; set; }
        public Employ EmpnumNavigation { get; set; }
        public Eqpmnt EqpnumNavigation { get; set; }
        public Actrec JobnumNavigation { get; set; }
        public Jobphs Jobphs { get; set; }
        public Loctax LoctaxNavigation { get; set; }
        public Eqpmnt OpreqpNavigation { get; set; }
        public Paygrp PaygrpNavigation { get; set; }
    }
}
