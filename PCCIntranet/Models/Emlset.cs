﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Emlset
    {
        public Guid Idnum { get; set; }
        public string Usrnme { get; set; }
        public string Otleml { get; set; }
        public string Cmpeml { get; set; }
        public string Cmppas { get; set; }
        public string Altsrv { get; set; }
        public short Altprt { get; set; }
        public byte Altath { get; set; }
        public byte Altssl { get; set; }
        public string Alteml { get; set; }
        public string Altpas { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }
    }
}
