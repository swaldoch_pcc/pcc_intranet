﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Alkdom
    {
        public Guid Idnum { get; set; }
        public Guid Idref { get; set; }
        public long Recnum { get; set; }
        public string Addrss { get; set; }
        public string Addrs2 { get; set; }
        public string Ctynme { get; set; }
        public string State { get; set; }
        public string Zipcde { get; set; }
        public string Crtnum { get; set; }
        public decimal Apprte { get; set; }
        public string Brgagr { get; set; }
        public string Occnum { get; set; }
        public string Arecde { get; set; }
        public string Licnum { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }

        public Employ IdrefNavigation { get; set; }
        public Employ RecnumNavigation { get; set; }
    }
}
