﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Cstcde
    {
        public Cstcde()
        {
            Actpay = new HashSet<Actpay>();
            Aialin = new HashSet<Aialin>();
            Arivln = new HashSet<Arivln>();
            Asmprt = new HashSet<Asmprt>();
            Bdglin = new HashSet<Bdglin>();
            Cscpln = new HashSet<Cscpln>();
            Dlypyr = new HashSet<Dlypyr>();
            Eqpcst = new HashSet<Eqpcst>();
            Eqpmln = new HashSet<Eqpmln>();
            Eqprvw = new HashSet<Eqprvw>();
            Fdrpeq = new HashSet<Fdrpeq>();
            Fdrpln = new HashSet<Fdrpln>();
            Hrcpln = new HashSet<Hrcpln>();
            Jobcst = new HashSet<Jobcst>();
            Lonlin = new HashSet<Lonlin>();
            Pcorln = new HashSet<Pcorln>();
            Pmcgln = new HashSet<Pmcgln>();
            Prplin = new HashSet<Prplin>();
            PtotkfBndcdeNavigation = new HashSet<Ptotkf>();
            PtotkfInscdeNavigation = new HashSet<Ptotkf>();
            PtotkfTaxcdeNavigation = new HashSet<Ptotkf>();
            Rcapeq = new HashSet<Rcapeq>();
            Rcapjb = new HashSet<Rcapjb>();
            Rcareq = new HashSet<Rcareq>();
            Rcarjb = new HashSet<Rcarjb>();
            Rcarln = new HashSet<Rcarln>();
            Rcceqp = new HashSet<Rcceqp>();
            Rccjob = new HashSet<Rccjob>();
            Rqprln = new HashSet<Rqprln>();
            Sbcgln = new HashSet<Sbcgln>();
            Sbcnln = new HashSet<Sbcnln>();
            Srvtyp = new HashSet<Srvtyp>();
            Tkflin = new HashSet<Tkflin>();
            Tkfprt = new HashSet<Tkfprt>();
            Tmcdln = new HashSet<Tmcdln>();
            Tmemln = new HashSet<Tmemln>();
            Uncpln = new HashSet<Uncpln>();
        }

        public Guid Idnum { get; set; }
        public decimal Recnum { get; set; }
        public string Cdenme { get; set; }
        public string Untdsc { get; set; }
        public long Divnum { get; set; }
        public int? Cmpcde { get; set; }
        public decimal Maxwge { get; set; }
        public int? Cmpcd2 { get; set; }
        public long? Dptnum { get; set; }
        public string Ntetxt { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }
        public string Inactv { get; set; }

        public Wkrcmp Cmpcd2Navigation { get; set; }
        public Wkrcmp CmpcdeNavigation { get; set; }
        public Cstdiv DivnumNavigation { get; set; }
        public Dptmnt DptnumNavigation { get; set; }
        public ICollection<Actpay> Actpay { get; set; }
        public ICollection<Aialin> Aialin { get; set; }
        public ICollection<Arivln> Arivln { get; set; }
        public ICollection<Asmprt> Asmprt { get; set; }
        public ICollection<Bdglin> Bdglin { get; set; }
        public ICollection<Cscpln> Cscpln { get; set; }
        public ICollection<Dlypyr> Dlypyr { get; set; }
        public ICollection<Eqpcst> Eqpcst { get; set; }
        public ICollection<Eqpmln> Eqpmln { get; set; }
        public ICollection<Eqprvw> Eqprvw { get; set; }
        public ICollection<Fdrpeq> Fdrpeq { get; set; }
        public ICollection<Fdrpln> Fdrpln { get; set; }
        public ICollection<Hrcpln> Hrcpln { get; set; }
        public ICollection<Jobcst> Jobcst { get; set; }
        public ICollection<Lonlin> Lonlin { get; set; }
        public ICollection<Pcorln> Pcorln { get; set; }
        public ICollection<Pmcgln> Pmcgln { get; set; }
        public ICollection<Prplin> Prplin { get; set; }
        public ICollection<Ptotkf> PtotkfBndcdeNavigation { get; set; }
        public ICollection<Ptotkf> PtotkfInscdeNavigation { get; set; }
        public ICollection<Ptotkf> PtotkfTaxcdeNavigation { get; set; }
        public ICollection<Rcapeq> Rcapeq { get; set; }
        public ICollection<Rcapjb> Rcapjb { get; set; }
        public ICollection<Rcareq> Rcareq { get; set; }
        public ICollection<Rcarjb> Rcarjb { get; set; }
        public ICollection<Rcarln> Rcarln { get; set; }
        public ICollection<Rcceqp> Rcceqp { get; set; }
        public ICollection<Rccjob> Rccjob { get; set; }
        public ICollection<Rqprln> Rqprln { get; set; }
        public ICollection<Sbcgln> Sbcgln { get; set; }
        public ICollection<Sbcnln> Sbcnln { get; set; }
        public ICollection<Srvtyp> Srvtyp { get; set; }
        public ICollection<Tkflin> Tkflin { get; set; }
        public ICollection<Tkfprt> Tkfprt { get; set; }
        public ICollection<Tmcdln> Tmcdln { get; set; }
        public ICollection<Tmemln> Tmemln { get; set; }
        public ICollection<Uncpln> Uncpln { get; set; }
    }
}
