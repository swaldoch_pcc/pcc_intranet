﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class ReqiU
    {
        public Guid Idnum { get; set; }

        public Reqinf IdnumNavigation { get; set; }
    }
}
