﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Acaemp
    {
        public Guid Idnum { get; set; }
        public Guid Idref { get; set; }
        public long Recnum { get; set; }
        public short Calyer { get; set; }
        public byte Calmnt { get; set; }
        public byte Wrksts { get; set; }
        public byte Offcvg { get; set; }
        public byte Safhrb { get; set; }
        public decimal Shrprm { get; set; }
        public string Lineid { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }

        public Employ IdrefNavigation { get; set; }
        public Employ RecnumNavigation { get; set; }
    }
}
