﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Plnrcd
    {
        public Plnrcd()
        {
            PlnrcvIdrefNavigation = new HashSet<Plnrcv>();
            PlnrcvRecnumNavigation = new HashSet<Plnrcv>();
            PnrclnIdrefNavigation = new HashSet<Pnrcln>();
            PnrclnRecnumNavigation = new HashSet<Pnrcln>();
        }

        public Guid Idnum { get; set; }
        public long Recnum { get; set; }
        public string Plnnum { get; set; }
        public long Jobnum { get; set; }
        public long Phsnum { get; set; }
        public long? Divnum { get; set; }
        public long? Vndnum { get; set; }
        public DateTime? Orgdte { get; set; }
        public string Dscrpt { get; set; }
        public string Usrdf1 { get; set; }
        public string Usrdf2 { get; set; }
        public byte Hotlst { get; set; }
        public byte Lckedt { get; set; }
        public DateTime? Entdte { get; set; }
        public string Usrnme { get; set; }
        public string Ntetxt { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }

        public Cstdiv DivnumNavigation { get; set; }
        public Actrec JobnumNavigation { get; set; }
        public Jobphs Jobphs { get; set; }
        public Actpay VndnumNavigation { get; set; }
        public PlnrU PlnrU { get; set; }
        public ICollection<Plnrcv> PlnrcvIdrefNavigation { get; set; }
        public ICollection<Plnrcv> PlnrcvRecnumNavigation { get; set; }
        public ICollection<Pnrcln> PnrclnIdrefNavigation { get; set; }
        public ICollection<Pnrcln> PnrclnRecnumNavigation { get; set; }
    }
}
