﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Rptdft
    {
        public Guid Idnum { get; set; }
        public string Rptnme { get; set; }
        public string Dftdta { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }
    }
}
