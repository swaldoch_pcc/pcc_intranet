﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class LeadProposals
    {
        public string LeadId { get; set; }
        public string ProposalId { get; set; }
        public string ProposalTitle { get; set; }
        public string OpportunityTitle { get; set; }
        public string Status { get; set; }
        public string CustomerContact { get; set; }
        public string Salesperson { get; set; }
        public string OwnerPrice { get; set; }
        public string ProposalStatus { get; set; }
        public string RequiredPayment { get; set; }
        public string PaymentStatus { get; set; }
        public string Updated { get; set; }
        public string LastViewed { get; set; }
        public string InternalNotes { get; set; }
        public string IntroductoryText { get; set; }
        public string ClosingText { get; set; }
    }
}
