﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Dshprf
    {
        public Guid Idnum { get; set; }
        public string Usrnme { get; set; }
        public string Dftref { get; set; }
        public short Pnltyp { get; set; }
        public byte Colnum { get; set; }
        public byte Locnum { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }
    }
}
