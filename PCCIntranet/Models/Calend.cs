﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Calend
    {
        public Guid Idnum { get; set; }
        public DateTime? Caldte { get; set; }
        public byte Wrksts { get; set; }
        public string Ntetxt { get; set; }
        public string Daydsc { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }
    }
}
