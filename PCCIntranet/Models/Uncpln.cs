﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Uncpln
    {
        public Guid Idnum { get; set; }
        public Guid Idref { get; set; }
        public long Recnum { get; set; }
        public long Phsnum { get; set; }
        public int Linnum { get; set; }
        public decimal? Cstcde { get; set; }
        public decimal Bdgcst { get; set; }
        public decimal Bdgunt { get; set; }
        public decimal Cstdte { get; set; }
        public decimal Untcmp { get; set; }
        public decimal Bdgprc { get; set; }
        public decimal Actprc { get; set; }
        public decimal Ovrund { get; set; }
        public decimal Untrem { get; set; }
        public decimal Cstcmp { get; set; }
        public decimal Ovrttl { get; set; }
        public string Usrdf1 { get; set; }
        public string Ntetxt { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }

        public Cstcde CstcdeNavigation { get; set; }
        public Untcmp IdrefNavigation { get; set; }
        public Jobphs Jobphs { get; set; }
        public Untcmp RecnumNavigation { get; set; }
    }
}
