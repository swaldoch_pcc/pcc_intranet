﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class ChangeOrders
    {
        public string Jobsite { get; set; }
        public string JobsiteId { get; set; }
        public string ChangeOrderId { get; set; }
        public string CustChangeOrderId { get; set; }
        public string Title { get; set; }
        public string Owner { get; set; }
        public string CreatedBy { get; set; }
        public string DateEntered { get; set; }
        public string Deadline { get; set; }
        public string PaymentDeadlineDate { get; set; }
        public string BuilderCost { get; set; }
        public string OwnerPrice { get; set; }
        public string OwnerPaid { get; set; }
        public string BalanceDue { get; set; }
        public string GrossProfit { get; set; }
        public string DatePaid { get; set; }
        public string ApprovalStatus { get; set; }
        public string OwnerLastViewed { get; set; }
        public string RelatedPoIds { get; set; }
        public string PoBuilderVariance { get; set; }
        public string PoCustomerVariance { get; set; }
        public string PaymentStatus { get; set; }
        public string SubsList { get; set; }
    }
}
