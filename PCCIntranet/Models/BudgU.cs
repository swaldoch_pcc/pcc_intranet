﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class BudgU
    {
        public Guid Idnum { get; set; }

        public Budget IdnumNavigation { get; set; }
    }
}
