﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Timemp
    {
        public Timemp()
        {
            Timmat = new HashSet<Timmat>();
            TmemlnIdrefNavigation = new HashSet<Tmemln>();
            TmemlnRecnumNavigation = new HashSet<Tmemln>();
        }

        public Guid Idnum { get; set; }
        public int Recnum { get; set; }
        public string Tbldsc { get; set; }
        public DateTime? Edtdte { get; set; }
        public byte Lckedt { get; set; }
        public string Usrnme { get; set; }
        public string Ntetxt { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }

        public ICollection<Timmat> Timmat { get; set; }
        public ICollection<Tmemln> TmemlnIdrefNavigation { get; set; }
        public ICollection<Tmemln> TmemlnRecnumNavigation { get; set; }
    }
}
