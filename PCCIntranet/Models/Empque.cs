﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Empque
    {
        public Guid Idnum { get; set; }
        public Guid Idref { get; set; }
        public long Recnum { get; set; }
        public decimal DedJ { get; set; }
        public decimal DedJ1 { get; set; }
        public decimal CrdE1 { get; set; }
        public decimal CrdE2 { get; set; }
        public decimal CrdK1 { get; set; }
        public decimal Addtax { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }

        public Employ IdrefNavigation { get; set; }
        public Employ RecnumNavigation { get; set; }
    }
}
