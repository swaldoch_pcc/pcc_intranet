﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace PCCIntranet.Models
{
    public partial class SafetyContext : DbContext
    {
        public SafetyContext()
        {
        }

        public SafetyContext(DbContextOptions<SafetyContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Observations> Observations { get; set; }
        public virtual DbSet<Safetyinspectionlog> Safetyinspectionlog { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer("Data Source=10.28.6.25\\PCC;Initial Catalog=PCC;Persist Security Info=False;User ID=pcc_reader;Password=InGodWeTrust;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Observations>(entity =>
            {
                entity.ToTable("observations", "Safety");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedNever();

                entity.Property(e => e.Auditid).HasColumnName("auditid");

                entity.Property(e => e.Notes).HasColumnName("notes");

                entity.Property(e => e.Observation).HasColumnName("observation");

                entity.Property(e => e.Observationid).HasColumnName("observationid");

                entity.Property(e => e.Status)
                    .HasColumnName("status")
                    .HasMaxLength(255);
            });

            modelBuilder.Entity<Safetyinspectionlog>(entity =>
            {
                entity.ToTable("safetyinspectionlog", "Safety");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedNever();

                entity.Property(e => e.Auditdate)
                    .HasColumnName("auditdate")
                    .HasColumnType("date");

                entity.Property(e => e.Auditor)
                    .IsRequired()
                    .HasColumnName("auditor")
                    .HasMaxLength(255);

                entity.Property(e => e.Jobsite)
                    .IsRequired()
                    .HasColumnName("jobsite")
                    .HasMaxLength(255);

                entity.Property(e => e.Superintendent)
                    .IsRequired()
                    .HasColumnName("superintendent")
                    .HasMaxLength(255);
            });
        }
    }
}
