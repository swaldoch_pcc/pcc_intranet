﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Lgrbal
    {
        public Guid Idnum { get; set; }
        public Guid Idref { get; set; }
        public long Recnum { get; set; }
        public long Lgract { get; set; }
        public short Postyr { get; set; }
        public byte Actprd { get; set; }
        public decimal Balnce { get; set; }
        public decimal Budget { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }

        public Lgract IdrefNavigation { get; set; }
        public Lgract LgractNavigation { get; set; }
    }
}
