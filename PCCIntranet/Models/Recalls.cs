﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Recalls
    {
        public Guid Id { get; set; }
        public string Vin { get; set; }
        public string RecallNum { get; set; }
        public string Description { get; set; }
        public DateTime? RecallDate { get; set; }
        public DateTime? RecallReceived { get; set; }
        public DateTime? WorkScheduled { get; set; }
        public DateTime? WorkCompleted { get; set; }
        public string ServiceLocation { get; set; }
        public string Notes { get; set; }
    }
}
