﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Dsptch
    {
        public Guid Idnum { get; set; }
        public byte Colnum { get; set; }
        public string Rectyp { get; set; }
        public long Empnum { get; set; }
        public string Disply { get; set; }
        public string EMail { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }
    }
}
