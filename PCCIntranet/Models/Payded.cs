﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Payded
    {
        public Payded()
        {
            Benfit = new HashSet<Benfit>();
            Empqtd = new HashSet<Empqtd>();
            InverseBsddedNavigation = new HashSet<Payded>();
            Tmcddd = new HashSet<Tmcddd>();
        }

        public Guid Idnum { get; set; }
        public short Recnum { get; set; }
        public string Clcnme { get; set; }
        public decimal Dftrte { get; set; }
        public decimal Dftmax { get; set; }
        public long? Dbtact { get; set; }
        public long? Dbtshp { get; set; }
        public long? Dbtovh { get; set; }
        public long? Dbtadm { get; set; }
        public long? Crdact { get; set; }
        public long? Crdsub { get; set; }
        public string Taxste { get; set; }
        public int? Taxloc { get; set; }
        public byte Taxtyp { get; set; }
        public byte Clctyp { get; set; }
        public byte Clcmth { get; set; }
        public short? Bsdded { get; set; }
        public byte Maxtyp { get; set; }
        public int? Uninum { get; set; }
        public byte Bnftyp { get; set; }
        public byte Ssctax { get; set; }
        public byte Medcre { get; set; }
        public byte Fedtax { get; set; }
        public byte Ftatax { get; set; }
        public byte Stetax { get; set; }
        public byte Steunp { get; set; }
        public byte Stedsb { get; set; }
        public byte Wkrcmp { get; set; }
        public byte Libins { get; set; }
        public byte Loctax { get; set; }
        public byte Benovr { get; set; }
        public byte Dspchk { get; set; }
        public byte Empsub { get; set; }
        public decimal Exmdol { get; set; }
        public decimal Addexm { get; set; }
        public byte Dftact { get; set; }
        public byte Dspinc { get; set; }
        public byte W2Box { get; set; }
        public string W2Cde { get; set; }
        public byte T4Box { get; set; }
        public byte T4skip { get; set; }
        public byte Shrtyp { get; set; }
        public short Aatrix { get; set; }
        public string Ntetxt { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }

        public Payded BsddedNavigation { get; set; }
        public Lgrsub Crd { get; set; }
        public Lgract CrdactNavigation { get; set; }
        public Lgract DbtactNavigation { get; set; }
        public Lgract DbtadmNavigation { get; set; }
        public Lgract DbtovhNavigation { get; set; }
        public Lgract DbtshpNavigation { get; set; }
        public Loctax TaxlocNavigation { get; set; }
        public Payuni UninumNavigation { get; set; }
        public PaydU PaydU { get; set; }
        public ICollection<Benfit> Benfit { get; set; }
        public ICollection<Empqtd> Empqtd { get; set; }
        public ICollection<Payded> InverseBsddedNavigation { get; set; }
        public ICollection<Tmcddd> Tmcddd { get; set; }
    }
}
