﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Pralem
    {
        public Guid Idnum { get; set; }
        public long Recnum { get; set; }
        public string Usrnme { get; set; }
        public string Dscrpt { get; set; }
        public string Messge { get; set; }
        public byte Alttyp { get; set; }
        public DateTime? Altdte { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }
    }
}
