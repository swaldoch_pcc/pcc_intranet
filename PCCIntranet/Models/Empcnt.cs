﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Empcnt
    {
        public Guid Idnum { get; set; }
        public Guid Idref { get; set; }
        public long Recnum { get; set; }
        public int Linnum { get; set; }
        public string Cntnme { get; set; }
        public string Relate { get; set; }
        public string Phnnum { get; set; }
        public string Phnext { get; set; }
        public string EMail { get; set; }
        public string Cllphn { get; set; }
        public string Faxnum { get; set; }
        public string Othphn { get; set; }
        public string Othdsc { get; set; }
        public string Ntetxt { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }

        public Employ IdrefNavigation { get; set; }
        public Employ RecnumNavigation { get; set; }
    }
}
