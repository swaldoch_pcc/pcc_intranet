﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Dshscr
    {
        public Guid Idnum { get; set; }
        public int Dshgrp { get; set; }
        public byte Scrgrp { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }

        public Grplst ScrgrpNavigation { get; set; }
    }
}
