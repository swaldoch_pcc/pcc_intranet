﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Usrlst
    {
        public Guid Idnum { get; set; }
        public string Usrnme { get; set; }
        public string Intsec { get; set; }
        public string Usrpsw { get; set; }
        public byte? Group1 { get; set; }
        public byte? Group2 { get; set; }
        public byte? Group3 { get; set; }
        public byte? Group4 { get; set; }
        public byte? Group5 { get; set; }
        public string Cmpadm { get; set; }
        public string Jobscr { get; set; }
        public string Broken { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }
        public string Pmexac { get; set; }

        public Grplst Group1Navigation { get; set; }
        public Grplst Group2Navigation { get; set; }
        public Grplst Group3Navigation { get; set; }
        public Grplst Group4Navigation { get; set; }
        public Grplst Group5Navigation { get; set; }
    }
}
