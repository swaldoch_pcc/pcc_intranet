﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Dbprps
    {
        public Guid Idnum { get; set; }
        public string Vernum { get; set; }
        public byte Prdedt { get; set; }
        public DateTime? Pepdte { get; set; }
        public byte Isarch { get; set; }
        public byte Oneise { get; set; }
        public long Ctcrec { get; set; }
        public string Dshbrd { get; set; }
        public string Docopy { get; set; }
        public byte Usdate { get; set; }
        public byte Issmpl { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }
    }
}
