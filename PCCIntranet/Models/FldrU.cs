﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class FldrU
    {
        public Guid Idnum { get; set; }

        public Fldrpt IdnumNavigation { get; set; }
    }
}
