﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class DailyLogs
    {
        public string JobsiteId { get; set; }
        public string Jobsite { get; set; }
        public string DailyLogId { get; set; }
        public string LogDate { get; set; }
        public string AddedBy { get; set; }
        public string LogNotes { get; set; }
        public string Tags { get; set; }
        public string WeatherCondition { get; set; }
        public string WeatherMaxMinTemp { get; set; }
        public string WeatherWind { get; set; }
        public string WeatherMaxMinHumidity { get; set; }
        public string WeatherRecordedDate { get; set; }
        public string WeatherEvent { get; set; }
        public string WeatherNotes { get; set; }
        public string RelatedItems { get; set; }
        public string WorkPerformedByPC { get; set; }
        public string WorkPerformedBySubs { get; set; }
        public string WorkPerformed3rdParty { get; set; }
        public string ProblemsDelays { get; set; }
        public string SpecialAssignments { get; set; }
        public string ExtraWork { get; set; }
        public string CompanyEquipment { get; set; }
        public string CompanyEquipmentNotes { get; set; }
        public string EquipmentOnRent { get; set; }
        public string EquipmentOnRentNotes { get; set; }
        public string MaterialsPurchased { get; set; }
        public string ToDos2WeekLookAhead { get; set; }
        public string Superintendent { get; set; }
        public string Masons { get; set; }
        public string Concrete { get; set; }
        public string CarpenterForman { get; set; }
        public string Drywall { get; set; }
        public string Painters { get; set; }
        public string Plumbers { get; set; }
        public string FlooringInstallers { get; set; }
        public string FixtureCrew { get; set; }
        public string Electricians { get; set; }
        public string HvacContractor { get; set; }
        public string SprinklerFireAlarm { get; set; }
        public string SiteWork { get; set; }
        public string Laborers { get; set; }
        public string Landscapers { get; set; }
        public string Specialty { get; set; }
        public string Weather { get; set; }
    }
}
