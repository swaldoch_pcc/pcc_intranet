﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Eqpcst
    {
        public Guid Idnum { get; set; }
        public long Recnum { get; set; }
        public long Eqpnum { get; set; }
        public string Trnnum { get; set; }
        public string Dscrpt { get; set; }
        public DateTime? Trndte { get; set; }
        public DateTime? Entdte { get; set; }
        public byte Actprd { get; set; }
        public byte Srcnum { get; set; }
        public byte Status { get; set; }
        public decimal Cstcde { get; set; }
        public byte Csttyp { get; set; }
        public long? Vndnum { get; set; }
        public long? Opreqp { get; set; }
        public long? Empnum { get; set; }
        public long? Payrec { get; set; }
        public byte Paytyp { get; set; }
        public decimal Csthrs { get; set; }
        public byte Eqptyp { get; set; }
        public decimal Eqpqty { get; set; }
        public decimal Cstamt { get; set; }
        public decimal Grswge { get; set; }
        public byte Eqpunt { get; set; }
        public string Usrnme { get; set; }
        public long Lgrrec { get; set; }
        public string Ntetxt { get; set; }
        public short Postyr { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }

        public Cstcde CstcdeNavigation { get; set; }
        public Csttyp CsttypNavigation { get; set; }
        public Employ EmpnumNavigation { get; set; }
        public Eqpmnt EqpnumNavigation { get; set; }
        public Eqpmnt OpreqpNavigation { get; set; }
        public Payrec PayrecNavigation { get; set; }
        public Source SrcnumNavigation { get; set; }
        public Actpay VndnumNavigation { get; set; }
    }
}
