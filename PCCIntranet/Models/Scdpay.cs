﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Scdpay
    {
        public Guid Idnum { get; set; }
        public Guid Idref { get; set; }
        public long Recnum { get; set; }
        public int Linnum { get; set; }
        public string Paynme { get; set; }
        public decimal Amount { get; set; }
        public decimal Amtpad { get; set; }
        public decimal Balnce { get; set; }
        public decimal Setpay { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }

        public Acpinv IdrefNavigation { get; set; }
        public Acpinv RecnumNavigation { get; set; }
    }
}
