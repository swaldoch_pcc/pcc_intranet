﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Srcnlc
    {
        public Guid Idnum { get; set; }
        public Guid Idref { get; set; }
        public long Recnum { get; set; }
        public int Linnum { get; set; }
        public long? Locnum { get; set; }
        public long Clnnum { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }

        public Srvcnt IdrefNavigation { get; set; }
        public Srvcnt RecnumNavigation { get; set; }
        public Srvloc Srvloc { get; set; }
    }
}
