﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Observations
    {
        public Guid Id { get; set; }
        public Guid Auditid { get; set; }
        public Guid Observationid { get; set; }
        public string Observation { get; set; }
        public string Notes { get; set; }
        public string Status { get; set; }
    }
}
