﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Invser
    {
        public Guid Idnum { get; set; }
        public Guid Idref { get; set; }
        public long Recnum { get; set; }
        public int Locnum { get; set; }
        public string Sernum { get; set; }
        public DateTime? Stkdte { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }

        public Tkfprt IdrefNavigation { get; set; }
        public Invloc LocnumNavigation { get; set; }
        public Tkfprt RecnumNavigation { get; set; }
    }
}
