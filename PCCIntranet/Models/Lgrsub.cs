﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Lgrsub
    {
        public Lgrsub()
        {
            Aiafrm = new HashSet<Aiafrm>();
            Payded = new HashSet<Payded>();
            Pricrd = new HashSet<Pricrd>();
            RcccrdLgrsub = new HashSet<Rcccrd>();
            RcccrdLgrsubNavigation = new HashSet<Rcccrd>();
            SubbalIdrefNavigation = new HashSet<Subbal>();
            SubbalLgrsub = new HashSet<Subbal>();
            Timmat = new HashSet<Timmat>();
            Untbll = new HashSet<Untbll>();
        }

        public Guid Idnum { get; set; }
        public long Recnum { get; set; }
        public string Shtnme { get; set; }
        public string Lngnme { get; set; }
        public long Ctract { get; set; }
        public decimal Begbal { get; set; }
        public decimal Endbal { get; set; }
        public decimal Strbal { get; set; }
        public string Ntetxt { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }
        public byte Inactv { get; set; }

        public Lgract CtractNavigation { get; set; }
        public ICollection<Aiafrm> Aiafrm { get; set; }
        public ICollection<Payded> Payded { get; set; }
        public ICollection<Pricrd> Pricrd { get; set; }
        public ICollection<Rcccrd> RcccrdLgrsub { get; set; }
        public ICollection<Rcccrd> RcccrdLgrsubNavigation { get; set; }
        public ICollection<Subbal> SubbalIdrefNavigation { get; set; }
        public ICollection<Subbal> SubbalLgrsub { get; set; }
        public ICollection<Timmat> Timmat { get; set; }
        public ICollection<Untbll> Untbll { get; set; }
    }
}
