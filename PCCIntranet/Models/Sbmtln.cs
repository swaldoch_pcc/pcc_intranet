﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Sbmtln
    {
        public Guid Idnum { get; set; }
        public Guid Idref { get; set; }
        public long Recnum { get; set; }
        public int Linnum { get; set; }
        public string Itmnum { get; set; }
        public string Dscrpt { get; set; }
        public string Actreq { get; set; }
        public DateTime? Reqdte { get; set; }
        public byte? Sbmsts { get; set; }
        public string Usrdf1 { get; set; }
        public string Ntetxt { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }

        public Submtl IdrefNavigation { get; set; }
        public Submtl RecnumNavigation { get; set; }
        public Sbmsts SbmstsNavigation { get; set; }
    }
}
