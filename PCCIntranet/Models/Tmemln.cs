﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Tmemln
    {
        public Guid Idnum { get; set; }
        public Guid Idref { get; set; }
        public int Recnum { get; set; }
        public int Linnum { get; set; }
        public long? Empnum { get; set; }
        public decimal? Cstcde { get; set; }
        public decimal Rate01 { get; set; }
        public decimal Rate02 { get; set; }
        public decimal Rate03 { get; set; }
        public decimal Minhrs { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }

        public Cstcde CstcdeNavigation { get; set; }
        public Employ EmpnumNavigation { get; set; }
        public Timemp IdrefNavigation { get; set; }
        public Timemp RecnumNavigation { get; set; }
    }
}
