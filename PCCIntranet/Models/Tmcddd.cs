﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Tmcddd
    {
        public Guid Idnum { get; set; }
        public Guid Idref { get; set; }
        public long Recnum { get; set; }
        public short Clcnum { get; set; }
        public decimal Amount { get; set; }
        public string Ovrrid { get; set; }
        public decimal Stewge { get; set; }
        public decimal Stegrs { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }
        public decimal Ytdamt { get; set; }

        public Payded ClcnumNavigation { get; set; }
        public Payrec IdrefNavigation { get; set; }
        public Payrec RecnumNavigation { get; set; }
    }
}
