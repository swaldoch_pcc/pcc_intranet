﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Pclsln
    {
        public Guid Idnum { get; set; }
        public Guid Idref { get; set; }
        public long Recnum { get; set; }
        public int Linnum { get; set; }
        public string Itemnu { get; set; }
        public string Dscrpt { get; set; }
        public string Locion { get; set; }
        public long? Divnum { get; set; }
        public long? Vndnum { get; set; }
        public string Contct { get; set; }
        public DateTime? Dscdte { get; set; }
        public DateTime? Notdte { get; set; }
        public DateTime? Schdte { get; set; }
        public DateTime? Cmpdte { get; set; }
        public DateTime? Appdte { get; set; }
        public decimal Appval { get; set; }
        public long? Billto { get; set; }
        public string Usrdf1 { get; set; }
        public string Ntetxt { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }

        public Actpay BilltoNavigation { get; set; }
        public Cstdiv DivnumNavigation { get; set; }
        public Pchlst IdrefNavigation { get; set; }
        public Pchlst RecnumNavigation { get; set; }
        public Actpay VndnumNavigation { get; set; }
    }
}
