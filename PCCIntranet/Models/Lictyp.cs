﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Lictyp
    {
        public Lictyp()
        {
            Emplic = new HashSet<Emplic>();
        }

        public Guid Idnum { get; set; }
        public short Recnum { get; set; }
        public string Typnme { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }

        public ICollection<Emplic> Emplic { get; set; }
    }
}
