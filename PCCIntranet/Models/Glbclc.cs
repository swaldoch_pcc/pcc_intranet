﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Glbclc
    {
        public Guid Idnum { get; set; }
        public string Clcnme { get; set; }
        public string Clcdsc { get; set; }
        public string Clcexp { get; set; }
        public byte Lckedt { get; set; }
        public byte Omwlck { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }
    }
}
