﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Mnuacs
    {
        public Guid Idnum { get; set; }
        public Guid Idref { get; set; }
        public byte Grpnum { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }

        public Grplst GrpnumNavigation { get; set; }
        public Usrmnu IdrefNavigation { get; set; }
    }
}
