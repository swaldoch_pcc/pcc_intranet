﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class UserGroups
    {
        public string Group { get; set; }
        public int UserId { get; set; }
    }
}
