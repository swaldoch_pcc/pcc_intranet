﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Source
    {
        public Source()
        {
            Eqpcst = new HashSet<Eqpcst>();
            Eqprvw = new HashSet<Eqprvw>();
            Invbal = new HashSet<Invbal>();
            Invhst = new HashSet<Invhst>();
            Invout = new HashSet<Invout>();
            Jobcst = new HashSet<Jobcst>();
            Lgrtrn = new HashSet<Lgrtrn>();
            Prichk = new HashSet<Prichk>();
            Pricrd = new HashSet<Pricrd>();
        }

        public Guid Idnum { get; set; }
        public byte Recnum { get; set; }
        public string Srcnme { get; set; }
        public string Srcdsc { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }

        public ICollection<Eqpcst> Eqpcst { get; set; }
        public ICollection<Eqprvw> Eqprvw { get; set; }
        public ICollection<Invbal> Invbal { get; set; }
        public ICollection<Invhst> Invhst { get; set; }
        public ICollection<Invout> Invout { get; set; }
        public ICollection<Jobcst> Jobcst { get; set; }
        public ICollection<Lgrtrn> Lgrtrn { get; set; }
        public ICollection<Prichk> Prichk { get; set; }
        public ICollection<Pricrd> Pricrd { get; set; }
    }
}
