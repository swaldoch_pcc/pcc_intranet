﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Rctnln
    {
        public Guid Idnum { get; set; }
        public Guid Idref { get; set; }
        public long Recnum { get; set; }
        public int Linnum { get; set; }
        public string Dscrpt { get; set; }
        public long? Lgract { get; set; }
        public long? Lgrsub { get; set; }
        public decimal Dbtamt { get; set; }
        public decimal Crdamt { get; set; }
        public string Usrdf1 { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }

        public Rcctrn IdrefNavigation { get; set; }
        public Lgract LgractNavigation { get; set; }
        public Rcctrn RecnumNavigation { get; set; }
    }
}
