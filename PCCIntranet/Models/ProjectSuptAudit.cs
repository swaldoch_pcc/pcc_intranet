﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class ProjectSuptAudit
    {
        public int Id { get; set; }
        public string Question { get; set; }
        public string AnswerType { get; set; }
        public int? RelatedTo { get; set; }
    }
}
