﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Prplin
    {
        public Guid Idnum { get; set; }
        public Guid Idref { get; set; }
        public long Recnum { get; set; }
        public long Phsnum { get; set; }
        public int Linnum { get; set; }
        public long Itmnum { get; set; }
        public decimal? Cstcde { get; set; }
        public string Dscrpt { get; set; }
        public decimal Matprp { get; set; }
        public decimal Labprp { get; set; }
        public decimal Eqpprp { get; set; }
        public decimal Subprp { get; set; }
        public decimal Othprp { get; set; }
        public decimal Usrcs6 { get; set; }
        public decimal Usrcs7 { get; set; }
        public decimal Usrcs8 { get; set; }
        public decimal Usrcs9 { get; set; }
        public decimal Ttlprp { get; set; }
        public string Usrdf1 { get; set; }
        public string Ntetxt { get; set; }
        public decimal Matorg { get; set; }
        public decimal Laborg { get; set; }
        public decimal Eqporg { get; set; }
        public decimal Suborg { get; set; }
        public decimal Othorg { get; set; }
        public decimal Cs6org { get; set; }
        public decimal Cs7org { get; set; }
        public decimal Cs8org { get; set; }
        public decimal Cs9org { get; set; }
        public decimal Ttlorg { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }

        public Biditm Biditm { get; set; }
        public Cstcde CstcdeNavigation { get; set; }
        public Propsl IdrefNavigation { get; set; }
        public Jobphs Jobphs { get; set; }
        public Propsl RecnumNavigation { get; set; }
    }
}
