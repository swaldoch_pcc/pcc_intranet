﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Budget
    {
        public Budget()
        {
            BdglinIdrefNavigation = new HashSet<Bdglin>();
            BdglinRecnumNavigation = new HashSet<Bdglin>();
        }

        public Guid Idnum { get; set; }
        public long Recnum { get; set; }
        public byte Lckedt { get; set; }
        public DateTime? Edtdte { get; set; }
        public string Usrnme { get; set; }
        public string Ntetxt { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }

        public Actrec RecnumNavigation { get; set; }
        public BudgU BudgU { get; set; }
        public ICollection<Bdglin> BdglinIdrefNavigation { get; set; }
        public ICollection<Bdglin> BdglinRecnumNavigation { get; set; }
    }
}
