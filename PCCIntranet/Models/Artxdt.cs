﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Artxdt
    {
        public Guid Idnum { get; set; }
        public Guid Idref { get; set; }
        public long Recnum { get; set; }
        public int Taxent { get; set; }
        public decimal Sbjtax { get; set; }
        public decimal Taxrte { get; set; }
        public decimal Taxamt { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }

        public Acrinv IdrefNavigation { get; set; }
        public Acrinv RecnumNavigation { get; set; }
        public Taxent TaxentNavigation { get; set; }
    }
}
