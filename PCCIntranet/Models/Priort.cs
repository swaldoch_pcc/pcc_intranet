﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Priort
    {
        public Priort()
        {
            Srvinv = new HashSet<Srvinv>();
            Srvsch = new HashSet<Srvsch>();
        }

        public Guid Idnum { get; set; }
        public byte Recnum { get; set; }
        public string Title { get; set; }
        public string Color { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }

        public ICollection<Srvinv> Srvinv { get; set; }
        public ICollection<Srvsch> Srvsch { get; set; }
    }
}
