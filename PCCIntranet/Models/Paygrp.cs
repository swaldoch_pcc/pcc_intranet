﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Paygrp
    {
        public Paygrp()
        {
            BenfitIdrefNavigation = new HashSet<Benfit>();
            BenfitPaygrpNavigation = new HashSet<Benfit>();
            Dlypyr = new HashSet<Dlypyr>();
            Employ = new HashSet<Employ>();
            Fdrpln = new HashSet<Fdrpln>();
            Jobpgp = new HashSet<Jobpgp>();
            Tmcdln = new HashSet<Tmcdln>();
        }

        public Guid Idnum { get; set; }
        public int Recnum { get; set; }
        public string Grpnme { get; set; }
        public string Wrkcls { get; set; }
        public byte Clslvl { get; set; }
        public decimal Clsprc { get; set; }
        public string Clscde { get; set; }
        public string Ovrrde { get; set; }
        public decimal Payrt1 { get; set; }
        public decimal Payrt2 { get; set; }
        public decimal Payrt3 { get; set; }
        public decimal Pcerte { get; set; }
        public int? Uninum { get; set; }
        public string Ntetxt { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }
        public string Inactv { get; set; }

        public Payuni UninumNavigation { get; set; }
        public ICollection<Benfit> BenfitIdrefNavigation { get; set; }
        public ICollection<Benfit> BenfitPaygrpNavigation { get; set; }
        public ICollection<Dlypyr> Dlypyr { get; set; }
        public ICollection<Employ> Employ { get; set; }
        public ICollection<Fdrpln> Fdrpln { get; set; }
        public ICollection<Jobpgp> Jobpgp { get; set; }
        public ICollection<Tmcdln> Tmcdln { get; set; }
    }
}
