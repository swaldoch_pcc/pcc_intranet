﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Jobtyp
    {
        public Jobtyp()
        {
            Actrec = new HashSet<Actrec>();
        }

        public Guid Idnum { get; set; }
        public short Recnum { get; set; }
        public string Typnme { get; set; }
        public string Ntetxt { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }

        public ICollection<Actrec> Actrec { get; set; }
    }
}
