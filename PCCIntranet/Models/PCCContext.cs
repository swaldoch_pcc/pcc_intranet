using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using PCCIntranet.Models;

namespace PCCIntranet.Models
{
    public partial class PCCContext : DbContext
    {
        public PCCContext()
        {
        }

        public PCCContext(DbContextOptions<PCCContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Baseline> Baseline { get; set; }
        public virtual DbSet<ChangeOrderLineItems> ChangeOrderLineItems { get; set; }
        public virtual DbSet<ChangeOrders> ChangeOrders { get; set; }
        public virtual DbSet<DailyLogs> DailyLogs { get; set; }
        public virtual DbSet<Employees> Employees { get; set; }
        public virtual DbSet<Jobsites> Jobsites { get; set; }
        public virtual DbSet<LeadActivities> LeadActivities { get; set; }
        public virtual DbSet<LeadProposals> LeadProposals { get; set; }
        public virtual DbSet<Leads> Leads { get; set; }
        public virtual DbSet<PmprojectReview> PmprojectReview { get; set; }
        public virtual DbSet<ProjectPmfeedback> ProjectPmfeedback { get; set; }
        public virtual DbSet<ProjectSuptAudit> ProjectSuptAudit { get; set; }
        public virtual DbSet<Safetyaudit> Safetyaudit { get; set; }
        public virtual DbSet<Schedules> Schedules { get; set; }
        public virtual DbSet<SiteAudit> SiteAudit { get; set; }
        public virtual DbSet<Subsassistant> SubsAssistant { get; set; }
        public virtual DbSet<Subscontact> Subscontact { get; set; }
        public virtual DbSet<Subs> SubsMaster { get; set; }
        public virtual DbSet<SubsTrades> Substrades { get; set; }
        public virtual DbSet<SuptProjectReview> SuptProjectReview { get; set; }
        public virtual DbSet<ToDos> ToDos { get; set; }
        public virtual DbSet<WorkDays> WorkDays { get; set; }
        public virtual DbSet<Users> Users { get; set; }
        public virtual DbSet<UserGroups> UserGroups { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Baseline>(entity =>
            {
                entity.HasKey(e => new { e.JobsiteId, e.ScheduleId });

                entity.ToTable("Baseline", "BuilderTrend");

                entity.Property(e => e.JobsiteId)
                    .HasColumnName("JobsiteID")
                    .HasMaxLength(255);

                entity.Property(e => e.ScheduleId)
                    .HasColumnName("ScheduleID")
                    .HasMaxLength(255);

                entity.Property(e => e.ActualDurationD)
                    .HasColumnName("Actual Duration (d)")
                    .HasMaxLength(255);

                entity.Property(e => e.AssignedTo)
                    .HasColumnName("Assigned To")
                    .HasMaxLength(255);

                entity.Property(e => e.BaseDurationD)
                    .HasColumnName("Base Duration (d)")
                    .HasMaxLength(255);

                entity.Property(e => e.BaseEndDate)
                    .HasColumnName("(Base) End Date")
                    .HasMaxLength(255);

                entity.Property(e => e.BaseStartDate)
                    .HasColumnName("(Base) Start Date")
                    .HasMaxLength(255);

                entity.Property(e => e.DirectShifts)
                    .HasColumnName("Direct Shifts")
                    .HasMaxLength(255);

                entity.Property(e => e.DurationChange)
                    .HasColumnName("Duration Change")
                    .HasMaxLength(255);

                entity.Property(e => e.Jobsite).HasMaxLength(255);

                entity.Property(e => e.OverallSlip)
                    .HasColumnName("Overall Slip")
                    .HasMaxLength(255);

                entity.Property(e => e.Reasons).HasMaxLength(255);

                entity.Property(e => e.ShiftNotes)
                    .HasColumnName("Shift Notes")
                    .HasMaxLength(255);

                entity.Property(e => e.Title).HasMaxLength(255);
            });

            modelBuilder.Entity<ChangeOrderLineItems>(entity =>
            {
                entity.HasKey(e => new { e.LineItemId, e.ChangeOrderId });

                entity.ToTable("Change Order Line Items", "BuilderTrend");

                entity.Property(e => e.LineItemId)
                    .HasColumnName("Line Item Id")
                    .HasMaxLength(255);

                entity.Property(e => e.ChangeOrderId)
                    .HasColumnName("Change Order Id")
                    .HasMaxLength(255);

                entity.Property(e => e.CostCode)
                    .HasColumnName("Cost Code")
                    .HasMaxLength(255);

                entity.Property(e => e.Description).HasMaxLength(255);

                entity.Property(e => e.InternalNotes)
                    .HasColumnName("Internal Notes")
                    .HasMaxLength(255);

                entity.Property(e => e.MarkupAmount)
                    .HasColumnName("Markup Amount")
                    .HasMaxLength(255);

                entity.Property(e => e.MarkupPerUnit)
                    .HasColumnName("Markup Per Unit")
                    .HasMaxLength(255);

                entity.Property(e => e.MarkupPercentage)
                    .HasColumnName("Markup Percentage")
                    .HasMaxLength(255);

                entity.Property(e => e.OwnerPrice)
                    .HasColumnName("Owner Price")
                    .HasMaxLength(255);

                entity.Property(e => e.Quantity)
                    .HasColumnName("Quantity ")
                    .HasMaxLength(255);

                entity.Property(e => e.Unit).HasMaxLength(255);

                entity.Property(e => e.UnitCost)
                    .HasColumnName("Unit Cost")
                    .HasMaxLength(255);
            });

            modelBuilder.Entity<ChangeOrders>(entity =>
            {
                entity.HasKey(e => new { e.JobsiteId, e.ChangeOrderId, e.CustChangeOrderId });

                entity.ToTable("Change Orders", "BuilderTrend");

                entity.Property(e => e.JobsiteId)
                    .HasColumnName("JobsiteID")
                    .HasMaxLength(255);

                entity.Property(e => e.ChangeOrderId)
                    .HasColumnName("ChangeOrderID")
                    .HasMaxLength(255);

                entity.Property(e => e.CustChangeOrderId)
                    .HasColumnName("Cust. Change Order ID")
                    .HasMaxLength(255);

                entity.Property(e => e.ApprovalStatus)
                    .HasColumnName("Approval Status")
                    .HasMaxLength(255);

                entity.Property(e => e.BalanceDue)
                    .HasColumnName("Balance Due")
                    .HasMaxLength(255);

                entity.Property(e => e.BuilderCost)
                    .HasColumnName("Builder Cost")
                    .HasMaxLength(255);

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("Created By")
                    .HasMaxLength(255);

                entity.Property(e => e.DateEntered)
                    .HasColumnName("Date Entered")
                    .HasMaxLength(255);

                entity.Property(e => e.DatePaid)
                    .HasColumnName("Date Paid")
                    .HasMaxLength(255);

                entity.Property(e => e.Deadline).HasMaxLength(255);

                entity.Property(e => e.GrossProfit)
                    .HasColumnName("Gross Profit")
                    .HasMaxLength(255);

                entity.Property(e => e.Jobsite).HasMaxLength(255);

                entity.Property(e => e.Owner).HasMaxLength(255);

                entity.Property(e => e.OwnerLastViewed)
                    .HasColumnName("Owner Last Viewed")
                    .HasMaxLength(255);

                entity.Property(e => e.OwnerPaid)
                    .HasColumnName("Owner Paid")
                    .HasMaxLength(255);

                entity.Property(e => e.OwnerPrice)
                    .HasColumnName("Owner Price")
                    .HasMaxLength(255);

                entity.Property(e => e.PaymentDeadlineDate)
                    .HasColumnName("Payment Deadline Date")
                    .HasMaxLength(255);

                entity.Property(e => e.PaymentStatus)
                    .HasColumnName("Payment Status")
                    .HasMaxLength(255);

                entity.Property(e => e.PoBuilderVariance)
                    .HasColumnName("PO Builder Variance")
                    .HasMaxLength(255);

                entity.Property(e => e.PoCustomerVariance)
                    .HasColumnName("PO Customer Variance")
                    .HasMaxLength(255);

                entity.Property(e => e.RelatedPoIds)
                    .HasColumnName("Related PO IDs")
                    .HasMaxLength(255);

                entity.Property(e => e.SubsList)
                    .HasColumnName("Subs List")
                    .HasMaxLength(255);

                entity.Property(e => e.Title).HasMaxLength(255);
            });

            modelBuilder.Entity<DailyLogs>(entity =>
            {
                entity.HasKey(e => new { e.JobsiteId, e.DailyLogId });

                entity.ToTable("Daily Logs", "BuilderTrend");

                entity.Property(e => e.JobsiteId)
                    .HasColumnName("JobsiteID")
                    .HasMaxLength(255);

                entity.Property(e => e.DailyLogId)
                    .HasColumnName("DailyLogID")
                    .HasMaxLength(255);

                entity.Property(e => e.AddedBy)
                    .HasColumnName("Added By")
                    .HasMaxLength(255);

                entity.Property(e => e.CarpenterForman).HasColumnName("Carpenter/Forman*");

                entity.Property(e => e.CompanyEquipment).HasColumnName("Company Equipment*");

                entity.Property(e => e.CompanyEquipmentNotes).HasColumnName("Company Equipment Notes*");

                entity.Property(e => e.Concrete).HasColumnName("Concrete*");

                entity.Property(e => e.Drywall).HasColumnName("Drywall*");

                entity.Property(e => e.Electricians).HasColumnName("Electricians*");

                entity.Property(e => e.EquipmentOnRent).HasColumnName("Equipment On Rent*");

                entity.Property(e => e.EquipmentOnRentNotes).HasColumnName("Equipment on Rent Notes*");

                entity.Property(e => e.ExtraWork).HasColumnName("Extra Work*");

                entity.Property(e => e.FixtureCrew).HasColumnName("Fixture Crew*");

                entity.Property(e => e.FlooringInstallers).HasColumnName("Flooring Installers*");

                entity.Property(e => e.HvacContractor).HasColumnName("HVAC Contractor*");

                entity.Property(e => e.Jobsite).HasMaxLength(255);

                entity.Property(e => e.Laborers).HasColumnName("Laborers*");

                entity.Property(e => e.Landscapers).HasColumnName("Landscapers*");

                entity.Property(e => e.LogDate).HasMaxLength(255);

                entity.Property(e => e.LogNotes).HasColumnName("Log Notes");

                entity.Property(e => e.Masons).HasColumnName("Masons*");

                entity.Property(e => e.MaterialsPurchased).HasColumnName("Materials Purchased*");

                entity.Property(e => e.Painters).HasColumnName("Painters*");

                entity.Property(e => e.Plumbers).HasColumnName("Plumbers*");

                entity.Property(e => e.ProblemsDelays).HasColumnName("Problems - Delays*");

                entity.Property(e => e.RelatedItems)
                    .HasColumnName("Related Items")
                    .HasMaxLength(255);

                entity.Property(e => e.SiteWork).HasColumnName("Site Work*");

                entity.Property(e => e.SpecialAssignments).HasColumnName("Special Assignments*");

                entity.Property(e => e.Specialty).HasColumnName("Specialty*");

                entity.Property(e => e.SprinklerFireAlarm).HasColumnName("Sprinkler/Fire Alarm*");

                entity.Property(e => e.Superintendent).HasColumnName("Superintendent*");

                entity.Property(e => e.Tags).HasMaxLength(255);

                entity.Property(e => e.ToDos2WeekLookAhead).HasColumnName("To-Dos/2 Week Look Ahead*");

                entity.Property(e => e.Weather).HasColumnName("Weather*");

                entity.Property(e => e.WeatherCondition)
                    .HasColumnName("Weather: Condition")
                    .HasMaxLength(255);

                entity.Property(e => e.WeatherEvent)
                    .HasColumnName("Weather Event")
                    .HasMaxLength(255);

                entity.Property(e => e.WeatherMaxMinHumidity)
                    .HasColumnName("Weather: Max/Min Humidity")
                    .HasMaxLength(255);

                entity.Property(e => e.WeatherMaxMinTemp)
                    .HasColumnName("Weather: Max/Min Temp")
                    .HasMaxLength(255);

                entity.Property(e => e.WeatherNotes).HasColumnName("Weather Notes");

                entity.Property(e => e.WeatherRecordedDate)
                    .HasColumnName("Weather Recorded Date")
                    .HasMaxLength(255);

                entity.Property(e => e.WeatherWind)
                    .HasColumnName("Weather: Wind")
                    .HasMaxLength(255);

                entity.Property(e => e.WorkPerformed3rdParty).HasColumnName("Work Performed 3rd Party*");

                entity.Property(e => e.WorkPerformedByPC).HasColumnName("Work Performed by P&C*");

                entity.Property(e => e.WorkPerformedBySubs).HasColumnName("Work Performed by Subs*");
            });

            modelBuilder.Entity<Employees>(entity =>
            {
                entity.ToTable("Employees", "HR");

                entity.Property(e => e.Id)
                    .HasColumnName("ID")
                    .ValueGeneratedNever();

                entity.Property(e => e.Fname)
                    .HasColumnName("FName")
                    .HasMaxLength(50);

                entity.Property(e => e.HireDate).HasColumnType("date");

                entity.Property(e => e.Lname)
                    .HasColumnName("LName")
                    .HasMaxLength(25);

                entity.Property(e => e.Title).HasMaxLength(50);
            });

            modelBuilder.Entity<Jobsites>(entity =>
            {
                entity.HasKey(e => e.JobsiteId);

                entity.ToTable("Jobsites", "BuilderTrend");

                entity.Property(e => e.JobsiteId)
                    .HasColumnName("JobsiteID")
                    .ValueGeneratedNever();

                entity.Property(e => e.AchLimit)
                    .HasColumnName("ACH Limit")
                    .HasMaxLength(255);

                entity.Property(e => e.ActualCompletion)
                    .HasColumnName("Actual Completion")
                    .HasMaxLength(255);

                entity.Property(e => e.ActualDurationWorkDays)
                    .HasColumnName("Actual Duration (Work days)")
                    .HasMaxLength(255);

                entity.Property(e => e.ActualStart)
                    .HasColumnName("Actual Start")
                    .HasMaxLength(255);

                entity.Property(e => e.BaselineDurationWorkDays)
                    .HasColumnName("Baseline Duration (Work days)")
                    .HasMaxLength(255);

                entity.Property(e => e.CalendarStatus)
                    .HasColumnName("Calendar Status")
                    .HasMaxLength(255);

                entity.Property(e => e.CcLimit)
                    .HasColumnName("CC Limit")
                    .HasMaxLength(255);

                entity.Property(e => e.Cell).HasMaxLength(255);

                entity.Property(e => e.ChangeOrderNotes).HasColumnName("Change Order Notes*");

                entity.Property(e => e.City).HasMaxLength(255);

                entity.Property(e => e.ContractPrice)
                    .HasColumnName("Contract Price")
                    .HasColumnType("money");

                entity.Property(e => e.Created).HasMaxLength(255);

                entity.Property(e => e.Email).HasMaxLength(255);

                entity.Property(e => e.EstimateStatus)
                    .HasColumnName("Estimate Status")
                    .HasMaxLength(255);

                entity.Property(e => e.GeneralNotes).HasColumnName("General Notes*");

                entity.Property(e => e.IndividualPoLimit)
                    .HasColumnName("Individual PO Limit")
                    .HasMaxLength(255);

                entity.Property(e => e.JobGroups)
                    .HasColumnName("Job Groups")
                    .HasMaxLength(255);

                entity.Property(e => e.JobPrefix)
                    .HasColumnName("Job Prefix")
                    .HasMaxLength(255);

                entity.Property(e => e.JobRunningTotal)
                    .HasColumnName("Job Running Total")
                    .HasMaxLength(255);

                entity.Property(e => e.JobType)
                    .HasColumnName("Job Type")
                    .HasMaxLength(255);

                entity.Property(e => e.License).HasColumnName("License -*");

                entity.Property(e => e.LicenseCityCountyExp).HasColumnName("License - City/County Exp*");

                entity.Property(e => e.LicensePhone).HasColumnName("License - Phone*");

                entity.Property(e => e.LicenseRequirements).HasColumnName("License- Requirements*");

                entity.Property(e => e.LicenseStateExpires).HasColumnName("License - State Expires*");

                entity.Property(e => e.LicenseSubs).HasColumnName("License - Subs*");

                entity.Property(e => e.LicensesContact).HasColumnName("Licenses - Contact*");

                entity.Property(e => e.LicensesFiledTo).HasColumnName("Licenses - Filed To*");

                entity.Property(e => e.LicensesNotes).HasColumnName("Licenses - Notes*");

                entity.Property(e => e.LicensingContact).HasColumnName("Licensing - Contact*");

                entity.Property(e => e.LicensingNeed).HasColumnName("Licensing - Need*");

                entity.Property(e => e.LicensingNotes).HasColumnName("Licensing - Notes*");

                entity.Property(e => e.LicensingPhone).HasColumnName("Licensing - Phone*");

                entity.Property(e => e.LicensingRequirements).HasColumnName("Licensing - Requirements*");

                entity.Property(e => e.Lot).HasMaxLength(255);

                entity.Property(e => e.OverallPoLimit)
                    .HasColumnName("Overall PO Limit")
                    .HasMaxLength(255);

                entity.Property(e => e.Owner).HasMaxLength(255);

                entity.Property(e => e.OwnerActivationStatus)
                    .HasColumnName("Owner Activation Status")
                    .HasMaxLength(255);

                entity.Property(e => e.OwnerBalance)
                    .HasColumnName("Owner Balance")
                    .HasMaxLength(255);

                entity.Property(e => e.OwnerLastViewed)
                    .HasColumnName("Owner Last Viewed")
                    .HasMaxLength(255);

                entity.Property(e => e.PCPersonnel).HasColumnName("P&C personnel*");

                entity.Property(e => e.PaymentsReceived)
                    .HasColumnName("Payments Received")
                    .HasMaxLength(255);

                entity.Property(e => e.Permit).HasMaxLength(255);

                entity.Property(e => e.PermitInfoContact).HasColumnName("Permit Info - Contact*");

                entity.Property(e => e.PermitInfoNotes).HasColumnName("Permit Info - Notes*");

                entity.Property(e => e.PermitInfoPhone).HasColumnName("Permit Info - Phone*");

                entity.Property(e => e.PermitInfoTapFee).HasColumnName("Permit Info - Tap Fee*");

                entity.Property(e => e.PermitInfoWater).HasColumnName("Permit Info - Water*");

                entity.Property(e => e.PermitIntoSewer).HasColumnName("Permit Into - Sewer*");

                entity.Property(e => e.PermittingContact).HasColumnName("Permitting - Contact*");

                entity.Property(e => e.PermittingNotes).HasColumnName("Permitting - Notes*");

                entity.Property(e => e.PermittingPhone).HasColumnName("Permitting - Phone*");

                entity.Property(e => e.PermittingRequirements).HasColumnName("Permitting - Requirements*");

                entity.Property(e => e.Phone).HasMaxLength(255);

                entity.Property(e => e.ProjectManager).HasColumnName("Project Manager");

                entity.Property(e => e.ProjectedCompletion)
                    .HasColumnName("Projected Completion")
                    .HasMaxLength(255);

                entity.Property(e => e.ProjectedStart)
                    .HasColumnName("Projected Start")
                    .HasMaxLength(255);

                entity.Property(e => e.State).HasMaxLength(255);

                entity.Property(e => e.Status).HasMaxLength(255);

                entity.Property(e => e.Street).HasMaxLength(255);

                entity.Property(e => e.SuperintendentFullName).HasColumnName("Superintendent (Full Name)*");

                entity.Property(e => e.TotalCostBillPoCost)
                    .HasColumnName("Total Cost (Bill/PO Cost)")
                    .HasMaxLength(255);

                entity.Property(e => e.TotalPaidBillPoPaid)
                    .HasColumnName("Total Paid (Bill/PO Paid)")
                    .HasMaxLength(255);

                entity.Property(e => e.TotalRemainingBillPoRemaining)
                    .HasColumnName("Total Remaining (Bill/PO Remaining)")
                    .HasMaxLength(255);

                entity.Property(e => e.Warranty).HasMaxLength(255);

                entity.Property(e => e.WorkingTemplate)
                    .HasColumnName("Working Template")
                    .HasMaxLength(255);

                entity.Property(e => e.Zip).HasMaxLength(255);
            });

            modelBuilder.Entity<LeadActivities>(entity =>
            {
                entity.HasKey(e => e.ActivityId);

                entity.ToTable("Lead Activities", "BuilderTrend");

                entity.Property(e => e.ActivityId)
                    .HasColumnName("ActivityID")
                    .ValueGeneratedNever();

                entity.Property(e => e.ActivityNotes).HasColumnName("Activity Notes");

                entity.Property(e => e.ContactDate)
                    .HasColumnName("Contact Date")
                    .HasColumnType("date");

                entity.Property(e => e.Employee).HasMaxLength(255);

                entity.Property(e => e.LastContacted)
                    .HasColumnName("Last Contacted")
                    .HasMaxLength(255);

                entity.Property(e => e.LeadId).HasColumnName("LeadID");

                entity.Property(e => e.OpportunityTitle)
                    .HasColumnName("Opportunity Title")
                    .HasMaxLength(255);

                entity.Property(e => e.Type).HasMaxLength(255);
            });

            modelBuilder.Entity<LeadProposals>(entity =>
            {
                entity.HasKey(e => new { e.LeadId, e.ProposalId });

                entity.ToTable("Lead Proposals", "BuilderTrend");

                entity.Property(e => e.LeadId)
                    .HasColumnName("LeadID")
                    .HasMaxLength(255);

                entity.Property(e => e.ProposalId)
                    .HasColumnName("ProposalID")
                    .HasMaxLength(255);

                entity.Property(e => e.ClosingText).HasColumnName("Closing Text");

                entity.Property(e => e.CustomerContact)
                    .HasColumnName("Customer Contact")
                    .HasMaxLength(255);

                entity.Property(e => e.InternalNotes).HasColumnName("Internal Notes");

                entity.Property(e => e.IntroductoryText).HasColumnName("Introductory Text");

                entity.Property(e => e.LastViewed)
                    .HasColumnName("Last Viewed")
                    .HasMaxLength(255);

                entity.Property(e => e.OpportunityTitle).HasColumnName("Opportunity Title");

                entity.Property(e => e.OwnerPrice)
                    .HasColumnName("Owner Price")
                    .HasMaxLength(255);

                entity.Property(e => e.PaymentStatus)
                    .HasColumnName("Payment Status")
                    .HasMaxLength(255);

                entity.Property(e => e.ProposalStatus)
                    .HasColumnName("Proposal Status")
                    .HasMaxLength(255);

                entity.Property(e => e.ProposalTitle).HasColumnName("Proposal Title");

                entity.Property(e => e.RequiredPayment)
                    .HasColumnName("Required Payment")
                    .HasMaxLength(255);

                entity.Property(e => e.Status).HasMaxLength(255);

                entity.Property(e => e.Updated).HasMaxLength(255);
            });

      modelBuilder.Entity<Leads>(entity =>
      {
        entity.HasKey(e => e.LeadId);

        entity.ToTable("Leads", "BuilderTrend");

        entity.Property(e => e.LeadId)
            .HasColumnName("LeadID")
            .HasMaxLength(255)
            .ValueGeneratedOnAdd();

        entity.Property(e => e.Age).HasMaxLength(255);

        entity.Property(e => e.AnticipatedConstStart)
            .HasColumnName("Anticipated Const Start* ")
            .HasColumnType("date");

        entity.Property(e => e.BuilderId)
            .HasColumnName("builderID")
            .HasMaxLength(255);

        entity.Property(e => e.CellPhoneNumber)
            .HasColumnName("Cell Phone Number")
            .HasMaxLength(255);

        entity.Property(e => e.Company).HasColumnName("Company*");

        entity.Property(e => e.Confidence).HasMaxLength(255);

        entity.Property(e => e.CreatedDate)
            .HasColumnName("Created Date")
            .HasColumnType("date");

        entity.Property(e => e.CustomerContact)
            .HasColumnName("Customer Contact")
            .HasMaxLength(255);

        entity.Property(e => e.Difference)
            .HasColumnName("Difference*")
            .HasColumnType("money");

        entity.Property(e => e.EmailAddresses)
            .HasColumnName("Email Addresses")
            .HasMaxLength(255);

        entity.Property(e => e.EstRevenueMax)
            .HasColumnName("Est. Revenue Max")
            .HasColumnType("money");

        entity.Property(e => e.EstRevenueMin)
            .HasColumnName("Est. Revenue Min")
            .HasColumnType("money");

        entity.Property(e => e.LastContacted)
            .HasColumnName("Last Contacted")
            .HasMaxLength(255);

        entity.Property(e => e.LeadNotes).HasColumnName("Lead Notes");

        entity.Property(e => e.OpportunityTitle).HasColumnName("Opportunity Title");

        entity.Property(e => e.PCBidDate)
            .HasColumnName("P&C Bid Date* ")
            .HasColumnType("date");

        entity.Property(e => e.PCBidTime).HasColumnName("P&C Bid Time*");

        entity.Property(e => e.PhoneNumber)
            .HasColumnName("Phone Number")
            .HasMaxLength(255);

        entity.Property(e => e.Prebidconference).HasColumnType("date");

        entity.Property(e => e.ProjectDuration).HasColumnName("Project Duration*");

        entity.Property(e => e.ProjectType)
            .HasColumnName("Project Type")
            .HasMaxLength(255);

        entity.Property(e => e.ProjectedSalesDate)
            .HasColumnName("Projected Sales Date")
            .HasColumnType("date");

        entity.Property(e => e.RelatedJob)
            .HasColumnName("Related Job")
            .HasMaxLength(255);

        entity.Property(e => e.Results)
            .HasColumnName("Results*")
            .HasMaxLength(255);

        entity.Property(e => e.SalesPhase).HasColumnName("Sales Phase*");

        entity.Property(e => e.SoldDate)
            .HasColumnName("Sold Date")
            .HasColumnType("date");

        entity.Property(e => e.Status).HasMaxLength(255);

        entity.Property(e => e.SubBidsDue)
            .HasColumnName("Sub Bids Due* ")
            .HasColumnType("date");

        entity.Property(e => e.SubBidsTime).HasColumnName("Sub Bids Time*");

        entity.Property(e => e.SuccessfulBidder)
            .HasColumnName("Successful Bidder*")
            .HasMaxLength(255);

        entity.Property(e => e.Tags).HasMaxLength(255);

        entity.Property(e => e.TradesNeeded).HasColumnName("Trades Needed*");
      });

      modelBuilder.Entity<PmprojectReview>(entity =>
                  {
                      entity.ToTable("PMProjectReview", "HR");

                      entity.Property(e => e.Id)
                          .HasColumnName("ID")
                          .ValueGeneratedOnAdd();

                      entity.Property(e => e.ReviewDate).HasDefaultValueSql("(getdate())");
                  });

            modelBuilder.Entity<ProjectPmfeedback>(entity =>
            {
                entity.ToTable("ProjectPMFeedback", "HR");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.AnswerType).HasMaxLength(255);
            });

            modelBuilder.Entity<ProjectSuptAudit>(entity =>
            {
                entity.ToTable("ProjectSuptAudit", "HR");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.AnswerType).HasMaxLength(255);
            });
            
            modelBuilder.Entity<Safetyaudit>(entity =>
            {
              entity.ToTable("safetyaudit", "HR");

              entity.Property(e => e.Id)
              .HasColumnName("ID")
              .ValueGeneratedOnAdd();

              entity.Property(e => e.Auditdate)
                  .HasColumnName("auditdate")
                  .HasDefaultValueSql("(getdate())");

              entity.Property(e => e.Auditor)
                  .HasColumnName("auditor")
                  .HasMaxLength(50);

              entity.Property(e => e.Projectmanager).HasMaxLength(50);

              entity.Property(e => e.Projectname).HasMaxLength(255);

              entity.Property(e => e.Q1).HasMaxLength(10);

              entity.Property(e => e.Q10).HasMaxLength(10);

              entity.Property(e => e.Q11).HasMaxLength(10);

              entity.Property(e => e.Q12).HasMaxLength(10);

              entity.Property(e => e.Q13).HasMaxLength(10);

              entity.Property(e => e.Q14).HasMaxLength(10);

              entity.Property(e => e.Q15).HasMaxLength(10);

              entity.Property(e => e.Q16).HasMaxLength(10);

              entity.Property(e => e.Q17).HasMaxLength(10);

              entity.Property(e => e.Q18).HasMaxLength(10);

              entity.Property(e => e.Q19).HasMaxLength(10);

              entity.Property(e => e.Q2).HasMaxLength(10);

              entity.Property(e => e.Q20).HasMaxLength(10);

              entity.Property(e => e.Q21).HasMaxLength(10);

              entity.Property(e => e.Q22).HasMaxLength(10);

              entity.Property(e => e.Q23).HasMaxLength(10);

              entity.Property(e => e.Q24).HasMaxLength(10);

              entity.Property(e => e.Q25).HasMaxLength(10);

              entity.Property(e => e.Q26).HasMaxLength(10);

              entity.Property(e => e.Q27).HasMaxLength(10);

              entity.Property(e => e.Q28).HasMaxLength(10);

              entity.Property(e => e.Q29).HasMaxLength(10);

              entity.Property(e => e.Q3).HasMaxLength(10);

              entity.Property(e => e.Q30).HasMaxLength(10);

              entity.Property(e => e.Q31).HasMaxLength(10);

              entity.Property(e => e.Q32).HasMaxLength(10);

              entity.Property(e => e.Q33).HasMaxLength(10);

              entity.Property(e => e.Q34).HasMaxLength(10);

              entity.Property(e => e.Q35).HasMaxLength(10);

              entity.Property(e => e.Q36).HasMaxLength(10);

              entity.Property(e => e.Q37).HasMaxLength(10);

              entity.Property(e => e.Q38).HasMaxLength(10);

              entity.Property(e => e.Q39).HasMaxLength(10);

              entity.Property(e => e.Q4).HasMaxLength(10);

              entity.Property(e => e.Q40).HasMaxLength(10);

              entity.Property(e => e.Q41).HasMaxLength(10);

              entity.Property(e => e.Q42).HasMaxLength(10);

              entity.Property(e => e.Q43).HasMaxLength(10);

              entity.Property(e => e.Q44).HasMaxLength(10);

              entity.Property(e => e.Q45).HasMaxLength(10);

              entity.Property(e => e.Q46).HasMaxLength(10);

              entity.Property(e => e.Q47).HasMaxLength(10);

              entity.Property(e => e.Q48).HasMaxLength(10);

              entity.Property(e => e.Q49).HasMaxLength(10);

              entity.Property(e => e.Q5).HasMaxLength(10);

              entity.Property(e => e.Q50).HasMaxLength(10);

              entity.Property(e => e.Q51).HasMaxLength(10);

              entity.Property(e => e.Q52).HasMaxLength(10);

              entity.Property(e => e.Q53).HasMaxLength(10);

              entity.Property(e => e.Q54).HasMaxLength(10);

              entity.Property(e => e.Q55).HasMaxLength(10);

              entity.Property(e => e.Q56).HasMaxLength(10);

              entity.Property(e => e.Q57).HasMaxLength(10);

              entity.Property(e => e.Q58).HasMaxLength(10);

              entity.Property(e => e.Q59).HasMaxLength(10);

              entity.Property(e => e.Q6).HasMaxLength(10);

              entity.Property(e => e.Q60).HasMaxLength(10);

              entity.Property(e => e.Q61).HasMaxLength(10);

              entity.Property(e => e.Q62).HasMaxLength(10);

              entity.Property(e => e.Q63).HasMaxLength(10);

              entity.Property(e => e.Q64).HasMaxLength(10);

              entity.Property(e => e.Q65).HasMaxLength(10);

              entity.Property(e => e.Q66).HasMaxLength(10);

              entity.Property(e => e.Q67).HasMaxLength(10);

              entity.Property(e => e.Q68).HasMaxLength(10);

              entity.Property(e => e.Q69).HasMaxLength(10);

              entity.Property(e => e.Q7).HasMaxLength(10);

              entity.Property(e => e.Q70).HasMaxLength(10);

              entity.Property(e => e.Q71).HasMaxLength(10);

              entity.Property(e => e.Q72).HasMaxLength(10);

              entity.Property(e => e.Q73).HasMaxLength(10);

              entity.Property(e => e.Q74).HasMaxLength(10);

              entity.Property(e => e.Q75).HasMaxLength(10);

              entity.Property(e => e.Q76).HasMaxLength(10);

              entity.Property(e => e.Q77).HasMaxLength(10);

              entity.Property(e => e.Q78).HasMaxLength(10);

              entity.Property(e => e.Q79).HasMaxLength(10);

              entity.Property(e => e.Q8).HasMaxLength(10);

              entity.Property(e => e.Q80).HasMaxLength(10);

              entity.Property(e => e.Q81).HasMaxLength(10);

              entity.Property(e => e.Q82).HasMaxLength(10);

              entity.Property(e => e.Q83).HasMaxLength(10);

              entity.Property(e => e.Q84).HasMaxLength(10);

              entity.Property(e => e.Q85).HasMaxLength(10);

              entity.Property(e => e.Q86).HasMaxLength(10);

              entity.Property(e => e.Q87).HasMaxLength(10);

              entity.Property(e => e.Q88).HasMaxLength(10);

              entity.Property(e => e.Q89).HasMaxLength(10);

              entity.Property(e => e.Q9).HasMaxLength(10);

              entity.Property(e => e.Q90).HasMaxLength(10);

              entity.Property(e => e.Q91).HasMaxLength(10);

              entity.Property(e => e.Q92).HasMaxLength(10);

              entity.Property(e => e.Q93).IsUnicode(false);

              entity.Property(e => e.Q94).HasMaxLength(255);

              entity.Property(e => e.Q95).HasMaxLength(255);

              entity.Property(e => e.Q96).HasMaxLength(255);

              entity.Property(e => e.Q97).HasMaxLength(255);

              entity.Property(e => e.Superintendent).HasMaxLength(50);
            });

            modelBuilder.Entity<Schedules>(entity =>
            {
                entity.HasKey(e => new { e.JobsiteId, e.ScheduleId });

                entity.ToTable("Schedules", "BuilderTrend");

                entity.Property(e => e.JobsiteId)
                    .HasColumnName("JobsiteID")
                    .HasMaxLength(255);

                entity.Property(e => e.ScheduleId)
                    .HasColumnName("ScheduleID")
                    .HasMaxLength(255);

                entity.Property(e => e.AllNotes)
                    .HasColumnName("All Notes")
                    .HasMaxLength(255);

                entity.Property(e => e.AssignedTo)
                    .HasColumnName("Assigned To")
                    .HasMaxLength(255);

                entity.Property(e => e.ConfirmationDate)
                    .HasColumnName("Confirmation Date")
                    .HasMaxLength(255);

                entity.Property(e => e.DurationDays)
                    .HasColumnName("Duration (Days)")
                    .HasMaxLength(255);

                entity.Property(e => e.Finish).HasMaxLength(255);

                entity.Property(e => e.InternalNotes)
                    .HasColumnName("Internal Notes")
                    .HasMaxLength(255);

                entity.Property(e => e.Jobsite).HasMaxLength(255);

                entity.Property(e => e.MarkedComplete)
                    .HasColumnName("Marked Complete")
                    .HasMaxLength(255);

                entity.Property(e => e.OwnerNotes)
                    .HasColumnName("Owner Notes")
                    .HasMaxLength(255);

                entity.Property(e => e.Phase).HasMaxLength(255);

                entity.Property(e => e.Reminder).HasMaxLength(255);

                entity.Property(e => e.Start).HasMaxLength(255);

                entity.Property(e => e.SubNotes)
                    .HasColumnName("Sub Notes")
                    .HasMaxLength(255);

                entity.Property(e => e.Tag).HasMaxLength(255);

                entity.Property(e => e.Title).HasMaxLength(255);
            });

            modelBuilder.Entity<SiteAudit>(entity =>
            {
                entity.ToTable("SiteAudit", "HR");

                entity.Property(e => e.Id)
                  .HasColumnName("ID")
                  .ValueGeneratedOnAdd();

                entity.Property(e => e.Auditdate)
                    .HasColumnName("auditdate")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Auditor)
                    .HasColumnName("auditor")
                    .HasMaxLength(50);

                entity.Property(e => e.Projectmanager).HasMaxLength(50);

                entity.Property(e => e.Projectname).HasMaxLength(255);

                entity.Property(e => e.Q1).HasMaxLength(10);

                entity.Property(e => e.Q10).HasMaxLength(10);

                entity.Property(e => e.Q11).HasMaxLength(10);

                entity.Property(e => e.Q12).IsUnicode(false);

                entity.Property(e => e.Q13).IsUnicode(false);

                entity.Property(e => e.Q14).HasMaxLength(10);

                entity.Property(e => e.Q15).IsUnicode(false);

                entity.Property(e => e.Q16).IsUnicode(false);

                entity.Property(e => e.Q17).HasMaxLength(10);

                entity.Property(e => e.Q18).HasMaxLength(10);

                entity.Property(e => e.Q19).HasMaxLength(10);

                entity.Property(e => e.Q2).IsUnicode(false);

                entity.Property(e => e.Q20).HasMaxLength(10);

              entity.Property(e => e.Q3).HasMaxLength(10);

              entity.Property(e => e.Q4).HasMaxLength(10);

              entity.Property(e => e.Q5).IsUnicode(false);

              entity.Property(e => e.Q6).IsUnicode(false);

              entity.Property(e => e.Q7).HasMaxLength(10);

              entity.Property(e => e.Q8).HasMaxLength(10);

              entity.Property(e => e.Q9).HasMaxLength(10);

              entity.Property(e => e.Q21).IsUnicode(false);

              entity.Property(e => e.Q22).HasMaxLength(10);

              entity.Property(e => e.Q23).IsUnicode(false);

              entity.Property(e => e.Q24).IsUnicode(false);

              entity.Property(e => e.Q25).HasMaxLength(10);

              entity.Property(e => e.Q26).IsUnicode(false);

              entity.Property(e => e.Q27).HasMaxLength(10);

              entity.Property(e => e.Q28).IsUnicode(false);

              entity.Property(e => e.Q29).HasMaxLength(10);

              entity.Property(e => e.Q30).IsUnicode(false);

              entity.Property(e => e.Q31).HasMaxLength(10);

              entity.Property(e => e.Q32).IsUnicode(false);

              entity.Property(e => e.Q33).HasMaxLength(10);

              entity.Property(e => e.Q34).HasMaxLength(10);

              entity.Property(e => e.Q35).IsUnicode(false);

              entity.Property(e => e.Q36).HasMaxLength(10);

              entity.Property(e => e.Q37).IsUnicode(false);

              entity.Property(e => e.Q38).HasMaxLength(10);

              entity.Property(e => e.Q39).IsUnicode(false);

              entity.Property(e => e.Q40).HasMaxLength(10);

              entity.Property(e => e.Q41).IsUnicode(false);

              entity.Property(e => e.Q42).IsUnicode(false);

              entity.Property(e => e.Q43).IsUnicode(false);

              entity.Property(e => e.Q44).IsUnicode(false);

              entity.Property(e => e.Q45).IsUnicode(false);

              entity.Property(e => e.Q46).HasMaxLength(10);

              entity.Property(e => e.Q47).IsUnicode(false);

              entity.Property(e => e.Q48).IsUnicode(false);

            });

            modelBuilder.Entity<Subsassistant>(entity =>
            {
                entity.HasKey(e => e.Assistantid);

                entity.ToTable("SubsAssistant", "Subs");

                entity.Property(e => e.Assistantid)
                    .HasColumnName("assistantid")
                    .ValueGeneratedNever();

                entity.Property(e => e.AsstAddress1)
                    .HasColumnName("asst_address1")
                    .HasMaxLength(50);

                entity.Property(e => e.AsstAddress2)
                    .HasColumnName("asst_address2")
                    .HasMaxLength(50);

                entity.Property(e => e.AsstCity)
                    .HasColumnName("asst_city")
                    .HasMaxLength(50);

                entity.Property(e => e.AsstDeskphone)
                    .HasColumnName("asst_deskphone")
                    .HasMaxLength(10);

                entity.Property(e => e.AsstEmail)
                    .HasColumnName("asst_email")
                    .HasMaxLength(100);

                entity.Property(e => e.AsstEmail2)
                    .HasColumnName("asst_email2")
                    .HasMaxLength(100);

                entity.Property(e => e.AsstExt)
                    .HasColumnName("asst_ext")
                    .HasMaxLength(10);

                entity.Property(e => e.AsstFname)
                    .HasColumnName("asst_fname")
                    .HasMaxLength(50);

                entity.Property(e => e.AsstLname)
                    .HasColumnName("asst_lname")
                    .HasMaxLength(50);

                entity.Property(e => e.AsstLocationname)
                    .HasColumnName("asst_locationname")
                    .HasMaxLength(50);

                entity.Property(e => e.AsstNotes)
                    .HasColumnName("asst_notes")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.AsstState)
                    .HasColumnName("asst_state")
                    .HasMaxLength(2);

                entity.Property(e => e.AsstTitle)
                    .HasColumnName("asst_title")
                    .HasMaxLength(50);

                entity.Property(e => e.AsstZip)
                    .HasColumnName("asst_zip")
                    .HasMaxLength(2);

                entity.Property(e => e.Asstmobilephone)
                    .HasColumnName("asstmobilephone")
                    .HasMaxLength(10);

                entity.Property(e => e.Companyid).HasColumnName("companyid");

                entity.Property(e => e.Contactid).HasColumnName("contactid");
            });

            modelBuilder.Entity<Subscontact>(entity =>
            {
                entity.HasKey(e => e.ContactId);

                entity.ToTable("Subscontact", "Subs");

                entity.Property(e => e.ContactId).HasColumnName("ContactID");

                entity.Property(e => e.Address1).HasMaxLength(50);

                entity.Property(e => e.Address2).HasMaxLength(50);

                entity.Property(e => e.City).HasMaxLength(50);

                entity.Property(e => e.CompanyId).HasColumnName("CompanyID");

                entity.Property(e => e.DeskPhone).HasMaxLength(10);

                entity.Property(e => e.Email)
                    .HasColumnName("email")
                    .HasMaxLength(100);

                entity.Property(e => e.Email2)
                    .HasColumnName("email2")
                    .HasMaxLength(100);

                entity.Property(e => e.Extension)
                    .HasColumnName("extension")
                    .HasMaxLength(10);

                entity.Property(e => e.Fname)
                    .HasColumnName("FName")
                    .HasMaxLength(50);

                entity.Property(e => e.Lname)
                    .HasColumnName("LName")
                    .HasMaxLength(50);

                entity.Property(e => e.LocationName).HasMaxLength(50);

                entity.Property(e => e.LocationPhone).HasMaxLength(10);

                entity.Property(e => e.MobilePhone).HasMaxLength(10);

                entity.Property(e => e.State).HasMaxLength(2);

                entity.Property(e => e.Title).HasMaxLength(50);

                entity.Property(e => e.Zip).HasMaxLength(5);
            });

            modelBuilder.Entity<Subs>(entity =>
            {
                entity.HasKey(e => e.Company);

                entity.ToTable("SubsMaster", "Subs");

                entity.Property(e => e.Company)
                    .HasMaxLength(255)
                    .ValueGeneratedNever();

                entity.Property(e => e.Address1).HasMaxLength(255);

                entity.Property(e => e.Address2).HasMaxLength(255);

                entity.Property(e => e.City).HasMaxLength(255);

                entity.Property(e => e.Email)
                    .HasColumnName("email")
                    .HasMaxLength(255);

                entity.Property(e => e.Fax)
                    .HasColumnName("fax")
                    .HasMaxLength(10);

                entity.Property(e => e.Id)
                    .HasColumnName("ID")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.Mainphone).HasMaxLength(15);

                entity.Property(e => e.NaicsCode)
                    .HasColumnName("naics_code")
                    .HasMaxLength(10);

                entity.Property(e => e.SicCode)
                    .HasColumnName("sic_code")
                    .HasMaxLength(10);

                entity.Property(e => e.State).HasMaxLength(2);

                entity.Property(e => e.Type)
                    .HasColumnName("type")
                    .HasMaxLength(255);

                entity.Property(e => e.Website)
                    .HasColumnName("website")
                    .HasMaxLength(255);

                entity.Property(e => e.Zip).HasMaxLength(5);
            });

            modelBuilder.Entity<SubsTrades>(entity =>
            {
              entity.ToTable("SubsTrades", "Subs");

              entity.Property(e => e.Id).HasColumnName("id");

              entity.Property(e => e.Divname)
                  .HasColumnName("divname")
                  .HasMaxLength(50);

              entity.Property(e => e.Divnum).HasColumnName("divnum");

              entity.Property(e => e.Tradeid).HasColumnName("tradeid");

              entity.Property(e => e.Tradename)
                  .HasColumnName("tradename")
                  .HasMaxLength(255);

              entity.Property(e => e.Vndnum).HasColumnName("vndnum");
            });

            modelBuilder.Entity<SuptProjectReview>(entity =>
                  {
                    entity.ToTable("SuptProjectReview", "HR");

                    entity.Property(e => e.Id)
                        .HasColumnName("ID")
                        .ValueGeneratedOnAdd(); 

                    entity.Property(e => e.Projectname).HasMaxLength(255);

                    entity.Property(e => e.Q1).HasMaxLength(10);

                    entity.Property(e => e.Q10).HasMaxLength(10);

                    entity.Property(e => e.Q11).HasMaxLength(10);

                    entity.Property(e => e.Q12).HasMaxLength(10);

                    entity.Property(e => e.Q13).HasMaxLength(10);

                    entity.Property(e => e.Q16).HasMaxLength(10);

                    entity.Property(e => e.Q18).HasMaxLength(10);

                    entity.Property(e => e.Q19).HasMaxLength(10);

                    entity.Property(e => e.Q2).HasMaxLength(10);

                    entity.Property(e => e.Q23).HasMaxLength(10);

                    entity.Property(e => e.Q25).HasMaxLength(10);

                    entity.Property(e => e.Q27).HasMaxLength(10);

                    entity.Property(e => e.Q30).HasMaxLength(10);

                    entity.Property(e => e.Q32).HasMaxLength(10);

                    entity.Property(e => e.Q37).HasMaxLength(10);

                    entity.Property(e => e.Q39).HasMaxLength(10);

                    entity.Property(e => e.Q4).HasMaxLength(10);

                    entity.Property(e => e.Q40).HasMaxLength(10);

                    entity.Property(e => e.Q46).HasMaxLength(10);

                    entity.Property(e => e.Q49).HasMaxLength(10);

                    entity.Property(e => e.Q51).HasMaxLength(10);

                    entity.Property(e => e.Q52).HasMaxLength(10);

                    entity.Property(e => e.Q55).HasMaxLength(10);

                    entity.Property(e => e.Q57).HasMaxLength(10);

                    entity.Property(e => e.Q58).HasMaxLength(10);

                    entity.Property(e => e.Q6).HasMaxLength(10);

                    entity.Property(e => e.Q60).HasMaxLength(10);

                    entity.Property(e => e.Q62).HasMaxLength(10);

                    entity.Property(e => e.Q8).HasMaxLength(10);

                    entity.Property(e => e.ReviewDate).HasDefaultValueSql("(getdate())");

                    entity.Property(e => e.Reviewed).HasMaxLength(50);

                    entity.Property(e => e.Reviewer).HasMaxLength(50);
                  });

            modelBuilder.Entity<ToDos>(entity =>
                  {
                      entity.HasKey(e => e.ToDoId);

                      entity.ToTable("ToDos", "BuilderTrend");

                      entity.Property(e => e.ToDoId)
                          .HasColumnName("ToDoID")
                          .ValueGeneratedNever();

                      entity.Property(e => e.AssignedTo).HasColumnName("Assigned To");

                      entity.Property(e => e.Completed).HasMaxLength(10);

                      entity.Property(e => e.CompletionDate)
                          .HasColumnName("Completion Date")
                          .HasColumnType("date");

                      entity.Property(e => e.CreatedBy).HasColumnName("Created By");

                      entity.Property(e => e.CreatedOn)
                          .HasColumnName("Created On")
                          .HasColumnType("date");

                      entity.Property(e => e.Due).HasColumnType("date");

                      entity.Property(e => e.HasChecklist).HasColumnName("Has Checklist");

                      entity.Property(e => e.JobsiteId).HasColumnName("JobsiteID");

                      entity.Property(e => e.RelatedDailyLog).HasColumnName("Related Daily Log");
                  });

            modelBuilder.Entity<WorkDays>(entity =>
            {
                entity.HasKey(e => new { e.JobsiteId, e.Title, e.StartDate, e.EndDate });

                entity.ToTable("Work Days", "BuilderTrend");

                entity.Property(e => e.JobsiteId)
                    .HasColumnName("JobsiteID")
                    .HasMaxLength(255);

                entity.Property(e => e.Title).HasMaxLength(255);

                entity.Property(e => e.StartDate)
                    .HasColumnName("Start Date")
                    .HasMaxLength(255);

                entity.Property(e => e.EndDate)
                    .HasColumnName("End Date")
                    .HasMaxLength(255);

                entity.Property(e => e.AllJobs)
                    .HasColumnName("All Jobs")
                    .HasMaxLength(255);

                entity.Property(e => e.Category).HasMaxLength(255);

                entity.Property(e => e.Days).HasMaxLength(255);

                entity.Property(e => e.Jobsite).HasMaxLength(255);

                entity.Property(e => e.Notes).HasMaxLength(255);

                entity.Property(e => e.SameEveryYear)
                    .HasColumnName("Same Every Year")
                    .HasMaxLength(255);

                entity.Property(e => e.Type).HasMaxLength(255);
            });

            modelBuilder.Entity<UserGroups>(entity =>
            {
              entity.HasKey(e => new { e.Group, e.UserId });

              entity.Property(e => e.Group).HasMaxLength(25);

              entity.Property(e => e.UserId).HasColumnName("UserID");
            });

            modelBuilder.Entity<Users>(entity =>
            {
              entity.HasKey(e => e.Username);

              entity.ToTable("users");

              entity.Property(e => e.Username)
                  .HasColumnName("username")
                  .HasMaxLength(25)
                  .ValueGeneratedNever();

              entity.Property(e => e.Id).HasColumnName("id");

            });
        }

    }
}
