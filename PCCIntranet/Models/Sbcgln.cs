﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Sbcgln
    {
        public Guid Idnum { get; set; }
        public Guid Idref { get; set; }
        public long Recnum { get; set; }
        public int Linnum { get; set; }
        public string Dscrpt { get; set; }
        public decimal Chghrs { get; set; }
        public decimal Chgunt { get; set; }
        public decimal Bdgprc { get; set; }
        public long? Vndnum { get; set; }
        public long? Vndctc { get; set; }
        public int? Ctclin { get; set; }
        public string Chgnum { get; set; }
        public byte Chgsts { get; set; }
        public DateTime? Chgdte { get; set; }
        public decimal? Cstcde { get; set; }
        public byte? Csttyp { get; set; }
        public string Usrdf1 { get; set; }
        public string Ntetxt { get; set; }
        public string Linref { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }

        public Cstcde CstcdeNavigation { get; set; }
        public Csttyp CsttypNavigation { get; set; }
        public Prmchg IdrefNavigation { get; set; }
        public Prmchg RecnumNavigation { get; set; }
        public Subcon VndctcNavigation { get; set; }
        public Actpay VndnumNavigation { get; set; }
    }
}
