﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class EmplU
    {
        public Guid Idnum { get; set; }

        public Employ IdnumNavigation { get; set; }
    }
}
