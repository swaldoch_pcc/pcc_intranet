﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Paypst
    {
        public Paypst()
        {
            Employ = new HashSet<Employ>();
            Jobpgp = new HashSet<Jobpgp>();
        }

        public Guid Idnum { get; set; }
        public int Recnum { get; set; }
        public string Pstnme { get; set; }
        public long Jobact { get; set; }
        public long? Eqpact { get; set; }
        public long Othact { get; set; }
        public long? Dptmnt { get; set; }
        public string Ntetxt { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }
        public byte? Eeocat { get; set; }

        public Dptmnt DptmntNavigation { get; set; }
        public Eeocat EeocatNavigation { get; set; }
        public Lgract EqpactNavigation { get; set; }
        public Lgract JobactNavigation { get; set; }
        public Lgract OthactNavigation { get; set; }
        public ICollection<Employ> Employ { get; set; }
        public ICollection<Jobpgp> Jobpgp { get; set; }
    }
}
