﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Crdlin
    {
        public Guid Idnum { get; set; }
        public Guid Idref { get; set; }
        public long Recnum { get; set; }
        public int Linnum { get; set; }
        public byte Lintyp { get; set; }
        public long Subact { get; set; }
        public long Lgrrcd { get; set; }
        public byte Ispryr { get; set; }
        public byte Status { get; set; }
        public string Trnnum { get; set; }
        public DateTime? Trndte { get; set; }
        public string Payee { get; set; }
        public decimal Crdamt { get; set; }
        public decimal Chgamt { get; set; }
        public string Trnhsh { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }

        public Crdrec IdrefNavigation { get; set; }
        public Crdrec RecnumNavigation { get; set; }
    }
}
