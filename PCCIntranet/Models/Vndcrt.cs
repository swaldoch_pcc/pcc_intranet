﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Vndcrt
    {
        public Guid Idnum { get; set; }
        public Guid Idref { get; set; }
        public long Recnum { get; set; }
        public int Linnum { get; set; }
        public string Dscrpt { get; set; }
        public DateTime? Dtercv { get; set; }
        public DateTime? Expdte { get; set; }
        public string Cntwrn { get; set; }
        public string Stppay { get; set; }
        public string Ntetxt { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }
        public long? Jobnum { get; set; }

        public Actpay IdrefNavigation { get; set; }
        public Actrec JobnumNavigation { get; set; }
        public Actpay RecnumNavigation { get; set; }
    }
}
