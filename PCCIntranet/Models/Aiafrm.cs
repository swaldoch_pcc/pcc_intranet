﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Aiafrm
    {
        public Aiafrm()
        {
            AialinIdrefNavigation = new HashSet<Aialin>();
            AialinRecnumNavigation = new HashSet<Aialin>();
        }

        public Guid Idnum { get; set; }
        public long Recnum { get; set; }
        public long Jobnum { get; set; }
        public long Phsnum { get; set; }
        public long? Achnum { get; set; }
        public string Achjob { get; set; }
        public int Appnum { get; set; }
        public DateTime? Period { get; set; }
        public decimal Retain { get; set; }
        public decimal Prvbil { get; set; }
        public decimal Prvret { get; set; }
        public decimal Maxret { get; set; }
        public string Dscrpt { get; set; }
        public long? Incact { get; set; }
        public long? Incsub { get; set; }
        public int? Taxdst { get; set; }
        public decimal Schttl { get; set; }
        public decimal Chgttl { get; set; }
        public decimal Conttl { get; set; }
        public decimal Prvttl { get; set; }
        public decimal Curttl { get; set; }
        public decimal Strttl { get; set; }
        public decimal Cmpttl { get; set; }
        public decimal Pctcmp { get; set; }
        public decimal Balttl { get; set; }
        public decimal Retttl { get; set; }
        public decimal Ttlern { get; set; }
        public decimal Crtdue { get; set; }
        public decimal Balcon { get; set; }
        public DateTime? Subdte { get; set; }
        public DateTime? Appdte { get; set; }
        public decimal Matret { get; set; }
        public string Usrdf1 { get; set; }
        public string Usrdf2 { get; set; }
        public decimal Taxabl { get; set; }
        public decimal Nontax { get; set; }
        public decimal Slstax { get; set; }
        public decimal Invttl { get; set; }
        public byte Status { get; set; }
        public byte Bllbas { get; set; }
        public decimal Erlsrt { get; set; }
        public decimal Balfin { get; set; }
        public long Lgrrec { get; set; }
        public string Bllcyc { get; set; }
        public decimal Secrte { get; set; }
        public decimal Prmret { get; set; }
        public decimal Ttlret { get; set; }
        public byte Hotlst { get; set; }
        public string Ntetxt { get; set; }
        public byte Cmbphs { get; set; }
        public decimal Pstsbj { get; set; }
        public decimal Pstamt { get; set; }
        public decimal Gstsbj { get; set; }
        public decimal Gstamt { get; set; }
        public decimal Hstsbj { get; set; }
        public decimal Hstamt { get; set; }
        public decimal Curhld { get; set; }
        public decimal Pstrte { get; set; }
        public decimal Gstrte { get; set; }
        public decimal Hstrte { get; set; }
        public byte Pstcmp { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }

        public Actpay AchnumNavigation { get; set; }
        public Lgrsub Inc { get; set; }
        public Lgract IncactNavigation { get; set; }
        public Actrec JobnumNavigation { get; set; }
        public Jobphs Jobphs { get; set; }
        public Taxdst TaxdstNavigation { get; set; }
        public AiafU AiafU { get; set; }
        public ICollection<Aialin> AialinIdrefNavigation { get; set; }
        public ICollection<Aialin> AialinRecnumNavigation { get; set; }
    }
}
