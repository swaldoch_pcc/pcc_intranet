﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Pnrcln
    {
        public Guid Idnum { get; set; }
        public Guid Idref { get; set; }
        public long Recnum { get; set; }
        public int Linnum { get; set; }
        public DateTime? Revdte { get; set; }
        public string Refnum { get; set; }
        public string Dscrpt { get; set; }
        public string Usrdf1 { get; set; }
        public string Ntetxt { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }

        public Plnrcd IdrefNavigation { get; set; }
        public Plnrcd RecnumNavigation { get; set; }
    }
}
