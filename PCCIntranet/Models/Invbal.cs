﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Invbal
    {
        public Invbal()
        {
            InvoutIdrefNavigation = new HashSet<Invout>();
            InvoutRecnumNavigation = new HashSet<Invout>();
        }

        public Guid Idnum { get; set; }
        public long Recnum { get; set; }
        public long Prtnum { get; set; }
        public decimal Trnqty { get; set; }
        public decimal Qtyrmn { get; set; }
        public DateTime? Trndte { get; set; }
        public int Trntme { get; set; }
        public decimal Prtprc { get; set; }
        public byte Srcnum { get; set; }
        public long Lgrref { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }

        public Tkfprt PrtnumNavigation { get; set; }
        public Source SrcnumNavigation { get; set; }
        public ICollection<Invout> InvoutIdrefNavigation { get; set; }
        public ICollection<Invout> InvoutRecnumNavigation { get; set; }
    }
}
