﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Srvloc
    {
        public Srvloc()
        {
            Srcneq = new HashSet<Srcneq>();
            Srcnlc = new HashSet<Srcnlc>();
            Srvinv = new HashSet<Srvinv>();
        }

        public Guid Idnum { get; set; }
        public Guid Idref { get; set; }
        public long Recnum { get; set; }
        public long Locnum { get; set; }
        public string Locnme { get; set; }
        public string Addrs1 { get; set; }
        public string Addrs2 { get; set; }
        public string Ctynme { get; set; }
        public string State { get; set; }
        public string Zipcde { get; set; }
        public string Phnnum { get; set; }
        public string Contct { get; set; }
        public short? Srvgeo { get; set; }
        public string Maploc { get; set; }
        public string Crsstr { get; set; }
        public int? Taxdst { get; set; }
        public string Usrdf1 { get; set; }
        public string Ntetxt { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }

        public Reccln IdrefNavigation { get; set; }
        public Reccln RecnumNavigation { get; set; }
        public Srvgeo SrvgeoNavigation { get; set; }
        public Taxdst TaxdstNavigation { get; set; }
        public ICollection<Srcneq> Srcneq { get; set; }
        public ICollection<Srcnlc> Srcnlc { get; set; }
        public ICollection<Srvinv> Srvinv { get; set; }
    }
}
