﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Clntyp
    {
        public Clntyp()
        {
            Reccln = new HashSet<Reccln>();
        }

        public Guid Idnum { get; set; }
        public byte Recnum { get; set; }
        public string Typnme { get; set; }
        public string Ntetxt { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }

        public ICollection<Reccln> Reccln { get; set; }
    }
}
