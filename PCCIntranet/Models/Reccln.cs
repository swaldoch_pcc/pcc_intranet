﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Reccln
    {
        public Reccln()
        {
            Actrec = new HashSet<Actrec>();
            Calnte = new HashSet<Calnte>();
            ClncntIdrefNavigation = new HashSet<Clncnt>();
            ClncntRecnumNavigation = new HashSet<Clncnt>();
            Coresp = new HashSet<Coresp>();
            ReferlClnnumNavigation = new HashSet<Referl>();
            ReferlIdrefNavigation = new HashSet<Referl>();
            ReferlReferdNavigation = new HashSet<Referl>();
            Reqinf = new HashSet<Reqinf>();
            Srvcnt = new HashSet<Srvcnt>();
            Srvinv = new HashSet<Srvinv>();
            SrvlocIdrefNavigation = new HashSet<Srvloc>();
            SrvlocRecnumNavigation = new HashSet<Srvloc>();
            Submtl = new HashSet<Submtl>();
            Trnmtl = new HashSet<Trnmtl>();
        }

        public Guid Idnum { get; set; }
        public long Recnum { get; set; }
        public string Shtnme { get; set; }
        public string Clnnme { get; set; }
        public string Grting { get; set; }
        public string Addrs1 { get; set; }
        public string Addrs2 { get; set; }
        public string Ctynme { get; set; }
        public string State { get; set; }
        public string Zipcde { get; set; }
        public string Bilad1 { get; set; }
        public string Bilad2 { get; set; }
        public string Bilcty { get; set; }
        public string Bilste { get; set; }
        public string Bilzip { get; set; }
        public string Shpad1 { get; set; }
        public string Shpad2 { get; set; }
        public string Shpcty { get; set; }
        public string Shpste { get; set; }
        public string Shpzip { get; set; }
        public string Usrdf1 { get; set; }
        public string Usrdf2 { get; set; }
        public string Usrdf3 { get; set; }
        public string Usrdf4 { get; set; }
        public string Usrdf5 { get; set; }
        public byte Usrdf6 { get; set; }
        public byte Usrdf7 { get; set; }
        public byte Usrdf8 { get; set; }
        public byte Usrdf9 { get; set; }
        public string Contct { get; set; }
        public string Contc2 { get; set; }
        public string Contc3 { get; set; }
        public string Cntds1 { get; set; }
        public string Cntds2 { get; set; }
        public string Cntds3 { get; set; }
        public string Phnnum { get; set; }
        public string Phn002 { get; set; }
        public string Phn003 { get; set; }
        public string Phnext { get; set; }
        public string Phext2 { get; set; }
        public string Phext3 { get; set; }
        public string Faxnum { get; set; }
        public string Fax002 { get; set; }
        public string Fax003 { get; set; }
        public string Cllphn { get; set; }
        public string Cell02 { get; set; }
        public string Cell03 { get; set; }
        public string Pagnum { get; set; }
        public string Pagr02 { get; set; }
        public string Pagr03 { get; set; }
        public string EMail { get; set; }
        public string Email2 { get; set; }
        public string Email3 { get; set; }
        public DateTime? Dtercv { get; set; }
        public long? Empnum { get; set; }
        public long? Mannum { get; set; }
        public int? Taxdst { get; set; }
        public DateTime? Lstctc { get; set; }
        public DateTime? Cllbck { get; set; }
        public string Lstmal { get; set; }
        public DateTime? Lstdte { get; set; }
        public DateTime? Pchdte { get; set; }
        public DateTime? Refdte { get; set; }
        public byte? Prdnum { get; set; }
        public string Dsctrm { get; set; }
        public decimal Dscrte { get; set; }
        public string Duetrm { get; set; }
        public decimal Finrte { get; set; }
        public int? Ledsrc { get; set; }
        public byte? Clntyp { get; set; }
        public byte Status { get; set; }
        public byte? Mallst { get; set; }
        public short? Region { get; set; }
        public byte? Cmpsze { get; set; }
        public byte? Srvcon { get; set; }
        public DateTime? Srvexp { get; set; }
        public decimal Clndsc { get; set; }
        public decimal Begbal { get; set; }
        public decimal Endbal { get; set; }
        public string Maploc { get; set; }
        public string Crsstr { get; set; }
        public byte Bilbas { get; set; }
        public string Pchnum { get; set; }
        public string Exmnum { get; set; }
        public string Crdnum { get; set; }
        public DateTime? Expdte { get; set; }
        public string Crdnme { get; set; }
        public string Crdtyp { get; set; }
        public string Ntetxt { get; set; }
        public byte Catxex { get; set; }
        public byte Pstexm { get; set; }
        public byte Nvrvlt { get; set; }
        public string Valtid { get; set; }
        public byte Vlttyp { get; set; }
        public string Vltnme { get; set; }
        public string Vltmin { get; set; }
        public string Vltlnm { get; set; }
        public string Vltad1 { get; set; }
        public string Vltad2 { get; set; }
        public string Vltcty { get; set; }
        public string Vltste { get; set; }
        public string Vltzip { get; set; }
        public string Vltcnt { get; set; }
        public string Vlteml { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }
        public byte Inactv { get; set; }
        public string Stmeml { get; set; }

        public Clntyp ClntypNavigation { get; set; }
        public Szelst CmpszeNavigation { get; set; }
        public Employ EmpnumNavigation { get; set; }
        public Ledsrc LedsrcNavigation { get; set; }
        public Mallst MallstNavigation { get; set; }
        public Employ MannumNavigation { get; set; }
        public Prdlst PrdnumNavigation { get; set; }
        public Srvgeo RegionNavigation { get; set; }
        public Conlst SrvconNavigation { get; set; }
        public Clnsts StatusNavigation { get; set; }
        public Taxdst TaxdstNavigation { get; set; }
        public ReccU ReccU { get; set; }
        public ICollection<Actrec> Actrec { get; set; }
        public ICollection<Calnte> Calnte { get; set; }
        public ICollection<Clncnt> ClncntIdrefNavigation { get; set; }
        public ICollection<Clncnt> ClncntRecnumNavigation { get; set; }
        public ICollection<Coresp> Coresp { get; set; }
        public ICollection<Referl> ReferlClnnumNavigation { get; set; }
        public ICollection<Referl> ReferlIdrefNavigation { get; set; }
        public ICollection<Referl> ReferlReferdNavigation { get; set; }
        public ICollection<Reqinf> Reqinf { get; set; }
        public ICollection<Srvcnt> Srvcnt { get; set; }
        public ICollection<Srvinv> Srvinv { get; set; }
        public ICollection<Srvloc> SrvlocIdrefNavigation { get; set; }
        public ICollection<Srvloc> SrvlocRecnumNavigation { get; set; }
        public ICollection<Submtl> Submtl { get; set; }
        public ICollection<Trnmtl> Trnmtl { get; set; }
    }
}
