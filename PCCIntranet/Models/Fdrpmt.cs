﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Fdrpmt
    {
        public Guid Idnum { get; set; }
        public Guid Idref { get; set; }
        public long Recnum { get; set; }
        public int Linnum { get; set; }
        public short? Mettyp { get; set; }
        public string Dscrpt { get; set; }
        public string Presnt { get; set; }
        public string Usrdf1 { get; set; }
        public string Ntetxt { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }

        public Fldrpt IdrefNavigation { get; set; }
        public Mtgtyp MettypNavigation { get; set; }
        public Fldrpt RecnumNavigation { get; set; }
    }
}
