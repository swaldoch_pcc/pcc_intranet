﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Sbcnln
    {
        public Guid Idnum { get; set; }
        public Guid Idref { get; set; }
        public long Recnum { get; set; }
        public int Linnum { get; set; }
        public string Dscrpt { get; set; }
        public decimal? Cstcde { get; set; }
        public byte? Csttyp { get; set; }
        public decimal Amount { get; set; }
        public decimal Change { get; set; }
        public decimal Cntrct { get; set; }
        public string Gstsbj { get; set; }
        public string Pstsbj { get; set; }
        public string Hstsbj { get; set; }
        public decimal Billed { get; set; }
        public decimal Remain { get; set; }
        public string Usrdf1 { get; set; }
        public string Ntetxt { get; set; }
        public string Linref { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }

        public Cstcde CstcdeNavigation { get; set; }
        public Csttyp CsttypNavigation { get; set; }
        public Subcon IdrefNavigation { get; set; }
        public Subcon RecnumNavigation { get; set; }
    }
}
