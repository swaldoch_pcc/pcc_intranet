﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Bdglin
    {
        public Guid Idnum { get; set; }
        public Guid Idref { get; set; }
        public long Recnum { get; set; }
        public long Phsnum { get; set; }
        public int Linnum { get; set; }
        public decimal Cstcde { get; set; }
        public decimal Hrsbdg { get; set; }
        public decimal Matbdg { get; set; }
        public decimal Labbdg { get; set; }
        public decimal Eqpbdg { get; set; }
        public decimal Subbdg { get; set; }
        public decimal Othbdg { get; set; }
        public decimal Usrcs6 { get; set; }
        public decimal Usrcs7 { get; set; }
        public decimal Usrcs8 { get; set; }
        public decimal Usrcs9 { get; set; }
        public decimal Ttlbdg { get; set; }
        public string Untdsc { get; set; }
        public decimal Estunt { get; set; }
        public decimal Untcst { get; set; }
        public string Usrdf1 { get; set; }
        public string Ntetxt { get; set; }
        public decimal Hrsorg { get; set; }
        public decimal Matorg { get; set; }
        public decimal Laborg { get; set; }
        public decimal Eqporg { get; set; }
        public decimal Suborg { get; set; }
        public decimal Othorg { get; set; }
        public decimal Cs6org { get; set; }
        public decimal Cs7org { get; set; }
        public decimal Cs8org { get; set; }
        public decimal Cs9org { get; set; }
        public decimal Ttlorg { get; set; }
        public decimal Euntor { get; set; }
        public decimal Ucstor { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }

        public Cstcde CstcdeNavigation { get; set; }
        public Budget IdrefNavigation { get; set; }
        public Jobphs Jobphs { get; set; }
        public Budget RecnumNavigation { get; set; }
    }
}
