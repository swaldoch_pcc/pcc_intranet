﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class ChangeOrderLineItems
    {
        public string LineItemId { get; set; }
        public string ChangeOrderId { get; set; }
        public string CostCode { get; set; }
        public string Description { get; set; }
        public string Quantity { get; set; }
        public string Unit { get; set; }
        public string UnitCost { get; set; }
        public string MarkupPercentage { get; set; }
        public string MarkupPerUnit { get; set; }
        public string MarkupAmount { get; set; }
        public string OwnerPrice { get; set; }
        public string InternalNotes { get; set; }
    }
}
