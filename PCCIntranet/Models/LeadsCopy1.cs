﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class LeadsCopy1
    {
        public string LeadId { get; set; }
        public string OpportunityTitle { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string CustomerContact { get; set; }
        public string Status { get; set; }
        public string RelatedJob { get; set; }
        public string Age { get; set; }
        public string Confidence { get; set; }
        public decimal? EstRevenueMin { get; set; }
        public decimal? EstRevenueMax { get; set; }
        public string LastContacted { get; set; }
        public string Salesperson { get; set; }
        public string Source { get; set; }
        public string ProjectType { get; set; }
        public string PhoneNumber { get; set; }
        public string CellPhoneNumber { get; set; }
        public string EmailAddresses { get; set; }
        public string LeadNotes { get; set; }
        public DateTime? SoldDate { get; set; }
        public DateTime? ProjectedSalesDate { get; set; }
        public string Tags { get; set; }
        public string TradesNeeded { get; set; }
        public DateTime? AnticipatedConstStart { get; set; }
        public string ProjectDuration { get; set; }
        public DateTime? SubBidsDue { get; set; }
        public DateTime? PCBidDate { get; set; }
        public string SalesPhase { get; set; }
        public string Company { get; set; }
        public string PCBidTime { get; set; }
        public string SubBidsTime { get; set; }
        public string BuilderId { get; set; }
        public DateTime? Prebidconference { get; set; }
        public string Results { get; set; }
        public string SuccessfulBidder { get; set; }
        public decimal? Difference { get; set; }
    }
}
