﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Pchtyp
    {
        public Pchtyp()
        {
            Actpay = new HashSet<Actpay>();
            Pchord = new HashSet<Pchord>();
        }

        public Guid Idnum { get; set; }
        public byte Recnum { get; set; }
        public string Typnme { get; set; }
        public string Ntetxt { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }

        public ICollection<Actpay> Actpay { get; set; }
        public ICollection<Pchord> Pchord { get; set; }
    }
}
