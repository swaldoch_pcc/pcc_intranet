﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Prgalt
    {
        public Guid Idnum { get; set; }
        public long Recnum { get; set; }
        public string Usrnme { get; set; }
        public byte Alttyp { get; set; }
        public byte Output { get; set; }
        public long? Sprvsr { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }
    }
}
