﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Emplic
    {
        public Guid Idnum { get; set; }
        public Guid Idref { get; set; }
        public long Empnum { get; set; }
        public short? Typnum { get; set; }
        public string Licnum { get; set; }
        public DateTime? Expdte { get; set; }
        public string Ntetxt { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }

        public Employ EmpnumNavigation { get; set; }
        public Employ IdrefNavigation { get; set; }
        public Lictyp TypnumNavigation { get; set; }
    }
}
