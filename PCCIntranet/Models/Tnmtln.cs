﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Tnmtln
    {
        public Guid Idnum { get; set; }
        public Guid Idref { get; set; }
        public long Recnum { get; set; }
        public int Linnum { get; set; }
        public string Itmnum { get; set; }
        public string Dscrpt { get; set; }
        public long Linqty { get; set; }
        public string Usrdf1 { get; set; }
        public string Ntetxt { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }

        public Trnmtl IdrefNavigation { get; set; }
        public Trnmtl RecnumNavigation { get; set; }
    }
}
