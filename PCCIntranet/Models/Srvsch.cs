﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Srvsch
    {
        public Guid Idnum { get; set; }
        public Guid Idref { get; set; }
        public long Recnum { get; set; }
        public int Linnum { get; set; }
        public long? Empnum { get; set; }
        public long? Eqpnum { get; set; }
        public long? Vndnum { get; set; }
        public byte Priort { get; set; }
        public DateTime? Schdte { get; set; }
        public TimeSpan? Schstr { get; set; }
        public TimeSpan? Schfin { get; set; }
        public decimal Esthrs { get; set; }
        public string Tvltim { get; set; }
        public DateTime? Findte { get; set; }
        public TimeSpan? Actstr { get; set; }
        public TimeSpan? Actfin { get; set; }
        public decimal Acthrs { get; set; }
        public DateTime? Bildte { get; set; }
        public string Usrdf1 { get; set; }
        public string Emlsnt { get; set; }
        public string Emlvld { get; set; }
        public long Emllst { get; set; }
        public string Emltyp { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }

        public Employ EmpnumNavigation { get; set; }
        public Eqpmnt EqpnumNavigation { get; set; }
        public Srvinv IdrefNavigation { get; set; }
        public Priort PriortNavigation { get; set; }
        public Srvinv RecnumNavigation { get; set; }
        public Actpay VndnumNavigation { get; set; }
    }
}
