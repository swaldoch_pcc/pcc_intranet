﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Payuni
    {
        public Payuni()
        {
            Employ = new HashSet<Employ>();
            Payded = new HashSet<Payded>();
            Paygrp = new HashSet<Paygrp>();
        }

        public Guid Idnum { get; set; }
        public int Recnum { get; set; }
        public string Uninme { get; set; }
        public string Addrs1 { get; set; }
        public string Addrs2 { get; set; }
        public string Ctynme { get; set; }
        public string State { get; set; }
        public string Zipcde { get; set; }
        public string Phnnum { get; set; }
        public int Rppact { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }

        public ICollection<Employ> Employ { get; set; }
        public ICollection<Payded> Payded { get; set; }
        public ICollection<Paygrp> Paygrp { get; set; }
    }
}
