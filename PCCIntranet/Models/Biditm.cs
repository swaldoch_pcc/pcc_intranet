﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Biditm
    {
        public Biditm()
        {
            Fdrpun = new HashSet<Fdrpun>();
            Prplin = new HashSet<Prplin>();
            PtotkfBiditm = new HashSet<Ptotkf>();
            PtotkfBiditm1 = new HashSet<Ptotkf>();
            PtotkfBiditmNavigation = new HashSet<Ptotkf>();
            Tkflin = new HashSet<Tkflin>();
            Unprln = new HashSet<Unprln>();
            Untlin = new HashSet<Untlin>();
        }

        public Guid Idnum { get; set; }
        public Guid Idref { get; set; }
        public long Recnum { get; set; }
        public long Itmnum { get; set; }
        public string Itmcde { get; set; }
        public string Itmnme { get; set; }
        public string Untdsc { get; set; }
        public decimal Bidqty { get; set; }
        public byte Itmtyp { get; set; }
        public decimal Ovrhed { get; set; }
        public decimal Profit { get; set; }
        public string Ntetxt { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }

        public Actrec IdrefNavigation { get; set; }
        public Actrec RecnumNavigation { get; set; }
        public ICollection<Fdrpun> Fdrpun { get; set; }
        public ICollection<Prplin> Prplin { get; set; }
        public ICollection<Ptotkf> PtotkfBiditm { get; set; }
        public ICollection<Ptotkf> PtotkfBiditm1 { get; set; }
        public ICollection<Ptotkf> PtotkfBiditmNavigation { get; set; }
        public ICollection<Tkflin> Tkflin { get; set; }
        public ICollection<Unprln> Unprln { get; set; }
        public ICollection<Untlin> Untlin { get; set; }
    }
}
