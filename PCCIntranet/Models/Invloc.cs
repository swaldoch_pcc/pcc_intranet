﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Invloc
    {
        public Invloc()
        {
            Apivln = new HashSet<Apivln>();
            Invhst = new HashSet<Invhst>();
            Invqty = new HashSet<Invqty>();
            Invser = new HashSet<Invser>();
            InvtlnDstlocNavigation = new HashSet<Invtln>();
            InvtlnInvlocNavigation = new HashSet<Invtln>();
            Pcorln = new HashSet<Pcorln>();
            Srvlin = new HashSet<Srvlin>();
            Tkflin = new HashSet<Tkflin>();
            Tkfprt = new HashSet<Tkfprt>();
        }

        public Guid Idnum { get; set; }
        public int Recnum { get; set; }
        public string Locnme { get; set; }
        public long? Empnum { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }

        public Employ EmpnumNavigation { get; set; }
        public ICollection<Apivln> Apivln { get; set; }
        public ICollection<Invhst> Invhst { get; set; }
        public ICollection<Invqty> Invqty { get; set; }
        public ICollection<Invser> Invser { get; set; }
        public ICollection<Invtln> InvtlnDstlocNavigation { get; set; }
        public ICollection<Invtln> InvtlnInvlocNavigation { get; set; }
        public ICollection<Pcorln> Pcorln { get; set; }
        public ICollection<Srvlin> Srvlin { get; set; }
        public ICollection<Tkflin> Tkflin { get; set; }
        public ICollection<Tkfprt> Tkfprt { get; set; }
    }
}
