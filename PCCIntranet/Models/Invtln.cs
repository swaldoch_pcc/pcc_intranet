﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Invtln
    {
        public Guid Idnum { get; set; }
        public Guid Idref { get; set; }
        public long Recnum { get; set; }
        public int Linnum { get; set; }
        public long? Prtnum { get; set; }
        public string Prtdsc { get; set; }
        public string Alpnum { get; set; }
        public string Untdsc { get; set; }
        public decimal Invqty { get; set; }
        public decimal Invcst { get; set; }
        public decimal Invttl { get; set; }
        public string Serial { get; set; }
        public int? Invloc { get; set; }
        public long? Dbtact { get; set; }
        public long? Dbtsub { get; set; }
        public int? Dstloc { get; set; }
        public string Usrdf1 { get; set; }
        public string Ntetxt { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }

        public Lgract DbtactNavigation { get; set; }
        public Invloc DstlocNavigation { get; set; }
        public Invalc IdrefNavigation { get; set; }
        public Invloc InvlocNavigation { get; set; }
        public Tkfprt PrtnumNavigation { get; set; }
        public Invalc RecnumNavigation { get; set; }
    }
}
