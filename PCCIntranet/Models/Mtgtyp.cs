﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Mtgtyp
    {
        public Mtgtyp()
        {
            Fdrpmt = new HashSet<Fdrpmt>();
        }

        public Guid Idnum { get; set; }
        public short Recnum { get; set; }
        public string Dscrpt { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }

        public ICollection<Fdrpmt> Fdrpmt { get; set; }
    }
}
