﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Fldrpt
    {
        public Fldrpt()
        {
            FdrpeqIdrefNavigation = new HashSet<Fdrpeq>();
            FdrpeqRecnumNavigation = new HashSet<Fdrpeq>();
            FdrpinIdrefNavigation = new HashSet<Fdrpin>();
            FdrpinRecnumNavigation = new HashSet<Fdrpin>();
            FdrplnIdrefNavigation = new HashSet<Fdrpln>();
            FdrplnRecnumNavigation = new HashSet<Fdrpln>();
            FdrpmtIdrefNavigation = new HashSet<Fdrpmt>();
            FdrpmtRecnumNavigation = new HashSet<Fdrpmt>();
            FdrporIdrefNavigation = new HashSet<Fdrpor>();
            FdrporRecnumNavigation = new HashSet<Fdrpor>();
            FdrpsbIdrefNavigation = new HashSet<Fdrpsb>();
            FdrpsbRecnumNavigation = new HashSet<Fdrpsb>();
            FdrpunIdrefNavigation = new HashSet<Fdrpun>();
            FdrpunRecnumNavigation = new HashSet<Fdrpun>();
        }

        public Guid Idnum { get; set; }
        public long Recnum { get; set; }
        public DateTime? Rptdte { get; set; }
        public long Jobnum { get; set; }
        public long Phsnum { get; set; }
        public string Dscrpt { get; set; }
        public long? Empnum { get; set; }
        public string Wthcon { get; set; }
        public string Tmptur { get; set; }
        public string Usrdf1 { get; set; }
        public string Usrdf2 { get; set; }
        public DateTime? Entdte { get; set; }
        public string Usrnme { get; set; }
        public byte Hotlst { get; set; }
        public byte Lckedt { get; set; }
        public string Ntetxt { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }

        public Employ EmpnumNavigation { get; set; }
        public Actrec JobnumNavigation { get; set; }
        public Jobphs Jobphs { get; set; }
        public FldrU FldrU { get; set; }
        public ICollection<Fdrpeq> FdrpeqIdrefNavigation { get; set; }
        public ICollection<Fdrpeq> FdrpeqRecnumNavigation { get; set; }
        public ICollection<Fdrpin> FdrpinIdrefNavigation { get; set; }
        public ICollection<Fdrpin> FdrpinRecnumNavigation { get; set; }
        public ICollection<Fdrpln> FdrplnIdrefNavigation { get; set; }
        public ICollection<Fdrpln> FdrplnRecnumNavigation { get; set; }
        public ICollection<Fdrpmt> FdrpmtIdrefNavigation { get; set; }
        public ICollection<Fdrpmt> FdrpmtRecnumNavigation { get; set; }
        public ICollection<Fdrpor> FdrporIdrefNavigation { get; set; }
        public ICollection<Fdrpor> FdrporRecnumNavigation { get; set; }
        public ICollection<Fdrpsb> FdrpsbIdrefNavigation { get; set; }
        public ICollection<Fdrpsb> FdrpsbRecnumNavigation { get; set; }
        public ICollection<Fdrpun> FdrpunIdrefNavigation { get; set; }
        public ICollection<Fdrpun> FdrpunRecnumNavigation { get; set; }
    }
}
