﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Vndrmt
    {
        public Guid Idnum { get; set; }
        public Guid Idref { get; set; }
        public long Recnum { get; set; }
        public string Rmtnme { get; set; }
        public string Rmtad1 { get; set; }
        public string Rmtad2 { get; set; }
        public string Rmtcty { get; set; }
        public string Rmtzip { get; set; }
        public string Ntetxt { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }

        public Actpay IdrefNavigation { get; set; }
        public Actpay RecnumNavigation { get; set; }
    }
}
