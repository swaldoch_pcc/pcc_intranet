﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Prdsts
    {
        public Guid Idnum { get; set; }
        public short Postyr { get; set; }
        public byte Actprd { get; set; }
        public byte Status { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }
    }
}
