using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Subscontact
    {
        public int CompanyId { get; set; }
        public string Fname { get; set; }
        public string Lname { get; set; }
        public string Title { get; set; }
        public string LocationName { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string DeskPhone { get; set; }
        public string Extension { get; set; }
        public string LocationPhone { get; set; }
        public string MobilePhone { get; set; }
        public string Email { get; set; }
        public string Email2 { get; set; }
        public string Notes { get; set; }
        public int ContactId { get; set; }
    }
}
