﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Asmcls
    {
        public Asmcls()
        {
            Assemb = new HashSet<Assemb>();
        }

        public Guid Idnum { get; set; }
        public long Recnum { get; set; }
        public string Clsnme { get; set; }
        public byte Indent { get; set; }
        public long Parcls { get; set; }
        public byte Haskid { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }

        public ICollection<Assemb> Assemb { get; set; }
    }
}
