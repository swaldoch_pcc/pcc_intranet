﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Acaind
    {
        public Guid Idnum { get; set; }
        public Guid Idref { get; set; }
        public long Recnum { get; set; }
        public string Indnme { get; set; }
        public byte Relatn { get; set; }
        public string Socsec { get; set; }
        public DateTime? Dtbrth { get; set; }
        public short Calyer { get; set; }
        public byte Rownum { get; set; }
        public byte Cvgjan { get; set; }
        public byte Cvgfeb { get; set; }
        public byte Cvgmar { get; set; }
        public byte Cvgapr { get; set; }
        public byte Cvgmay { get; set; }
        public byte Cvgjun { get; set; }
        public byte Cvgjul { get; set; }
        public byte Cvgaug { get; set; }
        public byte Cvgsep { get; set; }
        public byte Cvgoct { get; set; }
        public byte Cvgnov { get; set; }
        public byte Cvgdec { get; set; }
        public string Lineid { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }

        public Employ IdrefNavigation { get; set; }
        public Employ RecnumNavigation { get; set; }
    }
}
