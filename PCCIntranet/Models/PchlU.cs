﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class PchlU
    {
        public Guid Idnum { get; set; }

        public Pchlst IdnumNavigation { get; set; }
    }
}
