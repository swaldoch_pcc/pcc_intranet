﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Srvrte
    {
        public Srvrte()
        {
            Srvinv = new HashSet<Srvinv>();
        }

        public Guid Idnum { get; set; }
        public short Recnum { get; set; }
        public string Rtenme { get; set; }
        public string Ntetxt { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }

        public ICollection<Srvinv> Srvinv { get; set; }
    }
}
