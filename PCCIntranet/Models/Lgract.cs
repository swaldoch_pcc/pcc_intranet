﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Lgract
    {
        public Lgract()
        {
            Actpay = new HashSet<Actpay>();
            Actrec = new HashSet<Actrec>();
            Aiafrm = new HashSet<Aiafrm>();
            Apivln = new HashSet<Apivln>();
            Arivln = new HashSet<Arivln>();
            Bnkrec = new HashSet<Bnkrec>();
            CaTaxGtclacNavigation = new HashSet<CaTax>();
            CaTaxGtpdacNavigation = new HashSet<CaTax>();
            CaTaxPtclacNavigation = new HashSet<CaTax>();
            CaTaxQtpdacNavigation = new HashSet<CaTax>();
            CaTaxRitcacNavigation = new HashSet<CaTax>();
            CmpanyCcdactNavigation = new HashSet<Cmpany>();
            CmpanyDepactNavigation = new HashSet<Cmpany>();
            CmpanyVndeftNavigation = new HashSet<Cmpany>();
            CrcardIdrefNavigation = new HashSet<Crcard>();
            Crdrec = new HashSet<Crdrec>();
            EqpmntDepactNavigation = new HashSet<Eqpmnt>();
            EqpmntIntactNavigation = new HashSet<Eqpmnt>();
            InverseSumactNavigation = new HashSet<Lgract>();
            Invtln = new HashSet<Invtln>();
            LgrbalIdrefNavigation = new HashSet<Lgrbal>();
            LgrbalLgractNavigation = new HashSet<Lgrbal>();
            Lgrsub = new HashSet<Lgrsub>();
            Lgtnln = new HashSet<Lgtnln>();
            PaydedCrdactNavigation = new HashSet<Payded>();
            PaydedDbtactNavigation = new HashSet<Payded>();
            PaydedDbtadmNavigation = new HashSet<Payded>();
            PaydedDbtovhNavigation = new HashSet<Payded>();
            PaydedDbtshpNavigation = new HashSet<Payded>();
            PaypstEqpactNavigation = new HashSet<Paypst>();
            PaypstJobactNavigation = new HashSet<Paypst>();
            PaypstOthactNavigation = new HashSet<Paypst>();
            Pcorln = new HashSet<Pcorln>();
            Prichk = new HashSet<Prichk>();
            Pricrd = new HashSet<Pricrd>();
            Pstpyr = new HashSet<Pstpyr>();
            Rcapln = new HashSet<Rcapln>();
            Rcarln = new HashSet<Rcarln>();
            RcccrdActnumNavigation = new HashSet<Rcccrd>();
            RcccrdIssuerNavigation = new HashSet<Rcccrd>();
            Rctnln = new HashSet<Rctnln>();
            Srvlin = new HashSet<Srvlin>();
            SrvtypCshactNavigation = new HashSet<Srvtyp>();
            SrvtypDscgvnNavigation = new HashSet<Srvtyp>();
            SrvtypInvexpNavigation = new HashSet<Srvtyp>();
            SrvtypNtxincNavigation = new HashSet<Srvtyp>();
            SrvtypTaxincNavigation = new HashSet<Srvtyp>();
            Subbal = new HashSet<Subbal>();
            Timmat = new HashSet<Timmat>();
            Untbll = new HashSet<Untbll>();
        }

        public Guid Idnum { get; set; }
        public long Recnum { get; set; }
        public string Shtnme { get; set; }
        public string Lngnme { get; set; }
        public byte Subact { get; set; }
        public long? Sumact { get; set; }
        public byte? Csttyp { get; set; }
        public decimal Begbal { get; set; }
        public decimal Endbal { get; set; }
        public long Nxtchk { get; set; }
        public long Nxtdep { get; set; }
        public decimal Strbal { get; set; }
        public byte Acttyp { get; set; }
        public byte Dbtcrd { get; set; }
        public string Ntetxt { get; set; }
        public byte Jobsub { get; set; }
        public byte Iscrcd { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }
        public byte Inactv { get; set; }
        public string BnkId { get; set; }
        public long TrnId { get; set; }

        public Csttyp CsttypNavigation { get; set; }
        public Lgract SumactNavigation { get; set; }
        public Crcard CrcardRecnumNavigation { get; set; }
        public ICollection<Actpay> Actpay { get; set; }
        public ICollection<Actrec> Actrec { get; set; }
        public ICollection<Aiafrm> Aiafrm { get; set; }
        public ICollection<Apivln> Apivln { get; set; }
        public ICollection<Arivln> Arivln { get; set; }
        public ICollection<Bnkrec> Bnkrec { get; set; }
        public ICollection<CaTax> CaTaxGtclacNavigation { get; set; }
        public ICollection<CaTax> CaTaxGtpdacNavigation { get; set; }
        public ICollection<CaTax> CaTaxPtclacNavigation { get; set; }
        public ICollection<CaTax> CaTaxQtpdacNavigation { get; set; }
        public ICollection<CaTax> CaTaxRitcacNavigation { get; set; }
        public ICollection<Cmpany> CmpanyCcdactNavigation { get; set; }
        public ICollection<Cmpany> CmpanyDepactNavigation { get; set; }
        public ICollection<Cmpany> CmpanyVndeftNavigation { get; set; }
        public ICollection<Crcard> CrcardIdrefNavigation { get; set; }
        public ICollection<Crdrec> Crdrec { get; set; }
        public ICollection<Eqpmnt> EqpmntDepactNavigation { get; set; }
        public ICollection<Eqpmnt> EqpmntIntactNavigation { get; set; }
        public ICollection<Lgract> InverseSumactNavigation { get; set; }
        public ICollection<Invtln> Invtln { get; set; }
        public ICollection<Lgrbal> LgrbalIdrefNavigation { get; set; }
        public ICollection<Lgrbal> LgrbalLgractNavigation { get; set; }
        public ICollection<Lgrsub> Lgrsub { get; set; }
        public ICollection<Lgtnln> Lgtnln { get; set; }
        public ICollection<Payded> PaydedCrdactNavigation { get; set; }
        public ICollection<Payded> PaydedDbtactNavigation { get; set; }
        public ICollection<Payded> PaydedDbtadmNavigation { get; set; }
        public ICollection<Payded> PaydedDbtovhNavigation { get; set; }
        public ICollection<Payded> PaydedDbtshpNavigation { get; set; }
        public ICollection<Paypst> PaypstEqpactNavigation { get; set; }
        public ICollection<Paypst> PaypstJobactNavigation { get; set; }
        public ICollection<Paypst> PaypstOthactNavigation { get; set; }
        public ICollection<Pcorln> Pcorln { get; set; }
        public ICollection<Prichk> Prichk { get; set; }
        public ICollection<Pricrd> Pricrd { get; set; }
        public ICollection<Pstpyr> Pstpyr { get; set; }
        public ICollection<Rcapln> Rcapln { get; set; }
        public ICollection<Rcarln> Rcarln { get; set; }
        public ICollection<Rcccrd> RcccrdActnumNavigation { get; set; }
        public ICollection<Rcccrd> RcccrdIssuerNavigation { get; set; }
        public ICollection<Rctnln> Rctnln { get; set; }
        public ICollection<Srvlin> Srvlin { get; set; }
        public ICollection<Srvtyp> SrvtypCshactNavigation { get; set; }
        public ICollection<Srvtyp> SrvtypDscgvnNavigation { get; set; }
        public ICollection<Srvtyp> SrvtypInvexpNavigation { get; set; }
        public ICollection<Srvtyp> SrvtypNtxincNavigation { get; set; }
        public ICollection<Srvtyp> SrvtypTaxincNavigation { get; set; }
        public ICollection<Subbal> Subbal { get; set; }
        public ICollection<Timmat> Timmat { get; set; }
        public ICollection<Untbll> Untbll { get; set; }
    }
}
