﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Objref
    {
        public Guid Idnum { get; set; }
        public string Objref1 { get; set; }
        public string Objnam { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }
    }
}
