﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Apivln
    {
        public Guid Idnum { get; set; }
        public Guid Idref { get; set; }
        public long Recnum { get; set; }
        public int Linnum { get; set; }
        public long? Prtnum { get; set; }
        public string Prtdsc { get; set; }
        public string Alpnum { get; set; }
        public string Untdsc { get; set; }
        public decimal Linqty { get; set; }
        public decimal Linprc { get; set; }
        public decimal Extttl { get; set; }
        public decimal Hldamt { get; set; }
        public decimal Invamt { get; set; }
        public short? Taxcde { get; set; }
        public decimal Gstamt { get; set; }
        public decimal Pstamt { get; set; }
        public decimal Hstamt { get; set; }
        public long? Actnum { get; set; }
        public long? Subact { get; set; }
        public int? Invloc { get; set; }
        public string Sernum { get; set; }
        public string Usrdf1 { get; set; }
        public string Gstsbj { get; set; }
        public string Gstovr { get; set; }
        public string Pstsbj { get; set; }
        public string Pstovr { get; set; }
        public string Hstsbj { get; set; }
        public string Hstovr { get; set; }
        public string Hldovr { get; set; }
        public string Linref { get; set; }
        public long Invrec { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }

        public Lgract ActnumNavigation { get; set; }
        public Acpinv IdrefNavigation { get; set; }
        public Invloc InvlocNavigation { get; set; }
        public Tkfprt PrtnumNavigation { get; set; }
        public Acpinv RecnumNavigation { get; set; }
        public CaTax TaxcdeNavigation { get; set; }
    }
}
