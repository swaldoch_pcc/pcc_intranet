﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Ledsrc
    {
        public Ledsrc()
        {
            Reccln = new HashSet<Reccln>();
            Srvinv = new HashSet<Srvinv>();
        }

        public Guid Idnum { get; set; }
        public int Recnum { get; set; }
        public string Srcnme { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }

        public ICollection<Reccln> Reccln { get; set; }
        public ICollection<Srvinv> Srvinv { get; set; }
    }
}
