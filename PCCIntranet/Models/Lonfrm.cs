﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Lonfrm
    {
        public Lonfrm()
        {
            LonlinIdrefNavigation = new HashSet<Lonlin>();
            LonlinRecnumNavigation = new HashSet<Lonlin>();
        }

        public Guid Idnum { get; set; }
        public long Recnum { get; set; }
        public long Jobnum { get; set; }
        public long Phsnum { get; set; }
        public long Lndnum { get; set; }
        public string Lonnum { get; set; }
        public int Crtnum { get; set; }
        public DateTime? Period { get; set; }
        public string Dscrpt { get; set; }
        public DateTime? Subdte { get; set; }
        public DateTime? Aprdte { get; set; }
        public string Usrdf1 { get; set; }
        public string Usrdf2 { get; set; }
        public decimal Schamt { get; set; }
        public decimal Chgdte { get; set; }
        public decimal Ctcamt { get; set; }
        public decimal Prvamt { get; set; }
        public decimal Curamt { get; set; }
        public decimal Pctcmp { get; set; }
        public decimal Ttlcmp { get; set; }
        public decimal Balnce { get; set; }
        public decimal Tocomp { get; set; }
        public byte Status { get; set; }
        public byte Hotlst { get; set; }
        public string Ntetxt { get; set; }
        public byte Cmbphs { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }

        public Actrec JobnumNavigation { get; set; }
        public Jobphs Jobphs { get; set; }
        public Actpay LndnumNavigation { get; set; }
        public LonfU LonfU { get; set; }
        public ICollection<Lonlin> LonlinIdrefNavigation { get; set; }
        public ICollection<Lonlin> LonlinRecnumNavigation { get; set; }
    }
}
