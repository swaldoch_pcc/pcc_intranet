﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class SrviU
    {
        public Guid Idnum { get; set; }

        public Srvinv IdnumNavigation { get; set; }
    }
}
