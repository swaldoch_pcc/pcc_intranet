﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class PlnrU
    {
        public Guid Idnum { get; set; }

        public Plnrcd IdnumNavigation { get; set; }
    }
}
