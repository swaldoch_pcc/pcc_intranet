﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Grplst
    {
        public Grplst()
        {
            Cmpany = new HashSet<Cmpany>();
            Dshscr = new HashSet<Dshscr>();
            Fldacs = new HashSet<Fldacs>();
            Mnuacs = new HashSet<Mnuacs>();
            UsrlstGroup1Navigation = new HashSet<Usrlst>();
            UsrlstGroup2Navigation = new HashSet<Usrlst>();
            UsrlstGroup3Navigation = new HashSet<Usrlst>();
            UsrlstGroup4Navigation = new HashSet<Usrlst>();
            UsrlstGroup5Navigation = new HashSet<Usrlst>();
        }

        public Guid Idnum { get; set; }
        public byte Recnum { get; set; }
        public string Grpnme { get; set; }
        public string Pmsave { get; set; }
        public string Pmdlte { get; set; }
        public string Pmvoid { get; set; }
        public string Chgprd { get; set; }
        public string Prtchk { get; set; }
        public string Ntetxt { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }

        public ICollection<Cmpany> Cmpany { get; set; }
        public ICollection<Dshscr> Dshscr { get; set; }
        public ICollection<Fldacs> Fldacs { get; set; }
        public ICollection<Mnuacs> Mnuacs { get; set; }
        public ICollection<Usrlst> UsrlstGroup1Navigation { get; set; }
        public ICollection<Usrlst> UsrlstGroup2Navigation { get; set; }
        public ICollection<Usrlst> UsrlstGroup3Navigation { get; set; }
        public ICollection<Usrlst> UsrlstGroup4Navigation { get; set; }
        public ICollection<Usrlst> UsrlstGroup5Navigation { get; set; }
    }
}
