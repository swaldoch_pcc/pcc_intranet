﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Vehicles
    {
        public int Id { get; set; }
        public string Company { get; set; }
        public string Mfg { get; set; }
        public string Model { get; set; }
        public int? Year { get; set; }
        public string SerialNum { get; set; }
        public string Description { get; set; }
        public string Tag { get; set; }
        public string TagState { get; set; }
        public string Title { get; set; }
        public string TitleState { get; set; }
        public DateTime? PurchaseDate { get; set; }
        public string PrimaryUser { get; set; }
        public DateTime? IssueDate { get; set; }
        public DateTime? ReturnDate { get; set; }
        public int? IssueMileage { get; set; }
        public int? ReturnMileage { get; set; }
        public int? CurrentMileage { get; set; }
        public DateTime? CurrentMileageDate { get; set; }
        public string Color { get; set; }
        public string BodyStyle { get; set; }
        public string DriveTrain { get; set; }
        public double? EngineSize { get; set; }
        public string EngineType { get; set; }
        public string FuelType { get; set; }
        public DateTime? TagExpiration { get; set; }
        public decimal? OrigPurchasePrice { get; set; }
        public DateTime? SoldDate { get; set; }
        public decimal? SalePrice { get; set; }
        public string Config { get; set; }
    }
}
