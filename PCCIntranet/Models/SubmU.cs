﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class SubmU
    {
        public Guid Idnum { get; set; }
        public string Udf001 { get; set; }

        public Submtl IdnumNavigation { get; set; }
    }
}
