﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Ptotkf
    {
        public Ptotkf()
        {
            GlbvarIdrefNavigation = new HashSet<Glbvar>();
            GlbvarRecnumNavigation = new HashSet<Glbvar>();
            TkflinIdrefNavigation = new HashSet<Tkflin>();
            TkflinRecnumNavigation = new HashSet<Tkflin>();
        }

        public Guid Idnum { get; set; }
        public long Recnum { get; set; }
        public decimal Matovh { get; set; }
        public decimal Matpft { get; set; }
        public decimal Labovh { get; set; }
        public decimal Labpft { get; set; }
        public decimal Eqpovh { get; set; }
        public decimal Eqppft { get; set; }
        public decimal Subovh { get; set; }
        public decimal Subpft { get; set; }
        public decimal Otrovh { get; set; }
        public decimal Otrpft { get; set; }
        public decimal Cs6ovh { get; set; }
        public decimal Cs6pft { get; set; }
        public decimal Cs7ovh { get; set; }
        public decimal Cs7pft { get; set; }
        public decimal Cs8ovh { get; set; }
        public decimal Cs8pft { get; set; }
        public decimal Cs9ovh { get; set; }
        public decimal Cs9pft { get; set; }
        public DateTime? Edtdte { get; set; }
        public string Usrnme { get; set; }
        public byte Prtprc { get; set; }
        public decimal Paysbj { get; set; }
        public decimal Subsbj { get; set; }
        public decimal Bidsbj { get; set; }
        public decimal Taxsbj { get; set; }
        public long Bndlt1 { get; set; }
        public long Bndlt2 { get; set; }
        public long Bndlt3 { get; set; }
        public decimal Payrte { get; set; }
        public decimal Subrte { get; set; }
        public decimal Bidrte { get; set; }
        public decimal Taxrte { get; set; }
        public decimal Lt1rte { get; set; }
        public decimal Lt2rte { get; set; }
        public decimal Lt3rte { get; set; }
        public decimal Payamt { get; set; }
        public decimal Subamt { get; set; }
        public decimal Bidamt { get; set; }
        public decimal Taxamt { get; set; }
        public decimal Lt1amt { get; set; }
        public decimal Lt2amt { get; set; }
        public decimal Lt3amt { get; set; }
        public byte? Instyp { get; set; }
        public decimal? Inscde { get; set; }
        public byte? Taxtyp { get; set; }
        public decimal? Taxcde { get; set; }
        public byte? Bndtyp { get; set; }
        public decimal? Bndcde { get; set; }
        public byte Insinc { get; set; }
        public byte Taxinc { get; set; }
        public byte Bndinc { get; set; }
        public long Insitm { get; set; }
        public long Insphs { get; set; }
        public long Taxitm { get; set; }
        public long Taxphs { get; set; }
        public long Bnditm { get; set; }
        public long Bndphs { get; set; }
        public string Ntetxt { get; set; }
        public string Edtnme { get; set; }
        public byte Mrgmrk { get; set; }
        public decimal Mrgovr { get; set; }
        public decimal Mrkrte { get; set; }
        public decimal Mrkdlr { get; set; }
        public decimal Bidovr { get; set; }
        public byte Lckbid { get; set; }
        public string Wrkord { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }

        public Biditm Biditm { get; set; }
        public Biditm Biditm1 { get; set; }
        public Biditm BiditmNavigation { get; set; }
        public Cstcde BndcdeNavigation { get; set; }
        public Csttyp BndtypNavigation { get; set; }
        public Cstcde InscdeNavigation { get; set; }
        public Csttyp InstypNavigation { get; set; }
        public Jobphs Jobphs { get; set; }
        public Jobphs Jobphs1 { get; set; }
        public Jobphs JobphsNavigation { get; set; }
        public Actrec RecnumNavigation { get; set; }
        public Cstcde TaxcdeNavigation { get; set; }
        public Csttyp TaxtypNavigation { get; set; }
        public PtotU PtotU { get; set; }
        public ICollection<Glbvar> GlbvarIdrefNavigation { get; set; }
        public ICollection<Glbvar> GlbvarRecnumNavigation { get; set; }
        public ICollection<Tkflin> TkflinIdrefNavigation { get; set; }
        public ICollection<Tkflin> TkflinRecnumNavigation { get; set; }
    }
}
