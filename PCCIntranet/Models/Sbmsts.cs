﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Sbmsts
    {
        public Sbmsts()
        {
            Sbmtln = new HashSet<Sbmtln>();
        }

        public Guid Idnum { get; set; }
        public byte Recnum { get; set; }
        public string Stsnme { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }

        public ICollection<Sbmtln> Sbmtln { get; set; }
    }
}
