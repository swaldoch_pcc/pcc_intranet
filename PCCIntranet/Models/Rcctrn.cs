﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Rcctrn
    {
        public Rcctrn()
        {
            RcceqpIdrefNavigation = new HashSet<Rcceqp>();
            RcceqpRecnumNavigation = new HashSet<Rcceqp>();
            RccjobIdrefNavigation = new HashSet<Rccjob>();
            RccjobRecnumNavigation = new HashSet<Rccjob>();
            RctnlnIdrefNavigation = new HashSet<Rctnln>();
            RctnlnRecnumNavigation = new HashSet<Rctnln>();
        }

        public Guid Idnum { get; set; }
        public long Recnum { get; set; }
        public string Trnnum { get; set; }
        public DateTime? Trndte { get; set; }
        public string Prddys { get; set; }
        public string Dscrpt { get; set; }
        public string Usrdf1 { get; set; }
        public string Usrdf2 { get; set; }
        public long? Vndnum { get; set; }
        public string Payee1 { get; set; }
        public string Payee2 { get; set; }
        public string Addrs1 { get; set; }
        public string Addrs2 { get; set; }
        public string Ctynme { get; set; }
        public string State { get; set; }
        public string Usrnme { get; set; }
        public DateTime? Entdte { get; set; }
        public string Ntetxt { get; set; }
        public string Zipcde { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }
        public byte Bnkcat { get; set; }

        public Actpay VndnumNavigation { get; set; }
        public ICollection<Rcceqp> RcceqpIdrefNavigation { get; set; }
        public ICollection<Rcceqp> RcceqpRecnumNavigation { get; set; }
        public ICollection<Rccjob> RccjobIdrefNavigation { get; set; }
        public ICollection<Rccjob> RccjobRecnumNavigation { get; set; }
        public ICollection<Rctnln> RctnlnIdrefNavigation { get; set; }
        public ICollection<Rctnln> RctnlnRecnumNavigation { get; set; }
    }
}
