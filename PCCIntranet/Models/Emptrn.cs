﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Emptrn
    {
        public Guid Idnum { get; set; }
        public Guid Idref { get; set; }
        public long Empnum { get; set; }
        public short Typnum { get; set; }
        public DateTime? Lstdte { get; set; }
        public string Trcycl { get; set; }
        public DateTime? Nxtdte { get; set; }
        public string Ntetxt { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }

        public Employ EmpnumNavigation { get; set; }
        public Employ IdrefNavigation { get; set; }
        public Trntyp TypnumNavigation { get; set; }
    }
}
