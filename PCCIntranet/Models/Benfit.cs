﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Benfit
    {
        public Guid Idnum { get; set; }
        public Guid Idref { get; set; }
        public int Paygrp { get; set; }
        public short Dednum { get; set; }
        public decimal Dedrte { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }

        public Payded DednumNavigation { get; set; }
        public Paygrp IdrefNavigation { get; set; }
        public Paygrp PaygrpNavigation { get; set; }
    }
}
