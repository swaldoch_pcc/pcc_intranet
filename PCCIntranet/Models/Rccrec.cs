﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Rccrec
    {
        public Rccrec()
        {
            RcareqIdrefNavigation = new HashSet<Rcareq>();
            RcareqRecnumNavigation = new HashSet<Rcareq>();
            RcarjbIdrefNavigation = new HashSet<Rcarjb>();
            RcarjbRecnumNavigation = new HashSet<Rcarjb>();
            RcarlnIdrefNavigation = new HashSet<Rcarln>();
            RcarlnRecnumNavigation = new HashSet<Rcarln>();
        }

        public Guid Idnum { get; set; }
        public long Recnum { get; set; }
        public long Jobnum { get; set; }
        public long Phsnum { get; set; }
        public string Dscrpt { get; set; }
        public int? Taxdst { get; set; }
        public string Duetrm { get; set; }
        public string Dsctrm { get; set; }
        public string Rectim { get; set; }
        public DateTime? Nxtdte { get; set; }
        public byte Invtyp { get; set; }
        public byte Status { get; set; }
        public string Usrdf1 { get; set; }
        public string Usrdf2 { get; set; }
        public decimal Dscavl { get; set; }
        public decimal Retain { get; set; }
        public decimal Slstax { get; set; }
        public decimal Invttl { get; set; }
        public decimal Invnet { get; set; }
        public decimal Taxabl { get; set; }
        public decimal Nontax { get; set; }
        public DateTime? Edtdte { get; set; }
        public string Usrnme { get; set; }
        public byte Hotlst { get; set; }
        public string Ntetxt { get; set; }
        public decimal Subttl { get; set; }
        public decimal Hldamt { get; set; }
        public decimal Pstsbj { get; set; }
        public decimal Gstsbj { get; set; }
        public decimal Hstsbj { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }

        public Actrec JobnumNavigation { get; set; }
        public Jobphs Jobphs { get; set; }
        public Taxdst TaxdstNavigation { get; set; }
        public ICollection<Rcareq> RcareqIdrefNavigation { get; set; }
        public ICollection<Rcareq> RcareqRecnumNavigation { get; set; }
        public ICollection<Rcarjb> RcarjbIdrefNavigation { get; set; }
        public ICollection<Rcarjb> RcarjbRecnumNavigation { get; set; }
        public ICollection<Rcarln> RcarlnIdrefNavigation { get; set; }
        public ICollection<Rcarln> RcarlnRecnumNavigation { get; set; }
    }
}
