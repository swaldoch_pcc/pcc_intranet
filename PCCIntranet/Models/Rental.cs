﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Rental
    {
        public string RentalId { get; set; }
        public string Pcid { get; set; }
        public string DateIn { get; set; }
        public string DateOut { get; set; }
        public string EmployeeId { get; set; }
        public string JobId { get; set; }
    }
}
