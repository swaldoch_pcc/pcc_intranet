﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Eqprvw
    {
        public Guid Idnum { get; set; }
        public long Recnum { get; set; }
        public long Eqpnum { get; set; }
        public string Trnnum { get; set; }
        public string Dscrpt { get; set; }
        public DateTime? Trndte { get; set; }
        public DateTime? Entdte { get; set; }
        public byte Actprd { get; set; }
        public byte Srcnum { get; set; }
        public byte Status { get; set; }
        public decimal Cstcde { get; set; }
        public long? Eqpmnt { get; set; }
        public long? Jobnum { get; set; }
        public byte Eqpunt { get; set; }
        public byte Eqptyp { get; set; }
        public decimal Eqpqty { get; set; }
        public decimal Rvnamt { get; set; }
        public long Lgrrec { get; set; }
        public string Ntetxt { get; set; }
        public string Usrnme { get; set; }
        public short Postyr { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }

        public Cstcde CstcdeNavigation { get; set; }
        public Eqpmnt EqpmntNavigation { get; set; }
        public Eqpmnt EqpnumNavigation { get; set; }
        public Actrec JobnumNavigation { get; set; }
        public Source SrcnumNavigation { get; set; }
    }
}
