﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class PrmcU
    {
        public Guid Idnum { get; set; }

        public Prmchg IdnumNavigation { get; set; }
    }
}
