﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Loctax
    {
        public Loctax()
        {
            Actrec = new HashSet<Actrec>();
            Dlypyr = new HashSet<Dlypyr>();
            EmployLoctaxNavigation = new HashSet<Employ>();
            EmployLocwrkNavigation = new HashSet<Employ>();
            Payded = new HashSet<Payded>();
            Tmcdln = new HashSet<Tmcdln>();
            TmcdtxLocaleNavigation = new HashSet<Tmcdtx>();
            TmcdtxReslocNavigation = new HashSet<Tmcdtx>();
        }

        public Guid Idnum { get; set; }
        public int Recnum { get; set; }
        public string Dscrpt { get; set; }
        public decimal Resrte { get; set; }
        public decimal Nonrte { get; set; }
        public string Inctax { get; set; }
        public string Kckbck { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }

        public ICollection<Actrec> Actrec { get; set; }
        public ICollection<Dlypyr> Dlypyr { get; set; }
        public ICollection<Employ> EmployLoctaxNavigation { get; set; }
        public ICollection<Employ> EmployLocwrkNavigation { get; set; }
        public ICollection<Payded> Payded { get; set; }
        public ICollection<Tmcdln> Tmcdln { get; set; }
        public ICollection<Tmcdtx> TmcdtxLocaleNavigation { get; set; }
        public ICollection<Tmcdtx> TmcdtxReslocNavigation { get; set; }
    }
}
