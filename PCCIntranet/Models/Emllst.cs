﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Emllst
    {
        public Guid Idnum { get; set; }
        public long EntId { get; set; }
        public string Enttyp { get; set; }
        public string EMail { get; set; }
        public long RptId { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }

        public Rptscd Rpt { get; set; }
    }
}
