﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Eqptyp
    {
        public Eqptyp()
        {
            Eqpmnt = new HashSet<Eqpmnt>();
            Tmeqln = new HashSet<Tmeqln>();
        }

        public Guid Idnum { get; set; }
        public short Recnum { get; set; }
        public string Typnme { get; set; }
        public string Ntetxt { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }

        public ICollection<Eqpmnt> Eqpmnt { get; set; }
        public ICollection<Tmeqln> Tmeqln { get; set; }
    }
}
