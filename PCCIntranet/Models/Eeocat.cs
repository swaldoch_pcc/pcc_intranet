﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Eeocat
    {
        public Eeocat()
        {
            Paypst = new HashSet<Paypst>();
        }

        public Guid Idnum { get; set; }
        public byte Recnum { get; set; }
        public string Catnme { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }

        public ICollection<Paypst> Paypst { get; set; }
    }
}
