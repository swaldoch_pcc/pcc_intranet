﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Mnudat
    {
        public Guid Idnum { get; set; }
        public byte Menu1 { get; set; }
        public byte Menu2 { get; set; }
        public byte Menu3 { get; set; }
        public string Mnudsc { get; set; }
        public string Candsc { get; set; }
        public byte Mnutyp { get; set; }
        public byte Jobmnu { get; set; }
        public byte Jobind { get; set; }
        public string Jobttl { get; set; }
        public byte Rptmnu { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }
    }
}
