﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class WorkDays
    {
        public string JobsiteId { get; set; }
        public string Jobsite { get; set; }
        public string AllJobs { get; set; }
        public string SameEveryYear { get; set; }
        public string Title { get; set; }
        public string Notes { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string Days { get; set; }
        public string Category { get; set; }
        public string Type { get; set; }
    }
}
