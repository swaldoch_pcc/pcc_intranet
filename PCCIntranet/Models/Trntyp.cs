﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Trntyp
    {
        public Trntyp()
        {
            Emptrn = new HashSet<Emptrn>();
            Fdrpln = new HashSet<Fdrpln>();
        }

        public Guid Idnum { get; set; }
        public short Recnum { get; set; }
        public string Typnme { get; set; }
        public string Trcycl { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }

        public ICollection<Emptrn> Emptrn { get; set; }
        public ICollection<Fdrpln> Fdrpln { get; set; }
    }
}
