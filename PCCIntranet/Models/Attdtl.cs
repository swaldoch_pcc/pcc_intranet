﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Attdtl
    {
        public Guid Idnum { get; set; }
        public Guid Idref { get; set; }
        public long Recnum { get; set; }
        public string Target { get; set; }
        public string Dscrpt { get; set; }
        public string Attusr { get; set; }
        public DateTime? Attdte { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }

        public Atchmt IdrefNavigation { get; set; }
        public Atchmt RecnumNavigation { get; set; }
    }
}
