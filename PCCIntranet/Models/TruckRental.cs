﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class TruckRental
    {
        public Guid Id { get; set; }
        public string Itemid { get; set; }
        public string Job { get; set; }
        public string User { get; set; }
        public string Comments { get; set; }
        public DateTime? Issuedate { get; set; }
        public DateTime? Returndate { get; set; }
        public int? Issuemileage { get; set; }
        public int? Returnmileage { get; set; }
        public decimal? Issuefuel { get; set; }
        public decimal? Returnfuel { get; set; }
        public string Condition { get; set; }
        public string Purpose { get; set; }
    }
}
