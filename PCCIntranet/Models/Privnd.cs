﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Privnd
    {
        public Guid Idnum { get; set; }
        public long Recnum { get; set; }
        public long Vndnum { get; set; }
        public string Trnnum { get; set; }
        public DateTime? Trndte { get; set; }
        public string Dscrpt { get; set; }
        public decimal Amt199 { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }

        public Actpay VndnumNavigation { get; set; }
    }
}
