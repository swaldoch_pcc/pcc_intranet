﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Schedules
    {
        public string JobsiteId { get; set; }
        public string Jobsite { get; set; }
        public string ScheduleId { get; set; }
        public string Title { get; set; }
        public string MarkedComplete { get; set; }
        public string Start { get; set; }
        public string Finish { get; set; }
        public string DurationDays { get; set; }
        public string AllNotes { get; set; }
        public string InternalNotes { get; set; }
        public string SubNotes { get; set; }
        public string OwnerNotes { get; set; }
        public string Phase { get; set; }
        public string Tag { get; set; }
        public string ConfirmationDate { get; set; }
        public string Reminder { get; set; }
        public string AssignedTo { get; set; }
    }
}
