﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Pchord
    {
        public Pchord()
        {
            PcorlnIdrefNavigation = new HashSet<Pcorln>();
            PcorlnRecnumNavigation = new HashSet<Pcorln>();
        }

        public Guid Idnum { get; set; }
        public long Recnum { get; set; }
        public string Ordnum { get; set; }
        public DateTime? Orddte { get; set; }
        public long Vndnum { get; set; }
        public string Attion { get; set; }
        public long? Odrdby { get; set; }
        public long? Jobnum { get; set; }
        public long? Phsnum { get; set; }
        public long? Eqpmnt { get; set; }
        public string Dscrpt { get; set; }
        public string Docnum { get; set; }
        public byte Docsrc { get; set; }
        public int? Taxdst { get; set; }
        public DateTime? Appdte { get; set; }
        public DateTime? Schdte { get; set; }
        public DateTime? Deldte { get; set; }
        public string Delvia { get; set; }
        public string Ordtrm { get; set; }
        public byte? Ordtyp { get; set; }
        public byte Status { get; set; }
        public string Addrs1 { get; set; }
        public string Addrs2 { get; set; }
        public string Ctynme { get; set; }
        public string State { get; set; }
        public string Zipcde { get; set; }
        public string Usrdf1 { get; set; }
        public string Usrdf2 { get; set; }
        public decimal Rcvdte { get; set; }
        public decimal Currnt { get; set; }
        public decimal Cancel { get; set; }
        public decimal Subttl { get; set; }
        public decimal Slstax { get; set; }
        public decimal Pchttl { get; set; }
        public decimal Pchbal { get; set; }
        public DateTime? Entdte { get; set; }
        public string Usrnme { get; set; }
        public byte Hotlst { get; set; }
        public byte Lckedt { get; set; }
        public string Ntetxt { get; set; }
        public decimal? Tsknum { get; set; }
        public DateTime? Issdat { get; set; }
        public long Issbch { get; set; }
        public decimal Gstamt { get; set; }
        public decimal Pstamt { get; set; }
        public decimal Hstamt { get; set; }
        public decimal Expcst { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }

        public Eqpmnt EqpmntNavigation { get; set; }
        public Actrec JobnumNavigation { get; set; }
        public Jobphs Jobphs { get; set; }
        public Employ OdrdbyNavigation { get; set; }
        public Pchtyp OrdtypNavigation { get; set; }
        public Taxdst TaxdstNavigation { get; set; }
        public Schtsk TsknumNavigation { get; set; }
        public Actpay VndnumNavigation { get; set; }
        public PchoU PchoU { get; set; }
        public ICollection<Pcorln> PcorlnIdrefNavigation { get; set; }
        public ICollection<Pcorln> PcorlnRecnumNavigation { get; set; }
    }
}
