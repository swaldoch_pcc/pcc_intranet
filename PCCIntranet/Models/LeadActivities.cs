﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class LeadActivities
    {
        public int LeadId { get; set; }
        public int ActivityId { get; set; }
        public string Status { get; set; }
        public string Type { get; set; }
        public string OpportunityTitle { get; set; }
        public string Employee { get; set; }
        public DateTime? ContactDate { get; set; }
        public string ActivityNotes { get; set; }
        public string LastContacted { get; set; }
    }
}
