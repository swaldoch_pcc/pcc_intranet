﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class ModelNum
    {
        public string ModelId { get; set; }
        public string ModelNumber { get; set; }
        public string DescriptionId { get; set; }
        public string Mfgid { get; set; }
    }
}
