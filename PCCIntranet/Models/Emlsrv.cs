﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Emlsrv
    {
        public Guid Idnum { get; set; }
        public string Usrnme { get; set; }
        public byte Source { get; set; }
        public byte Method { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }
    }
}
