﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Taxent
    {
        public Taxent()
        {
            Artxdt = new HashSet<Artxdt>();
            Srtxdt = new HashSet<Srtxdt>();
            TaxdstEntty1Navigation = new HashSet<Taxdst>();
            TaxdstEntty2Navigation = new HashSet<Taxdst>();
            TaxdstEntty3Navigation = new HashSet<Taxdst>();
            TaxdstEntty4Navigation = new HashSet<Taxdst>();
            TaxdstEntty5Navigation = new HashSet<Taxdst>();
        }

        public Guid Idnum { get; set; }
        public int Recnum { get; set; }
        public string Entnme { get; set; }
        public string Paynme { get; set; }
        public string Addrs1 { get; set; }
        public string Addrs2 { get; set; }
        public string Ctynme { get; set; }
        public string State { get; set; }
        public string Zipcde { get; set; }
        public decimal Taxrt1 { get; set; }
        public decimal Limit1 { get; set; }
        public byte Lmtyp1 { get; set; }
        public string Ntetxt { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }

        public ICollection<Artxdt> Artxdt { get; set; }
        public ICollection<Srtxdt> Srtxdt { get; set; }
        public ICollection<Taxdst> TaxdstEntty1Navigation { get; set; }
        public ICollection<Taxdst> TaxdstEntty2Navigation { get; set; }
        public ICollection<Taxdst> TaxdstEntty3Navigation { get; set; }
        public ICollection<Taxdst> TaxdstEntty4Navigation { get; set; }
        public ICollection<Taxdst> TaxdstEntty5Navigation { get; set; }
    }
}
