﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Labor
    {
        public Guid Guid { get; set; }
        public int JobsiteId { get; set; }
        public string Jobsite { get; set; }
        public string ProjectManager { get; set; }
        public DateTime? ProjectedStart { get; set; }
        public DateTime? ActualStart { get; set; }
        public DateTime? ProjectedCompletion { get; set; }
        public DateTime? ActualCompletion { get; set; }
        public string Superintendent { get; set; }
        public string Labor1 { get; set; }
        public DateTime? Timestamp { get; set; }
    }
}
