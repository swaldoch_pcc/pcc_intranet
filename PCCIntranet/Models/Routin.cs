﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Routin
    {
        public Guid Idnum { get; set; }
        public long Rourec { get; set; }
        public int Linnum { get; set; }
        public string Routbl { get; set; }
        public string Routto { get; set; }
        public string Dscrpt { get; set; }
        public DateTime? Sentdt { get; set; }
        public DateTime? Needdt { get; set; }
        public DateTime? Retdte { get; set; }
        public string Ntetxt { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }
    }
}
