﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Fdrpun
    {
        public Guid Idnum { get; set; }
        public Guid Idref { get; set; }
        public long Recnum { get; set; }
        public int Linnum { get; set; }
        public long? Itmnum { get; set; }
        public string Itmcde { get; set; }
        public string Untdsc { get; set; }
        public decimal Cmptdy { get; set; }
        public long? Jobnum { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }

        public Biditm Biditm { get; set; }
        public Fldrpt IdrefNavigation { get; set; }
        public Fldrpt RecnumNavigation { get; set; }
    }
}
