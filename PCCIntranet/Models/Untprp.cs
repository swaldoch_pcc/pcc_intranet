﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Untprp
    {
        public Untprp()
        {
            UnprlnIdrefNavigation = new HashSet<Unprln>();
            UnprlnRecnumNavigation = new HashSet<Unprln>();
        }

        public Guid Idnum { get; set; }
        public long Recnum { get; set; }
        public byte Lckedt { get; set; }
        public DateTime? Edtdte { get; set; }
        public string Usrnme { get; set; }
        public decimal Ttlcst { get; set; }
        public byte Hotlst { get; set; }
        public string Ntetxt { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }

        public Actrec RecnumNavigation { get; set; }
        public UntpU UntpU { get; set; }
        public ICollection<Unprln> UnprlnIdrefNavigation { get; set; }
        public ICollection<Unprln> UnprlnRecnumNavigation { get; set; }
    }
}
