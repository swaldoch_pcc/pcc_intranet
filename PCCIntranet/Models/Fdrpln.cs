﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Fdrpln
    {
        public Guid Idnum { get; set; }
        public Guid Idref { get; set; }
        public long Recnum { get; set; }
        public int Linnum { get; set; }
        public long? Empnum { get; set; }
        public decimal? Cstcde { get; set; }
        public byte Paytyp { get; set; }
        public int? Paygrp { get; set; }
        public decimal Hrspce { get; set; }
        public string Usrdf1 { get; set; }
        public string Ntetxt { get; set; }
        public short? Absnce { get; set; }
        public short? Reqtrn { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }

        public Empabs AbsnceNavigation { get; set; }
        public Cstcde CstcdeNavigation { get; set; }
        public Employ EmpnumNavigation { get; set; }
        public Fldrpt IdrefNavigation { get; set; }
        public Paygrp PaygrpNavigation { get; set; }
        public Fldrpt RecnumNavigation { get; set; }
        public Trntyp ReqtrnNavigation { get; set; }
    }
}
