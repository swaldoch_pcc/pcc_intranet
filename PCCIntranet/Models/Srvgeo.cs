﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Srvgeo
    {
        public Srvgeo()
        {
            Reccln = new HashSet<Reccln>();
            Srvinv = new HashSet<Srvinv>();
            Srvloc = new HashSet<Srvloc>();
        }

        public Guid Idnum { get; set; }
        public short Recnum { get; set; }
        public string Dscrpt { get; set; }
        public string Geoclr { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }

        public ICollection<Reccln> Reccln { get; set; }
        public ICollection<Srvinv> Srvinv { get; set; }
        public ICollection<Srvloc> Srvloc { get; set; }
    }
}
