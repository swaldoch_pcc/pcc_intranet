﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Fldlib
    {
        public Guid Idnum { get; set; }
        public string Tblnam { get; set; }
        public short Fldnum { get; set; }
        public string Fldnme { get; set; }
        public string Flddsc { get; set; }
        public string Candsc { get; set; }
        public string Fldtyp { get; set; }
        public short Fldlen { get; set; }
        public short Flddec { get; set; }
        public long Lowval { get; set; }
        public long Hghval { get; set; }
        public string Relfld { get; set; }
        public byte Fldreq { get; set; }
        public byte Wndbtn { get; set; }
        public byte Shtbtn { get; set; }
        public byte Fndbtn { get; set; }
        public byte Qwklst { get; set; }
        public byte Pcklst { get; set; }
        public byte Outbtn { get; set; }
        public byte Usredt { get; set; }
        public byte Grddsp { get; set; }
        public byte Sepdsc { get; set; }
        public byte Grdttl { get; set; }
        public short Scnrel { get; set; }
        public byte Dsply0 { get; set; }
        public byte Dspdsc { get; set; }
        public byte Requnq { get; set; }
        public byte Iscryp { get; set; }
        public string Defalt { get; set; }
        public string Tbldsc { get; set; }
        public string Xmltag { get; set; }
        public string Canxml { get; set; }
        public byte Candrl { get; set; }
        public string Drlinc { get; set; }
        public byte Rptmn1 { get; set; }
        public byte Rptmn2 { get; set; }
        public byte Rptmn3 { get; set; }
        public byte Intrnl { get; set; }
        public string Prompt { get; set; }
        public string Canprm { get; set; }
        public byte Natkey { get; set; }
        public byte Recdsc { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }
    }
}
