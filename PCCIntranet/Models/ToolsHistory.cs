﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class ToolsHistory
    {
        public Guid ToolId { get; set; }
        public int? Pcid { get; set; }
        public string Pcnum { get; set; }
        public string SerialNum { get; set; }
        public DateTime? DatePurchased { get; set; }
        public DateTime? DateDisposed { get; set; }
        public string Disposition { get; set; }
        public string Tagnum { get; set; }
        public string Title { get; set; }
        public string Color { get; set; }
        public int? Yearmodel { get; set; }
        public string Location { get; set; }
        public string Condition { get; set; }
        public string Modelnum { get; set; }
        public string Mfg { get; set; }
        public string Description { get; set; }
    }
}
