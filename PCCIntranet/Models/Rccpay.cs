﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Rccpay
    {
        public Rccpay()
        {
            RcapeqIdrefNavigation = new HashSet<Rcapeq>();
            RcapeqRecnumNavigation = new HashSet<Rcapeq>();
            RcapjbIdrefNavigation = new HashSet<Rcapjb>();
            RcapjbRecnumNavigation = new HashSet<Rcapjb>();
            RcaplnIdrefNavigation = new HashSet<Rcapln>();
            RcaplnRecnumNavigation = new HashSet<Rcapln>();
        }

        public Guid Idnum { get; set; }
        public long Recnum { get; set; }
        public string Invnum { get; set; }
        public long Vndnum { get; set; }
        public long? Jobnum { get; set; }
        public long? Phsnum { get; set; }
        public string Dscrpt { get; set; }
        public string Duetrm { get; set; }
        public string Dsctrm { get; set; }
        public string Rectim { get; set; }
        public DateTime? Nxtdte { get; set; }
        public byte Invtyp { get; set; }
        public byte Status { get; set; }
        public string Usrdf1 { get; set; }
        public string Usrdf2 { get; set; }
        public decimal Dscavl { get; set; }
        public decimal Retain { get; set; }
        public decimal Setpay { get; set; }
        public decimal Invttl { get; set; }
        public decimal Invnet { get; set; }
        public DateTime? Edtdte { get; set; }
        public string Usrnme { get; set; }
        public int Btcnum { get; set; }
        public byte Hotlst { get; set; }
        public string Ntetxt { get; set; }
        public decimal Subttl { get; set; }
        public decimal Hldamt { get; set; }
        public decimal Gstamt { get; set; }
        public decimal Pstamt { get; set; }
        public decimal Hstamt { get; set; }
        public decimal Slstax { get; set; }
        public short? Taxcde { get; set; }
        public decimal Rcpamt { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }

        public Actrec JobnumNavigation { get; set; }
        public Jobphs Jobphs { get; set; }
        public CaTax TaxcdeNavigation { get; set; }
        public Actpay VndnumNavigation { get; set; }
        public ICollection<Rcapeq> RcapeqIdrefNavigation { get; set; }
        public ICollection<Rcapeq> RcapeqRecnumNavigation { get; set; }
        public ICollection<Rcapjb> RcapjbIdrefNavigation { get; set; }
        public ICollection<Rcapjb> RcapjbRecnumNavigation { get; set; }
        public ICollection<Rcapln> RcaplnIdrefNavigation { get; set; }
        public ICollection<Rcapln> RcaplnRecnumNavigation { get; set; }
    }
}
