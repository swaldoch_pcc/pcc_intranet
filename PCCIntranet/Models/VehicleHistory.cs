﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class VehicleHistory
    {
        public Guid HistoryId { get; set; }
        public string SerialNum { get; set; }
        public int? User { get; set; }
        public DateTime? IssueDate { get; set; }
        public DateTime? ReturnDate { get; set; }
        public Guid? ConditionReportId { get; set; }
        public int? MileageOut { get; set; }
        public int? MileageIn { get; set; }
        public string JobNum { get; set; }
    }
}
