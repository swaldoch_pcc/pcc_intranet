﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Empqtd
    {
        public Guid Idnum { get; set; }
        public Guid Idref { get; set; }
        public long Recnum { get; set; }
        public short Clcnum { get; set; }
        public string Active { get; set; }
        public decimal Dedrte { get; set; }
        public decimal Dedmax { get; set; }
        public byte Mrtsts { get; set; }
        public short Allows { get; set; }
        public short Addall { get; set; }
        public byte Tblnum { get; set; }
        public decimal Addste { get; set; }
        public decimal Offset { get; set; }
        public byte Td1cde { get; set; }
        public decimal Othcrd { get; set; }
        public byte Dsbldp { get; set; }
        public decimal Fstqtr { get; set; }
        public decimal Scdqtr { get; set; }
        public decimal Thdqtr { get; set; }
        public decimal Fthqtr { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }

        public Payded ClcnumNavigation { get; set; }
        public Employ IdrefNavigation { get; set; }
        public Employ RecnumNavigation { get; set; }
    }
}
