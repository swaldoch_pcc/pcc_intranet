﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Prmchg
    {
        public Prmchg()
        {
            PmcglnIdrefNavigation = new HashSet<Pmcgln>();
            PmcglnRecnumNavigation = new HashSet<Pmcgln>();
            SbcglnIdrefNavigation = new HashSet<Sbcgln>();
            SbcglnRecnumNavigation = new HashSet<Sbcgln>();
        }

        public Guid Idnum { get; set; }
        public long Recnum { get; set; }
        public string Chgnum { get; set; }
        public DateTime? Chgdte { get; set; }
        public long Jobnum { get; set; }
        public long Phsnum { get; set; }
        public string Dscrpt { get; set; }
        public string Reason { get; set; }
        public string Usrdf1 { get; set; }
        public string Usrdf2 { get; set; }
        public string Pchord { get; set; }
        public DateTime? Subdte { get; set; }
        public DateTime? Aprdte { get; set; }
        public DateTime? Invdte { get; set; }
        public short Delreq { get; set; }
        public short Dysdly { get; set; }
        public byte? Chgtyp { get; set; }
        public byte Status { get; set; }
        public string Submto { get; set; }
        public long? Submby { get; set; }
        public decimal Reqamt { get; set; }
        public decimal Appamt { get; set; }
        public decimal Cstamt { get; set; }
        public decimal Ovhamt { get; set; }
        public decimal Pftamt { get; set; }
        public decimal Reqpft { get; set; }
        public DateTime? Entdte { get; set; }
        public string Usrnme { get; set; }
        public string Chgscp { get; set; }
        public byte Hotlst { get; set; }
        public byte Lckedt { get; set; }
        public byte Actper { get; set; }
        public decimal Mrgamt { get; set; }
        public decimal Reqmrg { get; set; }
        public decimal Estamt { get; set; }
        public decimal Estovh { get; set; }
        public byte Upgrde { get; set; }
        public string Ntetxt { get; set; }
        public short Postyr { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }

        public Chgtyp ChgtypNavigation { get; set; }
        public Actrec JobnumNavigation { get; set; }
        public Jobphs Jobphs { get; set; }
        public Employ SubmbyNavigation { get; set; }
        public PrmcU PrmcU { get; set; }
        public ICollection<Pmcgln> PmcglnIdrefNavigation { get; set; }
        public ICollection<Pmcgln> PmcglnRecnumNavigation { get; set; }
        public ICollection<Sbcgln> SbcglnIdrefNavigation { get; set; }
        public ICollection<Sbcgln> SbcglnRecnumNavigation { get; set; }
    }
}
