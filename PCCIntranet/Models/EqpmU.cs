﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class EqpmU
    {
        public Guid Idnum { get; set; }

        public Eqpmnt IdnumNavigation { get; set; }
    }
}
