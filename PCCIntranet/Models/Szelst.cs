﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Szelst
    {
        public Szelst()
        {
            Reccln = new HashSet<Reccln>();
        }

        public Guid Idnum { get; set; }
        public byte Recnum { get; set; }
        public string Dscrpt { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }

        public ICollection<Reccln> Reccln { get; set; }
    }
}
