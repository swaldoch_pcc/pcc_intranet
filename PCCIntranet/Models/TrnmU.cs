﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class TrnmU
    {
        public Guid Idnum { get; set; }

        public Trnmtl IdnumNavigation { get; set; }
    }
}
