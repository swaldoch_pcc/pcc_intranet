﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Referl
    {
        public Guid Idnum { get; set; }
        public Guid Idref { get; set; }
        public long Clnnum { get; set; }
        public long Referd { get; set; }
        public DateTime? Refdte { get; set; }
        public string Refsle { get; set; }
        public string Refnte { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }

        public Reccln ClnnumNavigation { get; set; }
        public Reccln IdrefNavigation { get; set; }
        public Reccln ReferdNavigation { get; set; }
    }
}
