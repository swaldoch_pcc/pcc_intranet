﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Fdrpsb
    {
        public Guid Idnum { get; set; }
        public Guid Idref { get; set; }
        public long Recnum { get; set; }
        public int Linnum { get; set; }
        public long? Vndnum { get; set; }
        public TimeSpan? Arvtim { get; set; }
        public TimeSpan? Levtim { get; set; }
        public long Numemp { get; set; }
        public string Dscrpt { get; set; }
        public string Usrdf1 { get; set; }
        public string Ntetxt { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }

        public Fldrpt IdrefNavigation { get; set; }
        public Fldrpt RecnumNavigation { get; set; }
        public Actpay VndnumNavigation { get; set; }
    }
}
