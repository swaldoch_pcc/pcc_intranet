﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Inctyp
    {
        public Inctyp()
        {
            Fdrpin = new HashSet<Fdrpin>();
        }

        public Guid Idnum { get; set; }
        public short Recnum { get; set; }
        public string Dscrpt { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }

        public ICollection<Fdrpin> Fdrpin { get; set; }
    }
}
