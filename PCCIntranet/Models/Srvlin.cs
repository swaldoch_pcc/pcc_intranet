﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Srvlin
    {
        public Guid Idnum { get; set; }
        public Guid Idref { get; set; }
        public long Recnum { get; set; }
        public int Linnum { get; set; }
        public long? Asmnum { get; set; }
        public string Asmchk { get; set; }
        public long? Prtnum { get; set; }
        public string Dscrpt { get; set; }
        public string Alpnum { get; set; }
        public string Untdsc { get; set; }
        public decimal Prtqty { get; set; }
        public decimal Prtprc { get; set; }
        public decimal Extqty { get; set; }
        public decimal Extprc { get; set; }
        public string Tktnum { get; set; }
        public decimal Blldte { get; set; }
        public decimal Curbll { get; set; }
        public byte? Csttyp { get; set; }
        public string Slstax { get; set; }
        public string Gstsbj { get; set; }
        public decimal Gstamt { get; set; }
        public string Pstsbj { get; set; }
        public decimal Pstamt { get; set; }
        public string Hstsbj { get; set; }
        public decimal Hstamt { get; set; }
        public long? Actnum { get; set; }
        public long? Subact { get; set; }
        public int? Invloc { get; set; }
        public string Sernum { get; set; }
        public string Sitloc { get; set; }
        public long? Eqpnum { get; set; }
        public string Usrdf1 { get; set; }
        public string Shpnte { get; set; }
        public string Ntetxt { get; set; }
        public long? Clnnum { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }

        public Lgract ActnumNavigation { get; set; }
        public Assemb AsmnumNavigation { get; set; }
        public Csttyp CsttypNavigation { get; set; }
        public Srvinv IdrefNavigation { get; set; }
        public Invloc InvlocNavigation { get; set; }
        public Tkfprt PrtnumNavigation { get; set; }
        public Srvinv RecnumNavigation { get; set; }
    }
}
