﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Acpinv
    {
        public Acpinv()
        {
            AcppmtIdrefNavigation = new HashSet<Acppmt>();
            AcppmtRecnumNavigation = new HashSet<Acppmt>();
            ApivlnIdrefNavigation = new HashSet<Apivln>();
            ApivlnRecnumNavigation = new HashSet<Apivln>();
            ScdpayIdrefNavigation = new HashSet<Scdpay>();
            ScdpayRecnumNavigation = new HashSet<Scdpay>();
        }

        public Guid Idnum { get; set; }
        public long Recnum { get; set; }
        public string Invnum { get; set; }
        public string Pchord { get; set; }
        public string Ctcnum { get; set; }
        public long Vndnum { get; set; }
        public long? Jobnum { get; set; }
        public long? Phsnum { get; set; }
        public string Dscrpt { get; set; }
        public DateTime? Invdte { get; set; }
        public DateTime? Duedte { get; set; }
        public string Refnum { get; set; }
        public DateTime? Dscdte { get; set; }
        public byte Invtyp { get; set; }
        public byte Status { get; set; }
        public string Payee2 { get; set; }
        public string Usrdf1 { get; set; }
        public string Usrdf2 { get; set; }
        public decimal Dscavl { get; set; }
        public decimal Retain { get; set; }
        public decimal Setpay { get; set; }
        public decimal Amtpad { get; set; }
        public decimal Dsctkn { get; set; }
        public decimal Invttl { get; set; }
        public decimal Adjust { get; set; }
        public decimal Freigh { get; set; }
        public decimal Ttlpad { get; set; }
        public decimal Invbal { get; set; }
        public decimal Invnet { get; set; }
        public byte Actper { get; set; }
        public DateTime? Entdte { get; set; }
        public long Lgrrec { get; set; }
        public string Usrnme { get; set; }
        public int Btcnum { get; set; }
        public byte Hotlst { get; set; }
        public string Shpnum { get; set; }
        public decimal Cmpamt { get; set; }
        public decimal Usetax { get; set; }
        public string Ntetxt { get; set; }
        public long Vodrec { get; set; }
        public decimal Subttl { get; set; }
        public decimal Hldamt { get; set; }
        public decimal Hldbll { get; set; }
        public decimal Hldrem { get; set; }
        public decimal Pstamt { get; set; }
        public decimal Qstamt { get; set; }
        public decimal Gstamt { get; set; }
        public decimal Hstamt { get; set; }
        public decimal Invamt { get; set; }
        public decimal Slstax { get; set; }
        public short Postyr { get; set; }
        public short? Taxcde { get; set; }
        public decimal Rcpamt { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }
        public string Apinte { get; set; }
        public int? Usedst { get; set; }
        public decimal Usettl { get; set; }

        public Actrec JobnumNavigation { get; set; }
        public Jobphs Jobphs { get; set; }
        public CaTax TaxcdeNavigation { get; set; }
        public Taxdst UsedstNavigation { get; set; }
        public Actpay VndnumNavigation { get; set; }
        public AcpiU AcpiU { get; set; }
        public ICollection<Acppmt> AcppmtIdrefNavigation { get; set; }
        public ICollection<Acppmt> AcppmtRecnumNavigation { get; set; }
        public ICollection<Apivln> ApivlnIdrefNavigation { get; set; }
        public ICollection<Apivln> ApivlnRecnumNavigation { get; set; }
        public ICollection<Scdpay> ScdpayIdrefNavigation { get; set; }
        public ICollection<Scdpay> ScdpayRecnumNavigation { get; set; }
    }
}
