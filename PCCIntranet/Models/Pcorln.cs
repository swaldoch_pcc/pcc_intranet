﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Pcorln
    {
        public Guid Idnum { get; set; }
        public Guid Idref { get; set; }
        public long Recnum { get; set; }
        public int Linnum { get; set; }
        public long? Prtnum { get; set; }
        public string Prtdsc { get; set; }
        public string Alpnum { get; set; }
        public string Untdsc { get; set; }
        public decimal Linqty { get; set; }
        public decimal Linprc { get; set; }
        public decimal Extttl { get; set; }
        public decimal Rcvdte { get; set; }
        public decimal Currnt { get; set; }
        public decimal Cancel { get; set; }
        public string Gstsbj { get; set; }
        public string Pstsbj { get; set; }
        public string Hstsbj { get; set; }
        public decimal? Cstcde { get; set; }
        public byte? Csttyp { get; set; }
        public int? Invloc { get; set; }
        public long? Lgract { get; set; }
        public long? Subact { get; set; }
        public string Usrdf1 { get; set; }
        public string Ntetxt { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }

        public Cstcde CstcdeNavigation { get; set; }
        public Csttyp CsttypNavigation { get; set; }
        public Pchord IdrefNavigation { get; set; }
        public Invloc InvlocNavigation { get; set; }
        public Lgract LgractNavigation { get; set; }
        public Tkfprt PrtnumNavigation { get; set; }
        public Pchord RecnumNavigation { get; set; }
    }
}
