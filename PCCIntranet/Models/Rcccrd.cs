﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Rcccrd
    {
        public Guid Idnum { get; set; }
        public long Recnum { get; set; }
        public long Issuer { get; set; }
        public long Crdcrd { get; set; }
        public string Payee1 { get; set; }
        public DateTime? Nxtdte { get; set; }
        public string Prddys { get; set; }
        public DateTime? Lstpst { get; set; }
        public string Dscrpt { get; set; }
        public string Usrdf1 { get; set; }
        public string Usrdf2 { get; set; }
        public long Actnum { get; set; }
        public long? Subact { get; set; }
        public decimal Amount { get; set; }
        public string Usrnme { get; set; }
        public DateTime? Edtdte { get; set; }
        public string Ntetxt { get; set; }
        public short? Taxcde { get; set; }
        public decimal Gstamt { get; set; }
        public decimal Pstamt { get; set; }
        public decimal Hstamt { get; set; }
        public decimal Ttlamt { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }

        public Lgract ActnumNavigation { get; set; }
        public Lgract IssuerNavigation { get; set; }
        public Lgrsub Lgrsub { get; set; }
        public Lgrsub LgrsubNavigation { get; set; }
        public CaTax TaxcdeNavigation { get; set; }
    }
}
