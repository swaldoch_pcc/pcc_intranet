﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Rptscd
    {
        public Rptscd()
        {
            Emllst = new HashSet<Emllst>();
            Faxlst = new HashSet<Faxlst>();
        }

        public Guid Idnum { get; set; }
        public long Recnum { get; set; }
        public string Rptttl { get; set; }
        public string Rptkey { get; set; }
        public string Userid { get; set; }
        public DateTime? Creatd { get; set; }
        public string Scedul { get; set; }
        public string Output { get; set; }
        public string Rptfrm { get; set; }
        public string Dtafld { get; set; }
        public string Rptdft { get; set; }
        public string Ntetxt { get; set; }
        public string Subjct { get; set; }
        public string Tskgid { get; set; }
        public string Mchnam { get; set; }
        public string Sndcpy { get; set; }
        public string Rceipt { get; set; }
        public byte Implvl { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }
        public byte V19flg { get; set; }

        public ICollection<Emllst> Emllst { get; set; }
        public ICollection<Faxlst> Faxlst { get; set; }
    }
}
