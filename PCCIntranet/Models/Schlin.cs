﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Schlin
    {
        public Schlin()
        {
            Schemp = new HashSet<Schemp>();
            Scheqp = new HashSet<Scheqp>();
            Schprd = new HashSet<Schprd>();
            Schsub = new HashSet<Schsub>();
        }

        public Guid Idnum { get; set; }
        public Guid Idref { get; set; }
        public long Recnum { get; set; }
        public long Phsnum { get; set; }
        public int Linnum { get; set; }
        public decimal? Tsknum { get; set; }
        public string Tsknme { get; set; }
        public short Tskdur { get; set; }
        public byte? Tsktyp { get; set; }
        public DateTime? Fxddte { get; set; }
        public DateTime? Notbfr { get; set; }
        public DateTime? Notaft { get; set; }
        public DateTime? Strdte { get; set; }
        public DateTime? Findte { get; set; }
        public DateTime? Ltestr { get; set; }
        public DateTime? Ltefin { get; set; }
        public int Tskflt { get; set; }
        public short Orgdur { get; set; }
        public DateTime? Orgstr { get; set; }
        public DateTime? Orgfin { get; set; }
        public int Orgflt { get; set; }
        public string Usrdf1 { get; set; }
        public string Ntetxt { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }

        public Schedl IdrefNavigation { get; set; }
        public Jobphs Jobphs { get; set; }
        public Schedl Schedl { get; set; }
        public Schtsk TsknumNavigation { get; set; }
        public Tsktyp TsktypNavigation { get; set; }
        public ICollection<Schemp> Schemp { get; set; }
        public ICollection<Scheqp> Scheqp { get; set; }
        public ICollection<Schprd> Schprd { get; set; }
        public ICollection<Schsub> Schsub { get; set; }
    }
}
