﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Tkflin
    {
        public Guid Idnum { get; set; }
        public Guid Idref { get; set; }
        public long Recnum { get; set; }
        public long Itmnum { get; set; }
        public long Phsnum { get; set; }
        public int Linnum { get; set; }
        public long? Asmnum { get; set; }
        public string Asmchk { get; set; }
        public long? Prtnum { get; set; }
        public string Prtdsc { get; set; }
        public string Alpnum { get; set; }
        public string Untdsc { get; set; }
        public decimal Linqty { get; set; }
        public decimal Linprc { get; set; }
        public string Linlck { get; set; }
        public string Qtyfrm { get; set; }
        public decimal Extqty { get; set; }
        public decimal Extttl { get; set; }
        public int? Taxdst { get; set; }
        public decimal Slstax { get; set; }
        public decimal Ovhmrk { get; set; }
        public decimal Ovhamt { get; set; }
        public decimal Pftmrk { get; set; }
        public decimal Pftamt { get; set; }
        public decimal Bidprc { get; set; }
        public long? Prmvnd { get; set; }
        public decimal? Cstcde { get; set; }
        public byte? Csttyp { get; set; }
        public decimal? Tsknum { get; set; }
        public int? Invloc { get; set; }
        public string Usrdf1 { get; set; }
        public string Ntetxt { get; set; }
        public string Expnte { get; set; }
        public string Pstsbj { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }
        public string Rowtag { get; set; }

        public Assemb AsmnumNavigation { get; set; }
        public Biditm Biditm { get; set; }
        public Cstcde CstcdeNavigation { get; set; }
        public Csttyp CsttypNavigation { get; set; }
        public Ptotkf IdrefNavigation { get; set; }
        public Invloc InvlocNavigation { get; set; }
        public Jobphs Jobphs { get; set; }
        public Actpay PrmvndNavigation { get; set; }
        public Tkfprt PrtnumNavigation { get; set; }
        public Ptotkf RecnumNavigation { get; set; }
        public Taxdst TaxdstNavigation { get; set; }
        public Schtsk TsknumNavigation { get; set; }
    }
}
