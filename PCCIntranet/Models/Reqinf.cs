﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Reqinf
    {
        public Guid Idnum { get; set; }
        public long Recnum { get; set; }
        public long? Vndnum { get; set; }
        public long? Clnnum { get; set; }
        public string Attion { get; set; }
        public long Jobnum { get; set; }
        public long Phsnum { get; set; }
        public long? Divnum { get; set; }
        public string Dscrpt { get; set; }
        public DateTime? Subdte { get; set; }
        public string Rfinum { get; set; }
        public short? Rfityp { get; set; }
        public DateTime? Reqdte { get; set; }
        public long? Reqsby { get; set; }
        public string Ansrby { get; set; }
        public string Usrdf1 { get; set; }
        public string Usrdf2 { get; set; }
        public byte Status { get; set; }
        public DateTime? Retdte { get; set; }
        public DateTime? Appdte { get; set; }
        public string Reqinf1 { get; set; }
        public string Retinf { get; set; }
        public DateTime? Entdte { get; set; }
        public string Usrnme { get; set; }
        public byte Hotlst { get; set; }
        public byte Lckedt { get; set; }
        public byte Possch { get; set; }
        public byte Poschg { get; set; }
        public byte Plnchg { get; set; }
        public byte Usrck1 { get; set; }
        public byte Usrck2 { get; set; }
        public string Addrs1 { get; set; }
        public string Addrs2 { get; set; }
        public string Ctynme { get; set; }
        public string State { get; set; }
        public string Zipcde { get; set; }
        public string Ntetxt { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }

        public Reccln ClnnumNavigation { get; set; }
        public Cstdiv DivnumNavigation { get; set; }
        public Actrec JobnumNavigation { get; set; }
        public Jobphs Jobphs { get; set; }
        public Employ ReqsbyNavigation { get; set; }
        public Rfityp RfitypNavigation { get; set; }
        public Actpay VndnumNavigation { get; set; }
        public ReqiU ReqiU { get; set; }
    }
}
