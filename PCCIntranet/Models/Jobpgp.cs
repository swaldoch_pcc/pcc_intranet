﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Jobpgp
    {
        public Guid Idnum { get; set; }
        public Guid Idref { get; set; }
        public long Recnum { get; set; }
        public int? Reggrp { get; set; }
        public int? Paygrp { get; set; }
        public decimal Pcerte { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }

        public Actrec IdrefNavigation { get; set; }
        public Paygrp PaygrpNavigation { get; set; }
        public Actrec RecnumNavigation { get; set; }
        public Paypst ReggrpNavigation { get; set; }
    }
}
