﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Vndtyp
    {
        public Vndtyp()
        {
            Actpay = new HashSet<Actpay>();
        }

        public Guid Idnum { get; set; }
        public short Recnum { get; set; }
        public string Typnme { get; set; }
        public string Ntetxt { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }

        public ICollection<Actpay> Actpay { get; set; }
    }
}
