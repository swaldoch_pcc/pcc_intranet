﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Schdte
    {
        public Guid Idnum { get; set; }
        public Guid Idref { get; set; }
        public long Recnum { get; set; }
        public long Phsnum { get; set; }
        public decimal? Tsknum { get; set; }
        public DateTime? Schdte1 { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }

        public Schedl IdrefNavigation { get; set; }
        public Jobphs Jobphs { get; set; }
        public Schedl Schedl { get; set; }
        public Schtsk TsknumNavigation { get; set; }
    }
}
