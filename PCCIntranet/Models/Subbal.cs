﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Subbal
    {
        public Guid Idnum { get; set; }
        public Guid Idref { get; set; }
        public long Recnum { get; set; }
        public long Ctract { get; set; }
        public long Subact { get; set; }
        public short Postyr { get; set; }
        public byte Actprd { get; set; }
        public decimal Balnce { get; set; }
        public decimal Budget { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }

        public Lgract CtractNavigation { get; set; }
        public Lgrsub IdrefNavigation { get; set; }
        public Lgrsub Lgrsub { get; set; }
    }
}
