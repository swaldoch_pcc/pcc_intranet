﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Schtsk
    {
        public Schtsk()
        {
            Asmprt = new HashSet<Asmprt>();
            Pchord = new HashSet<Pchord>();
            Schdte = new HashSet<Schdte>();
            Schemp = new HashSet<Schemp>();
            Scheqp = new HashSet<Scheqp>();
            Schlin = new HashSet<Schlin>();
            SchprdPrdnumNavigation = new HashSet<Schprd>();
            SchprdTsknumNavigation = new HashSet<Schprd>();
            Schsub = new HashSet<Schsub>();
            Tkflin = new HashSet<Tkflin>();
            Tkfprt = new HashSet<Tkfprt>();
        }

        public Guid Idnum { get; set; }
        public decimal Recnum { get; set; }
        public string Tsknme { get; set; }
        public byte Tsktyp { get; set; }
        public string Ntetxt { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }

        public Tsktyp TsktypNavigation { get; set; }
        public ICollection<Asmprt> Asmprt { get; set; }
        public ICollection<Pchord> Pchord { get; set; }
        public ICollection<Schdte> Schdte { get; set; }
        public ICollection<Schemp> Schemp { get; set; }
        public ICollection<Scheqp> Scheqp { get; set; }
        public ICollection<Schlin> Schlin { get; set; }
        public ICollection<Schprd> SchprdPrdnumNavigation { get; set; }
        public ICollection<Schprd> SchprdTsknumNavigation { get; set; }
        public ICollection<Schsub> Schsub { get; set; }
        public ICollection<Tkflin> Tkflin { get; set; }
        public ICollection<Tkfprt> Tkfprt { get; set; }
    }
}
