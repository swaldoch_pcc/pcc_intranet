﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Invalc
    {
        public Invalc()
        {
            InvtlnIdrefNavigation = new HashSet<Invtln>();
            InvtlnRecnumNavigation = new HashSet<Invtln>();
        }

        public Guid Idnum { get; set; }
        public long Recnum { get; set; }
        public string Trnnum { get; set; }
        public long? Jobnum { get; set; }
        public long? Phsnum { get; set; }
        public string Addrs1 { get; set; }
        public string Addrs2 { get; set; }
        public string Ctynme { get; set; }
        public string State { get; set; }
        public string Zipcde { get; set; }
        public byte Status { get; set; }
        public string Cstord { get; set; }
        public DateTime? Trndte { get; set; }
        public DateTime? Aprdte { get; set; }
        public DateTime? Deldte { get; set; }
        public DateTime? Invdte { get; set; }
        public string Delvia { get; set; }
        public string Dscrpt { get; set; }
        public string Usrdf1 { get; set; }
        public string Usrdf2 { get; set; }
        public long Lgrrec { get; set; }
        public byte Actper { get; set; }
        public DateTime? Entdte { get; set; }
        public string Usrnme { get; set; }
        public string Ntetxt { get; set; }
        public short Postyr { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }

        public Actrec JobnumNavigation { get; set; }
        public Jobphs Jobphs { get; set; }
        public InvaU InvaU { get; set; }
        public ICollection<Invtln> InvtlnIdrefNavigation { get; set; }
        public ICollection<Invtln> InvtlnRecnumNavigation { get; set; }
    }
}
