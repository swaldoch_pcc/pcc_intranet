﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class AcpiU
    {
        public Guid Idnum { get; set; }

        public Acpinv IdnumNavigation { get; set; }
    }
}
