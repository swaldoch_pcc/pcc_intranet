﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class LaborSchedule
    {
        public Guid Guid { get; set; }
        public int? EmployeeId { get; set; }
        public int? JobId { get; set; }
        public DateTime? Date { get; set; }
    }
}
