﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Service
    {
        public Guid Id { get; set; }
        public string Vin { get; set; }
        public string ServiceType { get; set; }
        public string ServiceLocation { get; set; }
        public int? Mileage { get; set; }
        public decimal? Cost { get; set; }
        public string Notes { get; set; }
        public string PrimaryUser { get; set; }
        public DateTime? ServiceDate { get; set; }
        public string AdditionalServices { get; set; }
    }
}
