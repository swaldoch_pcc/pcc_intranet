﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Srvcnt
    {
        public Srvcnt()
        {
            SrcneqIdrefNavigation = new HashSet<Srcneq>();
            SrcneqRecnumNavigation = new HashSet<Srcneq>();
            SrcnlcIdrefNavigation = new HashSet<Srcnlc>();
            SrcnlcRecnumNavigation = new HashSet<Srcnlc>();
        }

        public Guid Idnum { get; set; }
        public long Recnum { get; set; }
        public string Connum { get; set; }
        public long Clnnum { get; set; }
        public string Dscrpt { get; set; }
        public DateTime? Strdte { get; set; }
        public DateTime? Expdte { get; set; }
        public byte Contyp { get; set; }
        public decimal Conamt { get; set; }
        public byte Disply { get; set; }
        public DateTime? Entdte { get; set; }
        public string Usrnme { get; set; }
        public string Ntetxt { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }

        public Reccln ClnnumNavigation { get; set; }
        public Conlst ContypNavigation { get; set; }
        public ICollection<Srcneq> SrcneqIdrefNavigation { get; set; }
        public ICollection<Srcneq> SrcneqRecnumNavigation { get; set; }
        public ICollection<Srcnlc> SrcnlcIdrefNavigation { get; set; }
        public ICollection<Srcnlc> SrcnlcRecnumNavigation { get; set; }
    }
}
