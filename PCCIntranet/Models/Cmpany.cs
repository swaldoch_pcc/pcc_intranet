﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Cmpany
    {
        public Guid Idnum { get; set; }
        public string Cmpnme { get; set; }
        public string Cmpad1 { get; set; }
        public string Cmpad2 { get; set; }
        public string Ctynme { get; set; }
        public string State { get; set; }
        public string Zipcde { get; set; }
        public string Licnum { get; set; }
        public string Fedtax { get; set; }
        public string Stetax { get; set; }
        public string Rslnum { get; set; }
        public string Phnnum { get; set; }
        public string Faxnum { get; set; }
        public string Usrdf1 { get; set; }
        public string Usrdf2 { get; set; }
        public string Archiv { get; set; }
        public string EMail { get; set; }
        public byte? Rccgrp { get; set; }
        public string Bnkact { get; set; }
        public string Rtnmbr { get; set; }
        public byte Acttyp { get; set; }
        public short Lstbch { get; set; }
        public string Ntetxt { get; set; }
        public byte Hrdrtn { get; set; }
        public byte Cmpdbt { get; set; }
        public byte Filbch { get; set; }
        public string Emlsvr { get; set; }
        public int Svrprt { get; set; }
        public string Dilout { get; set; }
        public byte Reqaut { get; set; }
        public string Orgbnk { get; set; }
        public string Dstbnk { get; set; }
        public string Optrtg { get; set; }
        public byte Idcode { get; set; }
        public byte Orgnme { get; set; }
        public byte Dstnme { get; set; }
        public byte Orgrtg { get; set; }
        public byte Dstrtg { get; set; }
        public byte Usessl { get; set; }
        public string Rtnvnd { get; set; }
        public string Bnkvnd { get; set; }
        public byte Actvnd { get; set; }
        public string Orgvnd { get; set; }
        public string Dstvnd { get; set; }
        public string Optvnd { get; set; }
        public byte Idcvnd { get; set; }
        public byte Dbtvnd { get; set; }
        public byte Hrdvnd { get; set; }
        public byte Filvnd { get; set; }
        public byte Ornmvn { get; set; }
        public byte Dsnmvn { get; set; }
        public byte Orrtvn { get; set; }
        public byte Dsrtvn { get; set; }
        public long? Depact { get; set; }
        public string Vndoid { get; set; }
        public string Vndctr { get; set; }
        public string Vndirt { get; set; }
        public string Vndrrt { get; set; }
        public string Vndart { get; set; }
        public string Vndist { get; set; }
        public string Vndsrt { get; set; }
        public byte Vndast { get; set; }
        public byte Vndtrc { get; set; }
        public byte Vndhrd { get; set; }
        public long? Vndeft { get; set; }
        public string Pyroid { get; set; }
        public string Pyrctr { get; set; }
        public string Pyrirt { get; set; }
        public string Pyrrrt { get; set; }
        public string Pyrart { get; set; }
        public string Pyrist { get; set; }
        public string Pyrsrt { get; set; }
        public short Pyrast { get; set; }
        public byte Pyrhrd { get; set; }
        public byte Pyrtrc { get; set; }
        public string Payarc { get; set; }
        public string Ccdmid { get; set; }
        public string Ccdmky { get; set; }
        public byte Usevlt { get; set; }
        public long? Ccdact { get; set; }
        public string Pyrrbc { get; set; }
        public string Vndrbc { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }
        public int Acapoc { get; set; }
        public string Cmpclr { get; set; }
        public string Bnkcld { get; set; }
        public string Eeonum { get; set; }
        public int Eeocde { get; set; }

        public Lgract CcdactNavigation { get; set; }
        public Lgract DepactNavigation { get; set; }
        public Grplst RccgrpNavigation { get; set; }
        public Lgract VndeftNavigation { get; set; }
    }
}
