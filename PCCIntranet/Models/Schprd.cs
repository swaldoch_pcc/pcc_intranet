﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Schprd
    {
        public Guid Idnum { get; set; }
        public Guid Idref { get; set; }
        public long Recnum { get; set; }
        public long Recphs { get; set; }
        public decimal Tsknum { get; set; }
        public long Jobnum { get; set; }
        public long Phsnum { get; set; }
        public decimal Prdnum { get; set; }
        public byte Reltyp { get; set; }
        public short Ledlag { get; set; }
        public string Ntetxt { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }

        public Schlin IdrefNavigation { get; set; }
        public Actrec JobnumNavigation { get; set; }
        public Jobphs Jobphs { get; set; }
        public Schtsk PrdnumNavigation { get; set; }
        public Schedl Rec { get; set; }
        public Jobphs RecNavigation { get; set; }
        public Schtsk TsknumNavigation { get; set; }
    }
}
