﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Rfityp
    {
        public Rfityp()
        {
            Reqinf = new HashSet<Reqinf>();
        }

        public Guid Idnum { get; set; }
        public short Recnum { get; set; }
        public string Dscrpt { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }

        public ICollection<Reqinf> Reqinf { get; set; }
    }
}
