using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace PCCIntranet.Models
{
    public partial class EquipmentContext : DbContext
    {
        public EquipmentContext()
        {
        }

        public EquipmentContext(DbContextOptions<EquipmentContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Categorypricing> Categorypricing { get; set; }
        public virtual DbSet<Consumables> Consumables { get; set; }
        public virtual DbSet<ConsumablesIssued> ConsumablesIssued { get; set; }
        public virtual DbSet<Employee> Employee { get; set; }
        public virtual DbSet<EquipmentRental> EquipmentRental { get; set; }
        public virtual DbSet<Recalls> Recalls { get; set; }
        public virtual DbSet<Rental> Rental { get; set; }
        public virtual DbSet<Service> Service { get; set; }
        public virtual DbSet<Tools> Tools { get; set; }
        public virtual DbSet<ToolsHistory> ToolsHistory { get; set; }
        public virtual DbSet<TruckRental> TruckRental { get; set; }
        public virtual DbSet<VehicleHistory> VehicleHistory { get; set; }
        public virtual DbSet<Vehicles> Vehicles { get; set; }
        public virtual DbSet<VehiclesRetired> VehiclesRetired { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {

            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Categorypricing>(entity =>
            {
                entity.HasKey(e => e.PriceId);

                entity.ToTable("Categorypricing", "Equipment");

                entity.Property(e => e.PriceId)
                    .HasColumnName("price_id")
                    .ValueGeneratedNever();

                entity.Property(e => e.Billonce)
                    .HasColumnName("billonce")
                    .HasMaxLength(10);

                entity.Property(e => e.Category)
                    .HasColumnName("category")
                    .HasMaxLength(255);

                entity.Property(e => e.Changedby)
                    .HasColumnName("changedby")
                    .HasMaxLength(255);

                entity.Property(e => e.Dailyrate)
                    .HasColumnName("dailyrate")
                    .HasColumnType("money");

                entity.Property(e => e.Isactive)
                    .IsRequired()
                    .HasColumnName("isactive")
                    .HasMaxLength(10);

                entity.Property(e => e.Jobbillable)
                    .HasColumnName("jobbillable")
                    .HasMaxLength(10);

                entity.Property(e => e.Pricenum).HasColumnName("pricenum");

                entity.Property(e => e.Ratelastchange)
                    .HasColumnName("ratelastchange")
                    .HasColumnType("date");

                entity.Property(e => e.Subcategory)
                    .HasColumnName("subcategory")
                    .HasMaxLength(255);
            });

            modelBuilder.Entity<Consumables>(entity =>
            {
                entity.HasKey(e => new { e.Id, e.ItemNum });

                entity.ToTable("Consumables", "Equipment");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.ItemNum).HasMaxLength(10);

                entity.Property(e => e.Vendor).HasMaxLength(25);

                entity.Property(e => e.VendorEmail).HasMaxLength(50);

                entity.Property(e => e.VendorPhone).HasMaxLength(15);

                entity.Property(e => e.WebsiteLoginName).HasMaxLength(50);

                entity.Property(e => e.WebsiteLoginPassword).HasMaxLength(50);
            });

            modelBuilder.Entity<ConsumablesIssued>(entity =>
            {
                entity.ToTable("ConsumablesIssued", "Equipment");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Comments).HasColumnName("comments");

                entity.Property(e => e.Description).HasColumnName("description");

                entity.Property(e => e.Issuedate)
                    .HasColumnName("issuedate")
                    .HasColumnType("datetime2(0)");

                entity.Property(e => e.Itemid)
                    .IsRequired()
                    .HasColumnName("itemid")
                    .HasMaxLength(25);

                entity.Property(e => e.Job)
                    .HasColumnName("job")
                    .HasMaxLength(255);

                entity.Property(e => e.Quantity).HasColumnName("quantity");

                entity.Property(e => e.User)
                    .HasColumnName("user")
                    .HasMaxLength(255);
            });

            modelBuilder.Entity<Employee>(entity =>
            {
                entity.ToTable("Employee", "Equipment");

                entity.Property(e => e.EmployeeId)
                    .HasColumnName("EmployeeID")
                    .HasMaxLength(255)
                    .ValueGeneratedNever();

                entity.Property(e => e.Employee1)
                    .HasColumnName("Employee")
                    .HasMaxLength(255);
            });

            modelBuilder.Entity<EquipmentRental>(entity =>
            {
                entity.HasKey(e => e.RentalId);

                entity.ToTable("EquipmentRental", "Equipment");

                entity.Property(e => e.RentalId)
                    .HasColumnName("rentalID")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.DateIn).HasColumnType("datetime2(0)");

                entity.Property(e => e.DateOut).HasColumnType("datetime2(0)");

                entity.Property(e => e.Description)
                    .HasColumnName("description")
                    .HasMaxLength(255);

                entity.Property(e => e.EquipmentId)
                    .IsRequired()
                    .HasColumnName("EquipmentID")
                    .HasMaxLength(50);

                entity.Property(e => e.Job)
                    .HasColumnName("job")
                    .HasMaxLength(25);

                entity.Property(e => e.Oldrentalid).HasColumnName("oldrentalid");

                entity.Property(e => e.User)
                    .IsRequired()
                    .HasColumnName("user")
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<Recalls>(entity =>
            {
                entity.ToTable("Recalls", "Equipment");

                entity.Property(e => e.Id)
                    .HasColumnName("ID")
                    .ValueGeneratedNever();

                entity.Property(e => e.RecallDate)
                    .HasColumnName("Recall_Date")
                    .HasColumnType("date");

                entity.Property(e => e.RecallNum)
                    .HasColumnName("Recall_Num")
                    .HasMaxLength(25);

                entity.Property(e => e.RecallReceived)
                    .HasColumnName("Recall_Received")
                    .HasColumnType("date");

                entity.Property(e => e.ServiceLocation)
                    .HasColumnName("Service_location")
                    .HasMaxLength(255);

                entity.Property(e => e.Vin)
                    .HasColumnName("VIN")
                    .HasMaxLength(25);

                entity.Property(e => e.WorkCompleted)
                    .HasColumnName("Work_completed")
                    .HasColumnType("date");

                entity.Property(e => e.WorkScheduled)
                    .HasColumnName("Work_scheduled")
                    .HasColumnType("date");
            });

            modelBuilder.Entity<Rental>(entity =>
            {
                entity.ToTable("Rental", "Equipment");

                entity.Property(e => e.RentalId)
                    .HasColumnName("RentalID")
                    .HasMaxLength(255)
                    .ValueGeneratedNever();

                entity.Property(e => e.DateIn).HasMaxLength(255);

                entity.Property(e => e.DateOut).HasMaxLength(255);

                entity.Property(e => e.EmployeeId)
                    .HasColumnName("EmployeeID")
                    .HasMaxLength(255);

                entity.Property(e => e.JobId)
                    .HasColumnName("JobID")
                    .HasMaxLength(255);

                entity.Property(e => e.Pcid)
                    .HasColumnName("PCID")
                    .HasMaxLength(255);
            });

            modelBuilder.Entity<Service>(entity =>
            {
                entity.ToTable("Service", "Equipment");

                entity.Property(e => e.Id)
                    .HasColumnName("ID")
                    .ValueGeneratedNever();

                entity.Property(e => e.AdditionalServices).HasColumnName("Additional services");

                entity.Property(e => e.Cost).HasColumnType("money");

                entity.Property(e => e.PrimaryUser)
                    .HasColumnName("Primary_user")
                    .HasMaxLength(255);

                entity.Property(e => e.ServiceDate)
                    .HasColumnName("Service_Date")
                    .HasColumnType("date");

                entity.Property(e => e.ServiceLocation)
                    .HasColumnName("Service_location")
                    .HasMaxLength(255);

                entity.Property(e => e.ServiceType).HasColumnName("Service_type");

                entity.Property(e => e.Vin)
                    .HasColumnName("VIN")
                    .HasMaxLength(25);
            });

            modelBuilder.Entity<Tools>(entity =>
            {
                entity.HasKey(e => e.ToolId);

                entity.ToTable("Tools", "Equipment");

                entity.Property(e => e.ToolId).ValueGeneratedOnAdd();

                entity.Property(e => e.Category)
                    .HasColumnName("category")
                    .HasMaxLength(25);

                entity.Property(e => e.Condition)
                    .HasColumnName("condition")
                    .HasMaxLength(50);

                entity.Property(e => e.DatePurchased).HasColumnType("date");

                entity.Property(e => e.Description).HasColumnName("description");

                entity.Property(e => e.Location)
                    .HasColumnName("location")
                    .HasMaxLength(50);

                entity.Property(e => e.Mfg)
                    .HasColumnName("mfg")
                    .HasMaxLength(50);

                entity.Property(e => e.Modelnum)
                    .HasColumnName("modelnum")
                    .HasMaxLength(50);

                entity.Property(e => e.Pcid).HasColumnName("PCID");

                entity.Property(e => e.Pcnum)
                    .HasColumnName("PCNum")
                    .HasMaxLength(50);

                entity.Property(e => e.Priceid).HasColumnName("priceid");

                entity.Property(e => e.SerialNum).HasMaxLength(50);
            });

            modelBuilder.Entity<ToolsHistory>(entity =>
            {
                entity.HasKey(e => e.ToolId);

                entity.ToTable("ToolsHistory", "Equipment");

                entity.Property(e => e.ToolId).ValueGeneratedNever();

                entity.Property(e => e.Color)
                    .HasColumnName("color")
                    .HasMaxLength(50);

                entity.Property(e => e.Condition)
                    .HasColumnName("condition")
                    .HasMaxLength(50);

                entity.Property(e => e.DateDisposed).HasColumnType("date");

                entity.Property(e => e.DatePurchased).HasColumnType("date");

                entity.Property(e => e.Description).HasColumnName("description");

                entity.Property(e => e.Disposition).HasMaxLength(50);

                entity.Property(e => e.Location)
                    .HasColumnName("location")
                    .HasMaxLength(50);

                entity.Property(e => e.Mfg)
                    .HasColumnName("mfg")
                    .HasMaxLength(50);

                entity.Property(e => e.Modelnum)
                    .HasColumnName("modelnum")
                    .HasMaxLength(50);

                entity.Property(e => e.Pcid).HasColumnName("PCID");

                entity.Property(e => e.Pcnum)
                    .HasColumnName("PCNum")
                    .HasMaxLength(50);

                entity.Property(e => e.SerialNum).HasMaxLength(50);

                entity.Property(e => e.Tagnum)
                    .HasColumnName("tagnum")
                    .HasMaxLength(50);

                entity.Property(e => e.Title)
                    .HasColumnName("title")
                    .HasMaxLength(50);

                entity.Property(e => e.Yearmodel).HasColumnName("yearmodel");
            });

            modelBuilder.Entity<TruckRental>(entity =>
            {
                entity.ToTable("TruckRental", "Equipment");

                entity.Property(e => e.Id).ValueGeneratedOnAdd();

                entity.Property(e => e.Comments).HasColumnName("comments");

                entity.Property(e => e.Condition).HasColumnName("condition");

                entity.Property(e => e.Issuedate)
                    .HasColumnName("issuedate")
                    .HasColumnType("datetime2(0)");

                entity.Property(e => e.Issuefuel)
                    .HasColumnName("issuefuel")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.Issuemileage).HasColumnName("issuemileage");

                entity.Property(e => e.Itemid)
                    .IsRequired()
                    .HasColumnName("itemid")
                    .HasMaxLength(25);

                entity.Property(e => e.Job)
                    .HasColumnName("job")
                    .HasMaxLength(255);

                entity.Property(e => e.Purpose).HasColumnName("purpose");

                entity.Property(e => e.Returndate)
                    .HasColumnName("returndate")
                    .HasColumnType("datetime2(0)");

                entity.Property(e => e.Returnfuel)
                    .HasColumnName("returnfuel")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.Returnmileage).HasColumnName("returnmileage");

                entity.Property(e => e.User)
                    .HasColumnName("user")
                    .HasMaxLength(255);
            });

            modelBuilder.Entity<VehicleHistory>(entity =>
            {
                entity.HasKey(e => e.HistoryId);

                entity.ToTable("VehicleHistory", "Equipment");

                entity.Property(e => e.HistoryId)
                    .HasColumnName("HistoryID")
                    .ValueGeneratedNever();

                entity.Property(e => e.ConditionReportId).HasColumnName("ConditionReportID");

                entity.Property(e => e.IssueDate).HasColumnType("date");

                entity.Property(e => e.JobNum).HasMaxLength(10);

                entity.Property(e => e.ReturnDate).HasColumnType("date");

                entity.Property(e => e.SerialNum).HasMaxLength(25);
            });

            modelBuilder.Entity<Vehicles>(entity =>
            {
                entity.HasKey(e => new { e.Id, e.Company });

                entity.ToTable("Vehicles", "Equipment");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Company).HasMaxLength(25);

                entity.Property(e => e.BodyStyle).HasMaxLength(25);

                entity.Property(e => e.Color).HasMaxLength(25);

                entity.Property(e => e.Config).HasMaxLength(25);

                entity.Property(e => e.CurrentMileageDate).HasColumnType("date");

                entity.Property(e => e.Description).HasMaxLength(255);

                entity.Property(e => e.DriveTrain).HasMaxLength(25);

                entity.Property(e => e.EngineType).HasMaxLength(25);

                entity.Property(e => e.FuelType).HasMaxLength(25);

                entity.Property(e => e.IssueDate).HasColumnType("date");

                entity.Property(e => e.Mfg).HasMaxLength(50);

                entity.Property(e => e.Model).HasMaxLength(50);

                entity.Property(e => e.OrigPurchasePrice).HasColumnType("money");

                entity.Property(e => e.PrimaryUser).HasMaxLength(50);

                entity.Property(e => e.PurchaseDate).HasColumnType("date");

                entity.Property(e => e.ReturnDate).HasColumnType("date");

                entity.Property(e => e.SalePrice).HasColumnType("money");

                entity.Property(e => e.SerialNum)
                    .HasColumnName("Serial_Num")
                    .HasMaxLength(50);

                entity.Property(e => e.SoldDate).HasColumnType("date");

                entity.Property(e => e.Tag).HasMaxLength(25);

                entity.Property(e => e.TagExpiration).HasColumnType("date");

                entity.Property(e => e.TagState)
                    .HasColumnName("Tag_State")
                    .HasMaxLength(25);

                entity.Property(e => e.Title).HasMaxLength(25);

                entity.Property(e => e.TitleState)
                    .HasColumnName("Title_State")
                    .HasMaxLength(25);
            });

            modelBuilder.Entity<VehiclesRetired>(entity =>
            {
                entity.HasKey(e => new { e.Id, e.Company });

                entity.ToTable("Vehicles_retired", "Equipment");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Company).HasMaxLength(25);

                entity.Property(e => e.BodyStyle).HasMaxLength(25);

                entity.Property(e => e.Color).HasMaxLength(25);

                entity.Property(e => e.Config).HasMaxLength(25);

                entity.Property(e => e.CurrentMileageDate).HasColumnType("date");

                entity.Property(e => e.Description).HasMaxLength(255);

                entity.Property(e => e.DriveTrain).HasMaxLength(25);

                entity.Property(e => e.EngineType).HasMaxLength(25);

                entity.Property(e => e.FuelType).HasMaxLength(25);

                entity.Property(e => e.IssueDate).HasColumnType("date");

                entity.Property(e => e.Mfg).HasMaxLength(50);

                entity.Property(e => e.Model).HasMaxLength(50);

                entity.Property(e => e.OrigPurchasePrice).HasColumnType("money");

                entity.Property(e => e.PrimaryUser).HasMaxLength(50);

                entity.Property(e => e.PurchaseDate).HasColumnType("date");

                entity.Property(e => e.ReturnDate).HasColumnType("date");

                entity.Property(e => e.SalePrice).HasColumnType("money");

                entity.Property(e => e.SerialNum)
                    .HasColumnName("Serial_Num")
                    .HasMaxLength(50);

                entity.Property(e => e.SoldDate).HasColumnType("date");

                entity.Property(e => e.Tag).HasMaxLength(25);

                entity.Property(e => e.TagExpiration).HasColumnType("date");

                entity.Property(e => e.TagState)
                    .HasColumnName("Tag_State")
                    .HasMaxLength(25);

                entity.Property(e => e.Title).HasMaxLength(25);

                entity.Property(e => e.TitleState)
                    .HasColumnName("Title_State")
                    .HasMaxLength(25);
            });
        }
    }
}
