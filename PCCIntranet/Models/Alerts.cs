﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Alerts
    {
        public Guid Idnum { get; set; }
        public long Recnum { get; set; }
        public string Usrnme { get; set; }
        public string Dscrpt { get; set; }
        public string Messge { get; set; }
        public DateTime? Altdte { get; set; }
        public long? Qryrec { get; set; }
        public byte Status { get; set; }
        public string Grdfld { get; set; }
        public string Grddta { get; set; }
        public string Sntfrm { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }

        public Altqry QryrecNavigation { get; set; }
    }
}
