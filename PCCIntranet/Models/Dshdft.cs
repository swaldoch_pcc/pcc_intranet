﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Dshdft
    {
        public Guid Idnum { get; set; }
        public string Dftref { get; set; }
        public short Dfttyp { get; set; }
        public string Dftcrt { get; set; }
        public string Dftnme { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }
    }
}
