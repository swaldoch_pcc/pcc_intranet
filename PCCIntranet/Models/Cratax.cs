﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Cratax
    {
        public Guid Idnum { get; set; }
        public string Actnum { get; set; }
        public string Dscrpt { get; set; }
        public byte T4Act { get; set; }
        public byte T5018 { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }
    }
}
