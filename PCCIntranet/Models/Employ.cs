﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Employ
    {
        public Employ()
        {
            AcaempIdrefNavigation = new HashSet<Acaemp>();
            AcaempRecnumNavigation = new HashSet<Acaemp>();
            AcaindIdrefNavigation = new HashSet<Acaind>();
            AcaindRecnumNavigation = new HashSet<Acaind>();
            ActrecEstempNavigation = new HashSet<Actrec>();
            ActrecSlsempNavigation = new HashSet<Actrec>();
            ActrecSprvsrNavigation = new HashSet<Actrec>();
            AlkdomIdrefNavigation = new HashSet<Alkdom>();
            Coresp = new HashSet<Coresp>();
            Dlypyr = new HashSet<Dlypyr>();
            EmpcntIdrefNavigation = new HashSet<Empcnt>();
            EmpcntRecnumNavigation = new HashSet<Empcnt>();
            EmplicEmpnumNavigation = new HashSet<Emplic>();
            EmplicIdrefNavigation = new HashSet<Emplic>();
            EmppayEmpnumNavigation = new HashSet<Emppay>();
            EmppayIdrefNavigation = new HashSet<Emppay>();
            EmpqtdIdrefNavigation = new HashSet<Empqtd>();
            EmpqtdRecnumNavigation = new HashSet<Empqtd>();
            EmpqueIdrefNavigation = new HashSet<Empque>();
            EmptrnEmpnumNavigation = new HashSet<Emptrn>();
            EmptrnIdrefNavigation = new HashSet<Emptrn>();
            Eqpcst = new HashSet<Eqpcst>();
            Eqpmln = new HashSet<Eqpmln>();
            Fdrpln = new HashSet<Fdrpln>();
            Fldrpt = new HashSet<Fldrpt>();
            Invloc = new HashSet<Invloc>();
            Jobcst = new HashSet<Jobcst>();
            Payrec = new HashSet<Payrec>();
            Pchord = new HashSet<Pchord>();
            Prmchg = new HashSet<Prmchg>();
            RecclnEmpnumNavigation = new HashSet<Reccln>();
            RecclnMannumNavigation = new HashSet<Reccln>();
            Reqinf = new HashSet<Reqinf>();
            Reqprp = new HashSet<Reqprp>();
            Schemp = new HashSet<Schemp>();
            SrvinvEmpnumNavigation = new HashSet<Srvinv>();
            SrvinvSlspsnNavigation = new HashSet<Srvinv>();
            Srvsch = new HashSet<Srvsch>();
            Submtl = new HashSet<Submtl>();
            Tmemln = new HashSet<Tmemln>();
            Trnmtl = new HashSet<Trnmtl>();
        }

        public Guid Idnum { get; set; }
        public long Recnum { get; set; }
        public string Lstnme { get; set; }
        public string Fstnme { get; set; }
        public string Midini { get; set; }
        public byte Status { get; set; }
        public string Addrs1 { get; set; }
        public string Addrs2 { get; set; }
        public string Ctynme { get; set; }
        public string State { get; set; }
        public string Zipcde { get; set; }
        public string Phnnum { get; set; }
        public string Pagnum { get; set; }
        public string Faxnum { get; set; }
        public string Cllphn { get; set; }
        public string Homnum { get; set; }
        public string EMail { get; set; }
        public string Socsec { get; set; }
        public long? Eqpnum { get; set; }
        public DateTime? Dtebth { get; set; }
        public DateTime? Dtehre { get; set; }
        public DateTime? Dteina { get; set; }
        public DateTime? Lstrse { get; set; }
        public byte Gender { get; set; }
        public byte Mrtsts { get; set; }
        public byte Hertge { get; set; }
        public int Paypst { get; set; }
        public int Wrkcmp { get; set; }
        public byte Empcmp { get; set; }
        public byte Crtrpt { get; set; }
        public string Taxste { get; set; }
        public byte Payprd { get; set; }
        public int? Paygrp { get; set; }
        public decimal Payrt1 { get; set; }
        public decimal Payrt2 { get; set; }
        public decimal Payrt3 { get; set; }
        public decimal Salary { get; set; }
        public decimal Advdue { get; set; }
        public decimal Accsck { get; set; }
        public decimal Sckrte { get; set; }
        public byte Sckmth { get; set; }
        public decimal Accvac { get; set; }
        public decimal Vacrte { get; set; }
        public byte Vacmth { get; set; }
        public string Usrdf1 { get; set; }
        public string Usrdf2 { get; set; }
        public int? Loctax { get; set; }
        public int? Uninum { get; set; }
        public string Ntetxt { get; set; }
        public decimal Qt1grs { get; set; }
        public decimal Qt2grs { get; set; }
        public decimal Qt3grs { get; set; }
        public decimal Qt4grs { get; set; }
        public decimal Qt1fic { get; set; }
        public decimal Qt2fic { get; set; }
        public decimal Qt3fic { get; set; }
        public decimal Qt4fic { get; set; }
        public decimal Qt1med { get; set; }
        public decimal Qt2med { get; set; }
        public decimal Qt3med { get; set; }
        public decimal Qt4med { get; set; }
        public string Imgfle { get; set; }
        public byte I9verf { get; set; }
        public byte Exmovr { get; set; }
        public byte Emptyp { get; set; }
        public decimal Comisn { get; set; }
        public string Actnum { get; set; }
        public string Rtnmbr { get; set; }
        public byte Prente { get; set; }
        public byte Acttyp { get; set; }
        public decimal Depamt { get; set; }
        public string Actnm2 { get; set; }
        public string Rtnmb2 { get; set; }
        public byte Prent2 { get; set; }
        public byte Acttp2 { get; set; }
        public decimal Dp2amt { get; set; }
        public string Actnm3 { get; set; }
        public string Rtnmb3 { get; set; }
        public byte Prent3 { get; set; }
        public byte Acttp3 { get; set; }
        public decimal Dp3amt { get; set; }
        public string Actnm4 { get; set; }
        public string Rtnmb4 { get; set; }
        public byte Prent4 { get; set; }
        public byte Acttp4 { get; set; }
        public decimal Dp4amt { get; set; }
        public byte Dirdep { get; set; }
        public byte Retchk { get; set; }
        public string Fullst { get; set; }
        public string Fulfst { get; set; }
        public byte Rtetyp { get; set; }
        public byte Rtetp2 { get; set; }
        public byte Rtetp3 { get; set; }
        public byte Rtetp4 { get; set; }
        public byte Sckchk { get; set; }
        public byte Htglst { get; set; }
        public byte Hiract { get; set; }
        public string Pyreml { get; set; }
        public int? Locwrk { get; set; }
        public decimal Othded { get; set; }
        public decimal Nthzne { get; set; }
        public decimal Lbspfn { get; set; }
        public decimal Addtax { get; set; }
        public byte Eftsts { get; set; }
        public string Eftiid { get; set; }
        public string Eftrtn { get; set; }
        public string Eftact { get; set; }
        public byte Eftst2 { get; set; }
        public string Eftid2 { get; set; }
        public string Eftrt2 { get; set; }
        public string Eftac2 { get; set; }
        public byte Eftst3 { get; set; }
        public string Eftid3 { get; set; }
        public string Eftrt3 { get; set; }
        public string Eftac3 { get; set; }
        public byte Eftst4 { get; set; }
        public string Eftid4 { get; set; }
        public string Eftrt4 { get; set; }
        public string Eftac4 { get; set; }
        public DateTime? Fstwrk { get; set; }
        public DateTime? Lstroe { get; set; }
        public decimal Vacdue { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }
        public byte W2elc { get; set; }
        public byte Acaelc { get; set; }
        public decimal Sckmax { get; set; }
        public decimal Sckbeg { get; set; }
        public int Paycls { get; set; }
        public byte Inactv { get; set; }
        public decimal Vacmax { get; set; }
        public decimal Vacbeg { get; set; }
        public decimal Scklmt { get; set; }
        public decimal Sckytd { get; set; }
        public decimal Fdskrt { get; set; }
        public decimal Fdsklm { get; set; }
        public decimal Fdsktd { get; set; }
        public decimal Vaclmt { get; set; }
        public decimal Vacytd { get; set; }
        public byte Eeoprx { get; set; }
        public byte W41C { get; set; }
        public byte W42C { get; set; }
        public decimal W43 { get; set; }
        public decimal W44A { get; set; }
        public decimal W44B { get; set; }
        public decimal W44C { get; set; }
        public DateTime? W4Dte { get; set; }

        public Eqpmnt EqpnumNavigation { get; set; }
        public Loctax LoctaxNavigation { get; set; }
        public Loctax LocwrkNavigation { get; set; }
        public Paygrp PaygrpNavigation { get; set; }
        public Paypst PaypstNavigation { get; set; }
        public Payuni UninumNavigation { get; set; }
        public Wkrcmp WrkcmpNavigation { get; set; }
        public Alkdom AlkdomRecnumNavigation { get; set; }
        public EmplU EmplU { get; set; }
        public Empque EmpqueRecnumNavigation { get; set; }
        public ICollection<Acaemp> AcaempIdrefNavigation { get; set; }
        public ICollection<Acaemp> AcaempRecnumNavigation { get; set; }
        public ICollection<Acaind> AcaindIdrefNavigation { get; set; }
        public ICollection<Acaind> AcaindRecnumNavigation { get; set; }
        public ICollection<Actrec> ActrecEstempNavigation { get; set; }
        public ICollection<Actrec> ActrecSlsempNavigation { get; set; }
        public ICollection<Actrec> ActrecSprvsrNavigation { get; set; }
        public ICollection<Alkdom> AlkdomIdrefNavigation { get; set; }
        public ICollection<Coresp> Coresp { get; set; }
        public ICollection<Dlypyr> Dlypyr { get; set; }
        public ICollection<Empcnt> EmpcntIdrefNavigation { get; set; }
        public ICollection<Empcnt> EmpcntRecnumNavigation { get; set; }
        public ICollection<Emplic> EmplicEmpnumNavigation { get; set; }
        public ICollection<Emplic> EmplicIdrefNavigation { get; set; }
        public ICollection<Emppay> EmppayEmpnumNavigation { get; set; }
        public ICollection<Emppay> EmppayIdrefNavigation { get; set; }
        public ICollection<Empqtd> EmpqtdIdrefNavigation { get; set; }
        public ICollection<Empqtd> EmpqtdRecnumNavigation { get; set; }
        public ICollection<Empque> EmpqueIdrefNavigation { get; set; }
        public ICollection<Emptrn> EmptrnEmpnumNavigation { get; set; }
        public ICollection<Emptrn> EmptrnIdrefNavigation { get; set; }
        public ICollection<Eqpcst> Eqpcst { get; set; }
        public ICollection<Eqpmln> Eqpmln { get; set; }
        public ICollection<Fdrpln> Fdrpln { get; set; }
        public ICollection<Fldrpt> Fldrpt { get; set; }
        public ICollection<Invloc> Invloc { get; set; }
        public ICollection<Jobcst> Jobcst { get; set; }
        public ICollection<Payrec> Payrec { get; set; }
        public ICollection<Pchord> Pchord { get; set; }
        public ICollection<Prmchg> Prmchg { get; set; }
        public ICollection<Reccln> RecclnEmpnumNavigation { get; set; }
        public ICollection<Reccln> RecclnMannumNavigation { get; set; }
        public ICollection<Reqinf> Reqinf { get; set; }
        public ICollection<Reqprp> Reqprp { get; set; }
        public ICollection<Schemp> Schemp { get; set; }
        public ICollection<Srvinv> SrvinvEmpnumNavigation { get; set; }
        public ICollection<Srvinv> SrvinvSlspsnNavigation { get; set; }
        public ICollection<Srvsch> Srvsch { get; set; }
        public ICollection<Submtl> Submtl { get; set; }
        public ICollection<Tmemln> Tmemln { get; set; }
        public ICollection<Trnmtl> Trnmtl { get; set; }
    }
}
