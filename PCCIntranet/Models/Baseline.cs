﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Baseline
    {
        public string JobsiteId { get; set; }
        public string Jobsite { get; set; }
        public string ScheduleId { get; set; }
        public string Title { get; set; }
        public string BaseDurationD { get; set; }
        public string ActualDurationD { get; set; }
        public string BaseStartDate { get; set; }
        public string BaseEndDate { get; set; }
        public string DirectShifts { get; set; }
        public string DurationChange { get; set; }
        public string OverallSlip { get; set; }
        public string Reasons { get; set; }
        public string ShiftNotes { get; set; }
        public string AssignedTo { get; set; }
    }
}
