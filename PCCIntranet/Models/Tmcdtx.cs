﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Tmcdtx
    {
        public Guid Idnum { get; set; }
        public Guid Idref { get; set; }
        public long Recnum { get; set; }
        public int Locale { get; set; }
        public decimal Wgettl { get; set; }
        public decimal Locrte { get; set; }
        public decimal Loctax { get; set; }
        public int? Resloc { get; set; }
        public decimal Resrte { get; set; }
        public decimal Effrte { get; set; }
        public decimal Restax { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }

        public Payrec IdrefNavigation { get; set; }
        public Loctax LocaleNavigation { get; set; }
        public Payrec RecnumNavigation { get; set; }
        public Loctax ReslocNavigation { get; set; }
    }
}
