﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Cstcmp
    {
        public Cstcmp()
        {
            CscplnIdrefNavigation = new HashSet<Cscpln>();
            CscplnRecnumNavigation = new HashSet<Cscpln>();
        }

        public Guid Idnum { get; set; }
        public long Recnum { get; set; }
        public decimal Lbrbur { get; set; }
        public DateTime? Edtdte { get; set; }
        public string Usrnme { get; set; }
        public string Ntetxt { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }

        public Actrec RecnumNavigation { get; set; }
        public ICollection<Cscpln> CscplnIdrefNavigation { get; set; }
        public ICollection<Cscpln> CscplnRecnumNavigation { get; set; }
    }
}
