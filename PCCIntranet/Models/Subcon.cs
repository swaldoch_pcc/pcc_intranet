﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Subcon
    {
        public Subcon()
        {
            Sbcgln = new HashSet<Sbcgln>();
            SbcnlnIdrefNavigation = new HashSet<Sbcnln>();
            SbcnlnRecnumNavigation = new HashSet<Sbcnln>();
        }

        public Guid Idnum { get; set; }
        public long Recnum { get; set; }
        public string Ctcnum { get; set; }
        public long? Divnum { get; set; }
        public long Vndnum { get; set; }
        public long Jobnum { get; set; }
        public long Phsnum { get; set; }
        public string Dscrpt { get; set; }
        public int? Taxdst { get; set; }
        public string Usrdf1 { get; set; }
        public string Usrdf2 { get; set; }
        public byte Status { get; set; }
        public byte? Contyp { get; set; }
        public decimal Rtnrte { get; set; }
        public decimal Pmtbnd { get; set; }
        public decimal Prfbnd { get; set; }
        public DateTime? Appdte { get; set; }
        public DateTime? Condte { get; set; }
        public DateTime? Orgstr { get; set; }
        public DateTime? Orgfin { get; set; }
        public DateTime? Strdte { get; set; }
        public DateTime? Subcmp { get; set; }
        public DateTime? Findte { get; set; }
        public byte Hotlst { get; set; }
        public byte Paysts { get; set; }
        public byte Lckedt { get; set; }
        public DateTime? Entdte { get; set; }
        public string Usrnme { get; set; }
        public decimal Cntttl { get; set; }
        public decimal Chgttl { get; set; }
        public decimal Ctcttl { get; set; }
        public decimal Taxttl { get; set; }
        public decimal Invttl { get; set; }
        public decimal Balttl { get; set; }
        public string Ntetxt { get; set; }
        public decimal Gstamt { get; set; }
        public decimal Pstamt { get; set; }
        public decimal Hstamt { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }

        public Sbctyp ContypNavigation { get; set; }
        public Cstdiv DivnumNavigation { get; set; }
        public Actrec JobnumNavigation { get; set; }
        public Jobphs Jobphs { get; set; }
        public Taxdst TaxdstNavigation { get; set; }
        public Actpay VndnumNavigation { get; set; }
        public SubcU SubcU { get; set; }
        public ICollection<Sbcgln> Sbcgln { get; set; }
        public ICollection<Sbcnln> SbcnlnIdrefNavigation { get; set; }
        public ICollection<Sbcnln> SbcnlnRecnumNavigation { get; set; }
    }
}
