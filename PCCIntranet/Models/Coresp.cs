﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Coresp
    {
        public Guid Idnum { get; set; }
        public long Recnum { get; set; }
        public long Jobnum { get; set; }
        public long Phsnum { get; set; }
        public long? Divnum { get; set; }
        public string Dscrpt { get; set; }
        public string Usrdf1 { get; set; }
        public string Usrdf2 { get; set; }
        public long? Clnnum { get; set; }
        public long? Vndnum { get; set; }
        public string Attion { get; set; }
        public string Addrs1 { get; set; }
        public string Addrs2 { get; set; }
        public string Ctynme { get; set; }
        public string State { get; set; }
        public string Zipcde { get; set; }
        public byte Status { get; set; }
        public byte? Cortyp { get; set; }
        public DateTime? Sntdte { get; set; }
        public DateTime? Recdte { get; set; }
        public long? Empnum { get; set; }
        public string Sntvia { get; set; }
        public string Cortxt { get; set; }
        public DateTime? Entdte { get; set; }
        public string Usrnme { get; set; }
        public byte Hotlst { get; set; }
        public byte Lckedt { get; set; }
        public string Ntetxt { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }

        public Reccln ClnnumNavigation { get; set; }
        public Cortyp CortypNavigation { get; set; }
        public Cstdiv DivnumNavigation { get; set; }
        public Employ EmpnumNavigation { get; set; }
        public Actrec JobnumNavigation { get; set; }
        public Jobphs Jobphs { get; set; }
        public Actpay VndnumNavigation { get; set; }
        public CoreU CoreU { get; set; }
    }
}
