﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Jobcst
    {
        public Guid Idnum { get; set; }
        public long Recnum { get; set; }
        public string Wrkord { get; set; }
        public long Jobnum { get; set; }
        public string Trnnum { get; set; }
        public string Dscrpt { get; set; }
        public DateTime? Trndte { get; set; }
        public DateTime? Entdte { get; set; }
        public byte Actprd { get; set; }
        public byte Srcnum { get; set; }
        public byte Status { get; set; }
        public byte Bllsts { get; set; }
        public long Phsnum { get; set; }
        public decimal Cstcde { get; set; }
        public byte Csttyp { get; set; }
        public long? Vndnum { get; set; }
        public long? Eqpnum { get; set; }
        public long? Empnum { get; set; }
        public long? Payrec { get; set; }
        public byte Paytyp { get; set; }
        public decimal Csthrs { get; set; }
        public decimal Cstamt { get; set; }
        public decimal Blgqty { get; set; }
        public decimal Blgamt { get; set; }
        public decimal Pieces { get; set; }
        public long Lgrrec { get; set; }
        public byte Blgunt { get; set; }
        public byte Eqptyp { get; set; }
        public byte Eqpunt { get; set; }
        public decimal Eqpqty { get; set; }
        public decimal Grswge { get; set; }
        public byte Ovrrde { get; set; }
        public decimal Blgttl { get; set; }
        public byte Active { get; set; }
        public long Acrinv { get; set; }
        public decimal Shwamt { get; set; }
        public decimal Ovhamt { get; set; }
        public decimal Pftamt { get; set; }
        public byte Taxabl { get; set; }
        public string Usrnme { get; set; }
        public string Ntetxt { get; set; }
        public short Postyr { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }

        public Cstcde CstcdeNavigation { get; set; }
        public Csttyp CsttypNavigation { get; set; }
        public Employ EmpnumNavigation { get; set; }
        public Eqpmnt EqpnumNavigation { get; set; }
        public Actrec JobnumNavigation { get; set; }
        public Jobphs Jobphs { get; set; }
        public Payrec PayrecNavigation { get; set; }
        public Source SrcnumNavigation { get; set; }
        public Actpay VndnumNavigation { get; set; }
    }
}
