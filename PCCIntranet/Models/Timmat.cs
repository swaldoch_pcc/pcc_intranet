﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Timmat
    {
        public Guid Idnum { get; set; }
        public long Recnum { get; set; }
        public int? Emptbl { get; set; }
        public int? Eqptbl { get; set; }
        public decimal Mtrhdn { get; set; }
        public decimal Mtrshw { get; set; }
        public decimal Mtrovh { get; set; }
        public decimal Mtrpft { get; set; }
        public decimal Labhdn { get; set; }
        public decimal Labshw { get; set; }
        public decimal Labovh { get; set; }
        public decimal Labpft { get; set; }
        public decimal Eqphdn { get; set; }
        public decimal Eqpshw { get; set; }
        public decimal Eqpovh { get; set; }
        public decimal Eqppft { get; set; }
        public decimal Subhdn { get; set; }
        public decimal Subshw { get; set; }
        public decimal Subovh { get; set; }
        public decimal Subpft { get; set; }
        public decimal Otrhdn { get; set; }
        public decimal Otrshw { get; set; }
        public decimal Otrovh { get; set; }
        public decimal Otrpft { get; set; }
        public decimal Cs6hdn { get; set; }
        public decimal Cs6shw { get; set; }
        public decimal Cs6ovh { get; set; }
        public decimal Cs6pft { get; set; }
        public decimal Cs7hdn { get; set; }
        public decimal Cs7shw { get; set; }
        public decimal Cs7ovh { get; set; }
        public decimal Cs7pft { get; set; }
        public decimal Cs8hdn { get; set; }
        public decimal Cs8shw { get; set; }
        public decimal Cs8ovh { get; set; }
        public decimal Cs8pft { get; set; }
        public decimal Cs9hdn { get; set; }
        public decimal Cs9shw { get; set; }
        public decimal Cs9ovh { get; set; }
        public decimal Cs9pft { get; set; }
        public byte Clcsts { get; set; }
        public DateTime? Invdte { get; set; }
        public decimal Invttl { get; set; }
        public decimal Subjct { get; set; }
        public decimal Slstax { get; set; }
        public string Taxdtl { get; set; }
        public long Jobphs { get; set; }
        public string Invnum { get; set; }
        public string Ntetxt { get; set; }
        public long Incact { get; set; }
        public long? Incsub { get; set; }
        public decimal Pstsbj { get; set; }
        public decimal Pstamt { get; set; }
        public decimal Gstsbj { get; set; }
        public decimal Gstamt { get; set; }
        public decimal Hstsbj { get; set; }
        public decimal Hstamt { get; set; }
        public decimal Taxhld { get; set; }
        public decimal Ntxhld { get; set; }
        public byte Pstmat { get; set; }
        public byte Pstlab { get; set; }
        public byte Psteqp { get; set; }
        public byte Pstsub { get; set; }
        public byte Pstoth { get; set; }
        public byte Pstcs6 { get; set; }
        public byte Pstcs7 { get; set; }
        public byte Pstcs8 { get; set; }
        public byte Pstcs9 { get; set; }
        public byte Pstmpf { get; set; }
        public byte Pstlpf { get; set; }
        public byte Pstepf { get; set; }
        public byte Pstspf { get; set; }
        public byte Pstopf { get; set; }
        public byte Pst6pf { get; set; }
        public byte Pst7pf { get; set; }
        public byte Pst8pf { get; set; }
        public byte Pst9pf { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }

        public Timemp EmptblNavigation { get; set; }
        public Timeqp EqptblNavigation { get; set; }
        public Lgrsub Inc { get; set; }
        public Lgract IncactNavigation { get; set; }
        public Actrec RecnumNavigation { get; set; }
    }
}
