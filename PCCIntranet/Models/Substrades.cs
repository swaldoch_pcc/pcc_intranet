using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
  public partial class SubsTrades
  {
    public int? Tradeid { get; set; }
    public string Tradename { get; set; }
    public int? Divnum { get; set; }
    public string Divname { get; set; }
    public int? Vndnum { get; set; }
    public int Id { get; set; }
  }
}
