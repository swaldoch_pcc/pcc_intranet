﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Bnklin
    {
        public Guid Idnum { get; set; }
        public Guid Idref { get; set; }
        public long Recnum { get; set; }
        public int Linnum { get; set; }
        public long Lgrrcd { get; set; }
        public byte Lintyp { get; set; }
        public byte Ispryr { get; set; }
        public byte Status { get; set; }
        public string Trnnum { get; set; }
        public DateTime? Trndte { get; set; }
        public string Payee { get; set; }
        public decimal Amount { get; set; }
        public string Trnhsh { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }

        public Bnkrec IdrefNavigation { get; set; }
        public Bnkrec RecnumNavigation { get; set; }
    }
}
