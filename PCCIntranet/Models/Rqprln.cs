﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Rqprln
    {
        public Guid Idnum { get; set; }
        public Guid Idref { get; set; }
        public long Recnum { get; set; }
        public int Linnum { get; set; }
        public long? Prtnum { get; set; }
        public string Prtdsc { get; set; }
        public string Alpnum { get; set; }
        public string Untdsc { get; set; }
        public decimal Linqty { get; set; }
        public decimal Linprc { get; set; }
        public decimal Extttl { get; set; }
        public decimal? Cstcde { get; set; }
        public byte? Csttyp { get; set; }
        public string Usrdf1 { get; set; }
        public string Ntetxt { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }

        public Cstcde CstcdeNavigation { get; set; }
        public Csttyp CsttypNavigation { get; set; }
        public Reqprp IdrefNavigation { get; set; }
        public Tkfprt PrtnumNavigation { get; set; }
        public Reqprp RecnumNavigation { get; set; }
    }
}
