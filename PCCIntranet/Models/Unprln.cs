﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Unprln
    {
        public Guid Idnum { get; set; }
        public Guid Idref { get; set; }
        public long Recnum { get; set; }
        public long Phsnum { get; set; }
        public int Linnum { get; set; }
        public long Biditm { get; set; }
        public string Itmcde { get; set; }
        public string Dscrpt { get; set; }
        public string Untdsc { get; set; }
        public decimal Numunt { get; set; }
        public decimal Cstunt { get; set; }
        public decimal Ttlcst { get; set; }
        public string Usrdf1 { get; set; }
        public string Ntetxt { get; set; }
        public decimal Untorg { get; set; }
        public decimal Cstorg { get; set; }
        public decimal Ttlorg { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }

        public Biditm BiditmNavigation { get; set; }
        public Untprp IdrefNavigation { get; set; }
        public Jobphs Jobphs { get; set; }
        public Untprp RecnumNavigation { get; set; }
    }
}
