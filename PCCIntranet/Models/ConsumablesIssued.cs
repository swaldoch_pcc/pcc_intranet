﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class ConsumablesIssued
    {
        public Guid Id { get; set; }
        public string Itemid { get; set; }
        public string Job { get; set; }
        public string User { get; set; }
        public string Description { get; set; }
        public int? Quantity { get; set; }
        public string Comments { get; set; }
        public DateTime? Issuedate { get; set; }
    }
}
