﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Conlst
    {
        public Conlst()
        {
            Reccln = new HashSet<Reccln>();
            Srvcnt = new HashSet<Srvcnt>();
        }

        public Guid Idnum { get; set; }
        public byte Recnum { get; set; }
        public string Dscrpt { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }

        public ICollection<Reccln> Reccln { get; set; }
        public ICollection<Srvcnt> Srvcnt { get; set; }
    }
}
