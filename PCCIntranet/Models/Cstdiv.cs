﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Cstdiv
    {
        public Cstdiv()
        {
            Aialin = new HashSet<Aialin>();
            Coresp = new HashSet<Coresp>();
            Cstcde = new HashSet<Cstcde>();
            Pclsln = new HashSet<Pclsln>();
            Plnrcd = new HashSet<Plnrcd>();
            Reqinf = new HashSet<Reqinf>();
            Subcon = new HashSet<Subcon>();
            Submtl = new HashSet<Submtl>();
            Trnmtl = new HashSet<Trnmtl>();
        }

        public Guid Idnum { get; set; }
        public long Recnum { get; set; }
        public string Divnme { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }

        public ICollection<Aialin> Aialin { get; set; }
        public ICollection<Coresp> Coresp { get; set; }
        public ICollection<Cstcde> Cstcde { get; set; }
        public ICollection<Pclsln> Pclsln { get; set; }
        public ICollection<Plnrcd> Plnrcd { get; set; }
        public ICollection<Reqinf> Reqinf { get; set; }
        public ICollection<Subcon> Subcon { get; set; }
        public ICollection<Submtl> Submtl { get; set; }
        public ICollection<Trnmtl> Trnmtl { get; set; }
    }
}
