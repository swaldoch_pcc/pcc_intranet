﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Asmprt
    {
        public Guid Idnum { get; set; }
        public Guid Idref { get; set; }
        public long Recnum { get; set; }
        public int Linnum { get; set; }
        public string Prtchk { get; set; }
        public long? Prtnum { get; set; }
        public string Prtnme { get; set; }
        public string Alpnum { get; set; }
        public string Prtunt { get; set; }
        public decimal Prtqty { get; set; }
        public decimal Prtbil { get; set; }
        public decimal Extprc { get; set; }
        public string Prtfrm { get; set; }
        public decimal? Prtcde { get; set; }
        public byte? Prttyp { get; set; }
        public decimal? Prttsk { get; set; }
        public string Usrdf1 { get; set; }
        public string Ntetxt { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }

        public Assemb IdrefNavigation { get; set; }
        public Cstcde PrtcdeNavigation { get; set; }
        public Tkfprt PrtnumNavigation { get; set; }
        public Schtsk PrttskNavigation { get; set; }
        public Csttyp PrttypNavigation { get; set; }
        public Assemb RecnumNavigation { get; set; }
    }
}
