﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Lonlin
    {
        public Guid Idnum { get; set; }
        public Guid Idref { get; set; }
        public long Recnum { get; set; }
        public int Linnum { get; set; }
        public decimal? Cstcde { get; set; }
        public string Cdenme { get; set; }
        public long? Vndnum { get; set; }
        public decimal Schamt { get; set; }
        public decimal Chgord { get; set; }
        public decimal Newcon { get; set; }
        public decimal Prvbll { get; set; }
        public decimal Curbll { get; set; }
        public decimal Pctcmp { get; set; }
        public decimal Ttlcmp { get; set; }
        public decimal Balcon { get; set; }
        public string Usrdf1 { get; set; }
        public string Ntetxt { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }

        public Cstcde CstcdeNavigation { get; set; }
        public Lonfrm IdrefNavigation { get; set; }
        public Lonfrm RecnumNavigation { get; set; }
        public Actpay VndnumNavigation { get; set; }
    }
}
