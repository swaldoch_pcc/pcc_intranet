﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Aialin
    {
        public Guid Idnum { get; set; }
        public Guid Idref { get; set; }
        public long Recnum { get; set; }
        public int Linnum { get; set; }
        public long? Divnum { get; set; }
        public decimal? Cstcde { get; set; }
        public string Dscrpt { get; set; }
        public long? Vndnum { get; set; }
        public decimal Schamt { get; set; }
        public decimal Chgamt { get; set; }
        public decimal Newcon { get; set; }
        public decimal Prvbll { get; set; }
        public decimal Curbll { get; set; }
        public decimal Retrte { get; set; }
        public decimal Curret { get; set; }
        public decimal Prvstr { get; set; }
        public decimal Curstr { get; set; }
        public decimal Strmat { get; set; }
        public decimal Strret { get; set; }
        public decimal Ttlcmp { get; set; }
        public decimal Pctcmp { get; set; }
        public decimal Balfin { get; set; }
        public decimal Retamt { get; set; }
        public decimal Prvhld { get; set; }
        public string Taxabl { get; set; }
        public string Gstsbj { get; set; }
        public decimal Gstamt { get; set; }
        public string Pstsbj { get; set; }
        public decimal Pstamt { get; set; }
        public string Hstsbj { get; set; }
        public decimal Hstamt { get; set; }
        public string Usrdf1 { get; set; }
        public string Ntetxt { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }

        public Cstcde CstcdeNavigation { get; set; }
        public Cstdiv DivnumNavigation { get; set; }
        public Aiafrm IdrefNavigation { get; set; }
        public Aiafrm RecnumNavigation { get; set; }
        public Actpay VndnumNavigation { get; set; }
    }
}
