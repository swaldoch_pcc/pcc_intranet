﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Apttyp
    {
        public Apttyp()
        {
            Calnte = new HashSet<Calnte>();
        }

        public Guid Idnum { get; set; }
        public byte Recnum { get; set; }
        public string Dscrpt { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }

        public ICollection<Calnte> Calnte { get; set; }
    }
}
