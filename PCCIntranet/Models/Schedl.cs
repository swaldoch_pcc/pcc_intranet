﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Schedl
    {
        public Schedl()
        {
            SchdteIdrefNavigation = new HashSet<Schdte>();
            SchdteSchedl = new HashSet<Schdte>();
            Schemp = new HashSet<Schemp>();
            Scheqp = new HashSet<Scheqp>();
            SchlinIdrefNavigation = new HashSet<Schlin>();
            SchlinSchedl = new HashSet<Schlin>();
            Schprd = new HashSet<Schprd>();
            Schsub = new HashSet<Schsub>();
        }

        public Guid Idnum { get; set; }
        public long Recnum { get; set; }
        public long Schphs { get; set; }
        public DateTime? Edtdte { get; set; }
        public string Usrnme { get; set; }
        public DateTime? Strdte { get; set; }
        public DateTime? Findte { get; set; }
        public string Chkbox { get; set; }
        public string Ntetxt { get; set; }
        public int Wrkdys { get; set; }
        public int Caldys { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }

        public Jobphs Jobphs { get; set; }
        public Actrec RecnumNavigation { get; set; }
        public ScheU ScheU { get; set; }
        public ICollection<Schdte> SchdteIdrefNavigation { get; set; }
        public ICollection<Schdte> SchdteSchedl { get; set; }
        public ICollection<Schemp> Schemp { get; set; }
        public ICollection<Scheqp> Scheqp { get; set; }
        public ICollection<Schlin> SchlinIdrefNavigation { get; set; }
        public ICollection<Schlin> SchlinSchedl { get; set; }
        public ICollection<Schprd> Schprd { get; set; }
        public ICollection<Schsub> Schsub { get; set; }
    }
}
