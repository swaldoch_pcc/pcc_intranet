﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Categorypricing
    {
        public Guid PriceId { get; set; }
        public string Isactive { get; set; }
        public string Category { get; set; }
        public string Subcategory { get; set; }
        public decimal? Dailyrate { get; set; }
        public DateTime? Ratelastchange { get; set; }
        public string Changedby { get; set; }
        public int? Pricenum { get; set; }
        public string Jobbillable { get; set; }
        public string Billonce { get; set; }
    }
}
