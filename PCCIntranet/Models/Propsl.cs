﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Propsl
    {
        public Propsl()
        {
            PrplinIdrefNavigation = new HashSet<Prplin>();
            PrplinRecnumNavigation = new HashSet<Prplin>();
        }

        public Guid Idnum { get; set; }
        public long Recnum { get; set; }
        public byte Lckedt { get; set; }
        public byte Hotlst { get; set; }
        public DateTime? Edtdte { get; set; }
        public string Usrnme { get; set; }
        public byte Colshw { get; set; }
        public string Ntetxt { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }

        public Actrec RecnumNavigation { get; set; }
        public PropU PropU { get; set; }
        public ICollection<Prplin> PrplinIdrefNavigation { get; set; }
        public ICollection<Prplin> PrplinRecnumNavigation { get; set; }
    }
}
