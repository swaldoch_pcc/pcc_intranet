﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Usrmnu
    {
        public Usrmnu()
        {
            Mnuacs = new HashSet<Mnuacs>();
        }

        public Guid Idnum { get; set; }
        public byte Menu1 { get; set; }
        public byte Menu2 { get; set; }
        public byte Menu3 { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }

        public ICollection<Mnuacs> Mnuacs { get; set; }
    }
}
