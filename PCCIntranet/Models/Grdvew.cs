﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Grdvew
    {
        public Guid Idnum { get; set; }
        public string Vewnme { get; set; }
        public string Vewdta { get; set; }
        public byte Omwlck { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }
    }
}
