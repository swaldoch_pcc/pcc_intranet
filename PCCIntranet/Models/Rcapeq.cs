﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Rcapeq
    {
        public Guid Idnum { get; set; }
        public Guid Idref { get; set; }
        public long Recnum { get; set; }
        public long Eqpnum { get; set; }
        public string Dscrpt { get; set; }
        public decimal Cstcde { get; set; }
        public byte Csttyp { get; set; }
        public decimal Cstamt { get; set; }
        public string Ntetxt { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }

        public Cstcde CstcdeNavigation { get; set; }
        public Csttyp CsttypNavigation { get; set; }
        public Eqpmnt EqpnumNavigation { get; set; }
        public Rccpay IdrefNavigation { get; set; }
        public Rccpay RecnumNavigation { get; set; }
    }
}
