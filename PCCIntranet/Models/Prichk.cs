﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Prichk
    {
        public Guid Idnum { get; set; }
        public int Recnum { get; set; }
        public string Trnnum { get; set; }
        public DateTime? Trndte { get; set; }
        public string Paydsc { get; set; }
        public byte Status { get; set; }
        public long Lgract { get; set; }
        public decimal Dbtamt { get; set; }
        public decimal Crdamt { get; set; }
        public byte Srcnum { get; set; }
        public byte Active { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }
        public byte Bnkcat { get; set; }
        public string Trnhsh { get; set; }

        public Lgract LgractNavigation { get; set; }
        public Source SrcnumNavigation { get; set; }
    }
}
