﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Atchmt
    {
        public Atchmt()
        {
            AttdtlIdrefNavigation = new HashSet<Attdtl>();
            AttdtlRecnumNavigation = new HashSet<Attdtl>();
        }

        public Guid Idnum { get; set; }
        public long Recnum { get; set; }
        public string Tblnme { get; set; }
        public long Tblrec { get; set; }
        public string Prntbl { get; set; }
        public long Prnrec { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }

        public ICollection<Attdtl> AttdtlIdrefNavigation { get; set; }
        public ICollection<Attdtl> AttdtlRecnumNavigation { get; set; }
    }
}
