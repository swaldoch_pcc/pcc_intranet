﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Consumables
    {
        public Guid Id { get; set; }
        public string ItemNum { get; set; }
        public string Description { get; set; }
        public int? Quantity { get; set; }
        public string Vendor { get; set; }
        public string VendorPhone { get; set; }
        public string VendorEmail { get; set; }
        public string VendorWebsite { get; set; }
        public string WebsiteLoginName { get; set; }
        public string WebsiteLoginPassword { get; set; }
    }
}
