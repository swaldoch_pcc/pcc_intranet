﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Clnsts
    {
        public Clnsts()
        {
            Reccln = new HashSet<Reccln>();
        }

        public Guid Idnum { get; set; }
        public byte Recnum { get; set; }
        public string Stsnme { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }

        public ICollection<Reccln> Reccln { get; set; }
    }
}
