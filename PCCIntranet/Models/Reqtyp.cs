﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Reqtyp
    {
        public Reqtyp()
        {
            Actpay = new HashSet<Actpay>();
            Reqprp = new HashSet<Reqprp>();
        }

        public Guid Idnum { get; set; }
        public byte Recnum { get; set; }
        public string Typnme { get; set; }
        public string Ntetxt { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }

        public ICollection<Actpay> Actpay { get; set; }
        public ICollection<Reqprp> Reqprp { get; set; }
    }
}
