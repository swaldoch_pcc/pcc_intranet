﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Invhst
    {
        public Guid Idnum { get; set; }
        public long Recnum { get; set; }
        public long Lgrrec { get; set; }
        public long Prtnum { get; set; }
        public int Locnum { get; set; }
        public byte Srcnum { get; set; }
        public long Trnrec { get; set; }
        public string Trnnum { get; set; }
        public DateTime? Trndte { get; set; }
        public DateTime? Entdte { get; set; }
        public string Sernum { get; set; }
        public byte Actprd { get; set; }
        public decimal Prtqty { get; set; }
        public decimal Prtprc { get; set; }
        public decimal Extprc { get; set; }
        public short Postyr { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }

        public Invloc LocnumNavigation { get; set; }
        public Tkfprt PrtnumNavigation { get; set; }
        public Source SrcnumNavigation { get; set; }
    }
}
