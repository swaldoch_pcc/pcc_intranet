﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Fdrpeq
    {
        public Guid Idnum { get; set; }
        public Guid Idref { get; set; }
        public long Recnum { get; set; }
        public int Linnum { get; set; }
        public long? Eqpnum { get; set; }
        public string Dscrpt { get; set; }
        public decimal? Cstcde { get; set; }
        public byte Eqpunt { get; set; }
        public decimal Oprtim { get; set; }
        public decimal Stdtim { get; set; }
        public decimal Idltim { get; set; }
        public string Ntetxt { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }

        public Cstcde CstcdeNavigation { get; set; }
        public Eqpmnt EqpnumNavigation { get; set; }
        public Fldrpt IdrefNavigation { get; set; }
        public Fldrpt RecnumNavigation { get; set; }
    }
}
