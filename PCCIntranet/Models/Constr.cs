﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Constr
    {
        public Guid Idnum { get; set; }
        public short Cnstid { get; set; }
        public short Prcond { get; set; }
        public string Prbool { get; set; }
        public short Dpcond { get; set; }
        public string Dpbool { get; set; }
        public string Cnddsc { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }
    }
}
