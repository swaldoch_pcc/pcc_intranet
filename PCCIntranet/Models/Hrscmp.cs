﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Hrscmp
    {
        public Hrscmp()
        {
            HrcplnIdrefNavigation = new HashSet<Hrcpln>();
            HrcplnRecnumNavigation = new HashSet<Hrcpln>();
        }

        public Guid Idnum { get; set; }
        public long Recnum { get; set; }
        public DateTime? Edtdte { get; set; }
        public string Usrnme { get; set; }
        public string Ntetxt { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }

        public Actrec RecnumNavigation { get; set; }
        public ICollection<Hrcpln> HrcplnIdrefNavigation { get; set; }
        public ICollection<Hrcpln> HrcplnRecnumNavigation { get; set; }
    }
}
