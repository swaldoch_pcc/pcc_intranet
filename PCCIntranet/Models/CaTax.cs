﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class CaTax
    {
        public CaTax()
        {
            Acpinv = new HashSet<Acpinv>();
            Actpay = new HashSet<Actpay>();
            Apivln = new HashSet<Apivln>();
            Rcccrd = new HashSet<Rcccrd>();
            Rccpay = new HashSet<Rccpay>();
        }

        public Guid Idnum { get; set; }
        public short Recnum { get; set; }
        public string Dscrpt { get; set; }
        public string Prvabr { get; set; }
        public string Isprim { get; set; }
        public decimal Gstrte { get; set; }
        public decimal Pstrte { get; set; }
        public string Compnd { get; set; }
        public decimal Hstrte { get; set; }
        public decimal Rcprte { get; set; }
        public long? Ptclac { get; set; }
        public long Gtclac { get; set; }
        public long Gtpdac { get; set; }
        public long? Qtpdac { get; set; }
        public long? Ritcac { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }

        public Lgract GtclacNavigation { get; set; }
        public Lgract GtpdacNavigation { get; set; }
        public Lgract PtclacNavigation { get; set; }
        public Lgract QtpdacNavigation { get; set; }
        public Lgract RitcacNavigation { get; set; }
        public ICollection<Acpinv> Acpinv { get; set; }
        public ICollection<Actpay> Actpay { get; set; }
        public ICollection<Apivln> Apivln { get; set; }
        public ICollection<Rcccrd> Rcccrd { get; set; }
        public ICollection<Rccpay> Rccpay { get; set; }
    }
}
