﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Srvtyp
    {
        public Srvtyp()
        {
            Srvinv = new HashSet<Srvinv>();
        }

        public Guid Idnum { get; set; }
        public int Recnum { get; set; }
        public string Typnme { get; set; }
        public string Typclr { get; set; }
        public long? Dptmnt { get; set; }
        public long Cshact { get; set; }
        public long Taxinc { get; set; }
        public long Ntxinc { get; set; }
        public long Dscgvn { get; set; }
        public long Invexp { get; set; }
        public decimal Cstcde { get; set; }
        public byte Csttyp { get; set; }
        public string Ntetxt { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }

        public Lgract CshactNavigation { get; set; }
        public Cstcde CstcdeNavigation { get; set; }
        public Csttyp CsttypNavigation { get; set; }
        public Dptmnt DptmntNavigation { get; set; }
        public Lgract DscgvnNavigation { get; set; }
        public Lgract InvexpNavigation { get; set; }
        public Lgract NtxincNavigation { get; set; }
        public Lgract TaxincNavigation { get; set; }
        public ICollection<Srvinv> Srvinv { get; set; }
    }
}
