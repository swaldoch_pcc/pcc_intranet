﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Pchlst
    {
        public Pchlst()
        {
            PclslnIdrefNavigation = new HashSet<Pclsln>();
            PclslnRecnumNavigation = new HashSet<Pclsln>();
        }

        public Guid Idnum { get; set; }
        public long Recnum { get; set; }
        public long Jobnum { get; set; }
        public long Phsnum { get; set; }
        public string Dscrpt { get; set; }
        public string Aprvby { get; set; }
        public DateTime? Aprdte { get; set; }
        public string Usrdf1 { get; set; }
        public string Usrdf2 { get; set; }
        public byte Hotlst { get; set; }
        public DateTime? Entdte { get; set; }
        public string Usrnme { get; set; }
        public byte Lckedt { get; set; }
        public string Ntetxt { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }

        public Actrec JobnumNavigation { get; set; }
        public Jobphs Jobphs { get; set; }
        public PchlU PchlU { get; set; }
        public ICollection<Pclsln> PclslnIdrefNavigation { get; set; }
        public ICollection<Pclsln> PclslnRecnumNavigation { get; set; }
    }
}
