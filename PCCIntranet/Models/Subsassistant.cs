using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Subsassistant
    {
        public int Companyid { get; set; }
        public int Contactid { get; set; }
        public int Assistantid { get; set; }
        public string AsstFname { get; set; }
        public string AsstLname { get; set; }
        public string AsstTitle { get; set; }
        public string AsstLocationname { get; set; }
        public string AsstAddress1 { get; set; }
        public string AsstAddress2 { get; set; }
        public string AsstCity { get; set; }
        public string AsstState { get; set; }
        public string AsstZip { get; set; }
        public string AsstDeskphone { get; set; }
        public string AsstExt { get; set; }
        public string Asstmobilephone { get; set; }
        public string AsstEmail { get; set; }
        public string AsstEmail2 { get; set; }
        public string AsstNotes { get; set; }
    }
}
