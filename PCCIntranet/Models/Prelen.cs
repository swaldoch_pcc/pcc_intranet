﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Prelen
    {
        public Guid Idnum { get; set; }
        public Guid Idref { get; set; }
        public long Recnum { get; set; }
        public string Ownnme { get; set; }
        public string Ownad1 { get; set; }
        public string Ownad2 { get; set; }
        public string Owncty { get; set; }
        public string Ownste { get; set; }
        public string Ownzip { get; set; }
        public string Lndnme { get; set; }
        public string Lndad1 { get; set; }
        public string Lndad2 { get; set; }
        public string Lndcty { get; set; }
        public string Lndste { get; set; }
        public string Lndzip { get; set; }
        public string Connme { get; set; }
        public string Conad1 { get; set; }
        public string Conad2 { get; set; }
        public string Concty { get; set; }
        public string Conste { get; set; }
        public string Conzip { get; set; }
        public string Subnme { get; set; }
        public string Subad1 { get; set; }
        public string Subad2 { get; set; }
        public string Subcty { get; set; }
        public string Subste { get; set; }
        public string Subzip { get; set; }
        public string Cntnme { get; set; }
        public string Cntad1 { get; set; }
        public string Cntad2 { get; set; }
        public string Cntcty { get; set; }
        public string Cntste { get; set; }
        public string Cntzip { get; set; }
        public string Dscrpt { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }

        public Actrec IdrefNavigation { get; set; }
        public Actrec RecnumNavigation { get; set; }
    }
}
