﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Glbvar
    {
        public Guid Idnum { get; set; }
        public Guid Idref { get; set; }
        public long Recnum { get; set; }
        public string Dscrpt { get; set; }
        public string Abvnme { get; set; }
        public string Untdsc { get; set; }
        public decimal Glbval { get; set; }
        public string Ntetxt { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }
        public int Linnum { get; set; }

        public Ptotkf IdrefNavigation { get; set; }
        public Ptotkf RecnumNavigation { get; set; }
    }
}
