﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Rcarln
    {
        public Guid Idnum { get; set; }
        public Guid Idref { get; set; }
        public long Recnum { get; set; }
        public int Linnum { get; set; }
        public long? Prtnum { get; set; }
        public string Dscrpt { get; set; }
        public string Alpnum { get; set; }
        public string Untdsc { get; set; }
        public decimal Linqty { get; set; }
        public decimal Linprc { get; set; }
        public decimal Extprc { get; set; }
        public string Taxabl { get; set; }
        public decimal Hldrte { get; set; }
        public decimal Hldamt { get; set; }
        public decimal Bllamt { get; set; }
        public string Gstsbj { get; set; }
        public decimal Gstamt { get; set; }
        public string Pstsbj { get; set; }
        public decimal Pstamt { get; set; }
        public string Hstsbj { get; set; }
        public decimal Hstamt { get; set; }
        public long? Lgract { get; set; }
        public long? Subact { get; set; }
        public decimal? Cstcde { get; set; }
        public string Usrdf1 { get; set; }
        public string Ntetxt { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }

        public Cstcde CstcdeNavigation { get; set; }
        public Rccrec IdrefNavigation { get; set; }
        public Lgract LgractNavigation { get; set; }
        public Tkfprt PrtnumNavigation { get; set; }
        public Rccrec RecnumNavigation { get; set; }
    }
}
