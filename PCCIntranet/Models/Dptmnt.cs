﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Dptmnt
    {
        public Dptmnt()
        {
            Actrec = new HashSet<Actrec>();
            Cstcde = new HashSet<Cstcde>();
            Dlypyr = new HashSet<Dlypyr>();
            Eqpmnt = new HashSet<Eqpmnt>();
            Paypst = new HashSet<Paypst>();
            Srvtyp = new HashSet<Srvtyp>();
            Tmcdln = new HashSet<Tmcdln>();
        }

        public Guid Idnum { get; set; }
        public long Recnum { get; set; }
        public string Dptnme { get; set; }
        public string Ntetxt { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }

        public ICollection<Actrec> Actrec { get; set; }
        public ICollection<Cstcde> Cstcde { get; set; }
        public ICollection<Dlypyr> Dlypyr { get; set; }
        public ICollection<Eqpmnt> Eqpmnt { get; set; }
        public ICollection<Paypst> Paypst { get; set; }
        public ICollection<Srvtyp> Srvtyp { get; set; }
        public ICollection<Tmcdln> Tmcdln { get; set; }
    }
}
