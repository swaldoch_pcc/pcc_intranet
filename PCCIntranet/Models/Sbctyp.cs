﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Sbctyp
    {
        public Sbctyp()
        {
            Actpay = new HashSet<Actpay>();
            Subcon = new HashSet<Subcon>();
        }

        public Guid Idnum { get; set; }
        public byte Recnum { get; set; }
        public string Dscrpt { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }

        public ICollection<Actpay> Actpay { get; set; }
        public ICollection<Subcon> Subcon { get; set; }
    }
}
