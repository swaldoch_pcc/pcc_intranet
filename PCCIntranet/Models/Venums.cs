﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Venums
    {
        public Guid Idnum { get; set; }
        public short Enumid { get; set; }
        public string Evalue { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }
    }
}
