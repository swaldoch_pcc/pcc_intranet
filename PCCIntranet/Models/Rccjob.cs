﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Rccjob
    {
        public Guid Idnum { get; set; }
        public Guid Idref { get; set; }
        public long Recnum { get; set; }
        public string Wrkord { get; set; }
        public long Jobnum { get; set; }
        public string Dscrpt { get; set; }
        public long Phsnum { get; set; }
        public decimal Cstcde { get; set; }
        public byte Csttyp { get; set; }
        public decimal Cstamt { get; set; }
        public string Ntetxt { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }

        public Cstcde CstcdeNavigation { get; set; }
        public Csttyp CsttypNavigation { get; set; }
        public Rcctrn IdrefNavigation { get; set; }
        public Actrec JobnumNavigation { get; set; }
        public Jobphs Jobphs { get; set; }
        public Rcctrn RecnumNavigation { get; set; }
    }
}
