using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Subs
    {
        public string Company { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string Mainphone { get; set; }
        public string Email { get; set; }
        public int Id { get; set; }
        public string Type { get; set; }
        public string NaicsCode { get; set; }
        public string SicCode { get; set; }
        public string Website { get; set; }
        public string Fax { get; set; }
    }
}
