﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Assemb
    {
        public Assemb()
        {
            AsmprtIdrefNavigation = new HashSet<Asmprt>();
            AsmprtRecnumNavigation = new HashSet<Asmprt>();
            Srvlin = new HashSet<Srvlin>();
            Tkflin = new HashSet<Tkflin>();
        }

        public Guid Idnum { get; set; }
        public long Recnum { get; set; }
        public string Asmnme { get; set; }
        public string Asmunt { get; set; }
        public string Asmfrm { get; set; }
        public long? Asmcls { get; set; }
        public decimal Ttlcst { get; set; }
        public string Usrnme { get; set; }
        public DateTime? Lstedt { get; set; }
        public string Imgfle { get; set; }
        public string Ntetxt { get; set; }
        public decimal Matcst { get; set; }
        public decimal Matmrk { get; set; }
        public decimal Matsel { get; set; }
        public decimal Labcst { get; set; }
        public decimal Labmrk { get; set; }
        public decimal Labsel { get; set; }
        public decimal Eqpcst { get; set; }
        public decimal Eqpmrk { get; set; }
        public decimal Eqpsel { get; set; }
        public decimal Subcst { get; set; }
        public decimal Submrk { get; set; }
        public decimal Subsel { get; set; }
        public decimal Othcst { get; set; }
        public decimal Othmrk { get; set; }
        public decimal Othsel { get; set; }
        public decimal Tp6cst { get; set; }
        public decimal Tp6mrk { get; set; }
        public decimal Tp6sel { get; set; }
        public decimal Tp7cst { get; set; }
        public decimal Tp7mrk { get; set; }
        public decimal Tp7sel { get; set; }
        public decimal Tp8cst { get; set; }
        public decimal Tp8mrk { get; set; }
        public decimal Tp8sel { get; set; }
        public decimal Tp9cst { get; set; }
        public decimal Tp9mrk { get; set; }
        public decimal Tp9sel { get; set; }
        public short Tvltim { get; set; }
        public decimal Tvlcst { get; set; }
        public decimal Tvlmrk { get; set; }
        public decimal Tvlsel { get; set; }
        public short Tchtim { get; set; }
        public decimal Tchcst { get; set; }
        public decimal Tchmrk { get; set; }
        public decimal Tchsel { get; set; }
        public short Asttim { get; set; }
        public decimal Astcst { get; set; }
        public decimal Astmrk { get; set; }
        public decimal Astsel { get; set; }
        public decimal Msccst { get; set; }
        public decimal Mscmrk { get; set; }
        public decimal Mscsel { get; set; }
        public decimal Ovhcst { get; set; }
        public decimal Ovhmrk { get; set; }
        public decimal Ovhsel { get; set; }
        public int Prmtim { get; set; }
        public decimal Prmcst { get; set; }
        public decimal Prmsel { get; set; }
        public int Addtim { get; set; }
        public decimal Addcst { get; set; }
        public decimal Addsel { get; set; }
        public decimal Cpmsel { get; set; }
        public decimal Cadsel { get; set; }
        public decimal Condsc { get; set; }
        public byte Pntbok { get; set; }
        public byte Pntprt { get; set; }
        public byte Taxabl { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }
        public byte Inactv { get; set; }

        public Asmcls AsmclsNavigation { get; set; }
        public AsseU AsseU { get; set; }
        public ICollection<Asmprt> AsmprtIdrefNavigation { get; set; }
        public ICollection<Asmprt> AsmprtRecnumNavigation { get; set; }
        public ICollection<Srvlin> Srvlin { get; set; }
        public ICollection<Tkflin> Tkflin { get; set; }
    }
}
