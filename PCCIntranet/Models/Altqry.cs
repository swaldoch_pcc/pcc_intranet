﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Altqry
    {
        public Altqry()
        {
            Alerts = new HashSet<Alerts>();
        }

        public Guid Idnum { get; set; }
        public long Recnum { get; set; }
        public string Tblnme { get; set; }
        public string Usrnme { get; set; }
        public byte Status { get; set; }
        public byte Frqncy { get; set; }
        public byte Dowdom { get; set; }
        public string Qrynme { get; set; }
        public string Qrytbl { get; set; }
        public string Qrydsp { get; set; }
        public string Qrysrt { get; set; }
        public string Qrysel { get; set; }
        public string Qrycrt { get; set; }
        public string Altdsc { get; set; }
        public string Altbdy { get; set; }
        public byte Output { get; set; }
        public byte Mailpr { get; set; }
        public byte Mailrr { get; set; }
        public string Mailto { get; set; }
        public string Mailcc { get; set; }
        public DateTime? Lstrun { get; set; }
        public DateTime? Nxtrun { get; set; }
        public DateTime? Snzdte { get; set; }
        public string Othusr { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }

        public ICollection<Alerts> Alerts { get; set; }
    }
}
