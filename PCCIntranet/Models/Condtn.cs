﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Condtn
    {
        public Guid Idnum { get; set; }
        public short Condid { get; set; }
        public string Pritbl { get; set; }
        public string Prifld { get; set; }
        public string Priqlf { get; set; }
        public string Logopr { get; set; }
        public string Trgobj { get; set; }
        public string Trgqlf { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }
    }
}
