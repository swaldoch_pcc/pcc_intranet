﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Plnrcv
    {
        public Guid Idnum { get; set; }
        public Guid Idref { get; set; }
        public long Recnum { get; set; }
        public int Linnum { get; set; }
        public long? Vendor { get; set; }
        public string Contct { get; set; }
        public short Numcpy { get; set; }
        public DateTime? Dtegiv { get; set; }
        public decimal Depamt { get; set; }
        public DateTime? Dtertn { get; set; }
        public string Usrdf1 { get; set; }
        public string Ntetxt { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }

        public Plnrcd IdrefNavigation { get; set; }
        public Plnrcd RecnumNavigation { get; set; }
        public Actpay VendorNavigation { get; set; }
    }
}
