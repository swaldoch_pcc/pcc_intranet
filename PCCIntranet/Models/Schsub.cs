﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Schsub
    {
        public Guid Idnum { get; set; }
        public Guid Idref { get; set; }
        public long Recnum { get; set; }
        public long Phsnum { get; set; }
        public decimal Tsknum { get; set; }
        public long Subnum { get; set; }
        public TimeSpan? Strtme { get; set; }
        public TimeSpan? Endtme { get; set; }
        public string Ntetxt { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }

        public Schlin IdrefNavigation { get; set; }
        public Jobphs Jobphs { get; set; }
        public Schedl Schedl { get; set; }
        public Actpay SubnumNavigation { get; set; }
        public Schtsk TsknumNavigation { get; set; }
    }
}
