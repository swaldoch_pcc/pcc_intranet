﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Vndbal
    {
        public Guid Idnum { get; set; }
        public Guid Idref { get; set; }
        public long Vndnum { get; set; }
        public DateTime? Trndte { get; set; }
        public decimal Balnce { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }

        public Actpay IdrefNavigation { get; set; }
        public Actpay VndnumNavigation { get; set; }
    }
}
