﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Pstpyr
    {
        public Guid Idnum { get; set; }
        public Guid Idref { get; set; }
        public long Recnum { get; set; }
        public int Pstnum { get; set; }
        public long Dptmnt { get; set; }
        public long Dbtact { get; set; }
        public decimal Amount { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }

        public Lgract DbtactNavigation { get; set; }
        public Payrec IdrefNavigation { get; set; }
        public Payrec RecnumNavigation { get; set; }
    }
}
