﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class ToDos
    {
        public int JobsiteId { get; set; }
        public string Jobsite { get; set; }
        public int ToDoId { get; set; }
        public string Title { get; set; }
        public string Notes { get; set; }
        public string Completed { get; set; }
        public DateTime? CompletionDate { get; set; }
        public string AssignedTo { get; set; }
        public string RelatedDailyLog { get; set; }
        public DateTime? Due { get; set; }
        public DateTime? CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public string Tags { get; set; }
        public string Reminder { get; set; }
        public string HasChecklist { get; set; }
    }
}
