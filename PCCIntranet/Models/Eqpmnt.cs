﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Eqpmnt
    {
        public Eqpmnt()
        {
            DlypyrEqpnumNavigation = new HashSet<Dlypyr>();
            DlypyrOpreqpNavigation = new HashSet<Dlypyr>();
            Employ = new HashSet<Employ>();
            EqpcstEqpnumNavigation = new HashSet<Eqpcst>();
            EqpcstOpreqpNavigation = new HashSet<Eqpcst>();
            EqpmlnIdrefNavigation = new HashSet<Eqpmln>();
            EqpmlnRecnumNavigation = new HashSet<Eqpmln>();
            EqprvwEqpmntNavigation = new HashSet<Eqprvw>();
            EqprvwEqpnumNavigation = new HashSet<Eqprvw>();
            Fdrpeq = new HashSet<Fdrpeq>();
            Jobcst = new HashSet<Jobcst>();
            Pchord = new HashSet<Pchord>();
            Rcapeq = new HashSet<Rcapeq>();
            Rcareq = new HashSet<Rcareq>();
            Rcceqp = new HashSet<Rcceqp>();
            Scheqp = new HashSet<Scheqp>();
            Srvsch = new HashSet<Srvsch>();
            Tmcdln = new HashSet<Tmcdln>();
            Tmeqln = new HashSet<Tmeqln>();
        }

        public Guid Idnum { get; set; }
        public long Recnum { get; set; }
        public string Eqpnme { get; set; }
        public string Shtnme { get; set; }
        public string Srlnum { get; set; }
        public string Licnum { get; set; }
        public string Pucnum { get; set; }
        public string Lonnum { get; set; }
        public long? Lender { get; set; }
        public DateTime? Pchdte { get; set; }
        public DateTime? Nxtpay { get; set; }
        public byte Lontyp { get; set; }
        public decimal Intrte { get; set; }
        public decimal Paymnt { get; set; }
        public decimal Deprte { get; set; }
        public long? Depact { get; set; }
        public long? Intact { get; set; }
        public long? Depmnt { get; set; }
        public string Usrdf1 { get; set; }
        public string Usrdf2 { get; set; }
        public string Lstloc { get; set; }
        public decimal Oprbll { get; set; }
        public decimal Oprcst { get; set; }
        public decimal Stbbll { get; set; }
        public decimal Stbcst { get; set; }
        public decimal Idlbll { get; set; }
        public decimal Idlcst { get; set; }
        public decimal Ttlhrs { get; set; }
        public decimal Ttlmls { get; set; }
        public decimal Begcap { get; set; }
        public decimal Capend { get; set; }
        public decimal Begdep { get; set; }
        public decimal Depend { get; set; }
        public decimal Beglon { get; set; }
        public decimal Lonend { get; set; }
        public short? Eqptyp { get; set; }
        public DateTime? Strdte { get; set; }
        public DateTime? Enddte { get; set; }
        public decimal Strval { get; set; }
        public decimal Endval { get; set; }
        public decimal Insrnc { get; set; }
        public decimal Financ { get; set; }
        public decimal Taxlic { get; set; }
        public decimal Inspct { get; set; }
        public decimal Ovrhed { get; set; }
        public decimal Storag { get; set; }
        public decimal Cstcap { get; set; }
        public decimal Othcst { get; set; }
        public byte Unttyp { get; set; }
        public decimal Estuse { get; set; }
        public decimal Fuloil { get; set; }
        public decimal Csttir { get; set; }
        public decimal Mntcst { get; set; }
        public decimal Othopr { get; set; }
        public DateTime? Lstpst { get; set; }
        public string Ntetxt { get; set; }
        public string Imgfle { get; set; }
        public decimal Fluchg { get; set; }
        public decimal Minrpr { get; set; }
        public decimal Oilchg { get; set; }
        public decimal Moblbl { get; set; }
        public decimal Dyopbl { get; set; }
        public decimal Dystbl { get; set; }
        public decimal Dyidbl { get; set; }
        public decimal Wkopbl { get; set; }
        public decimal Wkstbl { get; set; }
        public decimal Wkidbl { get; set; }
        public decimal Mtopbl { get; set; }
        public decimal Mtstbl { get; set; }
        public decimal Mtidbl { get; set; }
        public DateTime? Entdte { get; set; }
        public string Usrnme { get; set; }
        public byte Status { get; set; }
        public string Lstprd { get; set; }
        public decimal Orghrs { get; set; }
        public decimal Orgmls { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }
        public DateTime? Slddte { get; set; }
        public byte Inactv { get; set; }

        public Lgract DepactNavigation { get; set; }
        public Dptmnt DepmntNavigation { get; set; }
        public Eqptyp EqptypNavigation { get; set; }
        public Lgract IntactNavigation { get; set; }
        public Actpay LenderNavigation { get; set; }
        public EqpmU EqpmU { get; set; }
        public ICollection<Dlypyr> DlypyrEqpnumNavigation { get; set; }
        public ICollection<Dlypyr> DlypyrOpreqpNavigation { get; set; }
        public ICollection<Employ> Employ { get; set; }
        public ICollection<Eqpcst> EqpcstEqpnumNavigation { get; set; }
        public ICollection<Eqpcst> EqpcstOpreqpNavigation { get; set; }
        public ICollection<Eqpmln> EqpmlnIdrefNavigation { get; set; }
        public ICollection<Eqpmln> EqpmlnRecnumNavigation { get; set; }
        public ICollection<Eqprvw> EqprvwEqpmntNavigation { get; set; }
        public ICollection<Eqprvw> EqprvwEqpnumNavigation { get; set; }
        public ICollection<Fdrpeq> Fdrpeq { get; set; }
        public ICollection<Jobcst> Jobcst { get; set; }
        public ICollection<Pchord> Pchord { get; set; }
        public ICollection<Rcapeq> Rcapeq { get; set; }
        public ICollection<Rcareq> Rcareq { get; set; }
        public ICollection<Rcceqp> Rcceqp { get; set; }
        public ICollection<Scheqp> Scheqp { get; set; }
        public ICollection<Srvsch> Srvsch { get; set; }
        public ICollection<Tmcdln> Tmcdln { get; set; }
        public ICollection<Tmeqln> Tmeqln { get; set; }
    }
}
