﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Jobphs
    {
        public Jobphs()
        {
            Acpinv = new HashSet<Acpinv>();
            Acrinv = new HashSet<Acrinv>();
            Aiafrm = new HashSet<Aiafrm>();
            Bdglin = new HashSet<Bdglin>();
            Coresp = new HashSet<Coresp>();
            Cscpln = new HashSet<Cscpln>();
            Dlypyr = new HashSet<Dlypyr>();
            Fldrpt = new HashSet<Fldrpt>();
            Hrcpln = new HashSet<Hrcpln>();
            Invalc = new HashSet<Invalc>();
            Jobcst = new HashSet<Jobcst>();
            Lonfrm = new HashSet<Lonfrm>();
            Pchlst = new HashSet<Pchlst>();
            Pchord = new HashSet<Pchord>();
            Plnrcd = new HashSet<Plnrcd>();
            Prmchg = new HashSet<Prmchg>();
            Prplin = new HashSet<Prplin>();
            PtotkfJobphs = new HashSet<Ptotkf>();
            PtotkfJobphs1 = new HashSet<Ptotkf>();
            PtotkfJobphsNavigation = new HashSet<Ptotkf>();
            Rcapjb = new HashSet<Rcapjb>();
            Rcarjb = new HashSet<Rcarjb>();
            Rccjob = new HashSet<Rccjob>();
            Rccpay = new HashSet<Rccpay>();
            Rccrec = new HashSet<Rccrec>();
            Reqinf = new HashSet<Reqinf>();
            Reqprp = new HashSet<Reqprp>();
            Schdte = new HashSet<Schdte>();
            Schedl = new HashSet<Schedl>();
            Schemp = new HashSet<Schemp>();
            Scheqp = new HashSet<Scheqp>();
            Schlin = new HashSet<Schlin>();
            SchprdJobphs = new HashSet<Schprd>();
            SchprdRecNavigation = new HashSet<Schprd>();
            Schsub = new HashSet<Schsub>();
            Subcon = new HashSet<Subcon>();
            Submtl = new HashSet<Submtl>();
            Tkflin = new HashSet<Tkflin>();
            Tmcdln = new HashSet<Tmcdln>();
            Trnmtl = new HashSet<Trnmtl>();
            Uncpln = new HashSet<Uncpln>();
            Unprln = new HashSet<Unprln>();
            Untbll = new HashSet<Untbll>();
        }

        public Guid Idnum { get; set; }
        public Guid Idref { get; set; }
        public long Recnum { get; set; }
        public long Phsnum { get; set; }
        public string Phsnme { get; set; }
        public decimal Bllamt { get; set; }
        public decimal Retain { get; set; }
        public string Mdldsc { get; set; }
        public string Untdsc { get; set; }
        public decimal Untqty { get; set; }
        public string Ntetxt { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }

        public Actrec IdrefNavigation { get; set; }
        public Actrec RecnumNavigation { get; set; }
        public ICollection<Acpinv> Acpinv { get; set; }
        public ICollection<Acrinv> Acrinv { get; set; }
        public ICollection<Aiafrm> Aiafrm { get; set; }
        public ICollection<Bdglin> Bdglin { get; set; }
        public ICollection<Coresp> Coresp { get; set; }
        public ICollection<Cscpln> Cscpln { get; set; }
        public ICollection<Dlypyr> Dlypyr { get; set; }
        public ICollection<Fldrpt> Fldrpt { get; set; }
        public ICollection<Hrcpln> Hrcpln { get; set; }
        public ICollection<Invalc> Invalc { get; set; }
        public ICollection<Jobcst> Jobcst { get; set; }
        public ICollection<Lonfrm> Lonfrm { get; set; }
        public ICollection<Pchlst> Pchlst { get; set; }
        public ICollection<Pchord> Pchord { get; set; }
        public ICollection<Plnrcd> Plnrcd { get; set; }
        public ICollection<Prmchg> Prmchg { get; set; }
        public ICollection<Prplin> Prplin { get; set; }
        public ICollection<Ptotkf> PtotkfJobphs { get; set; }
        public ICollection<Ptotkf> PtotkfJobphs1 { get; set; }
        public ICollection<Ptotkf> PtotkfJobphsNavigation { get; set; }
        public ICollection<Rcapjb> Rcapjb { get; set; }
        public ICollection<Rcarjb> Rcarjb { get; set; }
        public ICollection<Rccjob> Rccjob { get; set; }
        public ICollection<Rccpay> Rccpay { get; set; }
        public ICollection<Rccrec> Rccrec { get; set; }
        public ICollection<Reqinf> Reqinf { get; set; }
        public ICollection<Reqprp> Reqprp { get; set; }
        public ICollection<Schdte> Schdte { get; set; }
        public ICollection<Schedl> Schedl { get; set; }
        public ICollection<Schemp> Schemp { get; set; }
        public ICollection<Scheqp> Scheqp { get; set; }
        public ICollection<Schlin> Schlin { get; set; }
        public ICollection<Schprd> SchprdJobphs { get; set; }
        public ICollection<Schprd> SchprdRecNavigation { get; set; }
        public ICollection<Schsub> Schsub { get; set; }
        public ICollection<Subcon> Subcon { get; set; }
        public ICollection<Submtl> Submtl { get; set; }
        public ICollection<Tkflin> Tkflin { get; set; }
        public ICollection<Tmcdln> Tmcdln { get; set; }
        public ICollection<Trnmtl> Trnmtl { get; set; }
        public ICollection<Uncpln> Uncpln { get; set; }
        public ICollection<Unprln> Unprln { get; set; }
        public ICollection<Untbll> Untbll { get; set; }
    }
}
