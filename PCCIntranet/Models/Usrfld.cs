﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Usrfld
    {
        public Usrfld()
        {
            Fldacs = new HashSet<Fldacs>();
        }

        public Guid Idnum { get; set; }
        public string Tblnam { get; set; }
        public string Fldnme { get; set; }
        public byte Usrunq { get; set; }
        public byte Lcklst { get; set; }
        public byte Qwkmtc { get; set; }
        public byte Usrreq { get; set; }
        public byte Usrskp { get; set; }
        public byte Lckedt { get; set; }
        public string Defalt { get; set; }
        public string Usrprm { get; set; }
        public string Usrdsc { get; set; }
        public byte Fldcse { get; set; }
        public byte Splchk { get; set; }
        public byte Hasgrp { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }
        public string Usrtyp { get; set; }

        public ICollection<Fldacs> Fldacs { get; set; }
    }
}
