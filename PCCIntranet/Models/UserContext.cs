using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace PCCIntranet.Models
{
    public partial class UserContext : DbContext
    {
        public UserContext()
        {
        }

        public UserContext(DbContextOptions<UserContext> options)
            : base(options)
        {
        }

        public virtual DbSet<UserGroups> UserGroups { get; set; }
        public virtual DbSet<UserPermissions> UserPermissions { get; set; }
        public virtual DbSet<Users> Users { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {

            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<UserGroups>(entity =>
            {
                entity.HasKey(e => new { e.Group, e.UserId });

                entity.Property(e => e.Group).HasMaxLength(25);

                entity.Property(e => e.UserId).HasColumnName("UserID");
            });

            modelBuilder.Entity<UserPermissions>(entity =>
            {
                entity.ToTable("user_permissions");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedNever();

                entity.Property(e => e.Admin).HasColumnName("admin");

                entity.Property(e => e.Apm).HasColumnName("apm");

                entity.Property(e => e.Auditmanager).HasColumnName("auditmanager");

                entity.Property(e => e.Audituser).HasColumnName("audituser");

                entity.Property(e => e.Bidedit).HasColumnName("bidedit");

                entity.Property(e => e.Equipmentmanager).HasColumnName("equipmentmanager");

                entity.Property(e => e.Equipmentuser).HasColumnName("equipmentuser");

                entity.Property(e => e.Executive).HasColumnName("executive");

                entity.Property(e => e.Fieldoperationsmanager).HasColumnName("fieldoperationsmanager");

                entity.Property(e => e.Fielduser).HasColumnName("fielduser");

                entity.Property(e => e.Financemanager).HasColumnName("financemanager");

                entity.Property(e => e.Financeuser).HasColumnName("financeuser");

                entity.Property(e => e.Laboredit).HasColumnName("laboredit");

                entity.Property(e => e.Pm).HasColumnName("pm");

                entity.Property(e => e.Safetymanager).HasColumnName("safetymanager");
            });

            modelBuilder.Entity<Users>(entity =>
            {
                entity.HasKey(e => e.Username);

                entity.ToTable("users");

                entity.Property(e => e.Username)
                    .HasColumnName("username")
                    .HasMaxLength(25)
                    .ValueGeneratedNever();

                entity.Property(e => e.FirstName).HasMaxLength(255);

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.LastName).HasMaxLength(255);

                entity.Property(e => e.Role).HasMaxLength(25);
            });
        }
    }
}
