﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Jobmnu
    {
        public Guid Idnum { get; set; }
        public string Userid { get; set; }
        public byte Menu1 { get; set; }
        public byte Menu2 { get; set; }
        public byte Menu3 { get; set; }
        public string Mnudsc { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }
    }
}
