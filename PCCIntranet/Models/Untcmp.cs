﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Untcmp
    {
        public Untcmp()
        {
            UncplnIdrefNavigation = new HashSet<Uncpln>();
            UncplnRecnumNavigation = new HashSet<Uncpln>();
        }

        public Guid Idnum { get; set; }
        public long Recnum { get; set; }
        public DateTime? Edtdte { get; set; }
        public string Usrnme { get; set; }
        public string Ntetxt { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }

        public Actrec RecnumNavigation { get; set; }
        public ICollection<Uncpln> UncplnIdrefNavigation { get; set; }
        public ICollection<Uncpln> UncplnRecnumNavigation { get; set; }
    }
}
