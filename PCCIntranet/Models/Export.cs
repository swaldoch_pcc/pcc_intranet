﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Export
    {
        public Guid Idnum { get; set; }
        public Guid Idref { get; set; }
        public long Recnum { get; set; }
        public long Jobnum { get; set; }
        public DateTime? Expdte { get; set; }
        public string Exptyp { get; set; }
        public string Filter { get; set; }
        public string Vndlst { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }

        public Actrec IdrefNavigation { get; set; }
        public Actrec JobnumNavigation { get; set; }
    }
}
