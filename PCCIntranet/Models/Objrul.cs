﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Objrul
    {
        public Guid Idnum { get; set; }
        public string Mbtabl { get; set; }
        public short Cnstid { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }
    }
}
