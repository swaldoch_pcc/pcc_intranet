﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Actpay
    {
        public Actpay()
        {
            Acpinv = new HashSet<Acpinv>();
            ActrecAchtctNavigation = new HashSet<Actrec>();
            ActrecLenderNavigation = new HashSet<Actrec>();
            Aiafrm = new HashSet<Aiafrm>();
            Aialin = new HashSet<Aialin>();
            Coresp = new HashSet<Coresp>();
            Eqpcst = new HashSet<Eqpcst>();
            Eqpmnt = new HashSet<Eqpmnt>();
            Fdrpsb = new HashSet<Fdrpsb>();
            Jobcst = new HashSet<Jobcst>();
            Lgrtrn = new HashSet<Lgrtrn>();
            Lonfrm = new HashSet<Lonfrm>();
            Lonlin = new HashSet<Lonlin>();
            Pchord = new HashSet<Pchord>();
            PclslnBilltoNavigation = new HashSet<Pclsln>();
            PclslnVndnumNavigation = new HashSet<Pclsln>();
            Plnrcd = new HashSet<Plnrcd>();
            Plnrcv = new HashSet<Plnrcv>();
            Privnd = new HashSet<Privnd>();
            Rccpay = new HashSet<Rccpay>();
            Rcctrn = new HashSet<Rcctrn>();
            Reqinf = new HashSet<Reqinf>();
            Reqprp = new HashSet<Reqprp>();
            Sbcgln = new HashSet<Sbcgln>();
            ScdlenScdvndNavigation = new HashSet<Scdlen>();
            ScdlenVndnumNavigation = new HashSet<Scdlen>();
            Schsub = new HashSet<Schsub>();
            Srvsch = new HashSet<Srvsch>();
            Subcon = new HashSet<Subcon>();
            Submtl = new HashSet<Submtl>();
            Tkflin = new HashSet<Tkflin>();
            Trnmtl = new HashSet<Trnmtl>();
            Untbll = new HashSet<Untbll>();
            VndbalIdrefNavigation = new HashSet<Vndbal>();
            VndbalVndnumNavigation = new HashSet<Vndbal>();
            VndcntIdrefNavigation = new HashSet<Vndcnt>();
            VndcntRecnumNavigation = new HashSet<Vndcnt>();
            VndcrtIdrefNavigation = new HashSet<Vndcrt>();
            VndcrtRecnumNavigation = new HashSet<Vndcrt>();
            Vndprc = new HashSet<Vndprc>();
            VndrmtIdrefNavigation = new HashSet<Vndrmt>();
            VndytdIdrefNavigation = new HashSet<Vndytd>();
            VndytdVndnumNavigation = new HashSet<Vndytd>();
        }

        public Guid Idnum { get; set; }
        public long Recnum { get; set; }
        public string Vndnme { get; set; }
        public string Shtnme { get; set; }
        public string Ownnme { get; set; }
        public string Addrs1 { get; set; }
        public string Addrs2 { get; set; }
        public string Ctynme { get; set; }
        public string State { get; set; }
        public string Zipcde { get; set; }
        public string Fedidn { get; set; }
        public string Steidn { get; set; }
        public string Resnum { get; set; }
        public string Actnum { get; set; }
        public string Licnum { get; set; }
        public string Usrdf1 { get; set; }
        public string Usrdf2 { get; set; }
        public string Phnnum { get; set; }
        public string Pagnum { get; set; }
        public string Faxnum { get; set; }
        public string Cllphn { get; set; }
        public string Homphn { get; set; }
        public string EMail { get; set; }
        public decimal Dscrte { get; set; }
        public string Dscdte { get; set; }
        public string Duedte { get; set; }
        public long? Lgrdft { get; set; }
        public decimal? Cdedft { get; set; }
        public byte? Typdft { get; set; }
        public byte Stsdft { get; set; }
        public byte Wrndft { get; set; }
        public decimal Begbal { get; set; }
        public decimal Endbal { get; set; }
        public short? Vndtyp { get; set; }
        public byte Prt199 { get; set; }
        public byte Minsts { get; set; }
        public int? Taxdst { get; set; }
        public byte Intrnl { get; set; }
        public decimal Cmprte { get; set; }
        public decimal Utxrte { get; set; }
        public byte Hotlst { get; set; }
        public byte? Ordtyp { get; set; }
        public string Orddsc { get; set; }
        public byte? Contyp { get; set; }
        public string Condsc { get; set; }
        public string Ntetxt { get; set; }
        public string Imgfle { get; set; }
        public byte Dupchk { get; set; }
        public byte? Rfptyp { get; set; }
        public string Rfpdsc { get; set; }
        public byte Sepchk { get; set; }
        public string Contct { get; set; }
        public byte Pomesg { get; set; }
        public byte Toltyp { get; set; }
        public decimal Tolamt { get; set; }
        public decimal Tolprc { get; set; }
        public decimal Tolexc { get; set; }
        public byte Dirdep { get; set; }
        public byte Prente { get; set; }
        public byte Acttyp { get; set; }
        public string Rtnmbr { get; set; }
        public string Bnkact { get; set; }
        public string Acheml { get; set; }
        public byte Eftpay { get; set; }
        public string Eftiid { get; set; }
        public string Eftrtn { get; set; }
        public string Eftact { get; set; }
        public string Eftcde { get; set; }
        public string Efteml { get; set; }
        public short? Taxcde { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }
        public byte Elc199 { get; set; }
        public string Eml199 { get; set; }
        public byte Inactv { get; set; }
        public byte Utwarn { get; set; }

        public Cstcde CdedftNavigation { get; set; }
        public Sbctyp ContypNavigation { get; set; }
        public Lgract LgrdftNavigation { get; set; }
        public Pchtyp OrdtypNavigation { get; set; }
        public Reqtyp RfptypNavigation { get; set; }
        public CaTax TaxcdeNavigation { get; set; }
        public Taxdst TaxdstNavigation { get; set; }
        public Csttyp TypdftNavigation { get; set; }
        public Vndtyp VndtypNavigation { get; set; }
        public ActpU ActpU { get; set; }
        public Vndrmt VndrmtRecnumNavigation { get; set; }
        public ICollection<Acpinv> Acpinv { get; set; }
        public ICollection<Actrec> ActrecAchtctNavigation { get; set; }
        public ICollection<Actrec> ActrecLenderNavigation { get; set; }
        public ICollection<Aiafrm> Aiafrm { get; set; }
        public ICollection<Aialin> Aialin { get; set; }
        public ICollection<Coresp> Coresp { get; set; }
        public ICollection<Eqpcst> Eqpcst { get; set; }
        public ICollection<Eqpmnt> Eqpmnt { get; set; }
        public ICollection<Fdrpsb> Fdrpsb { get; set; }
        public ICollection<Jobcst> Jobcst { get; set; }
        public ICollection<Lgrtrn> Lgrtrn { get; set; }
        public ICollection<Lonfrm> Lonfrm { get; set; }
        public ICollection<Lonlin> Lonlin { get; set; }
        public ICollection<Pchord> Pchord { get; set; }
        public ICollection<Pclsln> PclslnBilltoNavigation { get; set; }
        public ICollection<Pclsln> PclslnVndnumNavigation { get; set; }
        public ICollection<Plnrcd> Plnrcd { get; set; }
        public ICollection<Plnrcv> Plnrcv { get; set; }
        public ICollection<Privnd> Privnd { get; set; }
        public ICollection<Rccpay> Rccpay { get; set; }
        public ICollection<Rcctrn> Rcctrn { get; set; }
        public ICollection<Reqinf> Reqinf { get; set; }
        public ICollection<Reqprp> Reqprp { get; set; }
        public ICollection<Sbcgln> Sbcgln { get; set; }
        public ICollection<Scdlen> ScdlenScdvndNavigation { get; set; }
        public ICollection<Scdlen> ScdlenVndnumNavigation { get; set; }
        public ICollection<Schsub> Schsub { get; set; }
        public ICollection<Srvsch> Srvsch { get; set; }
        public ICollection<Subcon> Subcon { get; set; }
        public ICollection<Submtl> Submtl { get; set; }
        public ICollection<Tkflin> Tkflin { get; set; }
        public ICollection<Trnmtl> Trnmtl { get; set; }
        public ICollection<Untbll> Untbll { get; set; }
        public ICollection<Vndbal> VndbalIdrefNavigation { get; set; }
        public ICollection<Vndbal> VndbalVndnumNavigation { get; set; }
        public ICollection<Vndcnt> VndcntIdrefNavigation { get; set; }
        public ICollection<Vndcnt> VndcntRecnumNavigation { get; set; }
        public ICollection<Vndcrt> VndcrtIdrefNavigation { get; set; }
        public ICollection<Vndcrt> VndcrtRecnumNavigation { get; set; }
        public ICollection<Vndprc> Vndprc { get; set; }
        public ICollection<Vndrmt> VndrmtIdrefNavigation { get; set; }
        public ICollection<Vndytd> VndytdIdrefNavigation { get; set; }
        public ICollection<Vndytd> VndytdVndnumNavigation { get; set; }
    }
}
