﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Timeqp
    {
        public Timeqp()
        {
            Timmat = new HashSet<Timmat>();
            TmeqlnIdrefNavigation = new HashSet<Tmeqln>();
            TmeqlnRecnumNavigation = new HashSet<Tmeqln>();
        }

        public Guid Idnum { get; set; }
        public int Recnum { get; set; }
        public string Tbldsc { get; set; }
        public DateTime? Edtdte { get; set; }
        public byte Lckedt { get; set; }
        public string Usrnme { get; set; }
        public string Ntetxt { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }

        public ICollection<Timmat> Timmat { get; set; }
        public ICollection<Tmeqln> TmeqlnIdrefNavigation { get; set; }
        public ICollection<Tmeqln> TmeqlnRecnumNavigation { get; set; }
    }
}
