﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Rcapln
    {
        public Guid Idnum { get; set; }
        public Guid Idref { get; set; }
        public long Recnum { get; set; }
        public int Linnum { get; set; }
        public long? Prtnum { get; set; }
        public string Prtdsc { get; set; }
        public string Alpnum { get; set; }
        public string Untdsc { get; set; }
        public decimal Linqty { get; set; }
        public decimal Linprc { get; set; }
        public decimal Extttl { get; set; }
        public decimal Hldamt { get; set; }
        public decimal Invamt { get; set; }
        public decimal Gstamt { get; set; }
        public decimal Pstamt { get; set; }
        public decimal Hstamt { get; set; }
        public long? Actnum { get; set; }
        public long? Subact { get; set; }
        public string Usrdf1 { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }

        public Lgract ActnumNavigation { get; set; }
        public Rccpay IdrefNavigation { get; set; }
        public Tkfprt PrtnumNavigation { get; set; }
        public Rccpay RecnumNavigation { get; set; }
    }
}
