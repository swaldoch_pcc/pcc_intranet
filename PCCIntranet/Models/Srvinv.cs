﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Srvinv
    {
        public Srvinv()
        {
            SrtxdtIdrefNavigation = new HashSet<Srtxdt>();
            SrtxdtRecnumNavigation = new HashSet<Srtxdt>();
            SrvlinIdrefNavigation = new HashSet<Srvlin>();
            SrvlinRecnumNavigation = new HashSet<Srvlin>();
            SrvpmtIdrefNavigation = new HashSet<Srvpmt>();
            SrvpmtRecnumNavigation = new HashSet<Srvpmt>();
            SrvschIdrefNavigation = new HashSet<Srvsch>();
            SrvschRecnumNavigation = new HashSet<Srvsch>();
        }

        public Guid Idnum { get; set; }
        public long Recnum { get; set; }
        public string Ordnum { get; set; }
        public string Invnum { get; set; }
        public long Clnnum { get; set; }
        public DateTime? Orddte { get; set; }
        public string Dscrpt { get; set; }
        public long? Locnum { get; set; }
        public string Ctcnme { get; set; }
        public string Phnnum { get; set; }
        public string Addrs1 { get; set; }
        public string Addrs2 { get; set; }
        public string Ctynme { get; set; }
        public string State { get; set; }
        public string Zipcde { get; set; }
        public string Maploc { get; set; }
        public string Crsstr { get; set; }
        public DateTime? Duedte { get; set; }
        public DateTime? Dscdte { get; set; }
        public string Pchord { get; set; }
        public string Refnum { get; set; }
        public string Usrdf1 { get; set; }
        public string Usrdf2 { get; set; }
        public string Crdnum { get; set; }
        public DateTime? Expdte { get; set; }
        public string Crdnme { get; set; }
        public decimal Depost { get; set; }
        public decimal Dscavl { get; set; }
        public decimal Slstax { get; set; }
        public decimal Dsctkn { get; set; }
        public DateTime? Invdte { get; set; }
        public decimal Amtpad { get; set; }
        public DateTime? Clldte { get; set; }
        public TimeSpan? Clltim { get; set; }
        public DateTime? Dspdte { get; set; }
        public TimeSpan? Dsptim { get; set; }
        public DateTime? Schdte { get; set; }
        public TimeSpan? Schtim { get; set; }
        public DateTime? Strdte { get; set; }
        public TimeSpan? Strtim { get; set; }
        public DateTime? Findte { get; set; }
        public TimeSpan? Fintim { get; set; }
        public decimal Schhrs { get; set; }
        public decimal Acthrs { get; set; }
        public string Plcnum { get; set; }
        public string Plcnme { get; set; }
        public string Plcphn { get; set; }
        public string Plcad1 { get; set; }
        public string Plcad2 { get; set; }
        public string Plccty { get; set; }
        public string Cmpnme { get; set; }
        public string Adjnme { get; set; }
        public string Adjphn { get; set; }
        public string Adjad1 { get; set; }
        public string Adjad2 { get; set; }
        public string Adjcty { get; set; }
        public string Plctyp { get; set; }
        public decimal Dedabl { get; set; }
        public short? Rutnum { get; set; }
        public DateTime? Constd { get; set; }
        public DateTime? Conend { get; set; }
        public DateTime? Bildte { get; set; }
        public long? Empnum { get; set; }
        public long? Slspsn { get; set; }
        public int Invtyp { get; set; }
        public byte Status { get; set; }
        public byte Priort { get; set; }
        public int? Invsrc { get; set; }
        public int? Taxdst { get; set; }
        public byte Pmttyp { get; set; }
        public decimal Invttl { get; set; }
        public decimal Invbal { get; set; }
        public decimal Invnet { get; set; }
        public decimal Taxabl { get; set; }
        public decimal Nontax { get; set; }
        public decimal Ttlpad { get; set; }
        public byte Actper { get; set; }
        public int Bilcyc { get; set; }
        public short? Srvgeo { get; set; }
        public long Lgrrec { get; set; }
        public string Shpnte { get; set; }
        public string Ntetxt { get; set; }
        public string Imgfle { get; set; }
        public long? Jobnum { get; set; }
        public string Exmnum { get; set; }
        public byte Lckedt { get; set; }
        public DateTime? Entdte { get; set; }
        public string Usrnme { get; set; }
        public decimal Taxlin { get; set; }
        public decimal Ntxlin { get; set; }
        public decimal Taxmrt { get; set; }
        public decimal Ntxmrt { get; set; }
        public decimal Txadmk { get; set; }
        public decimal Ntadmk { get; set; }
        public decimal Txmktl { get; set; }
        public decimal Ntmktl { get; set; }
        public decimal Taxext { get; set; }
        public decimal Ntxext { get; set; }
        public decimal Taxovr { get; set; }
        public decimal Ntxovr { get; set; }
        public byte Taxlck { get; set; }
        public byte Ntxlck { get; set; }
        public long Vodrec { get; set; }
        public decimal Gstsbj { get; set; }
        public decimal Gstamt { get; set; }
        public decimal Pstsbj { get; set; }
        public decimal Pstamt { get; set; }
        public decimal Hstsbj { get; set; }
        public decimal Hstamt { get; set; }
        public decimal Invamt { get; set; }
        public decimal Gstrte { get; set; }
        public decimal Pstrte { get; set; }
        public decimal Hstrte { get; set; }
        public byte Pstcmp { get; set; }
        public short Postyr { get; set; }
        public byte Ovrtax { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }

        public Reccln ClnnumNavigation { get; set; }
        public Employ EmpnumNavigation { get; set; }
        public Ledsrc InvsrcNavigation { get; set; }
        public Srvtyp InvtypNavigation { get; set; }
        public Actrec JobnumNavigation { get; set; }
        public Priort PriortNavigation { get; set; }
        public Srvrte RutnumNavigation { get; set; }
        public Employ SlspsnNavigation { get; set; }
        public Srvgeo SrvgeoNavigation { get; set; }
        public Srvloc Srvloc { get; set; }
        public Taxdst TaxdstNavigation { get; set; }
        public SrviU SrviU { get; set; }
        public ICollection<Srtxdt> SrtxdtIdrefNavigation { get; set; }
        public ICollection<Srtxdt> SrtxdtRecnumNavigation { get; set; }
        public ICollection<Srvlin> SrvlinIdrefNavigation { get; set; }
        public ICollection<Srvlin> SrvlinRecnumNavigation { get; set; }
        public ICollection<Srvpmt> SrvpmtIdrefNavigation { get; set; }
        public ICollection<Srvpmt> SrvpmtRecnumNavigation { get; set; }
        public ICollection<Srvsch> SrvschIdrefNavigation { get; set; }
        public ICollection<Srvsch> SrvschRecnumNavigation { get; set; }
    }
}
