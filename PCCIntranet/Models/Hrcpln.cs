﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Hrcpln
    {
        public Guid Idnum { get; set; }
        public Guid Idref { get; set; }
        public long Recnum { get; set; }
        public long Phsnum { get; set; }
        public int Linnum { get; set; }
        public decimal? Cstcde { get; set; }
        public decimal Bdghrs { get; set; }
        public decimal Hrsdte { get; set; }
        public decimal Pctcmp { get; set; }
        public decimal Actcmp { get; set; }
        public decimal Hrscmp { get; set; }
        public decimal Ovrund { get; set; }
        public string Usrdf1 { get; set; }
        public string Ntetxt { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }

        public Cstcde CstcdeNavigation { get; set; }
        public Hrscmp IdrefNavigation { get; set; }
        public Jobphs Jobphs { get; set; }
        public Hrscmp RecnumNavigation { get; set; }
    }
}
