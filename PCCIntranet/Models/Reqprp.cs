﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Reqprp
    {
        public Reqprp()
        {
            RqprlnIdrefNavigation = new HashSet<Rqprln>();
            RqprlnRecnumNavigation = new HashSet<Rqprln>();
        }

        public Guid Idnum { get; set; }
        public long Recnum { get; set; }
        public string Reqnum { get; set; }
        public DateTime? Reqdte { get; set; }
        public long Vndnum { get; set; }
        public string Attion { get; set; }
        public long? Empnum { get; set; }
        public long Jobnum { get; set; }
        public long Phsnum { get; set; }
        public string Dscrpt { get; set; }
        public int? Taxdst { get; set; }
        public DateTime? Appdte { get; set; }
        public DateTime? Deldte { get; set; }
        public string Delvia { get; set; }
        public byte Status { get; set; }
        public byte? Reqtyp { get; set; }
        public string Addrs1 { get; set; }
        public string Addrs2 { get; set; }
        public string Ctynme { get; set; }
        public string State { get; set; }
        public string Zipcde { get; set; }
        public string Usrdf1 { get; set; }
        public string Usrdf2 { get; set; }
        public DateTime? Entdte { get; set; }
        public string Usrnme { get; set; }
        public decimal Subttl { get; set; }
        public decimal Slstax { get; set; }
        public decimal Reqttl { get; set; }
        public byte Hotlst { get; set; }
        public byte Lckedt { get; set; }
        public DateTime? Prcrcv { get; set; }
        public DateTime? Prcexp { get; set; }
        public string Ntetxt { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }

        public Employ EmpnumNavigation { get; set; }
        public Actrec JobnumNavigation { get; set; }
        public Jobphs Jobphs { get; set; }
        public Reqtyp ReqtypNavigation { get; set; }
        public Taxdst TaxdstNavigation { get; set; }
        public Actpay VndnumNavigation { get; set; }
        public ReqpU ReqpU { get; set; }
        public ICollection<Rqprln> RqprlnIdrefNavigation { get; set; }
        public ICollection<Rqprln> RqprlnRecnumNavigation { get; set; }
    }
}
