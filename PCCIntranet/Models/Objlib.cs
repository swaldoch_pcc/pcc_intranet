﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Objlib
    {
        public Guid Idnum { get; set; }
        public string Objnam { get; set; }
        public string Tblnam { get; set; }
        public string Hdrnam { get; set; }
        public string Addrq { get; set; }
        public string Modrq { get; set; }
        public string Delrq { get; set; }
        public string Qryrq { get; set; }
        public byte Menu1 { get; set; }
        public byte Menu2 { get; set; }
        public byte Menu3 { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }
    }
}
