﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class SubcU
    {
        public Guid Idnum { get; set; }

        public Subcon IdnumNavigation { get; set; }
    }
}
