﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Untlin
    {
        public Guid Idnum { get; set; }
        public Guid Idref { get; set; }
        public long Recnum { get; set; }
        public short Linnum { get; set; }
        public long Jobnum { get; set; }
        public long? Biditm { get; set; }
        public string Itmcde { get; set; }
        public string Itmnme { get; set; }
        public string Untdsc { get; set; }
        public decimal Untcst { get; set; }
        public decimal Schamt { get; set; }
        public decimal Schval { get; set; }
        public decimal Chgamt { get; set; }
        public decimal Chgval { get; set; }
        public decimal Newcon { get; set; }
        public decimal Cntval { get; set; }
        public decimal Prvbll { get; set; }
        public decimal Prvval { get; set; }
        public decimal Curbll { get; set; }
        public decimal Curval { get; set; }
        public decimal Ttlcmp { get; set; }
        public decimal Totval { get; set; }
        public decimal Balfin { get; set; }
        public decimal Remval { get; set; }
        public decimal Retrte { get; set; }
        public decimal Retamt { get; set; }
        public decimal Prvhld { get; set; }
        public string Taxabl { get; set; }
        public string Gstsbj { get; set; }
        public decimal Gstamt { get; set; }
        public string Pstsbj { get; set; }
        public decimal Pstamt { get; set; }
        public string Hstsbj { get; set; }
        public decimal Hstamt { get; set; }
        public string Usrdf1 { get; set; }
        public string Ntetxt { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }

        public Biditm BiditmNavigation { get; set; }
        public Untbll IdrefNavigation { get; set; }
        public Actrec JobnumNavigation { get; set; }
        public Untbll RecnumNavigation { get; set; }
    }
}
