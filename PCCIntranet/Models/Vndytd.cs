﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Vndytd
    {
        public Guid Idnum { get; set; }
        public Guid Idref { get; set; }
        public long Vndnum { get; set; }
        public short Fscyer { get; set; }
        public decimal Ytdact { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }

        public Actpay IdrefNavigation { get; set; }
        public Actpay VndnumNavigation { get; set; }
    }
}
