﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Objkey
    {
        public Guid Idnum { get; set; }
        public string Objnam { get; set; }
        public string Fldnam { get; set; }
        public byte Fldord { get; set; }
        public short Usradd { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }
    }
}
