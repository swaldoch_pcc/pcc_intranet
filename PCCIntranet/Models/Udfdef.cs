﻿using System;
using System.Collections.Generic;

namespace PCCIntranet.Models
{
    public partial class Udfdef
    {
        public Guid Idnum { get; set; }
        public string Tblnam { get; set; }
        public short Fldnum { get; set; }
        public string Fldnme { get; set; }
        public string Xmltag { get; set; }
        public string Fldtyp { get; set; }
        public short Fldlen { get; set; }
        public short Flddec { get; set; }
        public decimal Lowval { get; set; }
        public decimal Hghval { get; set; }
        public string Relfld { get; set; }
        public string Tbldsc { get; set; }
        public byte Qwklst { get; set; }
        public byte Usredt { get; set; }
        public byte Delfld { get; set; }
        public byte Rptmn1 { get; set; }
        public byte Rptmn2 { get; set; }
        public byte Rptmn3 { get; set; }
        public string Clcexp { get; set; }
        public string Author { get; set; }
        public DateTime? Insdte { get; set; }
        public DateTime? Upddte { get; set; }
    }
}
