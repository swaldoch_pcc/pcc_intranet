﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PCCIntranet.Models;

namespace PCCIntranet.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SubconsController : ControllerBase
    {
        private readonly SageContext _context;

        public SubconsController(SageContext context)
        {
            _context = context;
        }

        // GET: api/Subcons
        [HttpGet]
        public IEnumerable<Subcon> GetSubcon()
        {
            return _context.Subcon;
        }

        // GET: api/Subcons/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetSubcon([FromRoute] Guid id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var subcon = await _context.Subcon.FindAsync(id);

            if (subcon == null)
            {
                return NotFound();
            }

            return Ok(subcon);
        }

        // PUT: api/Subcons/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutSubcon([FromRoute] Guid id, [FromBody] Subcon subcon)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != subcon.Idnum)
            {
                return BadRequest();
            }

            _context.Entry(subcon).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!SubconExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Subcons
        [HttpPost]
        public async Task<IActionResult> PostSubcon([FromBody] Subcon subcon)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.Subcon.Add(subcon);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (SubconExists(subcon.Idnum))
                {
                    return new StatusCodeResult(StatusCodes.Status409Conflict);
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetSubcon", new { id = subcon.Idnum }, subcon);
        }

        // DELETE: api/Subcons/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteSubcon([FromRoute] Guid id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var subcon = await _context.Subcon.FindAsync(id);
            if (subcon == null)
            {
                return NotFound();
            }

            _context.Subcon.Remove(subcon);
            await _context.SaveChangesAsync();

            return Ok(subcon);
        }

        private bool SubconExists(Guid id)
        {
            return _context.Subcon.Any(e => e.Idnum == id);
        }
    }
}