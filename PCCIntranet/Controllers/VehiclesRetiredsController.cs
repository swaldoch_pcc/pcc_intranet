﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PCCIntranet.Models;

namespace PCCIntranet.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class VehiclesRetiredsController : ControllerBase
    {
        private readonly EquipmentContext _context;

        public VehiclesRetiredsController(EquipmentContext context)
        {
            _context = context;
        }

        // GET: api/VehiclesRetireds
        [HttpGet]
        public IEnumerable<VehiclesRetired> GetVehiclesRetired()
        {
            return _context.VehiclesRetired;
        }

        // GET: api/VehiclesRetireds/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetVehiclesRetired([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var vehiclesRetired = await _context.VehiclesRetired.FindAsync(id);

            if (vehiclesRetired == null)
            {
                return NotFound();
            }

            return Ok(vehiclesRetired);
        }

        // PUT: api/VehiclesRetireds/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutVehiclesRetired([FromRoute] int id, [FromBody] VehiclesRetired vehiclesRetired)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != vehiclesRetired.Id)
            {
                return BadRequest();
            }

            _context.Entry(vehiclesRetired).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!VehiclesRetiredExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/VehiclesRetireds
        [HttpPost]
        public async Task<IActionResult> PostVehiclesRetired([FromBody] VehiclesRetired vehiclesRetired)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.VehiclesRetired.Add(vehiclesRetired);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (VehiclesRetiredExists(vehiclesRetired.Id))
                {
                    return new StatusCodeResult(StatusCodes.Status409Conflict);
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetVehiclesRetired", new { id = vehiclesRetired.Id }, vehiclesRetired);
        }

        // DELETE: api/VehiclesRetireds/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteVehiclesRetired([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var vehiclesRetired = await _context.VehiclesRetired.FindAsync(id);
            if (vehiclesRetired == null)
            {
                return NotFound();
            }

            _context.VehiclesRetired.Remove(vehiclesRetired);
            await _context.SaveChangesAsync();

            return Ok(vehiclesRetired);
        }

        private bool VehiclesRetiredExists(int id)
        {
            return _context.VehiclesRetired.Any(e => e.Id == id);
        }
    }
}