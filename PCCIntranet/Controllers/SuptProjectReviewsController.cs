using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PCCIntranet.Models;

namespace PCCIntranet.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [DisableCors]
    public class SuptProjectReviewsController : ControllerBase
    {
        private readonly PCCContext _context;

        public SuptProjectReviewsController(PCCContext context)
        {
            _context = context;
        }

        // GET: api/SuptProjectReviews
        [HttpGet]
        public IEnumerable<SuptProjectReview> GetSuptProjectReview()
        {
            return _context.SuptProjectReview;
        }

        // GET: api/SuptProjectReviews/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetSuptProjectReview([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var suptProjectReview = await _context.SuptProjectReview.FindAsync(id);

            if (suptProjectReview == null)
            {
                return NotFound();
            }

            return Ok(suptProjectReview);
        }

        // PUT: api/SuptProjectReviews/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutSuptProjectReview([FromRoute] int id, [FromBody] SuptProjectReview suptProjectReview)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != suptProjectReview.Id)
            {
                return BadRequest();
            }

            _context.Entry(suptProjectReview).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!SuptProjectReviewExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/SuptProjectReviews
        [HttpPost]
        public async Task<IActionResult> PostSuptProjectReview([FromBody] SuptProjectReview suptProjectReview)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.SuptProjectReview.Add(suptProjectReview);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetSuptProjectReview", new { id = suptProjectReview.Id }, suptProjectReview);
        }

        // DELETE: api/SuptProjectReviews/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteSuptProjectReview([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var suptProjectReview = await _context.SuptProjectReview.FindAsync(id);
            if (suptProjectReview == null)
            {
                return NotFound();
            }

            _context.SuptProjectReview.Remove(suptProjectReview);
            await _context.SaveChangesAsync();

            return Ok(suptProjectReview);
        }

        private bool SuptProjectReviewExists(int id)
        {
            return _context.SuptProjectReview.Any(e => e.Id == id);
        }
    }
}
