﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PCCIntranet.Models;

namespace PCCIntranet.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EmptrnsController : ControllerBase
    {
        private readonly SageContext _context;

        public EmptrnsController(SageContext context)
        {
            _context = context;
        }

        // GET: api/Emptrns
        [HttpGet]
        public IEnumerable<Emptrn> GetEmptrn()
        {
            return _context.Emptrn;
        }

        // GET: api/Emptrns/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetEmptrn([FromRoute] Guid id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var emptrn = await _context.Emptrn.FindAsync(id);

            if (emptrn == null)
            {
                return NotFound();
            }

            return Ok(emptrn);
        }

        // PUT: api/Emptrns/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutEmptrn([FromRoute] Guid id, [FromBody] Emptrn emptrn)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != emptrn.Idnum)
            {
                return BadRequest();
            }

            _context.Entry(emptrn).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!EmptrnExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Emptrns
        [HttpPost]
        public async Task<IActionResult> PostEmptrn([FromBody] Emptrn emptrn)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.Emptrn.Add(emptrn);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (EmptrnExists(emptrn.Idnum))
                {
                    return new StatusCodeResult(StatusCodes.Status409Conflict);
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetEmptrn", new { id = emptrn.Idnum }, emptrn);
        }

        // DELETE: api/Emptrns/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteEmptrn([FromRoute] Guid id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var emptrn = await _context.Emptrn.FindAsync(id);
            if (emptrn == null)
            {
                return NotFound();
            }

            _context.Emptrn.Remove(emptrn);
            await _context.SaveChangesAsync();

            return Ok(emptrn);
        }

        private bool EmptrnExists(Guid id)
        {
            return _context.Emptrn.Any(e => e.Idnum == id);
        }
    }
}