﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PCCIntranet.Models;

namespace PCCIntranet.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RecallsController : ControllerBase
    {
        private readonly EquipmentContext _context;

        public RecallsController(EquipmentContext context)
        {
            _context = context;
        }

        // GET: api/Recalls
        [HttpGet]
        public IEnumerable<Recalls> GetRecalls()
        {
            return _context.Recalls;
        }

        // GET: api/Recalls/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetRecalls([FromRoute] Guid id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var recalls = await _context.Recalls.FindAsync(id);

            if (recalls == null)
            {
                return NotFound();
            }

            return Ok(recalls);
        }

        // PUT: api/Recalls/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutRecalls([FromRoute] Guid id, [FromBody] Recalls recalls)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != recalls.Id)
            {
                return BadRequest();
            }

            _context.Entry(recalls).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!RecallsExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Recalls
        [HttpPost]
        public async Task<IActionResult> PostRecalls([FromBody] Recalls recalls)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.Recalls.Add(recalls);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (RecallsExists(recalls.Id))
                {
                    return new StatusCodeResult(StatusCodes.Status409Conflict);
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetRecalls", new { id = recalls.Id }, recalls);
        }

        // DELETE: api/Recalls/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteRecalls([FromRoute] Guid id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var recalls = await _context.Recalls.FindAsync(id);
            if (recalls == null)
            {
                return NotFound();
            }

            _context.Recalls.Remove(recalls);
            await _context.SaveChangesAsync();

            return Ok(recalls);
        }

        private bool RecallsExists(Guid id)
        {
            return _context.Recalls.Any(e => e.Id == id);
        }
    }
}