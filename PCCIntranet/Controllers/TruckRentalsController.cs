﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PCCIntranet.Models;

namespace PCCIntranet.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TruckRentalsController : ControllerBase
    {
        private readonly EquipmentContext _context;

        public TruckRentalsController(EquipmentContext context)
        {
            _context = context;
        }

        // GET: api/TruckRentals
        [HttpGet]
        public IEnumerable<TruckRental> GetTruckRental()
        {
            return _context.TruckRental;
        }

        // GET: api/TruckRentals/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetTruckRental([FromRoute] Guid id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var truckRental = await _context.TruckRental.FindAsync(id);

            if (truckRental == null)
            {
                return NotFound();
            }

            return Ok(truckRental);
        }

        // PUT: api/TruckRentals/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutTruckRental([FromRoute] Guid id, [FromBody] TruckRental truckRental)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != truckRental.Id)
            {
                return BadRequest();
            }

            _context.Entry(truckRental).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TruckRentalExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/TruckRentals
        [HttpPost]
        public async Task<IActionResult> PostTruckRental([FromBody] TruckRental truckRental)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.TruckRental.Add(truckRental);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetTruckRental", new { id = truckRental.Id }, truckRental);
        }

        // DELETE: api/TruckRentals/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteTruckRental([FromRoute] Guid id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var truckRental = await _context.TruckRental.FindAsync(id);
            if (truckRental == null)
            {
                return NotFound();
            }

            _context.TruckRental.Remove(truckRental);
            await _context.SaveChangesAsync();

            return Ok(truckRental);
        }

        private bool TruckRentalExists(Guid id)
        {
            return _context.TruckRental.Any(e => e.Id == id);
        }
    }
}