﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PCCIntranet.Models;

namespace PCCIntranet.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ConsumablesIssuedsController : ControllerBase
    {
        private readonly EquipmentContext _context;

        public ConsumablesIssuedsController(EquipmentContext context)
        {
            _context = context;
        }

        // GET: api/ConsumablesIssueds
        [HttpGet]
        public IEnumerable<ConsumablesIssued> GetConsumablesIssued()
        {
            return _context.ConsumablesIssued;
        }

        // GET: api/ConsumablesIssueds/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetConsumablesIssued([FromRoute] Guid id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var consumablesIssued = await _context.ConsumablesIssued.FindAsync(id);

            if (consumablesIssued == null)
            {
                return NotFound();
            }

            return Ok(consumablesIssued);
        }

        // PUT: api/ConsumablesIssueds/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutConsumablesIssued([FromRoute] Guid id, [FromBody] ConsumablesIssued consumablesIssued)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != consumablesIssued.Id)
            {
                return BadRequest();
            }

            _context.Entry(consumablesIssued).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ConsumablesIssuedExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/ConsumablesIssueds
        [HttpPost]
        public async Task<IActionResult> PostConsumablesIssued([FromBody] ConsumablesIssued consumablesIssued)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.ConsumablesIssued.Add(consumablesIssued);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetConsumablesIssued", new { id = consumablesIssued.Id }, consumablesIssued);
        }

        // DELETE: api/ConsumablesIssueds/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteConsumablesIssued([FromRoute] Guid id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var consumablesIssued = await _context.ConsumablesIssued.FindAsync(id);
            if (consumablesIssued == null)
            {
                return NotFound();
            }

            _context.ConsumablesIssued.Remove(consumablesIssued);
            await _context.SaveChangesAsync();

            return Ok(consumablesIssued);
        }

        private bool ConsumablesIssuedExists(Guid id)
        {
            return _context.ConsumablesIssued.Any(e => e.Id == id);
        }
    }
}