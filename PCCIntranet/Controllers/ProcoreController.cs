using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using PCCIntranet.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PCCIntranet.Controllers
{
  [Route("api/[controller]")]
  [ApiController]
  [DisableCors]
  public class ProcoreController : ControllerBase
  {
    private readonly ProcoreService _procoreService;

    public ProcoreController(ProcoreService procoreService)
    {
      _procoreService = procoreService;
    }

    [HttpGet("list-projects")]
    public async Task<IActionResult> ListProjects()
    {
      var procoreprojects = await _procoreService.ListProjectsAsync();
      return Ok(procoreprojects);
    }
  }

}
