﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PCCIntranet.Models;

namespace PCCIntranet.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SubmtlsController : ControllerBase
    {
        private readonly SageContext _context;

        public SubmtlsController(SageContext context)
        {
            _context = context;
        }

        // GET: api/Submtls
        [HttpGet]
        public IEnumerable<Submtl> GetSubmtl()
        {
            return _context.Submtl;
        }

        // GET: api/Submtls/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetSubmtl([FromRoute] Guid id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var submtl = await _context.Submtl.FindAsync(id);

            if (submtl == null)
            {
                return NotFound();
            }

            return Ok(submtl);
        }

        // PUT: api/Submtls/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutSubmtl([FromRoute] Guid id, [FromBody] Submtl submtl)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != submtl.Idnum)
            {
                return BadRequest();
            }

            _context.Entry(submtl).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!SubmtlExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Submtls
        [HttpPost]
        public async Task<IActionResult> PostSubmtl([FromBody] Submtl submtl)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.Submtl.Add(submtl);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (SubmtlExists(submtl.Idnum))
                {
                    return new StatusCodeResult(StatusCodes.Status409Conflict);
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetSubmtl", new { id = submtl.Idnum }, submtl);
        }

        // DELETE: api/Submtls/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteSubmtl([FromRoute] Guid id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var submtl = await _context.Submtl.FindAsync(id);
            if (submtl == null)
            {
                return NotFound();
            }

            _context.Submtl.Remove(submtl);
            await _context.SaveChangesAsync();

            return Ok(submtl);
        }

        private bool SubmtlExists(Guid id)
        {
            return _context.Submtl.Any(e => e.Idnum == id);
        }
    }
}