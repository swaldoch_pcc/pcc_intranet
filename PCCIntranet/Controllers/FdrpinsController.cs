﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PCCIntranet.Models;

namespace PCCIntranet.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FdrpinsController : ControllerBase
    {
        private readonly SageContext _context;

        public FdrpinsController(SageContext context)
        {
            _context = context;
        }

        // GET: api/Fdrpins
        [HttpGet]
        public IEnumerable<Fdrpin> GetFdrpin()
        {
            return _context.Fdrpin;
        }

        // GET: api/Fdrpins/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetFdrpin([FromRoute] Guid id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var fdrpin = await _context.Fdrpin.FindAsync(id);

            if (fdrpin == null)
            {
                return NotFound();
            }

            return Ok(fdrpin);
        }

        // PUT: api/Fdrpins/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutFdrpin([FromRoute] Guid id, [FromBody] Fdrpin fdrpin)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != fdrpin.Idnum)
            {
                return BadRequest();
            }

            _context.Entry(fdrpin).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!FdrpinExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Fdrpins
        [HttpPost]
        public async Task<IActionResult> PostFdrpin([FromBody] Fdrpin fdrpin)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.Fdrpin.Add(fdrpin);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (FdrpinExists(fdrpin.Idnum))
                {
                    return new StatusCodeResult(StatusCodes.Status409Conflict);
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetFdrpin", new { id = fdrpin.Idnum }, fdrpin);
        }

        // DELETE: api/Fdrpins/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteFdrpin([FromRoute] Guid id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var fdrpin = await _context.Fdrpin.FindAsync(id);
            if (fdrpin == null)
            {
                return NotFound();
            }

            _context.Fdrpin.Remove(fdrpin);
            await _context.SaveChangesAsync();

            return Ok(fdrpin);
        }

        private bool FdrpinExists(Guid id)
        {
            return _context.Fdrpin.Any(e => e.Idnum == id);
        }
    }
}