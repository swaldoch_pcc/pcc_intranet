using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PCCIntranet.Models;

namespace PCCIntranet.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [DisableCors]

    public class UserPermissionsController : ControllerBase
    {
        private readonly UserContext _context;

        public UserPermissionsController(UserContext context)
        {
            _context = context;
        }

        // GET: api/UserPermissions
        [HttpGet]
        public IEnumerable<UserPermissions> GetUserPermissions()
        {
            return _context.UserPermissions;
        }

        // GET: api/UserPermissions/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetUserPermissions([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var userPermissions = await _context.UserPermissions.FindAsync(id);

            if (userPermissions == null)
            {
                return NotFound();
            }

            return Ok(userPermissions);
        }

        // PUT: api/UserPermissions/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutUserPermissions([FromRoute] int id, [FromBody] UserPermissions userPermissions)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != userPermissions.Id)
            {
                return BadRequest();
            }

            _context.Entry(userPermissions).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!UserPermissionsExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/UserPermissions
        [HttpPost]
        public async Task<IActionResult> PostUserPermissions([FromBody] UserPermissions userPermissions)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.UserPermissions.Add(userPermissions);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (UserPermissionsExists(userPermissions.Id))
                {
                    return new StatusCodeResult(StatusCodes.Status409Conflict);
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetUserPermissions", new { id = userPermissions.Id }, userPermissions);
        }

        // DELETE: api/UserPermissions/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteUserPermissions([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var userPermissions = await _context.UserPermissions.FindAsync(id);
            if (userPermissions == null)
            {
                return NotFound();
            }

            _context.UserPermissions.Remove(userPermissions);
            await _context.SaveChangesAsync();

            return Ok(userPermissions);
        }

        private bool UserPermissionsExists(int id)
        {
            return _context.UserPermissions.Any(e => e.Id == id);
        }
    }
}
