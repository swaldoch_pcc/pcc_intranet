﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PCCIntranet.Models;

namespace PCCIntranet.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FdrpmtsController : ControllerBase
    {
        private readonly SageContext _context;

        public FdrpmtsController(SageContext context)
        {
            _context = context;
        }

        // GET: api/Fdrpmts
        [HttpGet]
        public IEnumerable<Fdrpmt> GetFdrpmt()
        {
            return _context.Fdrpmt;
        }

        // GET: api/Fdrpmts/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetFdrpmt([FromRoute] Guid id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var fdrpmt = await _context.Fdrpmt.FindAsync(id);

            if (fdrpmt == null)
            {
                return NotFound();
            }

            return Ok(fdrpmt);
        }

        // PUT: api/Fdrpmts/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutFdrpmt([FromRoute] Guid id, [FromBody] Fdrpmt fdrpmt)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != fdrpmt.Idnum)
            {
                return BadRequest();
            }

            _context.Entry(fdrpmt).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!FdrpmtExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Fdrpmts
        [HttpPost]
        public async Task<IActionResult> PostFdrpmt([FromBody] Fdrpmt fdrpmt)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.Fdrpmt.Add(fdrpmt);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (FdrpmtExists(fdrpmt.Idnum))
                {
                    return new StatusCodeResult(StatusCodes.Status409Conflict);
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetFdrpmt", new { id = fdrpmt.Idnum }, fdrpmt);
        }

        // DELETE: api/Fdrpmts/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteFdrpmt([FromRoute] Guid id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var fdrpmt = await _context.Fdrpmt.FindAsync(id);
            if (fdrpmt == null)
            {
                return NotFound();
            }

            _context.Fdrpmt.Remove(fdrpmt);
            await _context.SaveChangesAsync();

            return Ok(fdrpmt);
        }

        private bool FdrpmtExists(Guid id)
        {
            return _context.Fdrpmt.Any(e => e.Idnum == id);
        }
    }
}