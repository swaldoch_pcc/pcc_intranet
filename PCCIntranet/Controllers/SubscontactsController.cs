﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PCCIntranet.Models;

namespace PCCIntranet.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SubscontactsController : ControllerBase
    {
        private readonly PCCContext _context;

        public SubscontactsController(PCCContext context)
        {
            _context = context;
        }

        // GET: api/Subscontacts
        [HttpGet]
        public IEnumerable<Subscontact> GetSubscontact()
        {
            return _context.Subscontact;
        }

        // GET: api/Subscontacts/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetSubscontact([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var subscontact = await _context.Subscontact.FindAsync(id);

            if (subscontact == null)
            {
                return NotFound();
            }

            return Ok(subscontact);
        }

        // PUT: api/Subscontacts/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutSubscontact([FromRoute] int id, [FromBody] Subscontact subscontact)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != subscontact.ContactId)
            {
                return BadRequest();
            }

            _context.Entry(subscontact).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!SubscontactExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Subscontacts
        [HttpPost]
        public async Task<IActionResult> PostSubscontact([FromBody] Subscontact subscontact)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.Subscontact.Add(subscontact);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetSubscontact", new { id = subscontact.ContactId }, subscontact);
        }

        // DELETE: api/Subscontacts/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteSubscontact([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var subscontact = await _context.Subscontact.FindAsync(id);
            if (subscontact == null)
            {
                return NotFound();
            }

            _context.Subscontact.Remove(subscontact);
            await _context.SaveChangesAsync();

            return Ok(subscontact);
        }

        private bool SubscontactExists(int id)
        {
            return _context.Subscontact.Any(e => e.ContactId == id);
        }
    }
}