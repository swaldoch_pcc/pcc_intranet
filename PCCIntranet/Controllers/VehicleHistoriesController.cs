﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PCCIntranet.Models;

namespace PCCIntranet.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class VehicleHistoriesController : ControllerBase
    {
        private readonly EquipmentContext _context;

        public VehicleHistoriesController(EquipmentContext context)
        {
            _context = context;
        }

        // GET: api/VehicleHistories
        [HttpGet]
        public IEnumerable<VehicleHistory> GetVehicleHistory()
        {
            return _context.VehicleHistory;
        }

        // GET: api/VehicleHistories/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetVehicleHistory([FromRoute] Guid id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var vehicleHistory = await _context.VehicleHistory.FindAsync(id);

            if (vehicleHistory == null)
            {
                return NotFound();
            }

            return Ok(vehicleHistory);
        }

        // PUT: api/VehicleHistories/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutVehicleHistory([FromRoute] Guid id, [FromBody] VehicleHistory vehicleHistory)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != vehicleHistory.HistoryId)
            {
                return BadRequest();
            }

            _context.Entry(vehicleHistory).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!VehicleHistoryExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/VehicleHistories
        [HttpPost]
        public async Task<IActionResult> PostVehicleHistory([FromBody] VehicleHistory vehicleHistory)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.VehicleHistory.Add(vehicleHistory);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (VehicleHistoryExists(vehicleHistory.HistoryId))
                {
                    return new StatusCodeResult(StatusCodes.Status409Conflict);
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetVehicleHistory", new { id = vehicleHistory.HistoryId }, vehicleHistory);
        }

        // DELETE: api/VehicleHistories/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteVehicleHistory([FromRoute] Guid id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var vehicleHistory = await _context.VehicleHistory.FindAsync(id);
            if (vehicleHistory == null)
            {
                return NotFound();
            }

            _context.VehicleHistory.Remove(vehicleHistory);
            await _context.SaveChangesAsync();

            return Ok(vehicleHistory);
        }

        private bool VehicleHistoryExists(Guid id)
        {
            return _context.VehicleHistory.Any(e => e.HistoryId == id);
        }
    }
}