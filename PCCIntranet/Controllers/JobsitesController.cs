using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PCCIntranet.Models;

using AutoMapper;
using System.IdentityModel.Tokens.Jwt;
using PCCIntranet.Helpers;
using Microsoft.Extensions.Options;
using System.Text;
using Microsoft.IdentityModel.Tokens;
using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;
using PCCIntranet.Services;
using PCCIntranet.Dtos;
using Microsoft.AspNetCore.Authentication.JwtBearer;

namespace PCCIntranet.Controllers
{
  
  [Route("api/[controller]")]
  [ApiController]
  [DisableCors]
  
  public class JobsitesController : ControllerBase
  {
    private readonly PCCContext _context;

    public JobsitesController(PCCContext context)
    {
      _context = context;
    }

    // GET: api/Jobsites
    [HttpGet]
    public IEnumerable<Jobsites> GetJobsites()
    {
      return _context.Jobsites;
    }

    // GET: api/Jobsites/5
    [HttpGet("{id}")]
    public async Task<IActionResult> GetJobsites([FromRoute] int id)
    {
      if (!ModelState.IsValid)
      {
        return BadRequest(ModelState);
      }

      var jobsites = await _context.Jobsites.FindAsync(id);

      if (jobsites == null)
      {
        return NotFound();
      }

      return Ok(jobsites);
    }

    // PUT: api/Jobsites/5
    [HttpPut("{id}")]
    public async Task<IActionResult> PutJobsites([FromRoute] int id, [FromBody] Jobsites jobsites)
    {
      if (!ModelState.IsValid)
      {
        return BadRequest(ModelState);
      }

      if (id != jobsites.JobsiteId)
      {
        return BadRequest();
      }

      _context.Entry(jobsites).State = EntityState.Modified;

      try
      {
        await _context.SaveChangesAsync();
      }
      catch (DbUpdateConcurrencyException)
      {
        if (!JobsitesExists(id))
        {
          return NotFound();
        }
        else
        {
          throw;
        }
      }

      return NoContent();
    }

    // POST: api/Jobsites
    [HttpPost]
    public async Task<IActionResult> PostJobsites([FromBody] Jobsites jobsites)
    {
      if (!ModelState.IsValid)
      {
        return BadRequest(ModelState);
      }

      _context.Jobsites.Add(jobsites);
      try
      {
        await _context.SaveChangesAsync();
      }
      catch (DbUpdateException)
      {
        if (JobsitesExists(jobsites.JobsiteId))
        {
          return new StatusCodeResult(StatusCodes.Status409Conflict);
        }
        else
        {
          throw;
        }
      }

      return CreatedAtAction("GetJobsites", new { id = jobsites.JobsiteId }, jobsites);
    }

    // DELETE: api/Jobsites/5
    [HttpDelete("{id}")]
    public async Task<IActionResult> DeleteJobsites([FromRoute] int id)
    {
      if (!ModelState.IsValid)
      {
        return BadRequest(ModelState);
      }

      var jobsites = await _context.Jobsites.FindAsync(id);
      if (jobsites == null)
      {
        return NotFound();
      }

      _context.Jobsites.Remove(jobsites);
      await _context.SaveChangesAsync();

      return Ok(jobsites);
    }

    private bool JobsitesExists(int id)
    {
      return _context.Jobsites.Any(e => e.JobsiteId == id);
    }
  }
}
