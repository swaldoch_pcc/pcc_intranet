﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PCCIntranet.Models;

namespace PCCIntranet.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PchlstsController : ControllerBase
    {
        private readonly SageContext _context;

        public PchlstsController(SageContext context)
        {
            _context = context;
        }

        // GET: api/Pchlsts
        [HttpGet]
        public IEnumerable<Pchlst> GetPchlst()
        {
            return _context.Pchlst;
        }

        // GET: api/Pchlsts/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetPchlst([FromRoute] Guid id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var pchlst = await _context.Pchlst.FindAsync(id);

            if (pchlst == null)
            {
                return NotFound();
            }

            return Ok(pchlst);
        }

        // PUT: api/Pchlsts/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutPchlst([FromRoute] Guid id, [FromBody] Pchlst pchlst)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != pchlst.Idnum)
            {
                return BadRequest();
            }

            _context.Entry(pchlst).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PchlstExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Pchlsts
        [HttpPost]
        public async Task<IActionResult> PostPchlst([FromBody] Pchlst pchlst)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.Pchlst.Add(pchlst);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (PchlstExists(pchlst.Idnum))
                {
                    return new StatusCodeResult(StatusCodes.Status409Conflict);
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetPchlst", new { id = pchlst.Idnum }, pchlst);
        }

        // DELETE: api/Pchlsts/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeletePchlst([FromRoute] Guid id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var pchlst = await _context.Pchlst.FindAsync(id);
            if (pchlst == null)
            {
                return NotFound();
            }

            _context.Pchlst.Remove(pchlst);
            await _context.SaveChangesAsync();

            return Ok(pchlst);
        }

        private bool PchlstExists(Guid id)
        {
            return _context.Pchlst.Any(e => e.Idnum == id);
        }
    }
}