using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PCCIntranet.Models;

namespace PCCIntranet.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [DisableCors]
  public class PmprojectReviewsController : ControllerBase
    {
        private readonly PCCContext _context;

        public PmprojectReviewsController(PCCContext context)
        {
            _context = context;
        }

        // GET: api/PmprojectReviews
        [HttpGet]
        public IEnumerable<PmprojectReview> GetPmprojectReview()
        {
            return _context.PmprojectReview;
        }

        // GET: api/PmprojectReviews/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetPmprojectReview([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var pmprojectReview = await _context.PmprojectReview.FindAsync(id);

            if (pmprojectReview == null)
            {
                return NotFound();
            }

            return Ok(pmprojectReview);
        }

        // PUT: api/PmprojectReviews/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutPmprojectReview([FromRoute] int id, [FromBody] PmprojectReview pmprojectReview)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != pmprojectReview.Id)
            {
                return BadRequest();
            }

            _context.Entry(pmprojectReview).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PmprojectReviewExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/PmprojectReviews
        [HttpPost]
        [DisableCors]
        public async Task<IActionResult> PostPmprojectReview([FromBody] PmprojectReview pmprojectReview)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.PmprojectReview.Add(pmprojectReview);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (PmprojectReviewExists(pmprojectReview.Id))
                {
                    return new StatusCodeResult(StatusCodes.Status409Conflict);
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetPmprojectReview", new { id = pmprojectReview.Id }, pmprojectReview);
        }

        // DELETE: api/PmprojectReviews/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeletePmprojectReview([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var pmprojectReview = await _context.PmprojectReview.FindAsync(id);
            if (pmprojectReview == null)
            {
                return NotFound();
            }

            _context.PmprojectReview.Remove(pmprojectReview);
            await _context.SaveChangesAsync();

            return Ok(pmprojectReview);
        }

        private bool PmprojectReviewExists(int id)
        {
            return _context.PmprojectReview.Any(e => e.Id == id);
        }
    }
}
