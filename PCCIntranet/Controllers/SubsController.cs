﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PCCIntranet.Models;

namespace PCCIntranet.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SubsController : ControllerBase
    {
        private readonly PCCContext _context;

        public SubsController(PCCContext context)
        {
            _context = context;
        }

        // GET: api/Subs
        [HttpGet]
        public IEnumerable<Subs> GetSubsMaster()
        {
            return _context.SubsMaster;
        }

        // GET: api/Subs/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetSubs([FromRoute] string id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var subs = await _context.SubsMaster.FindAsync(id);

            if (subs == null)
            {
                return NotFound();
            }

            return Ok(subs);
        }

        // PUT: api/Subs/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutSubs([FromRoute] string id, [FromBody] Subs subs)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != subs.Company)
            {
                return BadRequest();
            }

            _context.Entry(subs).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!SubsExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Subs
        [HttpPost]
        public async Task<IActionResult> PostSubs([FromBody] Subs subs)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.SubsMaster.Add(subs);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (SubsExists(subs.Company))
                {
                    return new StatusCodeResult(StatusCodes.Status409Conflict);
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetSubs", new { id = subs.Company }, subs);
        }

        // DELETE: api/Subs/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteSubs([FromRoute] string id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var subs = await _context.SubsMaster.FindAsync(id);
            if (subs == null)
            {
                return NotFound();
            }

            _context.SubsMaster.Remove(subs);
            await _context.SaveChangesAsync();

            return Ok(subs);
        }

        private bool SubsExists(string id)
        {
            return _context.SubsMaster.Any(e => e.Company == id);
        }
    }
}