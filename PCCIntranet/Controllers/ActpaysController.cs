﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PCCIntranet.Models;

namespace PCCIntranet.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ActpaysController : ControllerBase
    {
        private readonly SageContext _context;

        public ActpaysController(SageContext context)
        {
            _context = context;
        }

        // GET: api/Actpays
        [HttpGet]
        public IEnumerable<Actpay> GetActpay()
        {
            return _context.Actpay;
        }

        // GET: api/Actpays/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetActpay([FromRoute] Guid id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var actpay = await _context.Actpay.FindAsync(id);

            if (actpay == null)
            {
                return NotFound();
            }

            return Ok(actpay);
        }

        // PUT: api/Actpays/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutActpay([FromRoute] Guid id, [FromBody] Actpay actpay)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != actpay.Idnum)
            {
                return BadRequest();
            }

            _context.Entry(actpay).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ActpayExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Actpays
        [HttpPost]
        public async Task<IActionResult> PostActpay([FromBody] Actpay actpay)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.Actpay.Add(actpay);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (ActpayExists(actpay.Idnum))
                {
                    return new StatusCodeResult(StatusCodes.Status409Conflict);
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetActpay", new { id = actpay.Idnum }, actpay);
        }

        // DELETE: api/Actpays/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteActpay([FromRoute] Guid id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var actpay = await _context.Actpay.FindAsync(id);
            if (actpay == null)
            {
                return NotFound();
            }

            _context.Actpay.Remove(actpay);
            await _context.SaveChangesAsync();

            return Ok(actpay);
        }

        private bool ActpayExists(Guid id)
        {
            return _context.Actpay.Any(e => e.Idnum == id);
        }
    }
}