﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PCCIntranet.Models;

namespace PCCIntranet.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SafetyauditsController : ControllerBase
    {
        private readonly PCCContext _context;

        public SafetyauditsController(PCCContext context)
        {
            _context = context;
        }

        // GET: api/Safetyaudits
        [HttpGet]
        public IEnumerable<Safetyaudit> GetSafetyaudit()
        {
            return _context.Safetyaudit;
        }

        // GET: api/Safetyaudits/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetSafetyaudit([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var safetyaudit = await _context.Safetyaudit.FindAsync(id);

            if (safetyaudit == null)
            {
                return NotFound();
            }

            return Ok(safetyaudit);
        }

        // PUT: api/Safetyaudits/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutSafetyaudit([FromRoute] int id, [FromBody] Safetyaudit safetyaudit)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != safetyaudit.Id)
            {
                return BadRequest();
            }

            _context.Entry(safetyaudit).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!SafetyauditExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Safetyaudits
        [HttpPost]
        public async Task<IActionResult> PostSafetyaudit([FromBody] Safetyaudit safetyaudit)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.Safetyaudit.Add(safetyaudit);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetSafetyaudit", new { id = safetyaudit.Id }, safetyaudit);
        }

        // DELETE: api/Safetyaudits/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteSafetyaudit([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var safetyaudit = await _context.Safetyaudit.FindAsync(id);
            if (safetyaudit == null)
            {
                return NotFound();
            }

            _context.Safetyaudit.Remove(safetyaudit);
            await _context.SaveChangesAsync();

            return Ok(safetyaudit);
        }

        private bool SafetyauditExists(int id)
        {
            return _context.Safetyaudit.Any(e => e.Id == id);
        }
    }
}