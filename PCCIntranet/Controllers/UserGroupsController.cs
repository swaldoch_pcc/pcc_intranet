using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PCCIntranet.Models;

namespace PCCIntranet.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [DisableCors]
  public class UserGroupsController : ControllerBase
    {
        private readonly PCCContext _context;

        public UserGroupsController(PCCContext context)
        {
            _context = context;
        }

        // GET: api/UserGroups
        [HttpGet]
        public IEnumerable<UserGroups> GetUserGroups()
        {
            return _context.UserGroups;
        }

        // GET: api/UserGroups/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetUserGroups([FromRoute] string id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var userGroups = await _context.UserGroups.FindAsync(id);

            if (userGroups == null)
            {
                return NotFound();
            }

            return Ok(userGroups);
        }

        // PUT: api/UserGroups/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutUserGroups([FromRoute] string id, [FromBody] UserGroups userGroups)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != userGroups.Group)
            {
                return BadRequest();
            }

            _context.Entry(userGroups).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!UserGroupsExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/UserGroups
        [HttpPost]
        public async Task<IActionResult> PostUserGroups([FromBody] UserGroups userGroups)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.UserGroups.Add(userGroups);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (UserGroupsExists(userGroups.Group))
                {
                    return new StatusCodeResult(StatusCodes.Status409Conflict);
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetUserGroups", new { id = userGroups.Group }, userGroups);
        }

        // DELETE: api/UserGroups/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteUserGroups([FromRoute] string id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var userGroups = await _context.UserGroups.FindAsync(id);
            if (userGroups == null)
            {
                return NotFound();
            }

            _context.UserGroups.Remove(userGroups);
            await _context.SaveChangesAsync();

            return Ok(userGroups);
        }

        private bool UserGroupsExists(string id)
        {
            return _context.UserGroups.Any(e => e.Group == id);
        }
    }
}
