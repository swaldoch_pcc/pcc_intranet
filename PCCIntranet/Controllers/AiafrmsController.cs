﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PCCIntranet.Models;

namespace PCCIntranet.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AiafrmsController : ControllerBase
    {
        private readonly SageContext _context;

        public AiafrmsController(SageContext context)
        {
            _context = context;
        }

        // GET: api/Aiafrms
        [HttpGet]
        public IEnumerable<Aiafrm> GetAiafrm()
        {
            return _context.Aiafrm;
        }

        // GET: api/Aiafrms/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetAiafrm([FromRoute] Guid id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var aiafrm = await _context.Aiafrm.FindAsync(id);

            if (aiafrm == null)
            {
                return NotFound();
            }

            return Ok(aiafrm);
        }

        // PUT: api/Aiafrms/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutAiafrm([FromRoute] Guid id, [FromBody] Aiafrm aiafrm)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != aiafrm.Idnum)
            {
                return BadRequest();
            }

            _context.Entry(aiafrm).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!AiafrmExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Aiafrms
        [HttpPost]
        public async Task<IActionResult> PostAiafrm([FromBody] Aiafrm aiafrm)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.Aiafrm.Add(aiafrm);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (AiafrmExists(aiafrm.Idnum))
                {
                    return new StatusCodeResult(StatusCodes.Status409Conflict);
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetAiafrm", new { id = aiafrm.Idnum }, aiafrm);
        }

        // DELETE: api/Aiafrms/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteAiafrm([FromRoute] Guid id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var aiafrm = await _context.Aiafrm.FindAsync(id);
            if (aiafrm == null)
            {
                return NotFound();
            }

            _context.Aiafrm.Remove(aiafrm);
            await _context.SaveChangesAsync();

            return Ok(aiafrm);
        }

        private bool AiafrmExists(Guid id)
        {
            return _context.Aiafrm.Any(e => e.Idnum == id);
        }
    }
}