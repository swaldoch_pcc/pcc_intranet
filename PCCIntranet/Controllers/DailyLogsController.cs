using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PCCIntranet.Models;

namespace PCCIntranet.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [DisableCors]
  public class DailyLogsController : ControllerBase
    {
        private readonly PCCContext _context;

        public DailyLogsController(PCCContext context)
        {
            _context = context;
        }

        // GET: api/DailyLogs
        [HttpGet]
        public IEnumerable<DailyLogs> GetDailyLogs()
        {
            return _context.DailyLogs;
        }

        // GET: api/DailyLogs/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetDailyLogs([FromRoute] string id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var dailyLogs = await _context.DailyLogs.FindAsync(id);

            if (dailyLogs == null)
            {
                return NotFound();
            }

            return Ok(dailyLogs);
        }

        // PUT: api/DailyLogs/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutDailyLogs([FromRoute] string id, [FromBody] DailyLogs dailyLogs)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != dailyLogs.JobsiteId)
            {
                return BadRequest();
            }

            _context.Entry(dailyLogs).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!DailyLogsExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/DailyLogs
        [HttpPost]
        public async Task<IActionResult> PostDailyLogs([FromBody] DailyLogs dailyLogs)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.DailyLogs.Add(dailyLogs);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (DailyLogsExists(dailyLogs.JobsiteId))
                {
                    return new StatusCodeResult(StatusCodes.Status409Conflict);
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetDailyLogs", new { id = dailyLogs.JobsiteId }, dailyLogs);
        }

        // DELETE: api/DailyLogs/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteDailyLogs([FromRoute] string id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var dailyLogs = await _context.DailyLogs.FindAsync(id);
            if (dailyLogs == null)
            {
                return NotFound();
            }

            _context.DailyLogs.Remove(dailyLogs);
            await _context.SaveChangesAsync();

            return Ok(dailyLogs);
        }

        private bool DailyLogsExists(string id)
        {
            return _context.DailyLogs.Any(e => e.JobsiteId == id);
        }
    }
}
