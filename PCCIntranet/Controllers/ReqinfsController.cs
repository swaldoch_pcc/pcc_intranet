﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PCCIntranet.Models;

namespace PCCIntranet.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ReqinfsController : ControllerBase
    {
        private readonly SageContext _context;

        public ReqinfsController(SageContext context)
        {
            _context = context;
        }

        // GET: api/Reqinfs
        [HttpGet]
        public IEnumerable<Reqinf> GetReqinf()
        {
            return _context.Reqinf;
        }

        // GET: api/Reqinfs/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetReqinf([FromRoute] Guid id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var reqinf = await _context.Reqinf.FindAsync(id);

            if (reqinf == null)
            {
                return NotFound();
            }

            return Ok(reqinf);
        }

        // PUT: api/Reqinfs/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutReqinf([FromRoute] Guid id, [FromBody] Reqinf reqinf)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != reqinf.Idnum)
            {
                return BadRequest();
            }

            _context.Entry(reqinf).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ReqinfExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Reqinfs
        [HttpPost]
        public async Task<IActionResult> PostReqinf([FromBody] Reqinf reqinf)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.Reqinf.Add(reqinf);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (ReqinfExists(reqinf.Idnum))
                {
                    return new StatusCodeResult(StatusCodes.Status409Conflict);
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetReqinf", new { id = reqinf.Idnum }, reqinf);
        }

        // DELETE: api/Reqinfs/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteReqinf([FromRoute] Guid id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var reqinf = await _context.Reqinf.FindAsync(id);
            if (reqinf == null)
            {
                return NotFound();
            }

            _context.Reqinf.Remove(reqinf);
            await _context.SaveChangesAsync();

            return Ok(reqinf);
        }

        private bool ReqinfExists(Guid id)
        {
            return _context.Reqinf.Any(e => e.Idnum == id);
        }
    }
}