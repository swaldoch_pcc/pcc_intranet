﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PCCIntranet.Models;

namespace PCCIntranet.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FdrpeqsController : ControllerBase
    {
        private readonly SageContext _context;

        public FdrpeqsController(SageContext context)
        {
            _context = context;
        }

        // GET: api/Fdrpeqs
        [HttpGet]
        public IEnumerable<Fdrpeq> GetFdrpeq()
        {
            return _context.Fdrpeq;
        }

        // GET: api/Fdrpeqs/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetFdrpeq([FromRoute] Guid id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var fdrpeq = await _context.Fdrpeq.FindAsync(id);

            if (fdrpeq == null)
            {
                return NotFound();
            }

            return Ok(fdrpeq);
        }

        // PUT: api/Fdrpeqs/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutFdrpeq([FromRoute] Guid id, [FromBody] Fdrpeq fdrpeq)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != fdrpeq.Idnum)
            {
                return BadRequest();
            }

            _context.Entry(fdrpeq).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!FdrpeqExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Fdrpeqs
        [HttpPost]
        public async Task<IActionResult> PostFdrpeq([FromBody] Fdrpeq fdrpeq)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.Fdrpeq.Add(fdrpeq);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (FdrpeqExists(fdrpeq.Idnum))
                {
                    return new StatusCodeResult(StatusCodes.Status409Conflict);
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetFdrpeq", new { id = fdrpeq.Idnum }, fdrpeq);
        }

        // DELETE: api/Fdrpeqs/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteFdrpeq([FromRoute] Guid id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var fdrpeq = await _context.Fdrpeq.FindAsync(id);
            if (fdrpeq == null)
            {
                return NotFound();
            }

            _context.Fdrpeq.Remove(fdrpeq);
            await _context.SaveChangesAsync();

            return Ok(fdrpeq);
        }

        private bool FdrpeqExists(Guid id)
        {
            return _context.Fdrpeq.Any(e => e.Idnum == id);
        }
    }
}