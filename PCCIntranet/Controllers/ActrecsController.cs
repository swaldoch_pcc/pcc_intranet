﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PCCIntranet.Models;

namespace PCCIntranet.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ActrecsController : ControllerBase
    {
        private readonly SageContext _context;

        public ActrecsController(SageContext context)
        {
            _context = context;
        }

        // GET: api/Actrecs
        [HttpGet]
        public IEnumerable<Actrec> GetActrec()
        {
            return _context.Actrec;
        }

        // GET: api/Actrecs/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetActrec([FromRoute] Guid id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var actrec = await _context.Actrec.FindAsync(id);

            if (actrec == null)
            {
                return NotFound();
            }

            return Ok(actrec);
        }

        // PUT: api/Actrecs/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutActrec([FromRoute] Guid id, [FromBody] Actrec actrec)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != actrec.Idnum)
            {
                return BadRequest();
            }

            _context.Entry(actrec).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ActrecExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Actrecs
        [HttpPost]
        public async Task<IActionResult> PostActrec([FromBody] Actrec actrec)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.Actrec.Add(actrec);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (ActrecExists(actrec.Idnum))
                {
                    return new StatusCodeResult(StatusCodes.Status409Conflict);
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetActrec", new { id = actrec.Idnum }, actrec);
        }

        // DELETE: api/Actrecs/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteActrec([FromRoute] Guid id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var actrec = await _context.Actrec.FindAsync(id);
            if (actrec == null)
            {
                return NotFound();
            }

            _context.Actrec.Remove(actrec);
            await _context.SaveChangesAsync();

            return Ok(actrec);
        }

        private bool ActrecExists(Guid id)
        {
            return _context.Actrec.Any(e => e.Idnum == id);
        }
    }
}