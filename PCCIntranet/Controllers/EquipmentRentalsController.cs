﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PCCIntranet.Models;

namespace PCCIntranet.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EquipmentRentalsController : ControllerBase
    {
        private readonly EquipmentContext _context;

        public EquipmentRentalsController(EquipmentContext context)
        {
            _context = context;
        }

        // GET: api/EquipmentRentals
        [HttpGet]
        public IEnumerable<EquipmentRental> GetEquipmentRental()
        {
            return _context.EquipmentRental;
        }

        // GET: api/EquipmentRentals/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetEquipmentRental([FromRoute] Guid id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var equipmentRental = await _context.EquipmentRental.FindAsync(id);

            if (equipmentRental == null)
            {
                return NotFound();
            }

            return Ok(equipmentRental);
        }

        // PUT: api/EquipmentRentals/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutEquipmentRental([FromRoute] Guid id, [FromBody] EquipmentRental equipmentRental)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != equipmentRental.RentalId)
            {
                return BadRequest();
            }

            _context.Entry(equipmentRental).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!EquipmentRentalExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/EquipmentRentals
        [HttpPost]
        public async Task<IActionResult> PostEquipmentRental([FromBody] EquipmentRental equipmentRental)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.EquipmentRental.Add(equipmentRental);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (EquipmentRentalExists(equipmentRental.RentalId))
                {
                    return new StatusCodeResult(StatusCodes.Status409Conflict);
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetEquipmentRental", new { id = equipmentRental.RentalId }, equipmentRental);
        }

        // DELETE: api/EquipmentRentals/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteEquipmentRental([FromRoute] Guid id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var equipmentRental = await _context.EquipmentRental.FindAsync(id);
            if (equipmentRental == null)
            {
                return NotFound();
            }

            _context.EquipmentRental.Remove(equipmentRental);
            await _context.SaveChangesAsync();

            return Ok(equipmentRental);
        }

        private bool EquipmentRentalExists(Guid id)
        {
            return _context.EquipmentRental.Any(e => e.RentalId == id);
        }
    }
}