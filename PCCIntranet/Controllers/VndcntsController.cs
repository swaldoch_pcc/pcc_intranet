﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PCCIntranet.Models;

namespace PCCIntranet.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class VndcntsController : ControllerBase
    {
        private readonly SageContext _context;

        public VndcntsController(SageContext context)
        {
            _context = context;
        }

        // GET: api/Vndcnts
        [HttpGet]
        public IEnumerable<Vndcnt> GetVndcnt()
        {
            return _context.Vndcnt;
        }

        // GET: api/Vndcnts/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetVndcnt([FromRoute] Guid id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var vndcnt = await _context.Vndcnt.FindAsync(id);

            if (vndcnt == null)
            {
                return NotFound();
            }

            return Ok(vndcnt);
        }

        // PUT: api/Vndcnts/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutVndcnt([FromRoute] Guid id, [FromBody] Vndcnt vndcnt)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != vndcnt.Idnum)
            {
                return BadRequest();
            }

            _context.Entry(vndcnt).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!VndcntExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Vndcnts
        [HttpPost]
        public async Task<IActionResult> PostVndcnt([FromBody] Vndcnt vndcnt)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.Vndcnt.Add(vndcnt);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (VndcntExists(vndcnt.Idnum))
                {
                    return new StatusCodeResult(StatusCodes.Status409Conflict);
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetVndcnt", new { id = vndcnt.Idnum }, vndcnt);
        }

        // DELETE: api/Vndcnts/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteVndcnt([FromRoute] Guid id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var vndcnt = await _context.Vndcnt.FindAsync(id);
            if (vndcnt == null)
            {
                return NotFound();
            }

            _context.Vndcnt.Remove(vndcnt);
            await _context.SaveChangesAsync();

            return Ok(vndcnt);
        }

        private bool VndcntExists(Guid id)
        {
            return _context.Vndcnt.Any(e => e.Idnum == id);
        }
    }
}