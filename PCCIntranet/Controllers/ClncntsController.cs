﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PCCIntranet.Models;

namespace PCCIntranet.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ClncntsController : ControllerBase
    {
        private readonly SageContext _context;

        public ClncntsController(SageContext context)
        {
            _context = context;
        }

        // GET: api/Clncnts
        [HttpGet]
        public IEnumerable<Clncnt> GetClncnt()
        {
            return _context.Clncnt;
        }

        // GET: api/Clncnts/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetClncnt([FromRoute] Guid id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var clncnt = await _context.Clncnt.FindAsync(id);

            if (clncnt == null)
            {
                return NotFound();
            }

            return Ok(clncnt);
        }

        // PUT: api/Clncnts/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutClncnt([FromRoute] Guid id, [FromBody] Clncnt clncnt)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != clncnt.Idnum)
            {
                return BadRequest();
            }

            _context.Entry(clncnt).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ClncntExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Clncnts
        [HttpPost]
        public async Task<IActionResult> PostClncnt([FromBody] Clncnt clncnt)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.Clncnt.Add(clncnt);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (ClncntExists(clncnt.Idnum))
                {
                    return new StatusCodeResult(StatusCodes.Status409Conflict);
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetClncnt", new { id = clncnt.Idnum }, clncnt);
        }

        // DELETE: api/Clncnts/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteClncnt([FromRoute] Guid id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var clncnt = await _context.Clncnt.FindAsync(id);
            if (clncnt == null)
            {
                return NotFound();
            }

            _context.Clncnt.Remove(clncnt);
            await _context.SaveChangesAsync();

            return Ok(clncnt);
        }

        private bool ClncntExists(Guid id)
        {
            return _context.Clncnt.Any(e => e.Idnum == id);
        }
    }
}