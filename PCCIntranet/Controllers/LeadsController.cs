﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PCCIntranet.Models;

namespace PCCIntranet.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LeadsController : ControllerBase
    {
        private readonly PCCContext _context;

        public LeadsController(PCCContext context)
        {
            _context = context;
        }

        // GET: api/Leads
        [HttpGet]
        public IEnumerable<Leads> GetLeads()
        {
            return _context.Leads;
        }

        // GET: api/Leads/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetLeads([FromRoute] string id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var leads = await _context.Leads.FindAsync(id);

            if (leads == null)
            {
                return NotFound();
            }

            return Ok(leads);
        }

        // PUT: api/Leads/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutLeads([FromRoute] string id, [FromBody] Leads leads)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != leads.LeadId)
            {
                return BadRequest();
            }

            _context.Entry(leads).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!LeadsExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Leads
        [HttpPost]
        public async Task<IActionResult> PostLeads([FromBody] Leads leads)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.Leads.Add(leads);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetLeads", new { id = leads.LeadId }, leads);
        }

        // DELETE: api/Leads/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteLeads([FromRoute] string id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var leads = await _context.Leads.FindAsync(id);
            if (leads == null)
            {
                return NotFound();
            }

            _context.Leads.Remove(leads);
            await _context.SaveChangesAsync();

            return Ok(leads);
        }

        private bool LeadsExists(string id)
        {
            return _context.Leads.Any(e => e.LeadId == id);
        }
    }
}