﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PCCIntranet.Models;

namespace PCCIntranet.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FldrptsController : ControllerBase
    {
        private readonly SageContext _context;

        public FldrptsController(SageContext context)
        {
            _context = context;
        }

        // GET: api/Fldrpts
        [HttpGet]
        public IEnumerable<Fldrpt> GetFldrpt()
        {
            return _context.Fldrpt;
        }

        // GET: api/Fldrpts/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetFldrpt([FromRoute] Guid id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var fldrpt = await _context.Fldrpt.FindAsync(id);

            if (fldrpt == null)
            {
                return NotFound();
            }

            return Ok(fldrpt);
        }

        // PUT: api/Fldrpts/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutFldrpt([FromRoute] Guid id, [FromBody] Fldrpt fldrpt)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != fldrpt.Idnum)
            {
                return BadRequest();
            }

            _context.Entry(fldrpt).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!FldrptExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Fldrpts
        [HttpPost]
        public async Task<IActionResult> PostFldrpt([FromBody] Fldrpt fldrpt)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.Fldrpt.Add(fldrpt);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (FldrptExists(fldrpt.Idnum))
                {
                    return new StatusCodeResult(StatusCodes.Status409Conflict);
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetFldrpt", new { id = fldrpt.Idnum }, fldrpt);
        }

        // DELETE: api/Fldrpts/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteFldrpt([FromRoute] Guid id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var fldrpt = await _context.Fldrpt.FindAsync(id);
            if (fldrpt == null)
            {
                return NotFound();
            }

            _context.Fldrpt.Remove(fldrpt);
            await _context.SaveChangesAsync();

            return Ok(fldrpt);
        }

        private bool FldrptExists(Guid id)
        {
            return _context.Fldrpt.Any(e => e.Idnum == id);
        }
    }
}