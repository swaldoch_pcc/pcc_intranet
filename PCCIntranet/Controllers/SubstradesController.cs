using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PCCIntranet.Models;

namespace PCCIntranet.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SubsTradesController : ControllerBase
    {
        private readonly PCCContext _context;

        public SubsTradesController(PCCContext context)
        {
            _context = context;
        }

        // GET: api/SubsTrades
        [HttpGet]
        public IEnumerable<SubsTrades> GetSubstrades()
        {
            return _context.Substrades;
        }

        // GET: api/SubsTrades/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetSubsTrades([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var subsTrades = await _context.Substrades.FindAsync(id);

            if (subsTrades == null)
            {
                return NotFound();
            }

            return Ok(subsTrades);
        }

        // PUT: api/SubsTrades/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutSubsTrades([FromRoute] int id, [FromBody] SubsTrades subsTrades)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != subsTrades.Id)
            {
                return BadRequest();
            }

            _context.Entry(subsTrades).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!SubsTradesExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/SubsTrades
        [HttpPost]
        public async Task<IActionResult> PostSubsTrades([FromBody] SubsTrades subsTrades)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.Substrades.Add(subsTrades);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetSubsTrades", new { id = subsTrades.Id }, subsTrades);
        }

        // DELETE: api/SubsTrades/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteSubsTrades([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var subsTrades = await _context.Substrades.FindAsync(id);
            if (subsTrades == null)
            {
                return NotFound();
            }

            _context.Substrades.Remove(subsTrades);
            await _context.SaveChangesAsync();

            return Ok(subsTrades);
        }

        private bool SubsTradesExists(int id)
        {
            return _context.Substrades.Any(e => e.Id == id);
        }
    }
}
