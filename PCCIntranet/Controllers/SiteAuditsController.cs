using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PCCIntranet.Models;

namespace PCCIntranet.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [DisableCors]
    public class SiteAuditsController : ControllerBase
    {
        private readonly PCCContext _context;

        public SiteAuditsController(PCCContext context)
        {
            _context = context;
        }

        // GET: api/SiteAudits
        [HttpGet]
        public IEnumerable<SiteAudit> GetSiteAudit()
        {
            return _context.SiteAudit;
        }

        // GET: api/SiteAudits/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetSiteAudit([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var siteAudit = await _context.SiteAudit.FindAsync(id);

            if (siteAudit == null)
            {
                return NotFound();
            }

            return Ok(siteAudit);
        }

        // PUT: api/SiteAudits/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutSiteAudit([FromRoute] int id, [FromBody] SiteAudit siteAudit)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != siteAudit.Id)
            {
                return BadRequest();
            }

            _context.Entry(siteAudit).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!SiteAuditExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/SiteAudits
        [HttpPost]
        public async Task<IActionResult> PostSiteAudit([FromBody] SiteAudit siteAudit)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.SiteAudit.Add(siteAudit);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetSiteAudit", new { id = siteAudit.Id }, siteAudit);
        }

        // DELETE: api/SiteAudits/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteSiteAudit([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var siteAudit = await _context.SiteAudit.FindAsync(id);
            if (siteAudit == null)
            {
                return NotFound();
            }

            _context.SiteAudit.Remove(siteAudit);
            await _context.SaveChangesAsync();

            return Ok(siteAudit);
        }

        private bool SiteAuditExists(int id)
        {
            return _context.SiteAudit.Any(e => e.Id == id);
        }
    }
}
