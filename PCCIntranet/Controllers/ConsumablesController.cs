using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PCCIntranet.Models;

namespace PCCIntranet.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ConsumablesController : ControllerBase
    {
        private readonly EquipmentContext _context;

        public ConsumablesController(EquipmentContext context)
        {
            _context = context;
        }

        // GET: api/Consumables
        [HttpGet]
        public IEnumerable<Consumables> GetConsumables()
        {
            return _context.Consumables;
        }

        // GET: api/Consumables/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetConsumables([FromRoute] Guid id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var consumables = await _context.Consumables.FindAsync(id);

            if (consumables == null)
            {
                return NotFound();
            }

            return Ok(consumables);
        }

        // PUT: api/Consumables/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutConsumables([FromRoute] Guid id, [FromBody] Consumables consumables)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != consumables.Id)
            {
                return BadRequest();
            }

            _context.Entry(consumables).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ConsumablesExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Consumables
        [HttpPost]
        public async Task<IActionResult> PostConsumables([FromBody] Consumables consumables)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.Consumables.Add(consumables);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetConsumables", new { id = consumables.Id }, consumables);
        }

        // DELETE: api/Consumables/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteConsumables([FromRoute] Guid id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var consumables = await _context.Consumables.FindAsync(id);
            if (consumables == null)
            {
                return NotFound();
            }

            _context.Consumables.Remove(consumables);
            await _context.SaveChangesAsync();

            return Ok(consumables);
        }

        private bool ConsumablesExists(Guid id)
        {
            return _context.Consumables.Any(e => e.Id == id);
        }
    }
}
