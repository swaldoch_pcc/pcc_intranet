﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PCCIntranet.Models;

namespace PCCIntranet.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TrnmtlsController : ControllerBase
    {
        private readonly SageContext _context;

        public TrnmtlsController(SageContext context)
        {
            _context = context;
        }

        // GET: api/Trnmtls
        [HttpGet]
        public IEnumerable<Trnmtl> GetTrnmtl()
        {
            return _context.Trnmtl;
        }

        // GET: api/Trnmtls/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetTrnmtl([FromRoute] Guid id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var trnmtl = await _context.Trnmtl.FindAsync(id);

            if (trnmtl == null)
            {
                return NotFound();
            }

            return Ok(trnmtl);
        }

        // PUT: api/Trnmtls/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutTrnmtl([FromRoute] Guid id, [FromBody] Trnmtl trnmtl)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != trnmtl.Idnum)
            {
                return BadRequest();
            }

            _context.Entry(trnmtl).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TrnmtlExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Trnmtls
        [HttpPost]
        public async Task<IActionResult> PostTrnmtl([FromBody] Trnmtl trnmtl)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.Trnmtl.Add(trnmtl);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (TrnmtlExists(trnmtl.Idnum))
                {
                    return new StatusCodeResult(StatusCodes.Status409Conflict);
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetTrnmtl", new { id = trnmtl.Idnum }, trnmtl);
        }

        // DELETE: api/Trnmtls/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteTrnmtl([FromRoute] Guid id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var trnmtl = await _context.Trnmtl.FindAsync(id);
            if (trnmtl == null)
            {
                return NotFound();
            }

            _context.Trnmtl.Remove(trnmtl);
            await _context.SaveChangesAsync();

            return Ok(trnmtl);
        }

        private bool TrnmtlExists(Guid id)
        {
            return _context.Trnmtl.Any(e => e.Idnum == id);
        }
    }
}