﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PCCIntranet.Models;

namespace PCCIntranet.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EmploysController : ControllerBase
    {
        private readonly SageContext _context;

        public EmploysController(SageContext context)
        {
            _context = context;
        }

        // GET: api/Employs
        [HttpGet]
        public IEnumerable<Employ> GetEmploy()
        {
            return _context.Employ;
        }

        // GET: api/Employs/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetEmploy([FromRoute] Guid id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var employ = await _context.Employ.FindAsync(id);

            if (employ == null)
            {
                return NotFound();
            }

            return Ok(employ);
        }

        // PUT: api/Employs/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutEmploy([FromRoute] Guid id, [FromBody] Employ employ)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != employ.Idnum)
            {
                return BadRequest();
            }

            _context.Entry(employ).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!EmployExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Employs
        [HttpPost]
        public async Task<IActionResult> PostEmploy([FromBody] Employ employ)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.Employ.Add(employ);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (EmployExists(employ.Idnum))
                {
                    return new StatusCodeResult(StatusCodes.Status409Conflict);
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetEmploy", new { id = employ.Idnum }, employ);
        }

        // DELETE: api/Employs/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteEmploy([FromRoute] Guid id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var employ = await _context.Employ.FindAsync(id);
            if (employ == null)
            {
                return NotFound();
            }

            _context.Employ.Remove(employ);
            await _context.SaveChangesAsync();

            return Ok(employ);
        }

        private bool EmployExists(Guid id)
        {
            return _context.Employ.Any(e => e.Idnum == id);
        }
    }
}