using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PCCIntranet.Models;

namespace PCCIntranet.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [DisableCors]
  public class SubsassistantsController : ControllerBase
    {
        private readonly PCCContext _context;

        public SubsassistantsController(PCCContext context)
        {
            _context = context;
        }

        // GET: api/Subsassistants
        [HttpGet]
        public IEnumerable<Subsassistant> GetSubsAssistant()
        {
            return _context.SubsAssistant;
        }

        // GET: api/Subsassistants/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetSubsassistant([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var subsassistant = await _context.SubsAssistant.FindAsync(id);

            if (subsassistant == null)
            {
                return NotFound();
            }

            return Ok(subsassistant);
        }

        // PUT: api/Subsassistants/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutSubsassistant([FromRoute] int id, [FromBody] Subsassistant subsassistant)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != subsassistant.Assistantid)
            {
                return BadRequest();
            }

            _context.Entry(subsassistant).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!SubsassistantExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Subsassistants
        [HttpPost]
        public async Task<IActionResult> PostSubsassistant([FromBody] Subsassistant subsassistant)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.SubsAssistant.Add(subsassistant);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (SubsassistantExists(subsassistant.Assistantid))
                {
                    return new StatusCodeResult(StatusCodes.Status409Conflict);
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetSubsassistant", new { id = subsassistant.Assistantid }, subsassistant);
        }

        // DELETE: api/Subsassistants/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteSubsassistant([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var subsassistant = await _context.SubsAssistant.FindAsync(id);
            if (subsassistant == null)
            {
                return NotFound();
            }

            _context.SubsAssistant.Remove(subsassistant);
            await _context.SaveChangesAsync();

            return Ok(subsassistant);
        }

        private bool SubsassistantExists(int id)
        {
            return _context.SubsAssistant.Any(e => e.Assistantid == id);
        }
    }
}
