﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PCCIntranet.Models;

namespace PCCIntranet.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CategorypricingsController : ControllerBase
    {
        private readonly EquipmentContext _context;

        public CategorypricingsController(EquipmentContext context)
        {
            _context = context;
        }

        // GET: api/Categorypricings
        [HttpGet]
        public IEnumerable<Categorypricing> GetCategorypricing()
        {
            return _context.Categorypricing;
        }

        // GET: api/Categorypricings/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetCategorypricing([FromRoute] Guid id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var categorypricing = await _context.Categorypricing.FindAsync(id);

            if (categorypricing == null)
            {
                return NotFound();
            }

            return Ok(categorypricing);
        }

        // PUT: api/Categorypricings/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCategorypricing([FromRoute] Guid id, [FromBody] Categorypricing categorypricing)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != categorypricing.PriceId)
            {
                return BadRequest();
            }

            _context.Entry(categorypricing).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CategorypricingExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Categorypricings
        [HttpPost]
        public async Task<IActionResult> PostCategorypricing([FromBody] Categorypricing categorypricing)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.Categorypricing.Add(categorypricing);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (CategorypricingExists(categorypricing.PriceId))
                {
                    return new StatusCodeResult(StatusCodes.Status409Conflict);
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetCategorypricing", new { id = categorypricing.PriceId }, categorypricing);
        }

        // DELETE: api/Categorypricings/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteCategorypricing([FromRoute] Guid id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var categorypricing = await _context.Categorypricing.FindAsync(id);
            if (categorypricing == null)
            {
                return NotFound();
            }

            _context.Categorypricing.Remove(categorypricing);
            await _context.SaveChangesAsync();

            return Ok(categorypricing);
        }

        private bool CategorypricingExists(Guid id)
        {
            return _context.Categorypricing.Any(e => e.PriceId == id);
        }
    }
}