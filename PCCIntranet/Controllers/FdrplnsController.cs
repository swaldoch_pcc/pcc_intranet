﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PCCIntranet.Models;

namespace PCCIntranet.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FdrplnsController : ControllerBase
    {
        private readonly SageContext _context;

        public FdrplnsController(SageContext context)
        {
            _context = context;
        }

        // GET: api/Fdrplns
        [HttpGet]
        public IEnumerable<Fdrpln> GetFdrpln()
        {
            return _context.Fdrpln;
        }

        // GET: api/Fdrplns/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetFdrpln([FromRoute] Guid id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var fdrpln = await _context.Fdrpln.FindAsync(id);

            if (fdrpln == null)
            {
                return NotFound();
            }

            return Ok(fdrpln);
        }

        // PUT: api/Fdrplns/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutFdrpln([FromRoute] Guid id, [FromBody] Fdrpln fdrpln)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != fdrpln.Idnum)
            {
                return BadRequest();
            }

            _context.Entry(fdrpln).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!FdrplnExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Fdrplns
        [HttpPost]
        public async Task<IActionResult> PostFdrpln([FromBody] Fdrpln fdrpln)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.Fdrpln.Add(fdrpln);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (FdrplnExists(fdrpln.Idnum))
                {
                    return new StatusCodeResult(StatusCodes.Status409Conflict);
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetFdrpln", new { id = fdrpln.Idnum }, fdrpln);
        }

        // DELETE: api/Fdrplns/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteFdrpln([FromRoute] Guid id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var fdrpln = await _context.Fdrpln.FindAsync(id);
            if (fdrpln == null)
            {
                return NotFound();
            }

            _context.Fdrpln.Remove(fdrpln);
            await _context.SaveChangesAsync();

            return Ok(fdrpln);
        }

        private bool FdrplnExists(Guid id)
        {
            return _context.Fdrpln.Any(e => e.Idnum == id);
        }
    }
}