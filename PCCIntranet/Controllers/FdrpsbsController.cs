﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PCCIntranet.Models;

namespace PCCIntranet.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FdrpsbsController : ControllerBase
    {
        private readonly SageContext _context;

        public FdrpsbsController(SageContext context)
        {
            _context = context;
        }

        // GET: api/Fdrpsbs
        [HttpGet]
        public IEnumerable<Fdrpsb> GetFdrpsb()
        {
            return _context.Fdrpsb;
        }

        // GET: api/Fdrpsbs/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetFdrpsb([FromRoute] Guid id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var fdrpsb = await _context.Fdrpsb.FindAsync(id);

            if (fdrpsb == null)
            {
                return NotFound();
            }

            return Ok(fdrpsb);
        }

        // PUT: api/Fdrpsbs/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutFdrpsb([FromRoute] Guid id, [FromBody] Fdrpsb fdrpsb)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != fdrpsb.Idnum)
            {
                return BadRequest();
            }

            _context.Entry(fdrpsb).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!FdrpsbExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Fdrpsbs
        [HttpPost]
        public async Task<IActionResult> PostFdrpsb([FromBody] Fdrpsb fdrpsb)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.Fdrpsb.Add(fdrpsb);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (FdrpsbExists(fdrpsb.Idnum))
                {
                    return new StatusCodeResult(StatusCodes.Status409Conflict);
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetFdrpsb", new { id = fdrpsb.Idnum }, fdrpsb);
        }

        // DELETE: api/Fdrpsbs/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteFdrpsb([FromRoute] Guid id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var fdrpsb = await _context.Fdrpsb.FindAsync(id);
            if (fdrpsb == null)
            {
                return NotFound();
            }

            _context.Fdrpsb.Remove(fdrpsb);
            await _context.SaveChangesAsync();

            return Ok(fdrpsb);
        }

        private bool FdrpsbExists(Guid id)
        {
            return _context.Fdrpsb.Any(e => e.Idnum == id);
        }
    }
}