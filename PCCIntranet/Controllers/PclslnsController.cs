﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PCCIntranet.Models;

namespace PCCIntranet.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PclslnsController : ControllerBase
    {
        private readonly SageContext _context;

        public PclslnsController(SageContext context)
        {
            _context = context;
        }

        // GET: api/Pclslns
        [HttpGet]
        public IEnumerable<Pclsln> GetPclsln()
        {
            return _context.Pclsln;
        }

        // GET: api/Pclslns/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetPclsln([FromRoute] Guid id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var pclsln = await _context.Pclsln.FindAsync(id);

            if (pclsln == null)
            {
                return NotFound();
            }

            return Ok(pclsln);
        }

        // PUT: api/Pclslns/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutPclsln([FromRoute] Guid id, [FromBody] Pclsln pclsln)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != pclsln.Idnum)
            {
                return BadRequest();
            }

            _context.Entry(pclsln).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PclslnExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Pclslns
        [HttpPost]
        public async Task<IActionResult> PostPclsln([FromBody] Pclsln pclsln)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.Pclsln.Add(pclsln);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (PclslnExists(pclsln.Idnum))
                {
                    return new StatusCodeResult(StatusCodes.Status409Conflict);
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetPclsln", new { id = pclsln.Idnum }, pclsln);
        }

        // DELETE: api/Pclslns/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeletePclsln([FromRoute] Guid id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var pclsln = await _context.Pclsln.FindAsync(id);
            if (pclsln == null)
            {
                return NotFound();
            }

            _context.Pclsln.Remove(pclsln);
            await _context.SaveChangesAsync();

            return Ok(pclsln);
        }

        private bool PclslnExists(Guid id)
        {
            return _context.Pclsln.Any(e => e.Idnum == id);
        }
    }
}