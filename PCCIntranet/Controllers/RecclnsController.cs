﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PCCIntranet.Models;

namespace PCCIntranet.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RecclnsController : ControllerBase
    {
        private readonly SageContext _context;

        public RecclnsController(SageContext context)
        {
            _context = context;
        }

        // GET: api/Recclns
        [HttpGet]
        public IEnumerable<Reccln> GetReccln()
        {
            return _context.Reccln;
        }

        // GET: api/Recclns/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetReccln([FromRoute] Guid id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var reccln = await _context.Reccln.FindAsync(id);

            if (reccln == null)
            {
                return NotFound();
            }

            return Ok(reccln);
        }

        // PUT: api/Recclns/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutReccln([FromRoute] Guid id, [FromBody] Reccln reccln)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != reccln.Idnum)
            {
                return BadRequest();
            }

            _context.Entry(reccln).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!RecclnExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Recclns
        [HttpPost]
        public async Task<IActionResult> PostReccln([FromBody] Reccln reccln)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.Reccln.Add(reccln);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (RecclnExists(reccln.Idnum))
                {
                    return new StatusCodeResult(StatusCodes.Status409Conflict);
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetReccln", new { id = reccln.Idnum }, reccln);
        }

        // DELETE: api/Recclns/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteReccln([FromRoute] Guid id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var reccln = await _context.Reccln.FindAsync(id);
            if (reccln == null)
            {
                return NotFound();
            }

            _context.Reccln.Remove(reccln);
            await _context.SaveChangesAsync();

            return Ok(reccln);
        }

        private bool RecclnExists(Guid id)
        {
            return _context.Reccln.Any(e => e.Idnum == id);
        }
    }
}