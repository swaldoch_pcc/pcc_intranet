using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using System.IdentityModel.Tokens.Jwt;
using PCCIntranet.Helpers;
using Microsoft.Extensions.Options;
using System.Text;
using Microsoft.IdentityModel.Tokens;
using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;
using PCCIntranet.Services;
using PCCIntranet.Dtos;
using PCCIntranet.Models;

namespace PCCIntranet.Controllers
{
    
    [Route("[controller]")]
    [DisableCors]
    [ApiController]
    public class UsersController : ControllerBase
    {
        //private readonly PCCContext _context;
        private IUserService _userService;
        private IMapper _mapper;
        private readonly AppSettings _appSettings;

        public UsersController
        (
            //PCCContext context,
            IUserService userService,
            IMapper mapper,
            IOptions<AppSettings> appSettings)
        {
            //_context = context;
            _userService = userService;
            _mapper = mapper;
            _appSettings = appSettings.Value;
        }

        // GET: api/Users
        [HttpGet]
        public IActionResult GetAll()
        {
          var users = _userService.GetAll();
          var userDtos = _mapper.Map<IList<UserDto>>(users);
          return Ok(userDtos);
        }


        // GET: api/Users/5
        [HttpGet("{id}")]
        public IActionResult GetById(int id)
        {
          var user = _userService.GetById(id);
          var userDto = _mapper.Map<UserDto>(user);
          return Ok(userDto);
        }

    // PUT: api/Users/5

        [HttpPut("{id}")]
        public IActionResult Update(int id, [FromBody]UserDto userDto)
        {
          // map dto to entity and set id
          var user = _mapper.Map<Users>(userDto);
          user.Id = id;

          try
          {
            // save 
            _userService.Update(user, userDto.Password);
            return Ok();
          }
          catch (AppException ex)
          {
            // return error message if there was an exception
            return BadRequest(new { message = ex.Message });
          }
        }

        // POST: api/Users Authenticate

        [AllowAnonymous]
        [HttpPost("Authenticate")]
        public IActionResult Authenticate([FromBody]UserDto userDto)
        {
          var user = _userService.Authenticate(userDto.Username, userDto.Password);

          if (user == null)
            return BadRequest(new { message = "Username or password is incorrect" });

          var tokenHandler = new JwtSecurityTokenHandler();
          var key = Encoding.ASCII.GetBytes(_appSettings.Secret);
          var tokenDescriptor = new SecurityTokenDescriptor
          {
            Subject = new ClaimsIdentity(new Claim[]
              {
                        new Claim(ClaimTypes.Name, user.Id.ToString())
              }),
            Expires = DateTime.UtcNow.AddDays(7),
            SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
          };
          var token = tokenHandler.CreateToken(tokenDescriptor);
          var tokenString = tokenHandler.WriteToken(token);

          // return basic user info (without password) and token to store client side
          return Ok(new
          {
            Id = user.Id,
            Username = user.Username,
            FirstName = user.FirstName,
            LastName = user.LastName,
            Token = tokenString,
            Role = user.Role
          });
        }

    // POST: api/Users Register - Should be admin only!!!
        [AllowAnonymous]
        [HttpPost("register")]
        public IActionResult Register([FromBody]UserDto userDto)
        {
          // map dto to entity
          var user = _mapper.Map<Users>(userDto);

          try
          {
            // save 
            _userService.Create(user, userDto.Password);
            return Ok();
          }
          catch (AppException ex)
          {
            // return error message if there was an exception
            return BadRequest(new { message = ex.Message });
          }
        }

        // DELETE: api/Users/5
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
          _userService.Delete(id);
          return Ok();
        }


    }
}
