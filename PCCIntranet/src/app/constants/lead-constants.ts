export const PHASE = [
  '2 - Qualification',
  '3 - Budgeting',
  '4 - Bidding',
  '5 - Pending Award',
  '6 - Sold'
];

export const BIDSTATUS = [
  'Open',
  'Lost',
  'On hold',
  'No Opportunity'
]
