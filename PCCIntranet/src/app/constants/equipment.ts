import { menu } from '../models/equipment';

export const MENUS: menu[] = [
  { id: 1, category: 'Tool', item: 'Jobsite report' },
  { id: 2, category: 'Tool', item: 'Employee report' },
  { id: 3, category: 'tool', item: 'equipment report' },
  { id: 4, category: 'Vehicle/trailer', item: 'Jobsite report' },
  { id: 5, category: 'Vehicle/trailer', item: 'Employee report' },
  { id: 6, category: 'Safety equipment', item: 'Jobsite report' },
  { id: 7, category: 'Safety equipment', item: 'Employee report' },
  { id: 8, category: 'Safety equipment', item: 'Equipment report' },
  { id: 9, category: 'Consumable', item: 'Jobsite report' },
  { id: 10, category: 'Consumable', item: 'Employee report' },
  { id: 11, category: 'Consumable', item: 'Equipment report' }
];

export const ISSUEMENUS: menu[] = [
  { id: 1, category: 'Tool', item: 'Issue tool to job' },
  { id: 2, category: 'Tool', item: 'Issue tool to individual' },
  //{ id: 3, category: 'Tool', item: 'Issue tool to equipment' },
  { id: 4, category: 'Vehicle/trailer', item: 'Issue vehicle to job' },
  { id: 5, category: 'Vehicle/trailer', item: 'Issue vehicle to individual' },
  { id: 6, category: 'Safety equipment', item: 'Issue safety equipment to job' },
  { id: 7, category: 'Safety equipment', item: 'Issue safety equipment to individual' },
  //{ id: 8, category: 'Safety equipment', item: 'Issue safety equipment to equipment' },
  { id: 9, category: 'Consumable', item: 'Issue consumable to job' },
  { id: 10, category: 'Consumable', item: 'Issue consumable to individual' },
  //{ id: 11, category: 'Consumable', item: 'Issue consumable to equipment' }
]
