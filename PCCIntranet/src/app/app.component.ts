import { Component, OnInit, OnDestroy } from '@angular/core';
import { NavigationStart, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { AuthenticationService, AlertService } from './_services';



export let browserRefresh = false;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy {
  title = 'PCC Intranet';
  subscription: Subscription;
    username: string;

  constructor(private router: Router, private authenticationService: AuthenticationService) {
    authenticationService.getLoggedInName.subscribe(name => this.changeName(name));
  }

  private changeName(name: string): void {
    this.username = name;
  }

  ngOnInit() {  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  
  currentUser = JSON.parse(localStorage.getItem('currentUser'));
  
  public _opened: boolean = false;

  public _toggleSidebar() {
    this._opened = !this._opened;
  }


  public logout(): void {
    this.authenticationService.logout();
    
    this.router.navigate(['/login']);
  }
}
