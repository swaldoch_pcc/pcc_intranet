import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { FormGroup, FormControl, ReactiveFormsModule } from '@angular/forms';
import { Router } from '@angular/router';
import { Subs } from '../models/subs';
import { Trades } from '../models/trades';

@Component({
  selector: 'app-subs',
  templateUrl: './subs.component.html'
})
export class SubsComponent implements OnInit {

  subs: any;
  trades: any;
  

  constructor(private http: HttpClient, private router: Router) { }

  ngOnInit() {
    const url = 'https://intranet.pc-const.com/api/subs';
    const tradeurl = 'https://intranet.pc-const.com/api/substrades';

    this.http.get(url)
      .subscribe((data: Subs[]) => {
        this.subs = data;
        console.log(this.subs);
      });
    this.http.get(tradeurl)
      .subscribe((tradedata: Trades[]) => {
        this.trades = Array.from(new Set(tradedata));
        console.log(tradedata);
      });
  }

}

