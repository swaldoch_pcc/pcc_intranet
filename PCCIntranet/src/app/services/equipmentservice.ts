import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Tool, Condition, Description, Emp, EquipmentRental, Categorypricing, Job, Mfg, Modelnum, Pcnum, Recall, Serialnum, Service, Vehiclehistory, Vehicle, Vehicleretired, TruckRental, Consumable, ConsumableIssue } from '../models/equipmentmodels';
import { Observable, from, of } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})

export class EquipmentService {
  toolsurl: string = 'https://intranet.pc-const.com/api/Tools';
  conditionurl: string = 'https://intranet.pc-const.com/api/Conditions';
  descriptionurl: string = 'https://intranet.pc-const.com/api/Descriptions';
  employeeurl: string = 'https://intranet.pc-const.com/api/Employees1';
  equipmentrentalurl: string = 'https://intranet.pc-const.com/api/EquipmentRentals';
  joburl: string = 'https://intranet.pc-const.com/api/Jobs';
  mfgurl: string = 'https://intranet.pc-const.com/api/MFGs';
  modelnumurl: string = 'https://intranet.pc-const.com/api/Modelnums';
  pcnumurl: string = 'https://intranet.pc-const.com/api/Pcnums';
  recallurl: string = 'https://intranet.pc-const.com/api/Recalls';
  serialnumurl: string = 'https://intranet.pc-const.com/api/Serialnums';
  serviceurl: string = 'https://intranet.pc-const.com/api/Services';
  vehiclehistoryurl: string = 'https://intranet.pc-const.com/api/Vehiclehistories';
  vehiclesurl: string = 'https://intranet.pc-const.com/api/Vehicles';
  vehiclesretiredurl: string = 'https://intranet.pc-const.com/api/vehiclesretireds';
  pricingurl: string = 'https://intranet.pc-const.com/api/categorypricings';
  truckrentalurl: string = 'https://intranet.pc-const.com/api/truckrentals';
  consumablesurl: string = 'https://intranet.pc-const.com/api/consumables';
  consumablesissuedsurl: string = 'https://intranet.pc-const.com/api/consumablesissueds';

  constructor(private http: HttpClient) { }

  getTools(): Observable<Tool[]> {
    return this.http.get<Tool[]>(this.toolsurl)
  }

  getCondition(): Observable<Condition[]> {
    return this.http.get<Condition[]>(this.conditionurl)
  }

  getDescription(): Observable<Description[]> {
    return this.http.get<Description[]>(this.descriptionurl)
  }

  getEquipmentrental(): Observable<EquipmentRental[]> {
    return this.http.get<EquipmentRental[]>(this.equipmentrentalurl)
  }

  getMfg(): Observable<Mfg[]> {
    return this.http.get<Mfg[]>(this.mfgurl)
  }

  getModelnum(): Observable<Modelnum[]> {
    return this.http.get<Modelnum[]>(this.modelnumurl)
  }

  getPcnum(): Observable<Pcnum[]> {
    return this.http.get<Pcnum[]>(this.pcnumurl)
  }

  getRecall(): Observable<Recall[]> {
    return this.http.get<Recall[]>(this.recallurl)
  }

  getSerialnum(): Observable<Serialnum[]> {
    return this.http.get<Serialnum[]>(this.serialnumurl)
  }

  getService(): Observable<Service[]> {
    return this.http.get<Service[]>(this.serviceurl)
  }

  getVehiclehistory(): Observable<Vehiclehistory[]> {
    return this.http.get<Vehiclehistory[]>(this.vehiclehistoryurl)
  }

  getVehicles(): Observable<Vehicle[]> {
    return this.http.get<Vehicle[]>(this.vehiclesurl)
  }

  getVehiclesretired(): Observable<Vehicleretired[]> {
    return this.http.get<Vehicleretired[]>(this.vehiclesretiredurl)
  }

  getPricing(): Observable<Categorypricing[]> {
    return this.http.get<Categorypricing[]>(this.pricingurl)
  }

  getTruckRentals(): Observable<TruckRental[]> {
    return this.http.get<TruckRental[]>(this.truckrentalurl)
  }

  getConsumables(): Observable<Consumable[]> {
    return this.http.get<Consumable[]>(this.consumablesurl)
  }

  getConsumablesissued(): Observable<ConsumableIssue[]> {
    return this.http.get<ConsumableIssue[]>(this.consumablesissuedsurl)
  }

}
