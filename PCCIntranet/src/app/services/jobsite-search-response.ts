import { Jobsite } from '../models/jobsites';

export interface JobsiteSearchResponse {
  items: Jobsite[];
  total_count: number;
}
