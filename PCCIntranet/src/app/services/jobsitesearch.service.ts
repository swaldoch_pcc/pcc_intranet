import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Jobsite } from '../models/jobsites';
import { Observable, from, of } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})

export class JobsitesearchService {
  webApiUri: string = 'https://intranet.pc-const.com/api/Jobsites';
  

  constructor(private http: HttpClient) { }

  getJobsites() : Observable<Jobsite[]> {
    return this.http.get<Jobsite[]>(this.webApiUri)
  }

}
