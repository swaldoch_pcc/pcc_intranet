import { TestBed } from '@angular/core/testing';

import { JobsitesearchService } from './jobsitesearch.service';

describe('JobsitesearchService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: JobsitesearchService = TestBed.get(JobsitesearchService);
    expect(service).toBeTruthy();
  });
});
