import { TestBed } from '@angular/core/testing';

import { ProjectreviewService } from './projectreview.service';

describe('ProjectreviewService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ProjectreviewService = TestBed.get(ProjectreviewService);
    expect(service).toBeTruthy();
  });
});
