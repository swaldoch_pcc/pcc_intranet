import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ProcoreService {
  private apiBaseUrl = 'https://intranet.pc-const.com/api'; 

  constructor(private http: HttpClient) { }

  listProjects() {
    return this.http.get<any>(`${this.apiBaseUrl}/list-projects`);
  }
}
