import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DatePipe } from '@angular/common';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { SignaturePadModule } from 'angular2-signaturepad';
import { BrowserModule } from '@angular/platform-browser';
import { SidebarModule } from 'ng-sidebar';
import { JobsitesearchService } from '../app/services/jobsitesearch.service';
import { ProjectreviewService } from '../app/services/projectreview.service';
import { AppRoutingModule } from './app-routing.module';
import { LocationStrategy, HashLocationStrategy } from '@angular/common';
import { OrderModule } from 'ngx-order-pipe';
import { NgbDateStruct, NgbDateAdapter, NgbDateNativeAdapter, NgbDateParserFormatter, NgbCalendar } from '@ng-bootstrap/ng-bootstrap';
import { Ng2SearchPipeModule } from 'ng2-search-filter';


import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { HRComponent } from './HR/hr.component';
import { EquipmentauditComponent } from './Projects/Audits/equipmentaudit/equipmentaudit.component';
import { PmfeedbackComponent } from './Projects/Audits/pmfeedback/pmfeedback.component';
import { ProjectreviewComponent } from './Projects/Audits/projectreview/projectreview.component';
import { SafetyauditComponent } from './Projects/Audits/safetyaudit/safetyaudit.component';
import { SiteauditComponent } from './Projects/Audits/siteaudit/siteaudit.component';
import { ExecreportsComponent } from './Reports/execreports/execreports.component';
import { FieldreportsComponent } from './Reports/fieldreports/fieldreports.component';
import { PmreportsComponent } from './Reports/pmreports/pmreports.component';
import { SubsComponent } from './subs/subs.component';
import { SubsDetailComponent } from './subs/subsdetail.component';
import { SafetyComponent } from './HR/safety/safety.component';

import { AdminComponent } from './Admin/admin.component';
import { LoginComponent } from './Admin/login/login.component';
import { RegisterComponent } from './Admin/register/register.component';
import { ChangePasswordComponent } from './Admin/change-password/change-password.component';


import { AlertComponent } from './_directives';
import { AuthGuard } from './_guards';
import { JwtInterceptor, ErrorInterceptor } from './_helpers';
import { AlertService, AuthenticationService, UserService } from './_services';
import { CompletedsiteauditComponent } from './Reports/reports/completedsiteaudit/completedsiteaudit.component';
import { CompletedsafetyauditComponent } from './Reports/reports/completedsafetyaudit/completedsafetyaudit.component';
import { CompletedpmprojectreviewComponent } from './Reports/reports/completedpmprojectreview/completedpmprojectreview.component';
import { CompletedsuptprojectreviewComponent } from './Reports/reports/completedsuptprojectreview/completedsuptprojectreview.component';
import { LaborComponent } from './Projects/labor/labor.component';
import { LaborGanttComponent } from './Projects/labor-gantt/labor-gantt.component';
import { ProjectGanttComponent } from './Projects/project-gantt/project-gantt.component';
import { JobsSummaryComponent } from './Projects/jobs-summary/jobs-summary.component';
import { BidsComponent } from './Projects/bids/bids.component';
import { FeedbackComponent } from './HR/feedback/feedback.component';
import { CalendarComponent } from './Tools/calendar/calendar.component';
import { ToolrentalComponent } from './Equipment/toolrental/toolrental.component';
import { RegisterEquipmentComponent } from './Equipment/register-equipment/register-equipment.component';
import { IssueEquipmentComponent } from './Equipment/issue-equipment/issue-equipment.component';
import { EditEquipmentComponent } from './Equipment/edit-equipment/edit-equipment.component';
import { CloseoutsComponent } from './Projects/closeouts/closeouts.component';
import { SafetyactionitemsComponent } from './Projects/safetyactionitems/safetyactionitems.component';
import { ServiceComponent } from './Equipment/service/service.component';
import { CheckInEquipmentComponent } from './Equipment/check-in-equipment/check-in-equipment.component';
import { SubSolicitationComponent } from './Projects/sub-solicitation/sub-solicitation.component';




@NgModule({
  declarations: [
    AppComponent,
    SubsComponent,
    SubsDetailComponent,
    ProjectreviewComponent,
    SafetyauditComponent,
    PmfeedbackComponent,
    SiteauditComponent,
    EquipmentauditComponent,
    HomeComponent,
    ExecreportsComponent,
    PmreportsComponent,
    FieldreportsComponent,
    HRComponent,
    ChangePasswordComponent,
    LoginComponent,
    AlertComponent,
    SafetyComponent,
    RegisterComponent,
    AdminComponent,
    CompletedsiteauditComponent,
    CompletedsafetyauditComponent,
    CompletedpmprojectreviewComponent,
    CompletedsuptprojectreviewComponent,
    LaborComponent,
    LaborGanttComponent,
    ProjectGanttComponent,
    JobsSummaryComponent,
    BidsComponent,
    FeedbackComponent,
    CalendarComponent,
    ToolrentalComponent,
    RegisterEquipmentComponent,
    IssueEquipmentComponent,
    EditEquipmentComponent,
    CloseoutsComponent,
    SafetyactionitemsComponent,
    ServiceComponent,
    CheckInEquipmentComponent,
    SubSolicitationComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    SignaturePadModule,
    SidebarModule.forRoot(),
    HttpClientModule,
    ReactiveFormsModule,
    OrderModule,
    NgbModule,
    Ng2SearchPipeModule
  ],
 
  providers: [
    DatePipe,
    ProjectreviewService,
    JobsitesearchService,
    AuthGuard,
    AlertService,
    AuthenticationService,
    UserService,
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
    { provide: LocationStrategy, useClass: HashLocationStrategy }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }


