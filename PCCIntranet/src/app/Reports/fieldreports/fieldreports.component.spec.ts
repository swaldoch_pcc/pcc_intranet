import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FieldreportsComponent } from './fieldreports.component';

describe('FieldreportsComponent', () => {
  let component: FieldreportsComponent;
  let fixture: ComponentFixture<FieldreportsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FieldreportsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FieldreportsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
