import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PmreportsComponent } from './pmreports.component';

describe('PmreportsComponent', () => {
  let component: PmreportsComponent;
  let fixture: ComponentFixture<PmreportsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PmreportsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PmreportsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
