import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExecreportsComponent } from './execreports.component';

describe('ExecreportsComponent', () => {
  let component: ExecreportsComponent;
  let fixture: ComponentFixture<ExecreportsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExecreportsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExecreportsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
