import { Component, OnInit, NgModule, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { Jobsite } from '../../models/jobsites';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { FormGroup, FormControl, ReactiveFormsModule } from '@angular/forms';
import { Projectreview } from '../../models/projectreview';
import { Safetyaudit } from '../../models/safetyaudit';
import { Siteaudit } from 'src/app/models/siteaudit';
import { Pmprojectreviews } from 'src/app/models/pmprojectreviews';

@Component({
  selector: 'app-execreports',
  templateUrl: './execreports.component.html',
  styleUrls: ['./execreports.component.scss']
})
export class ExecreportsComponent implements OnInit {

  projects: any;
  safetyaudits: any;
  siteaudits: any;
  pmprojectreviews: any;

  constructor(private http: HttpClient, private router: Router) {

  }
  today = new Date();
  

  selectedProject: Projectreview;
  onSelect(project: Projectreview): void {
    this.reset();
    this.selectedProject = project;
    window.scrollTo(0, 0);
  }

  completedSafetyaudit: Projectreview;
  safetyauditSelect(safetyaudit: Projectreview): void {
    this.reset();
    this.completedSafetyaudit = safetyaudit;
    window.scrollTo(0, 0);
  }

  completedSafetyaudit1: Projectreview;
  safetyauditSelect1(safetyaudit: Projectreview): void {
    this.reset();
    this.completedSafetyaudit1 = safetyaudit;
    window.scrollTo(0, 0);
  }

  completedSiteaudit: Projectreview;
  siteauditSelect(siteaudit: Projectreview): void {
    this.reset();
    this.completedSiteaudit = siteaudit;
    window.scrollTo(0, 0);
  }

  completedSiteaudit1: Projectreview;
  siteauditSelect1(siteaudit: Projectreview): void {
    this.reset();
    this.completedSiteaudit1 = siteaudit;
    window.scrollTo(0, 0);
  }

  completedReview: Projectreview;
  pmprojectreviewSelect(pmprojectreview: Projectreview): void {
    this.reset();
    this.completedReview = pmprojectreview;
    window.scrollTo(0, 0);
  }

  completedReview1: Projectreview;
  pmprojectreviewSelect1(pmprojectreview: Projectreview): void {
    this.reset();
    this.completedReview1 = pmprojectreview;
    window.scrollTo(0, 0);
  }

  currentUser = JSON.parse(localStorage.getItem('currentUser'));


  ngOnInit(): void {
    const url = 'https://intranet.pc-const.com/api/Jobsites';
    const safetyauditurl = 'https://intranet.pc-const.com/api/safetyaudits';
    const siteauditurl = 'https://intranet.pc-const.com/api/siteaudits';
    const reviewurl = 'https://intranet.pc-const.com/api/Pmprojectreviews';

    this.http.get(url)
      .subscribe((data: Jobsite[]) => {
        this.projects = data;
      });

    this.http.get(safetyauditurl)
      .subscribe((data: Safetyaudit[]) => {
        this.safetyaudits = data;
      });

    this.http.get(siteauditurl)
      .subscribe((data: Siteaudit[]) => {
        this.siteaudits = data;
      });

    this.http.get(reviewurl)
      .subscribe((data: Pmprojectreviews[]) => {
        this.pmprojectreviews = data;
      });

  }

  public reset(): void {

    this.selectedProject = undefined;
    this.completedSafetyaudit = undefined;
    this.completedSafetyaudit1 = undefined;
    this.completedSiteaudit = undefined;
    this.completedSiteaudit1 = undefined;
    this.completedReview = undefined;
    this.completedReview1 = undefined;
    this.router.navigate(['/execreports']);
  }

}
