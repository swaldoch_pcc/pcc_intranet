import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CompletedsafetyauditComponent } from './completedsafetyaudit.component';

describe('CompletedsafetyauditComponent', () => {
  let component: CompletedsafetyauditComponent;
  let fixture: ComponentFixture<CompletedsafetyauditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CompletedsafetyauditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CompletedsafetyauditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
