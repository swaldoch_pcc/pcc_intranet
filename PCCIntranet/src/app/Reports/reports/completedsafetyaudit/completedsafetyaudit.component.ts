import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-completedsafetyaudit',
  templateUrl: './completedsafetyaudit.component.html',
  styleUrls: ['./completedsafetyaudit.component.scss']
})
export class CompletedsafetyauditComponent implements OnInit {

  @Input() completedAudit: any[];

  constructor() { }

  ngOnInit() {
  }

}
