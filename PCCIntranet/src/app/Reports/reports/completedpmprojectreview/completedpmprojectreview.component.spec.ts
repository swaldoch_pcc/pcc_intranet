import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CompletedpmprojectreviewComponent } from './completedpmprojectreview.component';

describe('CompletedpmprojectreviewComponent', () => {
  let component: CompletedpmprojectreviewComponent;
  let fixture: ComponentFixture<CompletedpmprojectreviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CompletedpmprojectreviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CompletedpmprojectreviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
