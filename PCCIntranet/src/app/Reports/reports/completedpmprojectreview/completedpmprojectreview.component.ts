import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-completedpmprojectreview',
  templateUrl: './completedpmprojectreview.component.html',
  styleUrls: ['./completedpmprojectreview.component.scss']
})
export class CompletedpmprojectreviewComponent implements OnInit {

  @Input() completedReview: any[];

  constructor() { }

  ngOnInit() {
  }

}
