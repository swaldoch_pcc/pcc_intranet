import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CompletedsiteauditComponent } from './completedsiteaudit.component';

describe('CompletedsiteauditComponent', () => {
  let component: CompletedsiteauditComponent;
  let fixture: ComponentFixture<CompletedsiteauditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CompletedsiteauditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CompletedsiteauditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
