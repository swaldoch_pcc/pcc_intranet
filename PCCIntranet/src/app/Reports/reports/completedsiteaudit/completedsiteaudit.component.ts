import { Component, OnInit, Input } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Siteaudit } from '../../../models/siteaudit';
import { Projectreview } from '../../../models/projectreview';

@Component({
  selector: 'app-completedsiteaudit',
  templateUrl: './completedsiteaudit.component.html',
  styleUrls: ['./completedsiteaudit.component.scss']
})
export class CompletedsiteauditComponent implements OnInit {

  @Input() completedAudit: any[];

  constructor() {

  }

  ngOnInit() {
  }
}
