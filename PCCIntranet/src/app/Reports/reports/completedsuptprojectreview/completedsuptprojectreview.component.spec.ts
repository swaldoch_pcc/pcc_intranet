import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CompletedsuptprojectreviewComponent } from './completedsuptprojectreview.component';

describe('CompletedsuptprojectreviewComponent', () => {
  let component: CompletedsuptprojectreviewComponent;
  let fixture: ComponentFixture<CompletedsuptprojectreviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CompletedsuptprojectreviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CompletedsuptprojectreviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
