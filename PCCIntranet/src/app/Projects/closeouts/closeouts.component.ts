import { Component, OnInit, Injectable } from '@angular/core';
import { OrderPipe } from 'ngx-order-pipe';
import { Router } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Jobsite } from '../../models/jobsites';
import { Employee } from '../../models/employees';
import { Subcontract } from '../../models/subcontracts';
import { NgbDateStruct, NgbDateAdapter, NgbDateParserFormatter, NgbCalendar } from '@ng-bootstrap/ng-bootstrap';
import { catchError, tap, map } from 'rxjs/operators';
import { GROUPS } from '../../constants/project-constants';
import { Safetyaudit } from '../../models/safetyaudit';
import { Siteaudit } from '../../models/siteaudit';
import { JobsitesearchService } from '../../services/jobsitesearch.service';
import { Projectreview } from '../../models/projectreview';

@Injectable()
export class CustomAdapter extends NgbDateAdapter<string> {
  readonly DELIMITER = '-';

  fromModel(value: string): NgbDateStruct {
    let result: NgbDateStruct = null;
    if (value) {
      let date = value.split(this.DELIMITER);
      result = {
        day: parseInt(date[2], 10),
        month: parseInt(date[1], 10),
        year: parseInt(date[0], 10)
      };
    }
    return result;
  }

  toModel(date: NgbDateStruct): string {
    let result: string = null;
    if (date) {
      result = date.year + this.DELIMITER + date.month + this.DELIMITER + date.day;
    }
    return result;
  }
}

@Injectable()
export class CustomDateParserFormatter extends NgbDateParserFormatter {

  readonly DELIMITER = '-';

  parse(value: string): NgbDateStruct {
    let result: NgbDateStruct = null;
    if (value) {
      let date = value.split(this.DELIMITER);
      result = {
        day: parseInt(date[2], 10),
        month: parseInt(date[1], 10),
        year: parseInt(date[0], 10)
      };
    }
    return result;
  }

  format(date: NgbDateStruct): string {
    let result: string = null;
    if (date) {
      result = date.year + this.DELIMITER + date.month + this.DELIMITER + date.day;
    }
    return result;
  }
}
@Component({
  selector: 'app-closeouts',
  templateUrl: './closeouts.component.html',
  styleUrls: ['./closeouts.component.scss']
})
export class CloseoutsComponent implements OnInit {

  today = new Date();
  targetDate: Date;

  constructor(private http: HttpClient, private router: Router, private orderPipe: OrderPipe, private calendar: NgbCalendar, private jobsiteService: JobsitesearchService) {

  }

  projects: Jobsite[];
  employees: any;
  subcontracts: any;
  teamleads: any;
  projectmanagers: any;
  teamA: any;
  teamB: any;
  all: any;
  selectedTeam: any;
  selectedPM: any;
  selectedProject: Jobsite;
  safetyaudits: Safetyaudit[];
  siteaudits: Siteaudit[];

  groups = GROUPS;

  projectSelect(project: Jobsite): void {
    this.selectedProject = project;
  }

  teamSelect(teamlead: any) {

    if (teamlead.team == 'teamA') {
      this.selectedTeam = this.teamA;
    }
    if (teamlead.team == 'teamB') {
      this.selectedTeam = this.teamB;
    }
    if (teamlead.team == 'all') {
      this.selectedTeam = this.teamA.concat(this.teamB);
    }
    this.selectedPM = this.teamA.concat(this.teamB);

  }

  pmSelect(pm: any) {
    this.selectedPM = pm;
    this.selectedTeam = this.teamA.concat(this.teamB);
  }

  completedsiteAudit: Projectreview;
  siteauditSelect(audit: Projectreview): void {
    this.completedsiteAudit = audit;
  }

  completedsafetyAudit: Projectreview;
  safetyauditSelect(safetyaudit: Projectreview): void {
    console.log(safetyaudit);
    this.completedsafetyAudit = safetyaudit;
    console.log(this.completedsafetyAudit);
  }

  getJobsites(): void {
    this.jobsiteService.getJobsites()
      .subscribe(projects => this.projects = projects);
  }


  ngOnInit(): void {
    const url = 'https://intranet.pc-const.com/api/Jobsites';
    const employeeurl = 'https://intranet.pc-const.com/api/Employees';
    const safetyauditurl = 'https://intranet.pc-const.com/api/safetyaudits';
    const siteauditurl = 'https://intranet.pc-const.com/api/siteaudits';

    this.teamleads = [
      { name: 'Nic Cornelison', team: 'teamA' },
      { name: 'Mike Davis', team: 'teamB' },
      { name: 'All Project Managers', team: 'all' }
    ];
    this.teamA = ['Nic Cornelison', 'Bryan Cooper', 'Mike Payne', 'Jimmy Lail', 'Stacie Cooper', 'Michael Johnson'];
    this.teamB = ['Mike Davis', 'Chase Steele', 'Mike Brown', 'Jeff Clabough', 'Jason Black', 'Jordan Cornelison'];

    this.projectmanagers = ['Bryan Cooper', 'Chase Steele', 'Jason Black', 'Jeff Clabough', 'Jimmy Lail', 'Jordan Cornelison', 'Michael Johnson', 'Mike Brown', 'Mike Payne', 'Mike Davis', 'Nic Cornelison', 'Stacie Cooper']

    this.selectedTeam = this.teamA.concat(this.teamB);
    this.selectedPM = this.teamA.concat(this.teamB);

    this.getJobsites();

    this.http.get(employeeurl)
      .subscribe((data: Employee[]) => {
        this.employees = data;
      });

    this.http.get(safetyauditurl)
      .subscribe((data: Safetyaudit[]) => {
        this.safetyaudits = data;
      });

    this.http.get(siteauditurl)
      .subscribe((data: Siteaudit[]) => {
        this.siteaudits = data;
      });

    this.targetDate = new Date();
    this.targetDate.setDate(this.targetDate.getDate() + 14);
  }

  public reset(project): void {
    this.selectedProject = undefined;
    this.router.navigate(['/jobs-summary']);
  }

  public resetTeam(teamlead): void {
    this.selectedTeam = undefined;
    this.router.navigate(['/labor']);
  }

  public updateProject(project): void {
    const url = 'https://intranet.pc-const.com/api/Jobsites/' + project.jobsiteId;
    console.log(url, project, { headers: { 'Content-Type': 'application/json' } });
    this.http.put(url, project)
      .subscribe(data => {
        console.log("put request is successful ", data);
      },
        error => {
          console.log("rrrrr", error);
        }
      );
  }

}


