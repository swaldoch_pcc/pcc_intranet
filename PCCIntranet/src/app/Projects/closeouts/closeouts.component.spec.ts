import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CloseoutsComponent } from './closeouts.component';

describe('CloseoutsComponent', () => {
  let component: CloseoutsComponent;
  let fixture: ComponentFixture<CloseoutsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CloseoutsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CloseoutsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
