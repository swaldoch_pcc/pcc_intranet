import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Jobsite } from '../../models/jobsites';

@Component({
  selector: 'app-jobs-summary',
  templateUrl: './jobs-summary.component.html',
  styleUrls: ['./jobs-summary.component.scss']
})
export class JobsSummaryComponent implements OnInit {

  constructor(private http: HttpClient, private router: Router ) { }

  projects: any;


  selectedProject: Jobsite;
  projectSelect(project: Jobsite): void {
    this.selectedProject = project;
  }

  ngOnInit(): void {
    const url = 'https://intranet.pc-const.com/api/Jobsites';

    this.http.get(url)
      .subscribe((data: Jobsite[]) => {
        this.projects = data;
      });
  }

  public reset(project): void {
    this.selectedProject = undefined;
    this.router.navigate(['/jobs-summary']);
  }

}
