import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubSolicitationComponent } from './sub-solicitation.component';

describe('SubSolicitationComponent', () => {
  let component: SubSolicitationComponent;
  let fixture: ComponentFixture<SubSolicitationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubSolicitationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubSolicitationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
