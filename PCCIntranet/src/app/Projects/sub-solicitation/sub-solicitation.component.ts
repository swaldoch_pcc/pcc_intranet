import { Component, Injectable, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { FormGroup, FormControl, ReactiveFormsModule, Validators } from '@angular/forms';
import { Lead } from '../../models/leads';
import { BiddingService } from '../../services/bidding.service';
import { OrderPipe } from 'ngx-order-pipe';
import { NgbDateStruct, NgbDateAdapter, NgbDateParserFormatter, NgbCalendar } from '@ng-bootstrap/ng-bootstrap';

@Injectable()
export class CustomAdapter extends NgbDateAdapter<string> {
  readonly DELIMITER = '-';

  fromModel(value: string): NgbDateStruct {
    let result: NgbDateStruct = null;
    if (value) {
      let date = value.split(this.DELIMITER);
      result = {
        day: parseInt(date[2], 10),
        month: parseInt(date[1], 10),
        year: parseInt(date[0], 10)
      };
    }
    return result;
  }

  toModel(date: NgbDateStruct): string {
    let result: string = null;
    if (date) {
      result = date.year + this.DELIMITER + date.month + this.DELIMITER + date.day;
    }
    return result;
  }
}

@Injectable()
export class CustomDateParserFormatter extends NgbDateParserFormatter {

  readonly DELIMITER = '-';

  parse(value: string): NgbDateStruct {
    let result: NgbDateStruct = null;
    if (value) {
      let date = value.split(this.DELIMITER);
      result = {
        day: parseInt(date[2], 10),
        month: parseInt(date[1], 10),
        year: parseInt(date[0], 10)
      };
    }
    return result;
  }

  format(date: NgbDateStruct): string {
    let result: string = null;
    if (date) {
      result = date.year + this.DELIMITER + date.month + this.DELIMITER + date.day;
    }
    return result;
  }
}

@Component({
  selector: 'app-sub-solicitation',
  templateUrl: './sub-solicitation.component.html',
  styleUrls: ['./sub-solicitation.component.scss'],

  providers: [
    { provide: NgbDateAdapter, useClass: CustomAdapter },
    { provide: NgbDateParserFormatter, useClass: CustomDateParserFormatter }
  ]
})

export class SubSolicitationComponent implements OnInit {

  constructor(private http: HttpClient, private router: Router, private biddingService: BiddingService) { }

  leads: Lead[];
  selectedLead: Lead;
  show: boolean = false;
  buttonName: any = 'New Opportunity';
  initialvalues: any;

  leadSelect(lead): void {
    this.selectedLead = lead;
  }

  getLeads(): void {
    this.biddingService.getLeads()
      .subscribe(leads => this.leads = leads);
  }

  colors = [
    { salesperson: "Bryan Cooper", color: 'tomato' },
    { salesperson: "Nic Cornelison", color: 'dodgerblue' },
    { salesperson: "Crystal Page", color: 'gray' },
    { salesperson: "Mike Payne", color: 'violet' },
    { salesperson: "Michael Johnson", color: 'slateblue' },
    { salesperson: "Chase Steele", color: 'aquamarine' },
    { salesperson: "Mike Brown", color: 'burlywood' },
    { salesperson: "Jordan Cornelison", color: 'crimson' },
    { salesperson: "Stacie Cooper", color: 'darkseagreen' },
    { salesperson: "Mike Davis", color: 'salmon' },
    { salesperson: "Royce Cornelison", color: 'cyan' }

  ]

  getColor(salesperson) {
    return this.colors.filter(item => item.salesperson === salesperson)[0].color
  }

  toggleForm(): void {
    this.show = !this.show;
    if (this.show)
      this.buttonName = "Cancel";
    else
      this.buttonName = "New Opportunity";
  }

  newleadForm = new FormGroup({
    opportunityTitle: new FormControl(null, [Validators.required]),
    confidence: new FormControl(null),
    estRevenueMin: new FormControl(null),
    estRevenueMax: new FormControl(null),
    salesPerson: new FormControl(null, [Validators.required]),
    leadNotes: new FormControl(null),
    anticipatedConstStart: new FormControl(null),
    projectDuration: new FormControl(null),
    PCBidDate: new FormControl(null),
    status: new FormControl('Open'),
    salesPhase: new FormControl('4 - Bidding')
  })

  ngOnInit(): void {

    this.getLeads();
    this.initialvalues = this.newleadForm.value;

  }

  public reset(lead): void {
    this.selectedLead = undefined;
    this.router.navigate(['/bids']);
  }

  public updateLead(lead): void {
    const url = 'https://intranet.pc-const.com/api/Leads/' + lead.leadId;
    console.log(url, lead, { headers: { 'Content-Type': 'application/json' } });
    this.http.put(url, lead)
      .subscribe(data => {
        console.log("put request is successful ", data);
      },
        error => {
          console.log("rrrrr", error);
        }
      );
  }

  onSubmit() {

    const url = 'https://intranet.pc-const.com/api/leads';
    const body = this.newleadForm.value;
    console.log(url, body, { headers: { 'Content-Type': 'application/json' } });
    this.http.post(url, body)
      .subscribe(data => {
        console.log(Response, data);
      },
        err => console.log(err)
      );
    this.newleadForm.reset(this.initialvalues);
  }

}
