import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SafetyactionitemsComponent } from './safetyactionitems.component';

describe('SafetyactionitemsComponent', () => {
  let component: SafetyactionitemsComponent;
  let fixture: ComponentFixture<SafetyactionitemsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SafetyactionitemsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SafetyactionitemsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
