import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PmfeedbackComponent } from './pmfeedback.component';

describe('PmfeedbackComponent', () => {
  let component: PmfeedbackComponent;
  let fixture: ComponentFixture<PmfeedbackComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PmfeedbackComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PmfeedbackComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
