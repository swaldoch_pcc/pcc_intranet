import { Component, OnInit, NgModule } from '@angular/core';
import { Router } from '@angular/router';
import { Jobsite } from '../../../models/jobsites';
import { Suptprojectreview } from './suptprojectreview';
import { HttpClient } from '@angular/common/http';
import { FormGroup, FormControl, ReactiveFormsModule } from '@angular/forms';

@Component({
  selector: 'app-pmfeedback',
  templateUrl: './pmfeedback.component.html',
  styleUrls: ['./pmfeedback.component.scss']
})

  @NgModule({
    imports: [
      ReactiveFormsModule
    ],
  })

export class PmfeedbackComponent implements OnInit {

  constructor(private http: HttpClient, private router: Router ) { }

  projects: any;
  suptprojectreviews: any;

  selectedSuptproject: Suptprojectreview;
  onSelect(project: Suptprojectreview): void {
    this.selectedSuptproject = project;
    console.log(this.selectedSuptproject);
    window.scrollTo(0, 0);
  }

  suptreviewForm = new FormGroup({
    projectName: new FormControl(null),
    reviewer: new FormControl(null),
    reviewed: new FormControl(null),
    q1: new FormControl(null),
    q2: new FormControl(null),
    q3: new FormControl(null),
    q4: new FormControl(null),
    q5: new FormControl(null),
    q6: new FormControl(null),
    q7: new FormControl(null),
    q8: new FormControl(null),
    q9: new FormControl(null),
    q10: new FormControl(null),
    q11: new FormControl(null),
    q12: new FormControl(null),
    q13: new FormControl(null),
    q14: new FormControl(null),
    q15: new FormControl(null),
    q16: new FormControl(null),
    q17: new FormControl(null),
    q18: new FormControl(null),
    q19: new FormControl(null),
    q20: new FormControl(null),
    q21: new FormControl(null),
    q22: new FormControl(null),
    q23: new FormControl(null),
    q24: new FormControl(null),
    q25: new FormControl(null),
    q26: new FormControl(null),
    q27: new FormControl(null),
    q28: new FormControl(null),
    q29: new FormControl(null),
    q30: new FormControl(null),
    q31: new FormControl(null),
    q32: new FormControl(null),
    q33: new FormControl(null),
    q34: new FormControl(null),
    q35: new FormControl(null),
    q36: new FormControl(null),
    q37: new FormControl(null),
    q38: new FormControl(null),
    q39: new FormControl(null),
    q40: new FormControl(null),
    q41: new FormControl(null),
    q42: new FormControl(null),
    q43: new FormControl(null),
    q44: new FormControl(null),
    q45: new FormControl(null),
    q46: new FormControl(null),
    q47: new FormControl(null),
    q48: new FormControl(null),
    q49: new FormControl(null),
    q50: new FormControl(null),
    q51: new FormControl(null),
    q52: new FormControl(null),
    q53: new FormControl(null),
    q54: new FormControl(null),
    q55: new FormControl(null),
    q56: new FormControl(null),
    q57: new FormControl(null),
    q58: new FormControl(null),
    q59: new FormControl(null),
    q60: new FormControl(null),
    q61: new FormControl(null),
    q62: new FormControl(null),
    q63: new FormControl(null),
    q64: new FormControl(null),
    q65: new FormControl(null),
    q66: new FormControl(null),
    q67: new FormControl(null),
    q68: new FormControl(null),
    q69: new FormControl(null),
    q70: new FormControl(null),
    q71: new FormControl(null)
  })

  ngOnInit(): void {
    const url = 'https://intranet.pc-const.com/api/Jobsites';

    this.http.get(url)
      .subscribe((data: Jobsite[]) => {
        this.projects = data;
      });
  }

  public reset(suptprojectreview): void {
    
    this.suptreviewForm.reset();
    this.router.navigate(['/pmfeedback']);
  }

  // Adjust form data when selected choice is changed - Project schedule.
  public clear_q1a(): void {
    this.selectedSuptproject.q4 = null 
    this.selectedSuptproject.q5 = null
  }

  public clear_q1b(): void {
    this.selectedSuptproject.q2 = null
    this.selectedSuptproject.q3 = null
    this.selectedSuptproject.q6 = null
    this.selectedSuptproject.q7 = null
  }

  public clear_q2a(): void {
    this.selectedSuptproject.q3 = null
  }

  public clear_2b(): void {
    this.selectedSuptproject.q6 = null
    this.selectedSuptproject.q7 = null
    this.selectedSuptproject.q8 = null
    this.selectedSuptproject.q9 = null
  }

  public clear_q4a(): void {
    this.selectedSuptproject.q3 = null
    this.selectedSuptproject.q5 = null
  }

  public clear_q4b(): void {
    this.selectedSuptproject.q2 = null
    this.selectedSuptproject.q3 = null
    this.selectedSuptproject.q6 = null
    this.selectedSuptproject.q7 = null
    this.selectedSuptproject.q8 = null
    this.selectedSuptproject.q9 = null
  }

  public clear_q6a(): void {
    this.selectedSuptproject.q7 = null
  }

  public clear_qc6(): void {
    this.selectedSuptproject.q7 = null
    this.selectedSuptproject.q8 = null
    this.selectedSuptproject.q9 = null
  }

  // Adjust form data when selected choice is changed - Project Book.
  public clear_q12a(): void {
    this.selectedSuptproject.q15 = null
  }

  public clear_q12b(): void {
    this.selectedSuptproject.q13 = null
    this.selectedSuptproject.q14 = null
  }

  public clear_q13a(): void {
    this.selectedSuptproject.q14 = null
    this.selectedSuptproject.q15 = null
  }

  // Adjust form data when selected choice is changed - Client Feedback.
  public clear_q18b(): void {
    this.selectedSuptproject.q19 = null
    this.selectedSuptproject.q20 = null
  }

  // Adjust form data when selected choice is changed - Resources.
  public clear_q23a(): void {
    this.selectedSuptproject.q24 = null
  }

  public clear_q25a(): void {
    this.selectedSuptproject.q26 = null
  }

  public clear_q27a(): void {
    this.selectedSuptproject.q28 = null
    this.selectedSuptproject.q29 = null
    this.selectedSuptproject.q30 = null
    this.selectedSuptproject.q31 = null
  }

  public clear_q32a(): void {
    this.selectedSuptproject.q33 = null
    this.selectedSuptproject.q34 = null
    this.selectedSuptproject.q35 = null
    this.selectedSuptproject.q36 = null
  }

  public clear_q32b(): void {
    this.selectedSuptproject.q37 = null
    this.selectedSuptproject.q38 = null
  }

  onSubmit() {
    
    const url = 'https://intranet.pc-const.com/api/SuptProjectReviews';
    const body = this.suptreviewForm.value;
    console.log(body);

    this.selectedSuptproject = undefined;
    return this.http.post(url, body)
      .subscribe(
        response => console.log(response),
        err => console.log(err)
      )
    
  }
}
