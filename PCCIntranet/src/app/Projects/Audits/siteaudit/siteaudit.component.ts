import { Component, OnInit, NgModule, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { Jobsite } from '../../../models/jobsites';
import { JobsitesearchService } from '../../../services/jobsitesearch.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { FormGroup, FormControl, ReactiveFormsModule } from '@angular/forms';
import { Projectreview } from '../../../models/projectreview';
import { Siteaudit } from '../../../models/siteaudit';
import { OrderPipe } from 'ngx-order-pipe';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';


@Component({
  selector: 'app-siteaudit',
  templateUrl: './siteaudit.component.html',
  styleUrls: ['./siteaudit.component.scss']
})

@NgModule({
    imports: [
      ReactiveFormsModule
  ],
})

export class SiteauditComponent implements OnInit {

  projects: Jobsite[];
  audits: any;
  selectedFile: any;
  

  currentUser = JSON.parse(localStorage.getItem('currentUser'));
  auditor = this.currentUser.firstName.concat(' ', this.currentUser.lastName);

  constructor(private http: HttpClient, private router: Router, private jobsiteService: JobsitesearchService) {

  }

  selectedProject: Projectreview;
  onSelect(project: Projectreview): void {
    this.selectedProject = project;
    this.completedAudit = undefined;
    window.scrollTo(0, 0);
  }

  completedAudit: Projectreview;
  auditSelect(audit: Projectreview): void {
    this.completedAudit = audit;
    this.selectedProject = undefined;
    window.scrollTo(0, 0);
  }

  getJobsites(): void {
    this.jobsiteService.getJobsites()
      .subscribe(projects => this.projects = projects);
  }

  reviewForm = new FormGroup({
    projectName: new FormControl(null),
    auditor: new FormControl(null),
    projectManager: new FormControl(null),
    superintendent: new FormControl(null),
    q1: new FormControl(null),
    q2: new FormControl(null),
    q3: new FormControl(null),
    q4: new FormControl(null),
    q5: new FormControl(null),
    q6: new FormControl(null),
    q7: new FormControl(null),
    q8: new FormControl(null),
    q9: new FormControl(null),
    q10: new FormControl(null),
    q11: new FormControl(null),
    q12: new FormControl(null),
    q13: new FormControl(null),
    q14: new FormControl(null),
    q15: new FormControl(null),
    q16: new FormControl(null),
    q17: new FormControl(null),
    q18: new FormControl(null),
    q19: new FormControl(null),
    q20: new FormControl(null),
    q21: new FormControl(null),
    q22: new FormControl(null),
    q23: new FormControl(null),
    q24: new FormControl(null),
    q25: new FormControl(null),
    q26: new FormControl(null),
    q27: new FormControl(null),
    q28: new FormControl(null),
    q29: new FormControl(null),
    q30: new FormControl(null),
    q31: new FormControl(null),
    q32: new FormControl(null),
    q33: new FormControl(null),
    q34: new FormControl(null),
    q35: new FormControl(null),
    q36: new FormControl(null),
    q37: new FormControl(null),
    q38: new FormControl(null),
    q39: new FormControl(null),
    q40: new FormControl(null),
    q41: new FormControl(null),
    q42: new FormControl(null),
    q43: new FormControl(null),
    q44: new FormControl(null),
    q45: new FormControl(null),
    q46: new FormControl(null),
    q47: new FormControl(null),
    q48: new FormControl(null)
  })


  ngOnInit(): void {
    const url = 'https://intranet.pc-const.com/api/Jobsites';
    const auditurl = 'https://intranet.pc-const.com/api/siteaudits';

/*    this.http.get(url)
      .subscribe((data: Jobsite[]) => {
        this.projects = data;
      });*/

    this.getJobsites(); 

    this.http.get(auditurl)
      .subscribe((data: Siteaudit[]) => {
        this.audits = data;
      });
  }

  public reset(projectreview): void {

    this.reviewForm = undefined;
    this.completedAudit = undefined;
    this.router.navigate(['/siteaudit']);
  }

  onSubmit() {

    const url = 'https://intranet.pc-const.com/api/siteaudits';
    const body = this.reviewForm.value;
    console.log(body);
    this.selectedProject = undefined;
    return this.http.post(url, body)
      .subscribe(
        response => console.log(response),
        err => console.log(err)
      )
  }

  onFilter() {

  }

  // Adujust form data when selected choice is changed - 

  public clear_q3(): void {
    this.selectedProject.Q4 = null
    this.selectedProject.Q5 = null
    this.selectedProject.Q6 = null
    this.selectedProject.Q19 = null
  }

  public clear_q10(): void {
    this.selectedProject.Q11 = null
    this.selectedProject.Q12 = null
    this.selectedProject.Q13 = null
  }

  public clear_q20(): void {
    this.selectedProject.Q21 = null
  }

  public clear_q43(): void {
    this.selectedProject.Q44 = null
  }

}




