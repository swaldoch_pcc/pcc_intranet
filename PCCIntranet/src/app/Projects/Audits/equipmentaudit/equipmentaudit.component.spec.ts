import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EquipmentauditComponent } from './equipmentaudit.component';

describe('EquipmentauditComponent', () => {
  let component: EquipmentauditComponent;
  let fixture: ComponentFixture<EquipmentauditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EquipmentauditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EquipmentauditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
