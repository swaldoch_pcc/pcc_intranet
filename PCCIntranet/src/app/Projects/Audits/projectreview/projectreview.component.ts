import { Component, OnInit, NgModule } from '@angular/core';
import { Router } from '@angular/router';
import { Jobsite } from '../../../models/jobsites';
import { Projectreview } from '../../../models/projectreview';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { FormGroup, FormControl, ReactiveFormsModule } from '@angular/forms';
import { Pmprojectreviews } from 'src/app/models/pmprojectreviews';

@Component({
  selector: 'app-projectreview',
  templateUrl: './projectreview.component.html',
  styleUrls: ['./projectreview.component.scss']
})

@NgModule({
  imports: [
    ReactiveFormsModule
  ],
})

export class ProjectreviewComponent implements OnInit {

  constructor(private http: HttpClient, private router: Router ) { }

  projects: any;
  reviews: any;
  currentUser = JSON.parse(localStorage.getItem('currentUser'));
  auditor = this.currentUser.firstName.concat(' ', this.currentUser.lastName);

  selectedProject: Projectreview;
  onSelect(project: Projectreview): void {
    this.selectedProject = project;
    this.completedReview = undefined;
    window.scrollTo(0, 0);
  }

  completedReview: Projectreview;
  reviewSelect(review: Projectreview): void {
    this.completedReview = review;
    this.selectedProject = undefined;
    window.scrollTo(0, 0);
  }

  reviewForm = new FormGroup({
    projectName: new FormControl(null),
    reviewer: new FormControl(null),
    reviewed: new FormControl(null),
    q1: new FormControl(null),
    q2: new FormControl(null),
    q3: new FormControl(null),
    q4: new FormControl(null),
    q5: new FormControl(null),
    q6: new FormControl(null),
    q7: new FormControl(null),
    q8: new FormControl(null),
    q9: new FormControl(null),
    q10: new FormControl(null),
    q11: new FormControl(null),
    q12: new FormControl(null),
    q13: new FormControl(null),
    q14: new FormControl(null),
    q15: new FormControl(null),
    q16: new FormControl(null),
    q17: new FormControl(null),
    q18: new FormControl(null),
    q19: new FormControl(null),
    q20: new FormControl(null),
    q21: new FormControl(null),
    q22: new FormControl(null),
    q23: new FormControl(null),
    q24: new FormControl(null),
    q25: new FormControl(null),
    q26: new FormControl(null),
    q27: new FormControl(null),
    q28: new FormControl(null),
    q29: new FormControl(null),
    q30: new FormControl(null)
  })

  ngOnInit(): void {
    const url = 'https://intranet.pc-const.com/api/Jobsites';
    const reviewurl = 'https://intranet.pc-const.com/api/Pmprojectreviews';

    this.http.get(url)
      .subscribe((data: Jobsite[]) => {
        this.projects = data;
      });

    this.http.get(reviewurl)
      .subscribe((data: Pmprojectreviews[]) => {
        this.reviews = data;
      });
  }
  
  public reset(projectreview): void {
    
    this.reviewForm = undefined;
    this.completedReview = undefined;
    this.router.navigate(['/projectreview']);
  }

  // Adujust form data when selected choice is changed - 

  public clear_q1b(): void {
    this.selectedProject.Q2 = null
  }

  public clear_q6b(): void {
    this.selectedProject.Q7 = null
    this.selectedProject.Q8 = null
    this.selectedProject.Q9 = null
  }

  public clear_q10b(): void {
    this.selectedProject.Q11 = null
    this.selectedProject.Q12 = null
  }

  public clear_q13b(): void {
    this.selectedProject.Q14 = null
  }

  public clear_q15b(): void {
    this.selectedProject.Q30 = null
  }

  public clear_q16b(): void {
    this.selectedProject.Q17 = null
  }



  onSubmit() {
    
    const url = 'https://intranet.pc-const.com/api/PmprojectReviews';
    const body = this.reviewForm.value;
    console.log(body);
    this.selectedProject = undefined;
    return this.http.post(url, body)
      .subscribe(
      response => console.log(response),
      err => console.log(err)
      )
    
    }
}
