import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SafetyauditComponent } from './safetyaudit.component';

describe('SafetyauditComponent', () => {
  let component: SafetyauditComponent;
  let fixture: ComponentFixture<SafetyauditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SafetyauditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SafetyauditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
