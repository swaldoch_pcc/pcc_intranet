import { Component, OnInit, NgModule, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { Jobsite } from '../../../models/jobsites';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { FormGroup, FormControl, ReactiveFormsModule } from '@angular/forms';
import { Projectreview } from '../../../models/projectreview';
import { Safetyaudit } from '../../../models/safetyaudit';
import { OrderPipe } from 'ngx-order-pipe';



@Component({
  selector: 'app-safetyaudit',
  templateUrl: './safetyaudit.component.html',
  styleUrls: ['./safetyaudit.component.scss']
})

@NgModule({
  imports: [
    ReactiveFormsModule
  ],
})

export class SafetyauditComponent implements OnInit {


  projects: any;
  audits: any;
  selectedFile: any;
  navigationSubscription;
  currentUser = JSON.parse(localStorage.getItem('currentUser'));
  auditor = this.currentUser.firstName.concat(' ', this.currentUser.lastName);

  constructor(private http: HttpClient, private router: Router, private orderPipe: OrderPipe) {
    
  }

  selectedProject: Projectreview;
  onSelect(project: Projectreview): void {
    this.selectedProject = project;
    this.completedAudit = undefined;
    window.scrollTo(0, 0);
  }

  completedAudit: Projectreview;
  auditSelect(audit: Projectreview): void {
    this.completedAudit = audit;
    this.selectedProject = undefined;
    window.scrollTo(0, 0);
  }

   

  reviewForm = new FormGroup({
    projectName: new FormControl(null),
    projectManager: new FormControl(null),
    superintendent: new FormControl(null),
    auditor: new FormControl(null),
    q1: new FormControl(null),
    q2: new FormControl(null),
    q3: new FormControl(null),
    q4: new FormControl(null),
    q5: new FormControl(null),
    q6: new FormControl(null),
    q7: new FormControl(null),
    q8: new FormControl(null),
    q9: new FormControl(null),
    q10: new FormControl(null),
    q11: new FormControl(null),
    q12: new FormControl(null),
    q13: new FormControl(null),
    q14: new FormControl(null),
    q15: new FormControl(null),
    q16: new FormControl(null),
    q17: new FormControl(null),
    q18: new FormControl(null),
    q19: new FormControl(null),
    q20: new FormControl(null),
    q21: new FormControl(null),
    q22: new FormControl(null),
    q23: new FormControl(null),
    q24: new FormControl(null),
    q25: new FormControl(null),
    q26: new FormControl(null),
    q27: new FormControl(null),
    q28: new FormControl(null),
    q29: new FormControl(null),
    q30: new FormControl(null),
    q31: new FormControl(null),
    q32: new FormControl(null),
    q33: new FormControl(null),
    q34: new FormControl(null),
    q35: new FormControl(null),
    q36: new FormControl(null),
    q37: new FormControl(null),
    q38: new FormControl(null),
    q39: new FormControl(null),
    q40: new FormControl(null),
    q41: new FormControl(null),
    q42: new FormControl(null),
    q43: new FormControl(null),
    q44: new FormControl(null),
    q45: new FormControl(null),
    q46: new FormControl(null),
    q47: new FormControl(null),
    q48: new FormControl(null),
    q49: new FormControl(null),
    q50: new FormControl(null),
    q51: new FormControl(null),
    q52: new FormControl(null),
    q53: new FormControl(null),
    q54: new FormControl(null),
    q55: new FormControl(null),
    q56: new FormControl(null),
    q57: new FormControl(null),
    q58: new FormControl(null),
    q59: new FormControl(null),
    q60: new FormControl(null),
    q61: new FormControl(null),
    q62: new FormControl(null),
    q63: new FormControl(null),
    q64: new FormControl(null),
    q65: new FormControl(null),
    q66: new FormControl(null),
    q67: new FormControl(null),
    q68: new FormControl(null),
    q69: new FormControl(null),
    q70: new FormControl(null),
    q71: new FormControl(null),
    q72: new FormControl(null),
    q73: new FormControl(null),
    q74: new FormControl(null),
    q75: new FormControl(null),
    q76: new FormControl(null),
    q77: new FormControl(null),
    q78: new FormControl(null),
    q79: new FormControl(null),
    q80: new FormControl(null),
    q81: new FormControl(null),
    q82: new FormControl(null),
    q83: new FormControl(null),
    q84: new FormControl(null),
    q85: new FormControl(null),
    q86: new FormControl(null),
    q87: new FormControl(null),
    q88: new FormControl(null),
    q89: new FormControl(null),
    q90: new FormControl(null),
    q91: new FormControl(null),
    q92: new FormControl(null),
    q93: new FormControl(null),
    q94: new FormControl(null),
    q95: new FormControl(null),
    q96: new FormControl(null),
    q97: new FormControl(null)
  })

  ngOnInit(): void {
    const url = 'https://intranet.pc-const.com/api/Jobsites';
    const auditurl = 'https://intranet.pc-const.com/api/safetyaudits';
    //const auditfilesurl = 'https://pconstruction.sharepoint.com/sites/intranet-photos/';
    //const httpOptions = {
    //  headers: new HttpHeaders({
    //    'Accept': 'application/json;odata=verbose',
    //    'Content-Type':'application/json;odata=verbos',
    //    'Authorization': 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsIng1dCI6IkhsQzBSMTJza3hOWjFXUXdtak9GXzZ0X3RERSIsImtpZCI6IkhsQzBSMTJza3hOWjFXUXdtak9GXzZ0X3RERSJ9.eyJhdWQiOiIwMDAwMDAwMy0wMDAwLTBmZjEtY2UwMC0wMDAwMDAwMDAwMDAvcGNvbnN0cnVjdGlvbi5zaGFyZXBvaW50LmNvbUBmODY0OGYzMy0wNTlmLTQ2MWUtODFlYS01NThkMjg5YjIzNTkiLCJpc3MiOiIwMDAwMDAwMS0wMDAwLTAwMDAtYzAwMC0wMDAwMDAwMDAwMDBAZjg2NDhmMzMtMDU5Zi00NjFlLTgxZWEtNTU4ZDI4OWIyMzU5IiwiaWF0IjoxNTgyMjE4Mjk2LCJuYmYiOjE1ODIyMTgyOTYsImV4cCI6MTU4MjI0NzM5NiwiaWRlbnRpdHlwcm92aWRlciI6IjAwMDAwMDAxLTAwMDAtMDAwMC1jMDAwLTAwMDAwMDAwMDAwMEBmODY0OGYzMy0wNTlmLTQ2MWUtODFlYS01NThkMjg5YjIzNTkiLCJuYW1laWQiOiJhYjA0NDc1NS00YTNkLTQ2NzItYmY4MC01ZjY3MDY2Y2QxODhAZjg2NDhmMzMtMDU5Zi00NjFlLTgxZWEtNTU4ZDI4OWIyMzU5Iiwib2lkIjoiMzk4ZDFjZmItOGM3YS00YWFmLThlOTktYzgzNzhjODljNjUyIiwic3ViIjoiMzk4ZDFjZmItOGM3YS00YWFmLThlOTktYzgzNzhjODljNjUyIiwidHJ1c3RlZGZvcmRlbGVnYXRpb24iOiJmYWxzZSJ9.T595eJrIvFrq5uGMqMUFpkPTYvlxhKWIg2f3D-TEQ7ReQb29PhLaPR_JQSZR9_2zXhqhERIZSB9ZXzjHaO74KjZPEqRymPCu0JyAakx7UiP7SBtsMALx-Ls2f4O8hHbenQQ7TWQt9heA--4iKLxlSH8Av54O2aWXRHlwxtlwGfFk9Ekfa-IOAGYnXf7xVnJg5PuawVjhJjX3dsj12IK6zRZCUqFpdz0_p6oY0y-KFTVAzgPZZAtPh6BSefCAKz6DxAOxPiM_1mGJI2LeWLWxNj7JCLmFA2koggwKXcZjoAgDvM-Lq9xXU4HgBNrPCSfEHJ5iWYSAxS6puBJusxLgTA'
    //  })
    //};

    this.http.get(url)
      .subscribe((data: Jobsite[]) => {
        this.projects = data;
      });

    this.http.get(auditurl)
      .subscribe((data: Safetyaudit[]) => {
        this.audits = data;
      });

    //this.http.get(auditfilesurl + "_api/web/GetFolderByServerRelativeUrl('/Documents/Safety')/Files", httpOptions)
    //  .subscribe(
    //    response => console.log(response),
    //    err => console.log(err))
  }

   public reset(projectreview): void {

     this.reviewForm = undefined;
     this.completedAudit = undefined;
     this.router.navigate(['/safetyaudit']);
  }

  onSubmit() {

    const url = 'https://intranet.pc-const.com/api/safetyaudits';
    const body = this.reviewForm.value;
    console.log(body);
    this.selectedProject = undefined;
    return this.http.post(url, body)
      .subscribe(
        response => console.log(response),
        err => console.log(err)
      )

  }

  onFileSelected(event) {
    this.selectedFile = event.target.files[0];
  }

  onUpload() {
    console.log(this.selectedFile); // You can use FormData upload to backend server
  }

  private signaturePadOptions: Object = { // passed through to szimek/signature_pad constructor
    'minWidth': 5,
    'canvasWidth': 1000,
    'canvasHeight': 200,
    'backgroundColor': "#ffffff"

  };


  electrical: any;

  public Electrical(): void {
    this.selectedProject.Q23 = "na";
    this.selectedProject.Q24 = "na";
    this.selectedProject.Q25 = "na";
    this.selectedProject.Q26 = "na";
    this.selectedProject.Q27 = "na";
    this.electrical = false;
  }

}
