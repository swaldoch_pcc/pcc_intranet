import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LaborGanttComponent } from './labor-gantt.component';

describe('LaborGanttComponent', () => {
  let component: LaborGanttComponent;
  let fixture: ComponentFixture<LaborGanttComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LaborGanttComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LaborGanttComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
