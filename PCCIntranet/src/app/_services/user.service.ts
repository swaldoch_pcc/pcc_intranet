import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { User } from '../_models';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  constructor(private http: HttpClient) { }

  getAll() {
    return this.http.get<User[]>(`https://intranet.pc-const.com/users`);
  }

  getById(id: number) {
    return this.http.get(`https://intranet.pc-const.com/users/` + id);
  }

  register(user: User) {
    return this.http.post(`https://intranet.pc-const.com/users/register`, user);
  }

  update(user: User) {
    return this.http.put(`https://intranet.pc-const.com/users/` + user.id, user);
  }

  delete(id: number) {
    return this.http.delete(`https://intranet.pc-const.com/users/` + id);
  }
}
