import { NgModule } from '@angular/core';
import { Routes, Router, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { HRComponent } from './HR/hr.component';
import { SubsComponent } from './subs/subs.component';
import { SubsDetailComponent } from './subs/subsdetail.component';
import { EquipmentauditComponent } from './Projects/Audits/equipmentaudit/equipmentaudit.component';
import { PmfeedbackComponent } from './Projects/Audits/pmfeedback/pmfeedback.component';
import { ProjectreviewComponent } from './Projects/Audits/projectreview/projectreview.component';
import { SafetyauditComponent } from './Projects/Audits/safetyaudit/safetyaudit.component';
import { SiteauditComponent } from './Projects/Audits/siteaudit/siteaudit.component';
import { ExecreportsComponent } from './Reports/execreports/execreports.component';
import { PmreportsComponent } from './Reports/pmreports/pmreports.component';
import { FieldreportsComponent } from './Reports/fieldreports/fieldreports.component';
import { AuthGuard } from './_guards';
import { LoginComponent } from './Admin/login/login.component';
import { RegisterComponent } from './Admin/register';
import { SafetyComponent } from './HR/safety/safety.component';
import { ChangePasswordComponent } from './Admin/change-password/change-password.component';
import { CompletedsiteauditComponent } from './Reports/reports/completedsiteaudit/completedsiteaudit.component';
import { CompletedsafetyauditComponent } from './Reports/reports/completedsafetyaudit/completedsafetyaudit.component';
import { CompletedpmprojectreviewComponent } from './Reports/reports/completedpmprojectreview/completedpmprojectreview.component';
import { CompletedsuptprojectreviewComponent } from './Reports/reports/completedsuptprojectreview/completedsuptprojectreview.component';
import { LaborGanttComponent } from './Projects/labor-gantt/labor-gantt.component';
import { JobsSummaryComponent } from './Projects/jobs-summary/jobs-summary.component';
import { BidsComponent } from './Projects/bids/bids.component';
import { LaborComponent } from './Projects/labor/labor.component';
import { ProjectGanttComponent } from './Projects/project-gantt/project-gantt.component';
import { ToolrentalComponent } from './Equipment/toolrental/toolrental.component';
import { RegisterEquipmentComponent } from './Equipment/register-equipment/register-equipment.component';
import { IssueEquipmentComponent } from './Equipment/issue-equipment/issue-equipment.component';
import { EditEquipmentComponent } from './Equipment/edit-equipment/edit-equipment.component';
import { CheckInEquipmentComponent } from './Equipment/check-in-equipment/check-in-equipment.component';
import { CloseoutsComponent } from './Projects/closeouts/closeouts.component';
import { ServiceComponent } from './Equipment/service/service.component';
import { SubSolicitationComponent } from './Projects/sub-solicitation/sub-solicitation.component';

//import { RoleGuard } from './_guards';


const routes: Routes = [
  { path: '', component: HomeComponent, canActivate: [AuthGuard] },
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'update-password', component: ChangePasswordComponent },

  { path: 'subcontractors', component: SubsComponent, canActivate: [AuthGuard] },
  { path: 'subsdetail', component: SubsDetailComponent, canActivate: [AuthGuard] },
  { path: 'equipmentaudit', component: EquipmentauditComponent, canActivate: [AuthGuard] },
  { path: 'pmfeedback', component: PmfeedbackComponent, canActivate: [AuthGuard] },
  { path: 'projectreview', component: ProjectreviewComponent, canActivate: [AuthGuard]  },
  { path: 'safetyaudit', component: SafetyauditComponent, canActivate: [AuthGuard] },
  { path: 'siteaudit', component: SiteauditComponent, canActivate: [AuthGuard] },
  { path: 'execreports', component: ExecreportsComponent, canActivate: [AuthGuard] },
  { path: 'pmreports', component: PmreportsComponent, canActivate: [AuthGuard] },
  { path: 'fieldreports', component: FieldreportsComponent, canActivate: [AuthGuard] },
  { path: 'hr', component: HRComponent, canActivate: [AuthGuard] },
  { path: 'safety', component: SafetyComponent, canActivate: [AuthGuard] },

  { path: 'completedsiteaudit', component: CompletedsiteauditComponent, canActivate: [AuthGuard]  },
  { path: 'labor-gantt', component: LaborGanttComponent, canActivate: [AuthGuard]  },
  { path: 'project-gantt', component: ProjectGanttComponent, canActivate: [AuthGuard]  },
  { path: 'jobs-summary', component: JobsSummaryComponent, canActivate: [AuthGuard]  },
  { path: 'bids', component: BidsComponent, canActivate: [AuthGuard] },
  { path: 'sub-solicitation', component: SubSolicitationComponent, canActivate: [AuthGuard] },
  { path: 'labor', component: LaborComponent, canActivate: [AuthGuard]  },
  { path: 'toolrental', component: ToolrentalComponent, canActivate: [AuthGuard]  },
  { path: 'toolsignout', component: IssueEquipmentComponent, canActivate: [AuthGuard]  },
  { path: 'tooledit', component: EditEquipmentComponent, canActivate: [AuthGuard] },
  { path: 'toolsignin', component: CheckInEquipmentComponent, canActivate: [AuthGuard] },
  { path: 'closeouts', component: CloseoutsComponent, canActivate: [AuthGuard] },
  { path: 'service', component: ServiceComponent, canActivate: [AuthGuard] },
  { path: 'new-tool', component: RegisterEquipmentComponent, canActivate: [AuthGuard] }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: false })],
  exports: [RouterModule]
})

export class AppRoutingModule { }


