import { Component, OnInit, NgModule, Injectable } from '@angular/core';
import { OrderPipe } from 'ngx-order-pipe';
import { Router } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { FormGroup, FormControl, ReactiveFormsModule } from '@angular/forms';
import { Emp, Job, Service, Vehiclehistory, Vehicle, Vehicleretired, TruckRental } from '../../models/equipmentmodels';
import { EquipmentService } from 'src/app/services/equipmentservice';
import { Jobsite } from '../../models/jobsites';
import { JobsitesearchService } from '../../services/jobsitesearch.service';
import { Employee } from '../../models/employees';
import { EmployeeService } from '../../services/employee';
import { NgbDateStruct, NgbDateAdapter, NgbDateParserFormatter, NgbCalendar } from '@ng-bootstrap/ng-bootstrap';

@Injectable()
export class CustomAdapter extends NgbDateAdapter<string> {
  readonly DELIMITER = '-';

  fromModel(value: string): NgbDateStruct {
    let result: NgbDateStruct = null;
    if (value) {
      let date = value.split(this.DELIMITER);
      result = {
        day: parseInt(date[2], 10),
        month: parseInt(date[1], 10),
        year: parseInt(date[0], 10)
      };
    }
    return result;
  }

  toModel(date: NgbDateStruct): string {
    let result: string = null;
    if (date) {
      result = date.year + this.DELIMITER + date.month + this.DELIMITER + date.day;
    }
    return result;
  }
}

@Injectable()
export class CustomDateParserFormatter extends NgbDateParserFormatter {

  readonly DELIMITER = '-';

  parse(value: string): NgbDateStruct {
    let result: NgbDateStruct = null;
    if (value) {
      let date = value.split(this.DELIMITER);
      result = {
        day: parseInt(date[2], 10),
        month: parseInt(date[1], 10),
        year: parseInt(date[0], 10)
      };
    }
    return result;
  }

  format(date: NgbDateStruct): string {
    let result: string = null;
    if (date) {
      result = date.year + this.DELIMITER + date.month + this.DELIMITER + date.day;
    }
    return result;
  }
}

@Component({
  selector: 'app-service',
  templateUrl: './service.component.html',
  styleUrls: ['./service.component.scss'],
  providers: [
    { provide: NgbDateAdapter, useClass: CustomAdapter },
    { provide: NgbDateParserFormatter, useClass: CustomDateParserFormatter }
  ]
})

@NgModule({
  imports: [
    ReactiveFormsModule
  ],
})

export class ServiceComponent implements OnInit {

  employees: Employee[];
  jobs: Jobsite[];
  trucks: Vehicle[];
  truckservices: Service[];
  types: any;
  show: boolean = false;
  buttonName: any = 'Show';

  currentUser = JSON.parse(localStorage.getItem('currentUser'));
  toolcoordinator = this.currentUser.firstName.concat(' ', this.currentUser.lastName);

  constructor(private http: HttpClient, private router: Router, private orderPipe: OrderPipe, private equipmentService: EquipmentService, private jobsiteService: JobsitesearchService, private employeeService: EmployeeService) { }

  selectedType: any;
  onSelect(type): void {
    this.selectedType = type;
  }

  getEmployees(): void {
    this.employeeService.getEmployees()
      .subscribe(employees => this.employees = employees);
  }

  getJobsites(): void {
    this.jobsiteService.getJobsites()
      .subscribe(jobs => this.jobs = jobs);
  }

  getVehicles(): void {
    this.equipmentService.getVehicles()
      .subscribe(trucks => this.trucks = trucks);
  }

  getService(): void {
    this.equipmentService.getService()
      .subscribe(truckservices => this.truckservices = truckservices);
  }

  serviceForm = new FormGroup({
    serialNum: new FormControl(null)
  })

  ngOnInit() {

    this.types = [
      'Record Service',
      'View Service Record'
    ]

    this.getEmployees();

    this.getJobsites();

    this.getService();

    this.getVehicles();

  }

}
