import { Component, OnInit, NgModule, Injectable } from '@angular/core';
import { OrderPipe } from 'ngx-order-pipe';
import { Router } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { FormGroup, FormControl, ReactiveFormsModule, Validators } from '@angular/forms';
import { Tool, Condition, Description, Emp, EquipmentRental, Job, Mfg, Modelnum, Pcnum, Recall, Serialnum, Service, Vehiclehistory, Vehicle, Vehicleretired } from '../../models/equipmentmodels';
import { EquipmentService } from 'src/app/services/equipmentservice';
import { NgbDateStruct, NgbDateAdapter, NgbDateParserFormatter, NgbCalendar } from '@ng-bootstrap/ng-bootstrap';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-edit-equipment',
  templateUrl: './edit-equipment.component.html',
  styleUrls: ['./edit-equipment.component.scss']
})
export class EditEquipmentComponent implements OnInit {

  constructor(private http: HttpClient, private equipmentService: EquipmentService) { }

  tools: Tool[];
  initialvalues: any;
  show: boolean = false;
  buttonName: any = 'New Tool';

  toggleForm(): void {
    this.show = !this.show;
    if (this.show)
      this.buttonName = "Tool List";
    else
      this.buttonName = "New Tool";
  }
 
  getTools(): void {
    this.equipmentService.getTools()
      .subscribe(tools => this.tools = tools);
  }

  currentUser = JSON.parse(localStorage.getItem('currentUser'));

  public updateTool(tool): void {
    const url = 'https://intranet.pc-const.com/api/tools/' + tool.toolId;
    console.log(url, tool, { headers: { 'Content-Type': 'application/json' } });
    this.http.put(url, tool)
      .subscribe(data => {
        console.log("put request is successful ", data);
      },
        error => {
          console.log("rrrrr", error);
        }
      );
  }

  newtoolForm = new FormGroup({
    pcnum: new FormControl(null, [Validators.required]),
    serialNum: new FormControl(null),
    datePurchased: new FormControl(null),
    mfg: new FormControl(null, [Validators.required]),
    modelnum: new FormControl(null, [Validators.required]),
    description: new FormControl(null, [Validators.required]),
    condition: new FormControl(null, [Validators.required]),
    location: new FormControl(null, [Validators.required])
  })

  public onSubmit(): void {
    const url = 'https://intranet.pc-const.com/api/tools/';
    const body = this.newtoolForm.value;
    console.log(url, body, { headers: { 'Content-Type': 'application/json' } });
    this.http.post(url, body)
      .subscribe(data => {
        console.log(Response, data);
      },
        err => console.log(err)
    );
    this.newtoolForm.reset(this.initialvalues);
  }

  ngOnInit() {
    this.getTools();
  }

}
