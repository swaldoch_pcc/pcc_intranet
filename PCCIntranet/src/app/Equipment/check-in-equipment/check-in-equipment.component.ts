import { Component, OnInit, NgModule, Injectable } from '@angular/core';
import { OrderPipe } from 'ngx-order-pipe';
import { Router } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { FormGroup, FormControl, ReactiveFormsModule } from '@angular/forms';
import { Tool, Condition, Description, Emp, EquipmentRental, Job, Mfg, Modelnum, Pcnum, Recall, Serialnum, Service, Vehiclehistory, Vehicle, Vehicleretired, TruckRental, Consumable, ConsumableIssue } from '../../models/equipmentmodels';
import { ISSUEMENUS } from '../../constants/equipment';
import { EquipmentService } from 'src/app/services/equipmentservice';
import { Jobsite } from '../../models/jobsites';
import { JobsitesearchService } from '../../services/jobsitesearch.service';
import { Employee } from '../../models/employees';
import { EmployeeService } from '../../services/employee';
import { NgbDateStruct, NgbDateAdapter, NgbDateParserFormatter, NgbCalendar } from '@ng-bootstrap/ng-bootstrap';

@Injectable()
export class CustomAdapter extends NgbDateAdapter<string> {
  readonly DELIMITER = '-';

  fromModel(value: string): NgbDateStruct {
    let result: NgbDateStruct = null;
    if (value) {
      let date = value.split(this.DELIMITER);
      result = {
        day: parseInt(date[2], 10),
        month: parseInt(date[1], 10),
        year: parseInt(date[0], 10)
      };
    }
    return result;
  }

  toModel(date: NgbDateStruct): string {
    let result: string = null;
    if (date) {
      result = date.year + this.DELIMITER + date.month + this.DELIMITER + date.day;
    }
    return result;
  }
}

@Injectable()
export class CustomDateParserFormatter extends NgbDateParserFormatter {

  readonly DELIMITER = '-';

  parse(value: string): NgbDateStruct {
    let result: NgbDateStruct = null;
    if (value) {
      let date = value.split(this.DELIMITER);
      result = {
        day: parseInt(date[2], 10),
        month: parseInt(date[1], 10),
        year: parseInt(date[0], 10)
      };
    }
    return result;
  }

  format(date: NgbDateStruct): string {
    let result: string = null;
    if (date) {
      result = date.year + this.DELIMITER + date.month + this.DELIMITER + date.day;
    }
    return result;
  }
}

@Component({
  selector: 'app-check-in-equipment',
  templateUrl: './check-in-equipment.component.html',
  styleUrls: ['./check-in-equipment.component.scss'],

  providers: [
    { provide: NgbDateAdapter, useClass: CustomAdapter },
    { provide: NgbDateParserFormatter, useClass: CustomDateParserFormatter }
  ]
})

@NgModule({
  imports: [
    ReactiveFormsModule
  ],
})

export class CheckInEquipmentComponent implements OnInit {

  tools: Tool[];
  employees: Employee[];
  equipmentrentals: EquipmentRental[];
  jobs: Jobsite[];
  trucks: Vehicle[];
  truckrentals: TruckRental[];
  consumables: Consumable[];
  types: any;
  show: boolean = false;
  buttonName: any = 'Show';

  currentUser = JSON.parse(localStorage.getItem('currentUser'));
  toolcoordinator = this.currentUser.firstName.concat(' ', this.currentUser.lastName);
  today = new Date();
    employee: string;
    pcnum: string;
    description: string;

  constructor(private http: HttpClient, private router: Router, private orderPipe: OrderPipe, private equipmentService: EquipmentService, private jobsiteService: JobsitesearchService, private employeeService: EmployeeService) { }

  selectedType: any;
  onSelect(type): void {
    this.selectedType = type;
  }

  getTools(): void {
    this.equipmentService.getTools()
      .subscribe(tools => this.tools = tools);
  }

  getEquipmentrental(): void {
    this.equipmentService.getEquipmentrental()
      .subscribe(equipmentrentals => this.equipmentrentals = equipmentrentals);
  }

  getEmployees(): void {
    this.employeeService.getEmployees()
      .subscribe(employees => this.employees = employees);
  }

  getJobsites(): void {
    this.jobsiteService.getJobsites()
      .subscribe(jobs => this.jobs = jobs);
  }

  getVehicles(): void {
    this.equipmentService.getVehicles()
      .subscribe(trucks => this.trucks = trucks);
  }

  getTruckRentals(): void {
    this.equipmentService.getTruckRentals()
      .subscribe(truckrentals => this.truckrentals = truckrentals);
  }

  getConsumables(): void {
    this.equipmentService.getConsumables()
      .subscribe(consumables => this.consumables = consumables);
  }



  // filter for active jobs.  Can't get map/pipe/filter functions to work properly.
  //getActiveJobsites(): void {
  //  this.jobsiteService.getJobsites()
  //  .map(jobs => jobs.filter(jobs => jobs.status === "Open")[0])
  //}

  signoutForm = new FormGroup({
    equipmentId: new FormControl(null),
    dateOut: new FormControl(null),
    dateIn: new FormControl(null),
    user: new FormControl(null),
    job: new FormControl(null),
    comments: new FormControl(null),
    description: new FormControl(null)
  })

  consumableForm = new FormGroup({
    itemId: new FormControl(null),
    issueDate: new FormControl(null),
    user: new FormControl(null),
    job: new FormControl(null),
    comments: new FormControl(null),
    description: new FormControl(null),
    quantity: new FormControl(null)
  })

  truckrentalForm = new FormGroup({
    itemid: new FormControl(null),
    job: new FormControl(null),
    user: new FormControl(null),
    comments: new FormControl(null),
    issuedate: new FormControl(null),
    returndate: new FormControl(null),
    issuemileage: new FormControl(null),
    returnmileage: new FormControl(null),
    issuefuel: new FormControl(null),
    returnfuel: new FormControl(null),
    condition: new FormControl(null),
    purpose: new FormControl(null)
  })

  public reset(): void {
    this.selectedType = undefined;
    this.signoutForm = undefined;
    this.truckrentalForm = undefined;
    this.router.navigate(['/issue-equipment']);
  }

  public resetMenu(): void {
    this.signoutForm = undefined;
  }

  public resetType(): void {
    this.selectedType = undefined;
    this.signoutForm = undefined;
  }

  ngOnInit() {

    this.types = [
      'Tool',
      'Vehicle/trailer',
      'Consumable'
    ];

    this.employee = '';
    this.pcnum = '';
    this.description = '';

    this.getTools();

    this.getEmployees();

    this.getEquipmentrental();

    this.getJobsites();

    this.getVehicles();

    this.getTruckRentals();

    this.getConsumables();

  }

  public updateRental(rental): void {
    const rentalUrl = 'https://intranet.pc-const.com/api/equipmentrentals/' + rental.rentalId;
    console.log(rentalUrl, rental, { headers: { 'Content-Type': 'application/json' } });
    this.http.put(rentalUrl, rental)
      .subscribe(data => {
        console.log("put request is successful ", data);
      },
        error => {
          console.log("rrrrr", error);
        }
      );
  }



}
