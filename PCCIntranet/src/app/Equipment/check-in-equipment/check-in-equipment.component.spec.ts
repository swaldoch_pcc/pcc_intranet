import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CheckInEquipmentComponent } from './check-in-equipment.component';

describe('CheckInEquipmentComponent', () => {
  let component: CheckInEquipmentComponent;
  let fixture: ComponentFixture<CheckInEquipmentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CheckInEquipmentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CheckInEquipmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
