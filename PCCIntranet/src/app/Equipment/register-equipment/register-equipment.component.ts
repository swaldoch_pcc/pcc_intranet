import { Component, OnInit, NgModule } from '@angular/core';
import { OrderPipe } from 'ngx-order-pipe';
import { Router } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { FormGroup, FormControl, ReactiveFormsModule } from '@angular/forms';
import { Tool, Condition, Description, Emp, EquipmentRental, Job, Mfg, Modelnum, Pcnum, Recall, Serialnum, Service, Vehiclehistory, Vehicle, Vehicleretired } from '../../models/equipmentmodels';

@Component({
  selector: 'app-register-equipment',
  templateUrl: './register-equipment.component.html',
  styleUrls: ['./register-equipment.component.scss']
})

export class RegisterEquipmentComponent implements OnInit {

  tools: any;
  conditions: any;
  descriptions: any;
  employees: any;
  equipmentrentals: any;
  jobs: any;
  mfgs: any;
  modelnums: any;
  pcnums: any;
  recalls: any;
  serialnums: any;
  services: any;
  vehiclehistorys: any;
  vehicles: any;
  vehiclesretired: any;
  types: any;

  constructor(private http: HttpClient) { }

  selectedType: any;
  onSelect(type): void {
    this.selectedType = type;
  }

  reset(): void {
    this.selectedType = undefined;
  };

  ngOnInit(): void {
    const toolsurl = 'https://intranet.pc-const.com/api/Tools';
    const conditionurl = 'https://intranet.pc-const.com/api/Conditions';
    const descriptionurl = 'https://intranet.pc-const.com/api/Descriptions';
    const employeeurl = 'https://intranet.pc-const.com/api/Employees1';
    const equipmentrentalurl = 'https://intranet.pc-const.com/api/EquipmentRentals';
    const joburl = 'https://intranet.pc-const.com/api/Jobs';
    const mfgurl = 'https://intranet.pc-const.com/api/MFGs';
    const modelnumurl = 'https://intranet.pc-const.com/api/Modelnums';
    const pcnumurl = 'https://intranet.pc-const.com/api/Pcnums';
    const recallurl = 'https://intranet.pc-const.com/api/Recalls';
    const serialnumurl = 'https://intranet.pc-const.com/api/Serialnums';
    const serviceurl = 'https://intranet.pc-const.com/api/Services';
    const vehiclehistoryurl = 'https://intranet.pc-const.com/api/Vehiclehistories';
    const vehiclesurl = 'https://intranet.pc-const.com/api/Vehicles';
    const vehiclesretiredurl = 'https://intranet.pc-const.com/api/vehiclesretireds';


    this.http.get(toolsurl)
      .subscribe((data: Tool[]) => {
        this.tools = data;
      });

    this.http.get(conditionurl)
      .subscribe((data: Condition[]) => {
        this.conditions = data;
      });

    this.http.get(descriptionurl)
      .subscribe((data: Description[]) => {
        this.descriptions = data;
      });

    this.http.get(employeeurl)
      .subscribe((data: Emp[]) => {
        this.employees = data;
      });

    this.http.get(equipmentrentalurl)
      .subscribe((data: EquipmentRental[]) => {
        this.equipmentrentals = data;
      });

    this.http.get(joburl)
      .subscribe((data: Job[]) => {
        this.jobs = data;
      });

    this.http.get(mfgurl)
      .subscribe((data: Mfg[]) => {
        this.mfgs = data;
      });

    this.http.get(modelnumurl)
      .subscribe((data: Modelnum[]) => {
        this.modelnums = data;
      });

    this.http.get(pcnumurl)
      .subscribe((data: Pcnum[]) => {
        this.pcnums = data;
      });

    this.http.get(recallurl)
      .subscribe((data: Recall[]) => {
        this.recalls = data;
      });

    this.http.get(serialnumurl)
      .subscribe((data: Serialnum[]) => {
        this.serialnums = data;
      });

    this.http.get(serviceurl)
      .subscribe((data: Service[]) => {
        this.services = data;
      });

    this.http.get(vehiclehistoryurl)
      .subscribe((data: Vehiclehistory[]) => {
        this.vehiclehistorys = data;
      });

    this.http.get(vehiclesurl)
      .subscribe((data: Vehicle[]) => {
        this.vehicles = data;
      });

    this.http.get(vehiclesretiredurl)
      .subscribe((data: Vehicleretired[]) => {
        this.vehiclesretired = data;
      });

  }


}
