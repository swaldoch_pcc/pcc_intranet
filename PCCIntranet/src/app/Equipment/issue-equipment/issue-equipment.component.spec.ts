import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IssueEquipmentComponent } from './issue-equipment.component';

describe('IssueEquipmentComponent', () => {
  let component: IssueEquipmentComponent;
  let fixture: ComponentFixture<IssueEquipmentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IssueEquipmentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IssueEquipmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
