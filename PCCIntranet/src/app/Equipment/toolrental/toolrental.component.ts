import { Component, OnInit, NgModule, Injectable } from '@angular/core';
import { OrderPipe } from 'ngx-order-pipe';
import { Router } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { FormGroup, FormControl, ReactiveFormsModule } from '@angular/forms';
import { Tool, Condition, Description, Emp, EquipmentRental, Categorypricing, Job, Mfg, Modelnum, Pcnum, Recall, Serialnum, Service, Vehiclehistory, Vehicle, Vehicleretired } from '../../models/equipmentmodels';
import { MENUS } from '../../constants/equipment';
import { EquipmentService } from 'src/app/services/equipmentservice';
import { Jobsite } from '../../models/jobsites';
import { JobsitesearchService } from '../../services/jobsitesearch.service';
import { Employee } from '../../models/employees';
import { EmployeeService } from '../../services/employee';
import { NgbDateStruct, NgbDateAdapter, NgbDateParserFormatter, NgbCalendar } from '@ng-bootstrap/ng-bootstrap';
import { DatePipe } from '@angular/common';


@Injectable()
export class CustomAdapter extends NgbDateAdapter<string> {
  readonly DELIMITER = '-';

  fromModel(value: string): NgbDateStruct {
    let result: NgbDateStruct = null;
    if (value) {
      let date = value.split(this.DELIMITER);
      result = {
        day: parseInt(date[2], 10),
        month: parseInt(date[1], 10),
        year: parseInt(date[0], 10)
      };
    }
    return result;
  }

  toModel(date: NgbDateStruct): string {
    let result: string = null;
    if (date) {
      result = date.year + this.DELIMITER + date.month + this.DELIMITER + date.day;
    }
    return result;
  }
}

@Injectable()
export class CustomDateParserFormatter extends NgbDateParserFormatter {

  readonly DELIMITER = '-';

  parse(value: string): NgbDateStruct {
    let result: NgbDateStruct = null;
    if (value) {
      let date = value.split(this.DELIMITER);
      result = {
        day: parseInt(date[2], 10),
        month: parseInt(date[1], 10),
        year: parseInt(date[0], 10)
      };
    }
    return result;
  }

  format(date: NgbDateStruct): string {
    let result: string = null;
    if (date) {
      result = date.year + this.DELIMITER + date.month + this.DELIMITER + date.day;
    }
    return result;
  }
}

@Component({
  selector: 'app-toolrental',
  templateUrl: './toolrental.component.html',
  styleUrls: ['./toolrental.component.scss'],

  providers: [
    { provide: NgbDateAdapter, useClass: CustomAdapter },
    { provide: NgbDateParserFormatter, useClass: CustomDateParserFormatter }
  ]
})

@NgModule({
  imports: [
    ReactiveFormsModule
  ],
})

export class ToolrentalComponent implements OnInit {

  tools: Tool[];
  conditions: Condition[];
  descriptions: Description[];
  employees: Employee[];
  equipmentrentals: EquipmentRental[];
  jobs: Jobsite[];
  mfgs: Mfg[];
  modelnums: Modelnum[];
  pricing: Categorypricing[];
  pcnums: Pcnum[];
  recalls: Recall[];
  serialnums: Serialnum[];
  services: Service[];
  vehiclehistorys: Vehiclehistory[];
  vehicles: Vehicle[];
  vehiclesretired: Vehicleretired[];
  types: any;
  show: boolean = false;
  buttonName: any = 'Show';
  
  currentUser = JSON.parse(localStorage.getItem('currentUser'));
  toolcoordinator = this.currentUser.firstName.concat(' ', this.currentUser.lastName);


  constructor(private http: HttpClient, private router: Router, private equipmentService: EquipmentService, private jobsiteService: JobsitesearchService, private employeeService: EmployeeService, private datePipe: DatePipe) {

  }

  today = new Date;
  startDate: Date;

  menus = MENUS;

  selectedType: any;
  onSelect(type): void {
    this.selectedType = type;
  }

  selectedMenu: any;
  menuSelect(menu): void {
    this.selectedMenu = menu;
  }

  selectedEmployee: any;
  employeeSelect(employee): void {
    this.selectedEmployee = employee;
    this.show = false;
  }

  selectedJob: any;
  jobSelect(job): void {
    this.selectedJob = job;
    this.show = false;
  }

  toggleActive(): void {
    this.show = !this.show;
    if (this.show)
      this.buttonName = "Hide"
    else
      this.buttonName = "Show";
    console.log(this.show);
    console.log(this.buttonName);
  }

  reset(): void {
    this.selectedType = undefined;
    this.selectedMenu = undefined;
    this.selectedJob = undefined;
    this.show = false;
  }

  resetEmployee(): void {
    this.selectedEmployee = undefined;
  }

  resetJobtransfer(): void {
    this.selectedJob = undefined;
  }

  getTools(): void {
    this.equipmentService.getTools()
      .subscribe(tools => this.tools = tools);
  }

  getEquipmentrental(): void {
    this.equipmentService.getEquipmentrental()
      .subscribe(equipmentrentals => this.equipmentrentals = equipmentrentals);
    
  }

  getPricing(): void {
    this.equipmentService.getPricing()
      .subscribe(pricing => this.pricing = pricing);
  }

  getVehicles(): void {
    this.equipmentService.getVehicles()
      .subscribe(vehicles => this.vehicles = vehicles);
  }

  getJobsites(): void {
    this.jobsiteService.getJobsites()
      .subscribe(jobs => this.jobs = jobs);
  }

  getEmployees(): void {
    this.employeeService.getEmployees()
      .subscribe(employees => this.employees = employees);
  }



  ngOnInit(): void {

    this.types = [
      'Tool',
      'Safety equipment',
      'Vehicle/trailer',
      'Consumable'
    ];

    this.getPricing();

    this.getEmployees();

    this.getJobsites();

    this.getVehicles();

    this.getEquipmentrental();

    this.getTools();
    
  }

  public updateRental(rental): void {
    const rentalUrl = 'https://intranet.pc-const.com/api/equipmentrentals/' + rental.rentalId;
    console.log(rentalUrl, rental, { headers: { 'Content-Type': 'application/json' } });
    this.http.put(rentalUrl, rental)
      .subscribe(data => {
        console.log("put request is successful ", data);
      },
        error => {
          console.log("rrrrr", error);
        }
      );
  }

  public changeJob(rental, job): void {
    console.log(rental, job);
    const rentalUrl = 'https://intranet.pc-const.com/api/equipmentrentals/' + rental.rentalId;

    rental.comments = 'test comment';
    console.log(rental);
  }

  public changeEmployee(): void {

  }

  calculateDiff(dateOut) {
    var date1: any = new Date(dateOut);
    var date2: any = new Date();
    var diffDays: any = Math.floor((date2 - date1) / (1000 * 60 * 60 * 24));

    return diffDays;
  }

  calculateCost(dateOut) {
    var date1: any = new Date(dateOut);
    var date2: any = new Date();
    var runningCost: any = Math.floor(100*(date2 - date1) / (1000 * 60 * 60 * 24));

 
    return runningCost;
  }

  //calculateCurrentweekcost(dateOut, equipmentId) {
  //  this.tools = this.getTools();
  //  this.startDate = new Date();
  //  this.startDate.setDate(this.startDate.getDate());
  //  if (this.startDate.getDay() > 1) {
  //    var diff: any = this.startDate.getDay() - 1;
  //    this.startDate.setDate(this.startDate.getDate() - diff);
  //  }
  //  if (this.startDate.getDay() < 1) {
  //    this.startDate.setDate(this.startDate.getDate() - 6);
  //  }
  //  console.log(this.startDate.getDay());
    
  //  if (dateOut > this.startDate) {
  //    var date1: any = new Date(dateOut);
  //  }
  //  else {
  //    var date1: any = new Date(this.startDate)
  //  }
  //  var date2: any = new Date();

  //  Object.keys(this.tools).some(Pcnum => Pcnum[equipmentId].index === 2);
  //  var rate: any = 10;
  //  var runningCost: any = Math.floor(rate * (date2 - date1) / (1000 * 60 * 60 * 24));
  //  console.log(date1);
  //  console.log(rate);

  //  console.log(runningCost);
  //  return runningCost;
  //}

}
