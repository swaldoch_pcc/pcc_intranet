import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ToolrentalComponent } from './toolrental.component';

describe('ToolrentalComponent', () => {
  let component: ToolrentalComponent;
  let fixture: ComponentFixture<ToolrentalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ToolrentalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ToolrentalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
