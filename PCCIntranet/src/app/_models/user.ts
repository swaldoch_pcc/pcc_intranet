export class User {
  id: number;
  username: string;
  password: string;
  firstName: string;
  lastName: string;
  passwordHash: string;
  passwordSalt: string;
  role: string;
}
