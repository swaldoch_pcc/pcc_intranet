import { Data } from '@angular/router';

export interface Tool {
  ToolId: string;
  Pcid: number;
  Pcnum: string;
  Serialnum: string;
  DatePurchased: Date;
  DateDisposed: Date;
  Disposition: string;
  Tagnum: string;
  Title: string;
  Color: string;
  Yearmodel: number;
  Location: string;
  Condition: string;
  Modelnum: string;
  Mfg: string;
  Description: string;
  Priceid: number;
  Category: string;
};

export interface Condition {
  ConditionId: string;
  Condition1: string;
};

export interface Description {
  DescriptionId: string;
  Description1: string;
};

export interface Emp {
  EmployeeId: string;
  Employee: string;
};

export interface EquipmentRental {
  RentalId: string;
  EquipmentId: string;
  DateOut: Date;
  Datein: Date;
  User: string;
  Jobnum: number;
  Comments: string;
  Oldrentalid: number;
  Job: string;
  Description: string;
};

export interface Job {
  JobId: string;
  Job1: string;
};

export interface Mfg {
  Mfgid: string;
  Mfg1: string;
};

export interface Modelnum {
  ModelId: string;
  ModelNumber: string;
  DescriptionId: string;
  Mfgid: string;
};

export interface Pcnum {
  Pcid: string;
  PcNum1: string;
  PurchaseDate: string;
  TagNum: string;
  Title: string;
  Mileage: string;
  ModelId: string;
  SerialId: string;
  ConditionId: string;
  Color: string;
  YearModel: string;
  Location: string;
};

export interface Recall {
  Id: string;
  Vin: string;
  RecallNum: string;
  Description: string;
  RecallDate: Date;
  RecallReceived: Date;
  WorkScheduled: Date;
  WorkCompleted: Date;
  ServiceLocation: string;
  Notes: string;
};

export interface Serialnum {
  SerialId: string;
  SerialNum1: string;
};

export interface Service {
  Id: string;
  Vin: string;
  ServiceType: string;
  ServiceLocation: string;
  Mileage: number;
  Cost: number;
  Notes: string;
  PrimaryUser: string;
  ServiceDate: Date;
  AdditionalServices: string;
};

export interface Vehiclehistory {
  HistoryId: string;
  SerialNum: string;
  User: number;
  IssueDate: Date;
  ReturnDate: Date;
  ConditionReportId: string;
  MileageOut: number;
  MileageIn: number;
  JobNum: string;
};

export interface Vehicle {
  Id: number;
  Company: string;
  Mfg: string;
  Model: string;
  Year: number;
  SerialNum: string;
  Description: string;
  Tag: string;
  TagState: string;
  PurchaseDate: Date;
  PrimaryUser: string;
  IssueDate: Date;
  ReturnDate: Date;
  IssueMileage: number;
  ReturnMileage: number;
  CurrentMileage: number;
  CurrentMileageDate: Date;
  Color: string;
  BodyStyle: string;
  DriveTrain: string;
  EngineSize: number;
  EngineType: string;
  FuelType: string;
  TagExpiration: Date;
  OrigPurchasePrice: number;
  SoldDate: Date;
  SalePrice: number;
  Config: string;
};

export interface Vehicleretired {
  Id: number;
  Company: string;
  Mfg: string;
  Model: string;
  Year: number;
  SerialNum: string;
  Description: string;
  Tag: string;
  TagState: string;
  PurchaseDate: Date;
  PrimaryUser: string;
  IssueDate: Date;
  ReturnDate: Date;
  IssueMileage: number;
  ReturnMileage: number;
  CurrentMileage: number;
  CurrentMileageDate: Date;
  Color: string;
  BodyStyle: string;
  DriveTrain: string;
  EngineSize: number;
  EngineType: string;
  FuelType: string;
  TagExpiration: Date;
  OrigPurchasePrice: number;
  SoldDate: Date;
  SalePrice: number;
  Config: string;
};

export interface Categorypricing {
  PriceId: string;
  Isactive: string;
  Category: string;
  Subcategory: string;
  Dailyrate: number;
  Ratelastchange: Date;
  Changedby: string;
  Pricenum: number;
}

export interface TruckRental {
  Id: string;
  itemid: string;
  job: string;
  user: string;
  comments: string;
  issuedate: Date;
  returndate: Date;
}

export interface Consumable {
  itemNum: number;
  description: string;
  quantity: number;
  vendor: string;
  vendorPhone: string;
  vendorEmail: string;
  vendorWebsite: string;
  websiteLoginName: string;
  websiteLoginPassword: string;
}

export interface ConsumableIssue {
  itemId: number;
  job: string;
  user: string;
  description: string;
  quantity: number;
  comments: string;
  issueDate: Date;
}
