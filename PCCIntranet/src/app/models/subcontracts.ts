export interface Subcontract {
  Recnum: number;
  Ctcnum: string;
  Vndnum: number;
  Jobnum: number;
  Dscrpt: string;
  Status: number;
  Contyp: string;
  Pmtbnd: number;
  Prfbnd: number;
  Cntttl: number;
  Balttl: number;
}
