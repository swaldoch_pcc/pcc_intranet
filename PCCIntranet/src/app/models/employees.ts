export interface Employee {
  Id: number;
  Fname: string;
  Lname: string;
  HireDate: Date;
  Title: string;
  Manager: number;
  Location: string;
  PrefName: string;
}
