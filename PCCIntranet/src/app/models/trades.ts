export interface Trades {
  id: number;
  tradeid: number;
  tradename: string;
  divnum: number;
  divname: string;
  vndnum: number;
}
