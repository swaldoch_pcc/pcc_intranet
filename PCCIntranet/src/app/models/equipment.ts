export interface menu {
  id: number;
  category: string;
  item: string;
}

export interface type {
  id: number;
  typename: string;
}
