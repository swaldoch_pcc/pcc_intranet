export interface Subs {
  Company: string;
  Address1: string;
  Address2: string;
  City: string;
  State: string;
  Zip: string;
  Mainphone: string;
  Email: string;
  Id: number;
  Type: string;
  NaicsCode: string;
  SicCode: string;
  Website: string;
  Fax: string;
}
