using AutoMapper;
using PCCIntranet.Dtos;
using PCCIntranet.Models;

namespace PCCIntranet.Helpers
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<Users, UserDto>();
            CreateMap<UserDto, Users>();
        }
    }
}
