using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.EntityFrameworkCore;
using PCCIntranet.Models;
using PCCIntranet.Controllers;
using Newtonsoft.Json;
using PCCIntranet.Helpers;
using PCCIntranet.Services;
using AutoMapper;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc.Authorization;
using Microsoft.AspNetCore.Authorization;

namespace PCCIntranet
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
          services.AddDbContext<PCCContext>(options => options.UseSqlServer("Server=10.28.6.25,1433;Initial Catalog=PCC;Persist Security Info=False;User ID=pcc_reader;Password=InGodWeTrust;MultipleActiveResultSets=True;Encrypt=True;TrustServerCertificate=True;Connection Timeout=30;"));
          services.AddDbContext<SageContext>(options => options.UseSqlServer("server=tcp:10.28.6.26\\SAGE100CON; initial catalog=PC Construction Inc; integrated security=false; user=pcc_reader; password=InGodWeTrust;Application Name=ProjectMaster"));
          services.AddDbContext<EquipmentContext>(options => options.UseSqlServer("Server=10.28.6.25,1433;Initial Catalog=PCC;Persist Security Info=False;User ID=pcc_reader;Password=InGodWeTrust;MultipleActiveResultSets=True;Encrypt=True;TrustServerCertificate=True;Connection Timeout=30;"));
          services.AddDbContext<UserContext>(options => options.UseSqlServer("Server=10.28.6.25,1433;Initial Catalog=PCC;Persist Security Info=False;User ID=pcc_reader;Password=InGodWeTrust;MultipleActiveResultSets=True;Encrypt=True;TrustServerCertificate=True;Connection Timeout=30;"));
          services.AddScoped<ProcoreService>();

      services.AddCors(options =>
          {
            options.AddPolicy("CorsPolicy",
              builder => builder.AllowAnyOrigin()
              .AllowAnyMethod()
              .AllowAnyHeader()
              .AllowCredentials());
          });

          services.AddMvc()
            .SetCompatibilityVersion(CompatibilityVersion.Version_2_1);



          services.AddAutoMapper();

          // configure strongly typed settings objects
          var appSettingsSection = Configuration.GetSection("AppSettings");
          services.Configure<AppSettings>(appSettingsSection);

          // configure jwt authentication
          var appSettings = appSettingsSection.Get<AppSettings>();
          var key = Encoding.ASCII.GetBytes(appSettings.Secret);
          services.AddAuthentication(x =>
          {
            x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
            x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
          })
          .AddJwtBearer(x =>
          {
            x.Events = new JwtBearerEvents
            {
              OnTokenValidated = context =>
              {
                var userService = context.HttpContext.RequestServices.GetRequiredService<IUserService>();
                var userId = int.Parse(context.Principal.Identity.Name);
                var user = userService.GetById(userId);
                if (user == null)
                {
                        // return unauthorized if user no longer exists
                        context.Fail("Unauthorized");
                }
                return Task.CompletedTask;
              }
            };
            x.RequireHttpsMetadata = false;
            x.SaveToken = true;
            x.TokenValidationParameters = new TokenValidationParameters
            {
              ValidateIssuerSigningKey = true,
              IssuerSigningKey = new SymmetricSecurityKey(key),
              ValidateIssuer = false,
              ValidateAudience = false
            };
          });

          // configure DI for application services
          services.AddScoped<IUserService, UserService>();

    }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseCors("CorsPolicy");

            app.UseAuthentication();

            app.UseMvc();

            app.UseDefaultFiles();
            app.UseStaticFiles();



        }
    }
}
