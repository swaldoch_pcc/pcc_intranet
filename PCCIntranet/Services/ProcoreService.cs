using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;

namespace PCCIntranet.Services
{
  public class ProcoreService
  {
    private readonly IConfiguration _configuration;
    private string _accessToken;

    public ProcoreService(IConfiguration configuration)
    {
      _configuration = configuration;
    }

    public async Task<JObject> ListProjectsAsync()
    {
      if (string.IsNullOrEmpty(_accessToken))
      {
        _accessToken = await GetAccessTokenAsync();
      }

      var baseUrl = _configuration["Procore:ApiBaseUrl"];
      var companyNumber = 168276;

      using (var httpClient = new HttpClient())
      {
        httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", _accessToken);
        var response = await httpClient.GetStringAsync($"{baseUrl}/projects?company_id={companyNumber}");
        return JObject.Parse(response);
      }
    }

    private async Task<string> GetAccessTokenAsync()
    {
      using (var httpClient = new HttpClient())
      {
        var requestBody = new FormUrlEncodedContent(new Dictionary<string, string>
        {
          ["grant_type"] = "refresh_token",
          ["client_id"] = _configuration["Procore:ClientId"],
          ["client_secret"] = _configuration["Procore:ClientSecret"],
          ["refresh_token"] = _configuration["Procore:RefreshToken"]
        });

        var response = await httpClient.PostAsync(_configuration["Procore:TokenEndpoint"], requestBody);
        var tokenObject = await response.Content.ReadAsAsync<TokenResponse>();
        return tokenObject.AccessToken;
      }
    }
  }

  public class TokenResponse
  {
    [JsonProperty("access_token")]
    public string AccessToken { get; set; }

    [JsonProperty("token_type")]
    public string TokenType { get; set; }

    [JsonProperty("expires_in")]
    public int ExpiresIn { get; set; }

    [JsonProperty("refresh_token")]
    public string RefreshToken { get; set; }

    [JsonProperty("created_at")]
    public int CreatedAt { get; set; }

  }
}
